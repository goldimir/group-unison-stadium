jQuery(document).ready(function($) {

  var wpbakery_builder_iframe = $('#vc_inline-frame');
  var wpbakery_builder_html = $('html.js_active');

  wpbakery_builder_iframe.ready(function(){
    wpbakery_builder_html.ready(function(){

      $('#main').on('click', '.tm-card', function() {

        if (this.classList.contains('active')) {
          this.classList.remove('active')
        } else {
          this.classList.add('active');
        }

      });

      /////////////////////////////////////////////
      // Jarallax
      /////////////////////////////////////////////
      $('.jarallax').jarallax({
          speed: 0.8
      });

      /////////////////////////////////////////////
      // MASONRY GRID
      /////////////////////////////////////////////

      function stthemes_masonry_grid() {
        var $grid = $('.masonry').imagesLoaded(function() {
          $grid.masonry({
            columnWidth: '.grid-sizer',
            itemSelector: '.grid-item',
            gutter: '.gutter-sizer',
            transitionDuration: 0,
          });
          stthemes_appearance_effect();
        });
      }

      /////////////////////////////////////////////
      // APPEARANCE EFFECT
      /////////////////////////////////////////////

      function stthemes_appearance_effect() {
        $('div[data-appearance-effect="1"] .post, div[data-appearance-effect="1"] .product').each(function(){
          $(this).one('inview', function(){
            $(this).addClass('animated fadeInUp');
          });
        });
      }

      /////////////////////////////////////////////
      // LOAD MORE
      /////////////////////////////////////////////

      function stthemes_load_more_btn() {
        if (typeof load_more_btn === 'undefined') {
          return;
        }
        var startPage = parseInt(load_more_btn.startPage) + 1,
          maxPages = parseInt(load_more_btn.maxPages),
          nextLink = load_more_btn.nextLink,
          noMore = load_more_btn.noMore,
          text = load_more_btn.text,
          loading = load_more_btn.loading;

        $('.vlt-pagination--load-more a span').text(text);

        var loadMoreContainer = $('.vlt-pagination-load-more-container'),
          postsContainer = loadMoreContainer.find('.masonry'),
          itemSelector = '.grid-item';

        loadMoreContainer.on('click', '.vlt-pagination--load-more a', function(e) {
          e.preventDefault();

          if (nextLink === null) {
            return;
          }

          var $this = $(this);

          if(!$this.hasClass('disabled')){
            $this.addClass('loading');
            $this.find('span').text('loading');
          }

          if (startPage <= maxPages) {
            $.ajax({
              type: 'POST',
              url: nextLink,
              dataType: 'html',
              success: function (data) {
                var newElems = $(data).find(itemSelector);
                if (newElems.length > 0) {

                  postsContainer.append(newElems);

                  postsContainer.imagesLoaded(function() {
                    postsContainer.masonry('appended', newElems);
                    stthemes_masonry_grid();
                  });

                  $this.removeClass('loading');
                  $this.find('span').text(text);

                } else {
                  $this.removeClass('loading').addClass('disabled');
                  $this.find('span').html(noMore);
                }
                startPage++;
                nextLink = nextLink.replace(/\/page\/[0-9]+/, '/page/' + startPage);

                if(startPage <= maxPages) {
                  $this.removeClass('loading');
                } else {
                  $this.removeClass('loading').addClass('disabled');
                  $this.find('span').html(noMore);
                }

              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
              }
            });
          }

        });
      }

      function stthemes_post_navigation() {

        $(window).on('scroll resize', function() {

          $('.single-post .vlt-main, .single-product .vlt-main').each(function() {
            var sT = $(window).scrollTop();
            var thisH = $(this).outerHeight();
            var oT = $(this).offset().top;
            var thisTop = oT - sT; // Get the top
            var winH = $(window).height() / 2;

            if (thisTop <= winH  && sT <= thisH + $(this).offset().top - winH) {
              // $(this).css('background', 'red');
              $('.vlt-post-navigation').addClass('is-visible');
            } else {
              // $(this).css('background', 'grey');
              $('.vlt-post-navigation').removeClass('is-visible');
            }
          });

        });

      }

      /**
       * Filter Video Gallery
       */
      let searchTaxVal = $('.video-gallery-tax');
      let selectFilter = $('.select-filter');
      let resetBtn = $('.reset-btn');
      let selectObj = {};

      selectFilter.on('change', function () {

        selectFilter.find(':selected').each(function( index ) {
          selectObj[ $(this).data('taxonomy')] = $(this).val();
        });
        resetBtn.fadeIn(600);

        $.ajax({
          url: '/wp-admin/admin-ajax.php',
          type: 'POST',
          data: {
            'action': 'fly_ajax_tax_search',
            'term': selectObj,
            // 'curr_vid_catalog': curr_catalog.catalog_video_id, // wp_localize_script from shortcode_video_catalog.php
          },
          beforeSend: function () {
            searchTaxVal.addClass("loading");
          },
          complete: function () {
            searchTaxVal.removeClass("loading");
          },
          success: function (result) {
            searchTaxVal.html(result);
          }
        });
      });

      resetBtn.on('click', function (e) {
        selectFilter.val('').trigger('change');
        $(this).fadeOut(600);
        e.preventDefault();
      });

      var stthemes_scripts = {
        init: function() {
          stthemes_masonry_grid();
          stthemes_post_navigation();
          stthemes_load_more_btn();
        }
      }

      stthemes_scripts.init();

    });
  });

});