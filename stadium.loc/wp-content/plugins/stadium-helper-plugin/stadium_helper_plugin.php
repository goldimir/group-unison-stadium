<?php

/**
 * Plugin Name: Stadium Helper Plugin
 * Description: Stadium Helper Plugin expands the functionality of the theme. Adds new icons, shortcodes and much more.
 * Version: 1.0
 * Author: Stadium
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if ( ! function_exists( 'STThemesHelperPlugin' ) ) {

    class STThemesHelperPlugin {

        /**
         * The single class instance.
         * @var $_instance
         */
        private static $_instance = null;

        /**
         * Main Instance
         * Ensures only one instance of this class exists in memory at any one time.
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
                self::$_instance->init_option();
                self::$_instance->init_hooks();
            }
            return self::$_instance;
        }

        /**
         * Path to the plugin directory
         * @var $plugin_path
         */
        public $plugin_path;

        /**
         * URL to the plugin directory
         * @var $plugin_url
         */
        public $plugin_url;

        /**
         * Plugin version
         * @var $plugin_version
         */
        public $plugin_version;

        public function __construct() {
            # We do nothing here!
        }

        public function init_option() {
            # Init plugin vars.
            $this->plugin_path             = plugin_dir_path( __FILE__ );
            $this->plugin_url              = plugin_dir_url( __FILE__ );

            # Load textdomain.
            load_plugin_textdomain( 'stthemes', false, $this->plugin_path . 'languages/' );

            # Init vc.
            if ( class_exists( 'Vc_Manager' ) ) {
                $this->init_vc();
            }

        }

        public function init_hooks() {
            add_action( 'wp_enqueue_scripts', array( $this, 'init_styles' ) );
            add_action( 'wp_footer', 'std_footer_shortcode_css' );
        }

        public function init_vc() {

            # Meta Box
	        if ( class_exists( 'RWMB_Loader' ) ) {
		        require_once $this->plugin_path . 'includes/metabox-video.php';
	        }

            # Visual Composer.
            require_once $this->plugin_path . 'includes/visual_composer.php';

            # Visual Composer Shortcodes.
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_button.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_hero_header.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_team_member.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_company_team_card.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_blog_layouts.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_mail_chimp.php';
            require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_video_catalog.php';

        }

        public function init_styles() {
            # Init JS libraries.
            wp_enqueue_script( 'jarallax', sttheme_helper_plugin()->plugin_url . 'assets/js/jarallax.min.js', array( 'jquery' ), $this->plugin_version, true );
            wp_enqueue_script( 'shortcodes-js', sttheme_helper_plugin()->plugin_url . 'assets/js/shortcodes.js', array(), $this->plugin_version, true );

            /* Print inline shortcode styles */
            if ( ! function_exists( 'std_footer_shortcode_css' ) ) {
                function std_footer_shortcode_css() {
                    global $std_inline_styles;
                    if ( $std_inline_styles != '' ) {
                        echo '<style data-id="theme-shortcode-styles">' . $std_inline_styles . '</style>';
                    }
                }
            }
        }
    }

    function sttheme_helper_plugin() {
        return STThemesHelperPlugin::instance();
    }
    add_action( 'plugins_loaded', 'sttheme_helper_plugin' );

}
