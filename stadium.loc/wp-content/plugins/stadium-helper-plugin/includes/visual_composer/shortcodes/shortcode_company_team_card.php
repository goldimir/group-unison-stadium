<?php

# Team Members

if ( !class_exists( 'STThemesVcCompanyTeamShortcode' ) ) {
    class STThemesVcCompanyTeamShortcode extends STThemesVcShortcode {

        function shortcode_name() {
            return 'sky_company_team';
        }

        public function __construct() {
            parent::__construct();
        }

        public function register_shortcode( $atts, $content = null ) {
            $identifier_class = uniqid( 'stthemes_custom_' );
            $output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

            extract( shortcode_atts( array(
                'image' => '',
                'title' => 'Example text',
                'buttons' => true,
                'btn_name' => 'Learn more',
                'btn_link' => '',
                'background_color' => '',
                'gradient_color_btn' => '',
                'background_btn_color_1' => '',
                'background_btn_color_2' => '',
                'about_member' => '',
                'new_window' => '',
                'facebook_link' => '',
                'twitter_link' => '',
                'skype_link' => '',
                'google_link' => '',
                'el_class' => '',
                'css' => '',
            ), $atts ) );

            if ( $new_window ) {
                $new_window = 'target="_blank"';
            }

            if ( $background_color ) {
                $background_color = 'background-color:'.$background_color.';';
            }

            if ( $background_btn_color_1 && $background_btn_color_2 ) {
                $background_btn_color = 'background-image: linear-gradient(-90deg, '.$background_btn_color_1.', '.$background_btn_color_2.');';
            }elseif ( $background_btn_color_1 ) {
                $background_btn_color = 'background-image: unset; background-color:'.$background_btn_color_1.';';
            }

            $about_member = vc_value_from_safe( $about_member );

            // Cut text for front-back sides
            // $short_about_member = STThemesVcShortcodeHelper::cut_text( $about_member );
            // $about_member = STThemesVcShortcodeHelper::cut_text( $about_member, true );
            $short_about_member = stadium_limit_text( $about_member, 15 );
            $about_member = stadium_limit_text( $about_member, 95 );

            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

            $output .= '<div class="cardContainer">';
            $output .= '<div class="tm-card">';

            $output .= '<div style="'.$background_color.'" class="side front ' . $css_class . '">';
            if ( $image ) {
                $output .= '<div class="img" style="background-image: url(' . wp_get_attachment_image_url( $image, 'stadium-full' ) . ');"></div>';
            }
            $output .= '<div class="info">';
            $output .= '<h2>'.$title.'</h2>';
            $output .= '<p>'.$short_about_member.'</p>';
            $output .= '</div></div>';

            $output .= '<div style="'.$background_color.'" class="side back">';
            $output .= '<div class="info">';
            $output .= '<h2>'.$title.'</h2>';
            $output .= '<p>'.$about_member.'</p>';
            $output .= '<ul class="list-inline">';

            if ( $facebook_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$facebook_link.'"><i class="fab fa-facebook-f"></i></a>';
                $output .= '</li>';
            }
            if ( $twitter_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$twitter_link.'"><i class="fab fa-twitter"></i></a>';
                $output .= '</li>';
            }
            if ( $skype_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$skype_link.'"><i class="fab fa-skype"></i></a>';
                $output .= '</li>';
            }
            if ( $google_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$google_link.'"><i class="fab fa-google"></i></a>';
                $output .= '</li>';
            }

            $href = null;
            if ( $btn_link !== '' ) {
                $href = vc_build_link( $btn_link );
            }

            $output .= '</ul>';

            if ( $buttons ) {
                $output .= '<a class="btn" style="'.$background_btn_color.'" target="'.trim($href['target']).'" href='.$href['url'].'>';
                $output .= '<h4>'.$btn_name.'</h4>';
                $output .= '<svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/><path d="M0-.25h24v24H0z" fill="none"/></svg>';
                $output .= '</a>';
            }

            $output .= '</div></div></div></div>';

            return $output;

        }

        public function vc_map_shortcode() {

            $vc_map = array(
                array(
                    'type' => 'attach_image',
                    'param_name' => 'image',
                    'heading' => esc_html__( 'Image', 'stthemes' ),
                    'description' => esc_html__( 'Select an image for Team Member. Image should be symmetrical in width and height.', 'stthemes' ),
                    'value' => '',
                    'holder' => 'img',
                    'class' => 'vc-preview-image',
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'title',
                    'heading' => esc_html__( 'Name', 'stthemes' ),
                    'description' => esc_html__( 'Enter a Name for Member.', 'stthemes' ),
                    'value' => 'Example text',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'textarea_safe',
                    'param_name' => 'about_member',
                    'heading' => esc_html__( 'About member', 'stthemes' ),
                    'description' => esc_html__( 'Enter text.', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'facebook_link',
                    'heading' => esc_html__( 'Link to Facebook Group', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'twitter_link',
                    'heading' => esc_html__( 'Link to Twitter social group', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'skype_link',
                    'heading' => esc_html__( 'Link to Skype account', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'google_link',
                    'heading' => esc_html__( 'Link to Google', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'new_window',
                    'heading' => esc_html__( 'Open links in new window', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'buttons',
                    'heading' => esc_html__( 'Button', 'stthemes' ),
                    'description' => esc_html__( 'Check it if you want to show button.', 'stthemes' ),
                    'value' => array(
                        esc_html__( 'Enable', 'stthemes' ) => '1'
                    ),
                    'std' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'btn_name',
                    'heading' => esc_html__( 'Text button', 'stthemes' ),
                    'value' => 'Learn more',
                    'admin_label' => true,
                    'dependency' => array(
                        'element' => 'buttons',
                        'value' => '1'
                    ),
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'vc_link',
                    'param_name' => 'btn_link',
                    'heading' => esc_html__( 'Link', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'dependency' => array(
                        'element' => 'buttons',
                        'value' => '1'
                    ),
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'background_color',
                    'heading' => esc_html__( 'Card Background Color', 'stthemes' ),
                    'description' => esc_html__( 'Color for Front & Back sides', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'background_btn_color_1',
                    'heading' => esc_html__( 'Button Color', 'stthemes' ),
                    'description' => esc_html__( 'Background button color', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'gradient_color_btn',
                    'heading' => esc_html__( 'Gradient Color', 'stthemes' ),
                    'description' => esc_html__( 'Add second color to button for gradient effect.', 'stthemes' ),
                    'value' => array(
                        esc_html__( 'Enable', 'stthemes' ) => '1'
                    ),
                    'group' => esc_html__( 'Extras', 'stthemes' )
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'background_btn_color_2',
                    'heading' => esc_html__( 'Second Gradient Color', 'stthemes' ),
                    'description' => esc_html__( 'Gradient color for Button', 'stthemes' ),
                    'value' => '',
                    'dependency' => array(
                        'element' => 'gradient_color_btn',
                        'value' => '1'
                    ),
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'el_class',
                    'heading' => esc_html__( 'Extra class', 'stthemes' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'css_editor',
                    'param_name' => 'css',
                    'heading' => esc_html__( 'CSS', 'stthemes' ),
                    'group' => esc_html__( 'Design', 'stthemes' ),
                )
            );

            vc_map( array(
                'name' => esc_html__( 'Company Team Card', 'stthemes' ),
                'base' => $this->shortcode_name(),
                'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-ico.png',
                'category' => esc_html__( 'SkyThemes', 'stthemes' ),
                'params' => $vc_map,
            ) );

        }
    }
    /**
     * Init class
     */
    new STThemesVcCompanyTeamShortcode;
}
