<?php

# Button

if ( !class_exists( 'STThemesVcButtonShortcode' ) ) {
    class STThemesVcButtonShortcode extends STThemesVcShortcode {

        function shortcode_name() {
            return 'sky_button';
        }

        public function __construct() {
            parent::__construct();
        }

        public function register_shortcode( $atts, $content = null ) {
            $identifier_class = uniqid( 'stthemes_custom_' );
            $output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

            extract( shortcode_atts( array(
                'btn_name' => 'Learn more',
                'btn_link' => '',
                'pos' => '',
                'text_btn_color' => '',
                'text_btn_color_hover' => '',
                'background_color' => '',
                'background_color_hover' => '',
                'el_class' => '',
                'css' => '',
            ), $atts ) );

            $href = null;
            if ( $btn_link !== '' ) {
                $href = vc_build_link( $btn_link );
            }

            $styles = $this->shortcode_styles( $atts );
            $css_id = $styles[ 'id' ];
            $css_id = ' data-id="' . $css_id . '"';

            // Classes for html template
            $class = $styles[ 'classes' ];
            $class = implode(' ', $class);
            $class = str_replace('.', '', $class);

            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $pos .' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );


            $output .= '<div class="' . $css_class .'">';

            $output .= '<a class="'.$class.'" '.$css_id.' style="'/*.$background_btn_color.$text_btn_color*/.'" target="'.trim($href['target']).'" href='.$href['url'].'>'.$btn_name.'</a>';
            // $output .= '<svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/><path d="M0-.25h24v24H0z" fill="none"/></svg>';
            
            $output .= '</div>';

            return $output;

        }

        /* Generate shortcode styles */
        function shortcode_styles( $styles ) {
            global $std_inline_styles;
	        $css_id       = uniqid( 'sky_button-' . rand( 1, 100 ) );
            $main_class = '.sky-main-btn';
            $css_selector = $main_class . '[data-id="' . $css_id . '"]' . $main_class;

            // Classes for html template
            $css_temp_selector = array();
            $css_temp_selector[ $main_class ] = $main_class;

            $style        = '';

            if ( isset($styles[ 'background_color' ]) ) {
//                $css_temp_selector[ $class_background_color ] .= $main_class.'__background ';
                $style .= $css_selector . '__background ' . '{';
                    $style .= 'background-color:' . $styles[ 'background_color' ] . ';';
                $style .= '}';
            }

            if ( isset($styles[ 'background_color_hover' ]) ) {
                $style .= $css_selector . '__background:hover ' . '{';
                    $style .= 'background-color:' . $styles[ 'background_color_hover' ] . ';';
                $style .= '}';
            }

            if ( isset($styles[ 'text_btn_color' ]) ) {
//                $css_temp_selector[ $class_text_btn_color ] .= $main_class.'__text_color ';
                $style .= $css_selector . '__text_color ' . '{';
                    $style .= 'color:' . $styles[ 'text_btn_color' ] . ';';
                $style .= '}';
            }

            if ( isset($styles[ 'text_btn_color_hover' ]) ) {
                $style .= $css_selector . '__text_color:hover ' . '{';
                    $style .= 'color:' . $styles[ 'text_btn_color_hover' ] . ';';
                $style .= '}';
            }

            $std_inline_styles .= $style;
            $this->style = $css_id;

            return array(
                'id'  => $css_id,
                'css' => $style,
                'classes' => $css_temp_selector,
            );
        }

        public function vc_map_shortcode() {

            $vc_map = array(
                array(
                    'type' => 'textfield',
                    'param_name' => 'btn_name',
                    'heading' => esc_html__( 'Text button', 'stthemes' ),
                    'value' => 'Learn more',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'vc_link',
                    'param_name' => 'btn_link',
                    'heading' => esc_html__( 'Link', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'dropdown',
                    'param_name' => 'pos',
                    'heading' => esc_html__( 'Align Button', 'stthemes' ),
                    'value' => array(
                        esc_html__( 'Left', 'stthemes' ) => 'text-left',
                        esc_html__( 'Right', 'stthemes' ) => 'text-right',
                        esc_html__( 'Center', 'stthemes' ) => 'text-center',
                    ),
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'heading' => 'Choose button color',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'text_btn_color',
                    'heading' => esc_html__( 'Text Color', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column mpc-advanced-field',
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'text_btn_color_hover',
                    'heading' => esc_html__( 'Text Hover Color', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column mpc-advanced-field',
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'background_color',
                    'heading' => esc_html__( 'Button Color', 'stthemes' ),
                    'description' => esc_html__( 'Background button color', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column mpc-advanced-field',
                ),
                array(
                    'type' => 'colorpicker',
                    'param_name' => 'background_color_hover',
                    'heading' => esc_html__( 'Button Hover Color', 'stthemes' ),
                    'description' => esc_html__( 'Background button hover color', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column mpc-advanced-field',
                ),
                array(
                    'type' => 'st_separate',
                    'param_name' => 'st_separate_name',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'el_class',
                    'heading' => esc_html__( 'Extra class', 'stthemes' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'css_editor',
                    'param_name' => 'css',
                    'heading' => esc_html__( 'CSS', 'stthemes' ),
                    'group' => esc_html__( 'Design', 'stthemes' ),
                )
            );

            vc_map( array(
                'name' => esc_html__( 'Button', 'stthemes' ),
                'base' => $this->shortcode_name(),
                'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-ico.png',
                'category' => esc_html__( 'SkyThemes', 'stthemes' ),
                'params' => $vc_map,
            ) );

        }
    }
    /**
     * Init class
     */
    new STThemesVcButtonShortcode;
}
