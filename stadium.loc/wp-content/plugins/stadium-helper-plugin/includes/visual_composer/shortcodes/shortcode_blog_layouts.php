<?php

# Blog layouts

if ( !class_exists( 'STThemesVcBlogLayoutsShortcode' ) ) {
	class STThemesVcBlogLayoutsShortcode extends STThemesVcShortcode {

		function shortcode_name() {
			return 'std_blog_layouts';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'stthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'layout' => 3,
				'category' => '',
				'ignore' => '',
				'maxposts' => 6,
				'effect' => '',
				'read_more_btn' => 1,
				'pagination' => 'numeric',
				'order_post' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			set_query_var( 'read_more_btn', $read_more_btn );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$categories = $ignored = '';

			if ( $category ){
				$categories = trim( $category, ' ' );
				$categories = explode( ',', $categories );
			}

			if ( $ignore ){
				$ignored = trim( $ignore, ' ' );
				$ignored = explode( ',', $ignored );
			}

			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : (get_query_var( 'page' ) ? get_query_var( 'page' ) : 1);

			$args = array(
				'post_type' => 'post',
				'cat' => $categories,
				// 'posts_per_page' => $pagination == 'none' ? -1 : $maxposts,
				'posts_per_page' => $maxposts,
				'paged' => $paged,
				'post__not_in' => $ignored,
				'order' => $order_post,
			);
			$new_query = new WP_Query( $args );

			$css_class .= ' vlt-posts-layout'.$layout.'-container';
			$css_class .= ' vlt-pagination-'.$pagination.'-container';
			$css_class .= ' clearfix';

			ob_start();

		?>

			<div class="<?php echo $css_class; ?>">
				<!-- Default -->
				<?php if ( $layout == 1 ) : ?>
					<div class="masonry clearfix" data-masonry-col="1" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>
						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">';
								get_template_part( 'template-parts/post/post-style', 'default' );
								echo '</div>';
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Masonry 2 Col -->
				<?php if ( $layout == 2 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">';
								get_template_part( 'template-parts/post/post-style', 'masonry' );
								echo '</div>';
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Masonry 3 Col -->
				<?php if ( $layout == 3 ) : ?>
					<div class="masonry clearfix" data-masonry-col="3" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php 
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">'; 
								get_template_part( 'template-parts/post/post-style', 'masonry' );
								echo '</div>';
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- First Default / Other Masonry 2 Col -->
				<?php if ( $layout == 4 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- First Default / Other Masonry 3 Col -->
				<?php if ( $layout == 5 ) : ?>
					<div class="masonry clearfix" data-masonry-col="3" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Default / Every second and third post Masonry -->
				<?php if ( $layout == 6 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current % 3 == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<?php

					echo stadium_get_pagination( $new_query, $pagination );
					wp_reset_postdata();

				?>

			</div>

		<?php

			return ob_get_clean();
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'dropdown',
					'param_name' => 'layout',
					'heading' => esc_html__( 'Layout', 'stthemes' ),
					'description' => esc_html__( 'Select a blog layout from the list below.', 'stthemes' ),
					'admin_label' => true,
					'std' => 3,
					'value' => array(
						esc_html__( 'Default', 'stthemes' ) => 1,

						esc_html__( 'Masonry 2 Columns', 'stthemes' ) => 2,
						esc_html__( 'Masonry 3 Columns', 'stthemes' ) => 3,

						esc_html__( 'First Post Default / Other Masonry 2 Columns', 'stthemes' ) => 4,
						esc_html__( 'First Post Default / Other Masonry 3 Columns', 'stthemes' ) => 5,

						esc_html__( 'Default / Each second and third post Masonry', 'stthemes' ) => 6,
					),
					'group' => esc_html__( 'Style', 'stthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'pagination',
					'heading' => esc_html__( 'Type navigation', 'stthemes' ),
					'description' => esc_html__( 'Select a type of navigation or disable it.', 'stthemes' ),
					'admin_label' => true,
					'value' => array(
						esc_html__( 'Numeric', 'stthemes' ) => 'numeric',
						esc_html__( 'Paged', 'stthemes' ) => 'paged',
						esc_html__( 'Load More', 'stthemes' ) => 'load-more',
						esc_html__( 'None', 'stthemes' ) => 'none',
					),
					'group' => esc_html__( 'Style', 'stthemes' ),
					// 'dependency' => array(
					//     'callback' => 'myCustomDependencyCallbackLayout',
					// ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'order_post',
					'heading' => esc_html__( 'Sort order', 'stthemes' ),
					'description' => esc_html__( 'Select sorting order.', 'stthemes' ),
					'admin_label' => true,
					'value' => array(
						esc_html__( 'Descending', 'stthemes' ) => 'DESC',
						esc_html__( 'Ascending', 'stthemes' ) => 'ASC',
					),
					'group' => esc_html__( 'Loop', 'stthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'read_more_btn',
					'heading' => esc_html__( 'Read more button', 'stthemes' ),
					'description' => esc_html__( 'Check it if you want to show Read more button.', 'stthemes' ),
					'std' => 'enable',
					'value' => array(
						esc_html__( 'Enable', 'stthemes' ) => 'enable'
					),
					// 'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'Style', 'stthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'effect',
					'heading' => esc_html__( 'Appearance Effect', 'stthemes' ),
					'description' => esc_html__( 'Check it if you want to activate appearance effect.', 'stthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'stthemes' ) => '1'
					),
					'group' => esc_html__( 'Style', 'stthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'maxposts',
					'heading' => esc_html__( 'Maximum Posts', 'stthemes' ),
					'description' => esc_html__( 'Select a maximum number of posts (If you select -1 then all posts will be shown).', 'stthemes' ),
					'admin_label' => true,
					'value' => 6,
					'min' => -1,
					'max' => 999999,
					'step' => 1,
					'group' => esc_html__( 'Loop', 'stthemes' ),
				),
				array(
					'type' => 'vl_select',
					'param_name' => 'category',
					'heading' => esc_html__( 'Show posts from categories', 'stthemes' ),
					'description' => esc_html__( 'Enter a categories which you want to display (If this field is empty, all posts will be displayed).', 'stthemes' ),
					'admin_label' => true,
					'multiple' => 'multiple',
					'value' => STThemesVcShortcodeHelper::get_terms( 'category' ),
					'group' => esc_html__( 'Loop', 'stthemes' ),
				),
				array(
					'type' => 'vl_select',
					'param_name' => 'ignore',
					'heading' => esc_html__( 'Ignore Posts', 'stthemes' ),
					'description' => esc_html__( 'Enter a posts which you want to hide from loop (If this field is empty, all posts will be displayed).', 'stthemes' ),
					'admin_label' => true,
					'multiple' => 'multiple',
					'value' => STThemesVcShortcodeHelper::get_posts(
						array(
							'post_type' => 'post',
							'posts_per_page' => -1,
						)
					),
					'group' => esc_html__( 'Loop', 'stthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'stthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'stthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'stthemes' ),
					'group' => esc_html__( 'Design', 'stthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Blog Layouts', 'stthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-ico.png',
				'category' => esc_html__( 'SkyThemes', 'stthemes' ),
				// 'front_enqueue_js' => 'http://stadium.loc/wp-content/plugins/stadium-helper-plugin/assets/js/tess.js',
				'params' => $vc_map,
				// 'is_container' => true,
				// 'content_element' => true,
				// 'js_view' => 'VcColumnView',
			) );

		}
	}
	/**
	 * Init class
	 */
	new STThemesVcBlogLayoutsShortcode;

	/**
	 * The "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
	 */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Vlt_Blog_Layouts extends WPBakeryShortCodesContainer {}
	}

}
