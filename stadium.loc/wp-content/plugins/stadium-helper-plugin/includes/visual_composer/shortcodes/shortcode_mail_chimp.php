<?php

# Mail Chimp

if ( !class_exists( 'STThemesVcMailChimpShortcode' ) ) {
    class STThemesVcMailChimpShortcode extends STThemesVcShortcode {

        function shortcode_name() {
            return 'sky_mail_chimp';
        }

        public function __construct() {
            parent::__construct();
        }

        public function register_shortcode( $atts, $content = null ) {
            $identifier_class = uniqid( 'stthemes_custom_' );
            $output = $el_class = $css = '';

            extract( shortcode_atts( array(
                'el_class' => '',
                'css' => '',
            ), $atts ) );


            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

	        $output .= '<div class="mpc-mailchimp ' . $css_class . '">';
	        if ( ! isset($atts[ 'form_id' ]) ) {
	        	$output .= '<p><br>Please select your form from dropdown list created in <a href="/wp-admin/admin.php?page=mailchimp-for-wp-forms" target="_blank">Mailchimp form option</a>.<br></p>';
	        }
	        else {
	        	$output .= do_shortcode( '[mc4wp_form id="' . esc_attr( $atts[ 'form_id' ] ) . '"]' );
	        };
	        $output .= '</div>';

            return $output;
        }

        public function vc_map_shortcode() {

			$forms_list = array( '' => '', );

			if ( function_exists( 'mc4wp_get_forms' ) ) {
				$forms = mc4wp_get_forms();

				if ( ! empty( $forms ) ) {
					foreach ( $forms as $form ) {
						if ( isset( $form->name ) && isset( $form->ID ) ) {
							$forms_list[ $form->name ] = $form->ID;
						}
					}
				}
			}

            $vc_map = array(
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Form', 'mpc' ),
					'param_name'  => 'form_id',
					'admin_label' => true,
					'tooltip'     => __( 'Select form you want to style and display.', 'mpc' ),
					'value'       => $forms_list,
					'std'         => '',
					'description' => __( 'Make sure you are using <a href="https://wordpress.org/plugins/mailchimp-for-wp/" target="_blank">MailChimp for WordPress</a>.', 'mpc' ),
				),
                array(
                    'type' => 'textfield',
                    'param_name' => 'el_class',
                    'heading' => esc_html__( 'Extra class', 'stthemes' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'css_editor',
                    'param_name' => 'css',
                    'heading' => esc_html__( 'CSS', 'stthemes' ),
                    'group' => esc_html__( 'Design', 'stthemes' ),
                )
            );

            vc_map( array(
                'name' => esc_html__( 'Mail Chimp', 'stthemes' ),
                'base' => $this->shortcode_name(),
                'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-mail-chimp-ico.png',
                'category' => esc_html__( 'SkyThemes', 'stthemes' ),
                'params' => $vc_map,
            ) );

        }
    }
    /**
     * Init class
     */
    new STThemesVcMailChimpShortcode;
}
