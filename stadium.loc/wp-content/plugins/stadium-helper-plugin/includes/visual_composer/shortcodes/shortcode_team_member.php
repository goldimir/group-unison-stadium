<?php

# Team Members

if ( !class_exists( 'STThemesVcTeamMemberShortcode' ) ) {
    class STThemesVcTeamMemberShortcode extends STThemesVcShortcode {

        function shortcode_name() {
            return 'sky_team_member';
        }

        public function __construct() {
            parent::__construct();
        }

        public function register_shortcode( $atts, $content = null ) {
            $identifier_class = uniqid( 'stthemes_custom_' );
            $output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

            extract( shortcode_atts( array(
                'image' => '',
                'title' => 'Example text',
                'about_member' => '',
                'new_window' => '',
                'facebook_link' => '',
                'skype_link' => '',
                'twitter_link' => '',
                'google_link' => '',
                'el_class' => '',
                'css' => '',
            ), $atts ) );

            $about_member = vc_value_from_safe( $about_member );

            // Cut text for fron-ent side
            // $short_about_member = STThemesVcShortcodeHelper::cut_text( $about_member );
            $short_about_member = stadium_limit_text( $about_member, 15 );

            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

            $css_class .= ' mainflip';

            $output .= '<section class="team">';
            $output .= '<div class="image-flip">';
            $output .= '<div class="' . $css_class . '">';

            $output .= '<div class="frontside">';
            $output .= '<div class="card">';
            $output .= '<div class="card-body text-center">';
            if ( $image ) {
                $output .= '<p><img src="' . wp_get_attachment_image_url( $image, 'stadium-full' ) . '" alt="' . $title . '" class="img-fluid"></p>';
            }
            $output .= '<h4 class="card-title">'.$title.'</h4>';
            $output .= '<p class="card-text">'.$short_about_member.'</p>';
            $output .= '<button class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';

            $output .= '<div class="backside">';
            $output .= '<div class="card">';
            $output .= '<div class="card-body text-center mt-4">';
            $output .= '<h4 class="card-title">'.$title.'</h4>';
            $output .= '<p class="card-text">'.$about_member.'</p>';
            $output .= '<ul class="list-inline">';

            if ( $new_window ) {
                $new_window = 'target="_blank"';
            }

            if ( $facebook_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$facebook_link.'"><i class="fab fa-facebook-f"></i></a>';
                $output .= '</li>';
            }
            if ( $twitter_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$twitter_link.'"><i class="fab fa-twitter"></i></a>';
                $output .= '</li>';
            }
            if ( $skype_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$skype_link.'"><i class="fab fa-skype"></i></a>';
                $output .= '</li>';
            }
            if ( $google_link ) {
                $output .= '<li class="list-inline-item">';
                $output .= '<a class="social-icon text-xs-center" '.$new_window.' href="'.$google_link.'"><i class="fab fa-google"></i></a>';
                $output .= '</li>';
            }
            
            $output .= '</ul></div></div></div></div></div></section>';

            return $output;

        }

        public function vc_map_shortcode() {

            $vc_map = array(
                array(
                    'type' => 'attach_image',
                    'param_name' => 'image',
                    'heading' => esc_html__( 'Image', 'stthemes' ),
                    'description' => esc_html__( 'Select an image for Team Member. Image should be symmetrical in width and height.', 'stthemes' ),
                    'value' => '',
                    'holder' => 'img',
                    'class' => 'vc-preview-image',
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'title',
                    'heading' => esc_html__( 'Name', 'stthemes' ),
                    'description' => esc_html__( 'Enter a Name for Member.', 'stthemes' ),
                    'value' => 'Example text',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' ),
                ),
                array(
                    'type' => 'textarea_safe',
                    'param_name' => 'about_member',
                    'heading' => esc_html__( 'About member', 'stthemes' ),
                    'description' => esc_html__( 'Enter text.', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'facebook_link',
                    'heading' => esc_html__( 'Link to Facebook Group', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'twitter_link',
                    'heading' => esc_html__( 'Link to Twitter social group', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'skype_link',
                    'heading' => esc_html__( 'Link to Skype account', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'google_link',
                    'heading' => esc_html__( 'Link to Google', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'new_window',
                    'heading' => esc_html__( 'Open links in new window', 'stthemes' ),
                    'value' => '',
                    'admin_label' => true,
                    'group' => esc_html__( 'General', 'stthemes' )
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'el_class',
                    'heading' => esc_html__( 'Extra class', 'stthemes' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'css_editor',
                    'param_name' => 'css',
                    'heading' => esc_html__( 'CSS', 'stthemes' ),
                    'group' => esc_html__( 'Design', 'stthemes' ),
                )
            );

            vc_map( array(
                'name' => esc_html__( 'Team Member Card', 'stthemes' ),
                'base' => $this->shortcode_name(),
                'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-ico.png',
                'category' => esc_html__( 'SkyThemes', 'stthemes' ),
                'params' => $vc_map
            ) );

        }
    }
    /**
     * Init class
     */
    new STThemesVcTeamMemberShortcode;
}
