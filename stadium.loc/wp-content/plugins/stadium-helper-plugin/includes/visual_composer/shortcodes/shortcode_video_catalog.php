<?php

# Video Catalog
if ( !class_exists( 'RWMB_Loader' ) )
	return;

if ( !class_exists( 'STThemesVcVideoCatalogShortcode' ) ) {
    class STThemesVcVideoCatalogShortcode extends STThemesVcShortcode {

        function shortcode_name() {
            return 'sky_video_catalog';
        }

        public function __construct() {
            parent::__construct();
        }

        public function register_shortcode( $atts, $content = null ) {
            $identifier_class = uniqid( 'stthemes_custom_' );
            $output = $el_class = $css = '';

            extract( shortcode_atts( array(
//                'catalog_video_id' => '',
                'el_class' => '',
                'css' => '',
            ), $atts ) );

//            wp_localize_script(
//                    'shortcodes-js',
//                    'curr_catalog',
//                    array(
//                        'catalog_video_id' => $catalog_video_id,
//                    )
//                );

            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

	        $output .= '<div class="wrap-vg ' . $css_class . '">';
	        $output .= '<div class="filter-video-gallery form-stadium">';
	        $output .= '<form class="vg-filter">';

	        $taxonomies = get_object_taxonomies( 'video-catalog', 'object' );
	        $taxonomies_name = array_keys( $taxonomies );
	        $terms = get_terms($taxonomies_name);

	        foreach ( $taxonomies as $taxonomy ) {

		     $output .= '<div class="form-group"><select class="form-control select-filter selectbasic">';
			 $output .= '<option selected data-taxonomy="' . $taxonomy->name . '" value="">' . $taxonomy->label . '</option>';

			        foreach ( $terms as $term ) {
				        if ( $term->taxonomy == $taxonomy->name ) {
				        	$output .= '<option data-taxonomy="' . $term->taxonomy . '" value="' . $term->slug . '">' . $term->name . '</option>';
				        }
			        }

		     $output .= '</select><i class="fas fa-chevron-down"></i></div>';
	        }

	        $output .= '<input type="submit" value="Reset" class="form-control reset-btn sky-main-btn">';
	        $output .= '</form>';
	        $output .= '</div>';

	        $output .= '<div class="video-gallery-tax">';

	        $args = array(
		        'post_status' => 'publish',
		        'orderby'     => 'date',
		        'order'       => 'DESC',
		        'numberposts' => -1,
		        'post_type'   => 'video-catalog',
	        );
	        $posts = get_posts( $args );
	        foreach ( $posts as $post ) {
		        $videos = rwmb_meta( 'cat_video_id', '', $post->ID );
		        foreach ( $videos as $video ) {
			        $output .= '<article class="video">';
			        $output .= '<figure><video controls src="' . $video['src'] . '"></video></figure>';
			        $output .= '<h2 class="title">' . $post->post_title . '</h2>';
			        $output .= '</article>';

		        }
	        }

	        $output .= '</div>';
	        $output .= '</div>';
            return $output;
        }

        public function vc_map_shortcode() {

            $vc_map = array(
//				array(
//					'type'        => 'dropdown',
//					'heading'     => __( 'Video Catalog', 'stthemes' ),
//					'param_name'  => 'catalog_video_id',
//					'admin_label' => true,
//					'tooltip'     => __( 'Select video you want to display.', 'stthemes' ),
//					'value'       => STThemesVcShortcodeHelper::get_video_posts(
//						[
//							'post_status' => 'publish',
//							'orderby'     => 'date',
//							'order'       => 'DESC',
//							'numberposts' => -1,
//							'post_type'   => 'mb-post-type',
//						]
//					),
//				),
                array(
                    'type' => 'textfield',
                    'param_name' => 'el_class',
                    'heading' => esc_html__( 'Extra class', 'stthemes' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stthemes' ),
                    'value' => '',
                    'group' => esc_html__( 'Extras', 'stthemes' ),
                ),
                array(
                    'type' => 'css_editor',
                    'param_name' => 'css',
                    'heading' => esc_html__( 'CSS', 'stthemes' ),
                    'group' => esc_html__( 'Design', 'stthemes' ),
                )
            );

            vc_map( array(
                'name' => esc_html__( 'Video Catalog', 'stthemes' ),
                'base' => $this->shortcode_name(),
                'icon' => sttheme_helper_plugin()->plugin_url . 'assets/img/sky-shortcode-ico.png',
                'category' => esc_html__( 'SkyThemes', 'stthemes' ),
                'params' => $vc_map,
            ) );

        }
    }
    /**
     * Init class
     */
    new STThemesVcVideoCatalogShortcode;
}
