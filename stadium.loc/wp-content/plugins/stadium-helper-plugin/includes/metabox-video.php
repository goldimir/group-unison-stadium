<?php

function video_category_register_post_type() {
    $args = array (
        'label' => esc_html__( 'Video Catalog', 'text-domain' ),
        'labels' => array(
            'menu_name' => esc_html__( 'Video Catalog', 'text-domain' ),
            'name_admin_bar' => esc_html__( 'Video Catalog', 'text-domain' ),
            'add_new' => esc_html__( 'Add new', 'text-domain' ),
            'add_new_item' => esc_html__( 'Add new Video Catalog', 'text-domain' ),
            'new_item' => esc_html__( 'New Video Catalog', 'text-domain' ),
            'edit_item' => esc_html__( 'Edit Video Catalog', 'text-domain' ),
            'view_item' => esc_html__( 'View Video Catalog', 'text-domain' ),
            'update_item' => esc_html__( 'Update Video Catalog', 'text-domain' ),
            'all_items' => esc_html__( 'All Video Catalog', 'text-domain' ),
            'search_items' => esc_html__( 'Search Video Catalog', 'text-domain' ),
            'parent_item_colon' => esc_html__( 'Parent Video Catalog', 'text-domain' ),
            'not_found' => esc_html__( 'No Video Catalog found', 'text-domain' ),
            'not_found_in_trash' => esc_html__( 'No Video Catalog found in Trash', 'text-domain' ),
            'name' => esc_html__( 'Video Catalog', 'text-domain' ),
            'singular_name' => esc_html__( 'Video Catalog', 'text-domain' ),
        ),
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'show_in_rest' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite_no_front' => false,
        'menu_icon' => 'dashicons-admin-collapse',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
        ),
        'rewrite' => true,
    );

    register_post_type( 'video-catalog', $args );
}
add_action( 'init', 'video_category_register_post_type' );

function category_register_taxonomy() {

    $args = array (
        'label' => esc_html__( 'Categories', 'text-domain' ),
        'labels' => array(
            'menu_name' => esc_html__( 'Categories', 'text-domain' ),
            'all_items' => esc_html__( 'All Categories', 'text-domain' ),
            'edit_item' => esc_html__( 'Edit Categories', 'text-domain' ),
            'view_item' => esc_html__( 'View Categories', 'text-domain' ),
            'update_item' => esc_html__( 'Update Categories', 'text-domain' ),
            'add_new_item' => esc_html__( 'Add new Categories', 'text-domain' ),
            'new_item_name' => esc_html__( 'New Categories', 'text-domain' ),
            'parent_item' => esc_html__( 'Parent Categories', 'text-domain' ),
            'parent_item_colon' => esc_html__( 'Parent Categories:', 'text-domain' ),
            'search_items' => esc_html__( 'Search Categories', 'text-domain' ),
            'popular_items' => esc_html__( 'Popular Categories', 'text-domain' ),
            'separate_items_with_commas' => esc_html__( 'Separate Categories with commas', 'text-domain' ),
            'add_or_remove_items' => esc_html__( 'Add or remove Categories', 'text-domain' ),
            'choose_from_most_used' => esc_html__( 'Choose most used Categories', 'text-domain' ),
            'not_found' => esc_html__( 'No Categories found', 'text-domain' ),
            'name' => esc_html__( 'Categories', 'text-domain' ),
            'singular_name' => esc_html__( 'Categories', 'text-domain' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => false,
        'show_in_rest' => true,
        'hierarchical' => true,
        'query_var' => true,
        'sort' => false,
        'rewrite_no_front' => false,
        'rewrite_hierarchical' => false,
        'rewrite' => true,
    );

    register_taxonomy( 'categories', array( 'video-catalog' ), $args );
}
add_action( 'init', 'category_register_taxonomy', 0 );

function color_register_taxonomy() {

    $args = array (
        'label' => esc_html__( 'Color', 'text-domain' ),
        'labels' => array(
            'menu_name' => esc_html__( 'Color', 'text-domain' ),
            'all_items' => esc_html__( 'All Color', 'text-domain' ),
            'edit_item' => esc_html__( 'Edit Color', 'text-domain' ),
            'view_item' => esc_html__( 'View Color', 'text-domain' ),
            'update_item' => esc_html__( 'Update Color', 'text-domain' ),
            'add_new_item' => esc_html__( 'Add new Color', 'text-domain' ),
            'new_item_name' => esc_html__( 'New Color', 'text-domain' ),
            'parent_item' => esc_html__( 'Parent Color', 'text-domain' ),
            'parent_item_colon' => esc_html__( 'Parent Color:', 'text-domain' ),
            'search_items' => esc_html__( 'Search Color', 'text-domain' ),
            'popular_items' => esc_html__( 'Popular Color', 'text-domain' ),
            'separate_items_with_commas' => esc_html__( 'Separate Color with commas', 'text-domain' ),
            'add_or_remove_items' => esc_html__( 'Add or remove Color', 'text-domain' ),
            'choose_from_most_used' => esc_html__( 'Choose most used Color', 'text-domain' ),
            'not_found' => esc_html__( 'No Color found', 'text-domain' ),
            'name' => esc_html__( 'Color', 'text-domain' ),
            'singular_name' => esc_html__( 'Color', 'text-domain' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => false,
        'show_in_rest' => true,
        'hierarchical' => true,
        'query_var' => true,
        'sort' => false,
        'rewrite_no_front' => false,
        'rewrite_hierarchical' => false,
        'rewrite' => true,
    );

    register_taxonomy( 'color', array( 'video-catalog' ), $args );
}
add_action( 'init', 'color_register_taxonomy', 0 );

function manufacture_register_taxonomy() {

    $args = array (
        'label' => esc_html__( 'Manufacture', 'text-domain' ),
        'labels' => array(
            'menu_name' => esc_html__( 'Manufacture', 'text-domain' ),
            'all_items' => esc_html__( 'All Manufacture', 'text-domain' ),
            'edit_item' => esc_html__( 'Edit Manufacture', 'text-domain' ),
            'view_item' => esc_html__( 'View Manufacture', 'text-domain' ),
            'update_item' => esc_html__( 'Update Manufacture', 'text-domain' ),
            'add_new_item' => esc_html__( 'Add new Manufacture', 'text-domain' ),
            'new_item_name' => esc_html__( 'New Manufacture', 'text-domain' ),
            'parent_item' => esc_html__( 'Parent Manufacture', 'text-domain' ),
            'parent_item_colon' => esc_html__( 'Parent Manufacture:', 'text-domain' ),
            'search_items' => esc_html__( 'Search Manufacture', 'text-domain' ),
            'popular_items' => esc_html__( 'Popular Manufacture', 'text-domain' ),
            'separate_items_with_commas' => esc_html__( 'Separate Manufacture with commas', 'text-domain' ),
            'add_or_remove_items' => esc_html__( 'Add or remove Manufacture', 'text-domain' ),
            'choose_from_most_used' => esc_html__( 'Choose most used Manufacture', 'text-domain' ),
            'not_found' => esc_html__( 'No Manufacture found', 'text-domain' ),
            'name' => esc_html__( 'Manufacture', 'text-domain' ),
            'singular_name' => esc_html__( 'Manufacture', 'text-domain' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => false,
        'show_in_rest' => true,
        'hierarchical' => true,
        'query_var' => true,
        'sort' => false,
        'rewrite_no_front' => false,
        'rewrite_hierarchical' => false,
        'rewrite' => true,
    );

    register_taxonomy( 'manufacture', array( 'video-catalog' ), $args );
}
add_action( 'init', 'manufacture_register_taxonomy', 0 );

// Add filter for new taxonomies on top of admin panel
add_action( 'restrict_manage_posts', 'filter_by_taxonomies' , 10, 2);
function filter_by_taxonomies( $post_type ) {

	// A list of taxonomy slugs to filter by
	$taxonomies = get_object_taxonomies($post_type);

	foreach ( $taxonomies as $taxonomy_slug ) {

		// Retrieve taxonomy data
		$taxonomy_obj = get_taxonomy( $taxonomy_slug );
		$taxonomy_name = $taxonomy_obj->labels->name;

		// Retrieve taxonomy terms
		$terms = get_terms( $taxonomy_slug );

		// Display filter HTML
		echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
		echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
		foreach ( $terms as $term ) {
			printf(
				'<option value="%1$s" %2$s>%3$s (%4$s)</option>',
				$term->slug,
				( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
				$term->name,
				$term->count
			);
		}
		echo '</select>';
	}
}

//MB Post Type with plugin
add_filter( 'rwmb_meta_boxes', 'mb_prefix_meta_boxes_tax' );
function mb_prefix_meta_boxes_tax( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'  => 'video',
        'title'  => 'Video',
        'post_types' => 'video-catalog',
        'fields' => array(
            array(
                'name'             => 'Video',
                'id'               => 'cat_video_id',
                'type'             => 'video',
                'max_file_uploads' => 0,
                'force_delete'     => false,
            ),
        ),
    );
    return $meta_boxes;
}

/**
 * AJAX Search Filter Video Gallery
 */
function tax_query_filter( $items ) {

	$query = [];
	foreach ( $items as $taxonomy => $term ){
		$tax_term_query = array(
			'taxonomy' => $taxonomy,
			'field' => 'slug',
			'terms' => $term,
		);
		if( $term == '' ){
			$tax_term_query['operator'] = 'AND';
		};
		array_push($query, $tax_term_query);
	}
	$filter_query = [
		'relation' => 'AND',
		$query
	];
	return $filter_query;
}

function fly_ajax_tax_search(){

	$list_filters = $_POST['term'];
//	$curr_vid_cat = $_POST['curr_vid_catalog'];

	$args = array(
		'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'DESC',
		'numberposts' => -1,
		'post_type'   => 'video-catalog',
		'tax_query'   => tax_query_filter( $list_filters ),
	);
	$posts = get_posts( $args );

	foreach ( $posts as $post ) {
		$videos = rwmb_meta( 'cat_video_id', '', $post->ID );
		foreach ( $videos as $video ) {
			?>

			<article class="video">
				<figure><video controls src="<?php echo $video['src']; ?>"></video></figure>
				<h2 class="title"><?php echo $post->post_title ?></h2>
			</article>

			<?php
		}
	}
	wp_reset_postdata();
	exit;
}
add_action('wp_ajax_nopriv_fly_ajax_tax_search','fly_ajax_tax_search');
add_action('wp_ajax_fly_ajax_tax_search','fly_ajax_tax_search');