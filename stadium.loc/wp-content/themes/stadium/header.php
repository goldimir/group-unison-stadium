<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stadium
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'stadium/before_site' ); ?>

    <header class="header <?php echo ( get_theme_mod( 'sticky_header' ) == 1 ? 'header-sticky ' : ''); ?>">
        <?php do_action( 'stadium/top_of_header' ); ?>
        <div class="container<?php echo get_theme_mod( 'header_container_width' ); ?>">
            <div class="row">
                <?php if ( true == get_theme_mod( 'show_logo' ) ): ?>
                <div class="col-md-2">
                    <div class="logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <?php if ( get_theme_mod( 'header_default_logo' ) ) : ?>
                                <img src="<?php echo get_theme_mod( 'header_default_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                            <?php else: ?>
                                <h2 class="site-title"><?php echo get_theme_mod( 'company_name' ) ?></h2>
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
                <?php endif ?>
                <div class="<?php echo ( true == get_theme_mod( 'show_logo' ) ) ? 'col-md-10' : 'col-sm-12' ; ?> sky-menu-box">
                    <nav role="navigation" class="skydropdown">
	                    <?php
	                    wp_nav_menu(
		                    [
			                    'theme_location' => 'main-top-menu',
			                    'fallback_cb'    => '__return_empty_string',
		                    ]
	                    );
	                    ?>
                    </nav>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
        <?php do_action( 'stadium/under_header' ); ?>