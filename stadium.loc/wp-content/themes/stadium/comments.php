<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stadium
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}

# Custom Comment Fields
add_filter('comment_form_default_fields', 'extend_comment_custom_default_fields');
function extend_comment_custom_default_fields($fields) {

    unset($fields['url']);

    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );

    $fields[ 'author' ] = '<div class="form-group">'.
      '<input id="author" class="form-control" name="author" type="text" placeholder="Name" value="'. esc_attr( $commenter['comment_author'] ) .
      '" size="30" tabindex="1" ' . $aria_req . ' /></div>';

    $fields[ 'email' ] = '<div class="form-group">'.
      '<input id="email" class="form-control" name="email" type="text" placeholder="Email" value="'. esc_attr( $commenter['comment_author_email'] ) .
      '" size="30"  tabindex="2" ' . $aria_req . ' /></div>';

    $fields[ 'phone' ] = '<div class="form-group">'.
      '<input id="phone" class="form-control" name="phone" type="text" placeholder="Phone" size="30"  tabindex="4" /></div>';

  return $fields;
}

// add Custom Fields
add_filter( 'comment_text', 'modify_extend_comment');
function modify_extend_comment( $text ){
  if ( $commentphone = get_comment_meta( get_comment_ID(), 'phone', true ) ) {
    $commentphone = '<strong>Phone:</strong> ' . esc_attr( $commentphone ) . '<br/>';
    $text = $commentphone . $text;
  }
    return $text;
}

add_filter('comment_form_defaults', 'comment_form_custom_args');
function comment_form_custom_args( $arg ) {
    $arg['comment_field'] = '<div class="form-group"><textarea id="comment" class="form-control" name="comment" placeholder="Comment" cols="45" rows="8" aria-required="true"></textarea></div>';
    $arg['class_submit'] = 'submit sky-main-btn';
    return $arg;
}
?>

<div id="comments" class="comments-area">

    <?php
    // You can start editing here -- including this comment!
    if ( have_comments() ) :
        ?>
        <h2 class="comments-title">
            <?php
            $stadium_comment_count = get_comments_number();
            if ( '1' === $stadium_comment_count ) {
                printf(
                    /* translators: 1: title. */
                    esc_html__( 'One Comment on %1$s', 'stadium' ),
                    '<!--span>' . get_the_title() . '</span-->'
                );
            } else {
                printf( // WPCS: XSS OK.
                    /* translators: 1: comment count number, 2: title. */
                    esc_html( _nx( '%1$s thought on %2$s', '%1$s Comments %2$s', $stadium_comment_count, 'comments title', 'stadium' ) ),
                    number_format_i18n( $stadium_comment_count ),
                    '<!--span>' . get_the_title() . '</span-->'
                );
            }
            ?>
        </h2><!-- .comments-title -->

        <ol class="comment-list">
            <?php
            wp_list_comments( array(
                'style'      => 'ol',
                'short_ping' => true,
            ) );
            ?>
        </ol><!-- .comment-list -->

        <footer class="vlt-post-footer">
            <?php 
                if ( 'load-more-nav' == get_theme_mod( 'comment_nav' ) ) {
                    echo stadium_get_pagination( null, 'load-more-comments' );
                 } elseif ( 'paged-nav' == get_theme_mod( 'comment_nav' ) ) {
                    echo stadium_get_pagination( null, 'comments-post' );
                 }
              ?>
        </footer>
        <?php

        // If comments are closed and there are comments, let's leave a little note, shall we?
        if ( ! comments_open() ) :
            ?>
            <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'stadium' ); ?></p>
            <?php
        endif;

    endif; // Check for have_comments().
?>

<div class="row">
    <div class="col-lg-6">
        <div class="form-stadium">
            <?php comment_form(); ?>
        </div>
    </div>
</div>

</div><!-- #comments -->
