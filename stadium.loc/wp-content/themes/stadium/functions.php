<?php
/**
 * Stadium functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Stadium
 */

define( 'STADIUM_THEME_DIRECTORY', esc_url( trailingslashit( get_template_directory_uri() ) ) );
define( 'STADIUM_REQUIRE_DIRECTORY', trailingslashit( get_template_directory() ) );

/**
 * Implement the Custom Scripts & Styles feature.
 */
require STADIUM_REQUIRE_DIRECTORY .'inc/theme-enqueue.php';

/**
 * Custom template tags for this theme.
 */
require STADIUM_REQUIRE_DIRECTORY .'inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require STADIUM_REQUIRE_DIRECTORY .'inc/template-functions.php';

/**
 * Customizer additions.
 */
// require STADIUM_REQUIRE_DIRECTORY .'inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require STADIUM_REQUIRE_DIRECTORY .'inc/jetpack.php';
}

/**
 * Load TGM Plugins.
 */
require STADIUM_REQUIRE_DIRECTORY .'inc/tgm-stadium.php';

/**
 * Customizer additions # Kirki fields
 */
if ( class_exists( 'Kirki' ) ) {
    require_once STADIUM_REQUIRE_DIRECTORY . 'inc/framework/customizer.php';
}

/**
 * Theme actions.
 */
require STADIUM_REQUIRE_DIRECTORY .'inc/theme-actions.php';

/**
 * Theme demo import.
 */
if ( class_exists( 'OCDI_Plugin' ) ) {
    require STADIUM_REQUIRE_DIRECTORY . 'inc/theme-demo-import.php';
}