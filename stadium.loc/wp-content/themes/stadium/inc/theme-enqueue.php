<?php

class ThemeEnqueueAssets{

    public function __construct() {
        $theme_info = wp_get_theme();
        $this->assets_dir = STADIUM_THEME_DIRECTORY . 'assets/';
        $this->theme_version = $theme_info[ 'Version' ];
        $this->init_assets();
    }

    public function init_assets() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles_admin' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts_admin' ) );
    }

    public function enqueue_styles_admin() {
        wp_enqueue_style( 'stadium-custom-admin-css', $this->assets_dir .'css/wp-admin.css', array(), $this->theme_version );
    }

    public function enqueue_scripts_admin() {
    }

    public function enqueue_scripts() {

        wp_enqueue_script( 'imagesloaded' );
        wp_enqueue_script( 'jquery-masonry' );

        wp_enqueue_script( 'scrollTo', $this->assets_dir .'vendors/jquery.scrollTo.min.js', array( 'jquery' ), $this->theme_version, true );
        wp_enqueue_script( 'bootstrap-bundle-js', $this->assets_dir .'vendors/bootstrap.bundle.min.js', array('jquery'), '4.1.1', true);
        wp_enqueue_script( 'inview', $this->assets_dir .'vendors/jquery.inview.min.js', array( 'jquery' ), $this->theme_version, true );
        wp_enqueue_script( 'sticky', $this->assets_dir .'vendors/jquery.sticky.js', array( 'jquery' ), $this->theme_version, true );
        wp_enqueue_script( 'theme-script', $this->assets_dir .'js/theme.js', array('jquery'), $this->theme_version, true );
    }

    public function enqueue_styles() {
        wp_enqueue_style( 'bootstrap-css', $this->assets_dir .'css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome-css', $this->assets_dir .'css/font-awesome.all.min.css' );
        wp_enqueue_style( 'animate-css', $this->assets_dir .'css/animate.min.css' );
        wp_enqueue_style( 'stadium-theme-style', $this->assets_dir .'css/theme.css', array( 'bootstrap-css' ) );
        wp_enqueue_style( 'stadium-style', get_stylesheet_uri(), array(), $this->theme_version );
    }

}
new ThemeEnqueueAssets;