<?php
if ( ! function_exists( 'stadium_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function stadium_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Stadium, use a find and replace
         * to change 'stadium' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'stadium', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        /*
         * Register Menus.
         *
         */
        function change_nav_menu_args( $args ) {
            if ( $args['theme_location'] === 'main-top-menu' ) {
                $args['menu_class'] = 'skydown-mob skydropdown-list';
                $args['walker'] = new Stadium_Top_Nav_Menu();
            }
            $args['container'] = false;
            return $args;
        }
        add_filter( 'wp_nav_menu_args', 'change_nav_menu_args', 1001 );

        register_nav_menus( array(
            'main-top-menu' => esc_html__( 'Main Top Menu', 'stadium' ),
            'footer-top-menu' => esc_html__( 'Footer Top Menu', 'stadium' ),
            'footer-bot-menu' => esc_html__( 'Footer Bottom Menu', 'stadium' ),
        ) );

        /**
         * Custom Walker Top Menu
         */
        require get_template_directory() . '/inc/custom-walker-main-top-menu.php';

        /**
         * Remove WPBakery html tags from the_excerpt
         *
         */
        if(!function_exists('remove_wpbpb_from_excerpt'))  {
          function remove_wpbpb_from_excerpt( $excerpt ) {
            $patterns = array("/\[[\/]?vc_[^\]]*\]/","/\{[\{] ?vc_[^\}]*\}\}/");
            $replacements = "";
            $clean_excerpt = preg_replace($patterns, $replacements, $excerpt);
            return $clean_excerpt;
          }
        }
        add_filter( 'the_excerpt', 'remove_wpbpb_from_excerpt' , 11, 1 );

        /**
         * AJAX Search
         */
        function fly_ajax_search(){
            $args = array( 
                'post_type'        => 'any', 
                'post_status'      => 'publish', 
                'order'            => 'DESC', 
                'orderby'          => 'date', 
                's'                => $_POST['term'], 
                'posts_per_page'   => 5 
            );

            $query = new WP_Query( $args );

            if( $query->have_posts() ){
                while ( $query->have_posts() ) { $query->the_post();?> 
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php the_excerpt(); ?></li> 
            <?php }
            } else { ?>
                <li><a href="#">Nothing found</a></li>
            <?php }
            exit;
        }
        add_action('wp_ajax_nopriv_fly_ajax_search','fly_ajax_search');
        add_action('wp_ajax_fly_ajax_search','fly_ajax_search');

        /**
         * Add custom width to WPBakery standart shortcode vc_section
         */
        // $attributes = array(
        //     'type' => 'dropdown',
        //     'heading' => "Container Width",
        //     'param_name' => 'container_width',
        //     'weight' => 10,
        //     'value' => array(
        //         'Full Width' => '',
        //         'Box' => 'container mx-auto',
        //     ),
        // );
        // vc_add_param( 'vc_section', $attributes );

        /**
         * WYSIWYG valid HTML elements to accept ALL
         */
        function override_mce_options($initArray) {
            $opts = '+*[*]';
            $initArray['valid_elements'] = $opts;
            $initArray['extended_valid_elements'] = $opts;
            return $initArray;
        }
        add_filter('tiny_mce_before_init', 'override_mce_options');

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support( 'custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ) );

        # Trim limit text
        if ( ! function_exists( 'stadium_limit_text' ) ) {
            function stadium_limit_text( $content = false, $max_words = 18 ) {

                if ( $content == false ) {
                    return;
                }
                $content = preg_replace( "~(?:\[/?)[^/\]]+/?\]~s", '', $content );
                $content = strip_tags( $content );
                $words   = explode( ' ', $content, $max_words + 1 );
                if ( count( $words ) > $max_words ) {
                    array_pop( $words );
                    array_push( $words, '...', '' );
                }
                $content = implode( ' ', $words );
                $content = esc_html( $content );

                return $content;

            }
        }

        # Post taxonomy
        if ( ! function_exists( 'stadium_get_post_taxonomy' ) ) {
            function stadium_get_post_taxonomy( $post_id, $taxonomy, $delimiter = ', ', $get = 'name', $link = true ) {

                $tags = wp_get_post_terms( $post_id, $taxonomy );
                $list = '';
                foreach ( $tags as $tag ) {
                    if ( $link ) {
                        $list .= '<a href="' . get_category_link( $tag->term_id ) . '">' . $tag->$get . '</a>' . $delimiter;
                    } else {
                        $list .= $tag->$get . $delimiter;
                    }
                }
                return substr( $list, 0, strlen( $delimiter ) * ( -1 ) );

            }
        }

        # Post thumbnail
        if ( ! function_exists( 'stadium_get_post_thumbnail' ) ) {
            function stadium_get_post_thumbnail( $size = 'stadium-full' ) {

                global $post;
                $output = '';
                $primary_image = wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), $size, '', array( 'class' => '' ) );

                $output .= $primary_image;

                return apply_filters( 'stadium/get_post_thumbnail', $output );
            }
        }

        # Post Share Buttons
        if ( ! function_exists( 'stthemes_get_post_share_buttons' ) ) {
            function stthemes_get_post_share_buttons( $postID ) {
                $url = urlencode( get_permalink( $postID ) );
                $title = urlencode( get_the_title( $postID ) );
                $media = wp_get_attachment_image_src(get_post_thumbnail_id( $postID, 'stadium-full' ) );
                $output = '';
                $output .= '<a class="vlt-single-icon twitter" target="_blank" href="https://twitter.com/home?status='.$title.'+'.$url.'"><i class="fab fa-twitter"></i></a>';
                $output .= '<a class="vlt-single-icon facebook" target="_blank" href="https://www.facebook.com/share.php?u='.$url.'&title='.$title.'"><i class="fab fa-facebook-f"></i></a>';
                $output .= '<a class="vlt-single-icon pinterest" target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media='.$media[0].'&url='.$url.'&is_video=false&description='.$title.'"><i class="fab fa-pinterest"></i></a>';
                $output .= '<a class="vlt-single-icon linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?url='. $url .'&title=' . $title . '"><i class="fab fa-linkedin-in"></i></a>';

                return apply_filters( 'stthemes/get_post_share_buttons', $output );
            }
        }

        # Load more button
        if ( ! function_exists( 'stadium_load_more_btn' ) ) {
            function stadium_load_more_btn( $wp_query = null ) {

                if( $wp_query == null ) {
                    global $wp_query;
                } else {
                    $wp_query = $wp_query;
                }

                $max = $wp_query->max_num_pages;
                $paged = ( get_query_var( 'paged' ) > 1 ) ? get_query_var( 'paged' ) : 1;

                wp_localize_script(
                    'shortcodes-js',
                    'load_more_btn',
                    array(
                        'startPage' => $paged,
                        'maxPages' => $max,
                        'nextLink' => next_posts( $max, false ),
                        'text' => esc_html__( 'Load More', 'stadium' ),
                        'noMore' => esc_html__( 'No More Posts', 'stadium' ),
                        'loading' => esc_html__( 'Loading', 'stadium' ),
                    )
                );
            }
        }

        # All paginations
        if ( ! function_exists( 'stadium_get_pagination' ) ) {
            function stadium_get_pagination( $query = null, $paginated = 'numeric' ) {

                if ( $query == null ) {
                    global $wp_query;
                    $query = $wp_query;
                }

                $page  = $query->query_vars['paged'];
                $pages = $query->max_num_pages;
                $comments = get_comment_pages_count();
                $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );

                //Count posts in current post category
                $posts = get_the_category()[0]->count;

                if ( $page == 0 ) {
                    $page = 1;
                }

                $output = '';

                if( $comments > 1 ) {
                    if ( $paginated == 'comments-post' ) {
                        $output .= '<nav class="vlt-pagination vlt-pagination--paged">';
                        $output .= '<ul>';

                        $prev_link = get_previous_comments_link('<i class="fas fa-chevron-left"></i><span>'. 'Prev' .'</span>');
                        $next_link = get_next_comments_link('<i class="fas fa-chevron-right"></i><span>'. 'Next' .'</span>');

                        if( $prev_link ) { 
                            $output .= '<li class="prev-page">' . $prev_link . '</li>';
                        } else { 
                            $output .= '<li class="prev-page not-active"><a href=""><i class="fas fa-chevron-left"></i></a></li>';
                        }; 

                        if( $next_link ) { 
                            $output .= '<li class="next-page">' . $next_link . '</li>';
                        } else { 
                            $output .= '<li class="next-page not-active"><a href=""><i class="fas fa-chevron-right"></i></a></li>';
                        }; 

                        $output .= '</ul>';
                        $output .= '</nav>';
                    }
                    if ( $paginated == 'load-more-comments' ) {

                        $cpage = get_query_var('cpage') ? get_query_var('cpage') : 1;
                         
                        if( $cpage > 1 ) {
                            $output .= '<nav class="vlt-pagination vlt-pagination--load-more"><a href="#" class="sky-main-btn comment_loadmore"><i class="fas fa-spinner icofont-rotate"></i><span>Load More</span></a></nav>
                                <script>
                                        var parent_post_id = ' . get_the_ID() . ',
                                            cpage = ' . $cpage . ';
                                </script>';
                            $output .= stadium_load_more_btn();
                        }
                    }
                }

                if( $posts > 1 ) {
                    if ( $paginated == 'paged-posts' ) {
                        $output .= '<nav class="vlt-pagination vlt-pagination--paged">';
                        $output .= '<ul>';

                        $prev_post = get_adjacent_post(true, '', true);
                        $next_post = get_adjacent_post(true, '', false);

                        if( $prev_post ) { 
                            $output .= '<li class="prev-page"><a href="' . get_permalink($prev_post->ID) . '"><i class="fas fa-chevron-left"></i><span>'.esc_html__( 'Prev', 'stadium' ).'</span></a></li>';
                        } else { 
                            $output .= '<li class="prev-page not-active"><a href=""><i class="fas fa-chevron-left"></i></a></li>';
                        }; 

                        if( $next_post ) { 
                            $output .= '<li class="next-page"><a href="' . get_permalink($next_post->ID) . '"><i class="fas fa-chevron-right"></i><span>'.esc_html__( 'Next', 'stadium' ).'</span></a></li>';
                        } else { 
                            $output .= '<li class="next-page not-active"><a href=""><i class="fas fa-chevron-right"></i></a></li>';
                        }; 

                        $output .= '</ul>';
                        $output .= '</nav>';
                    }
                }

                if ( $pages > 1 ) {

                    if ( $paginated == 'paged' ) {
                        $output .= '<nav class="vlt-pagination vlt-pagination--paged">';
                        $output .= '<ul>';
                        if ( $page - 1 >= 1 ) {
                            $output .= '<li class="prev-page"><a href="' . get_pagenum_link( $page - 1 ) . '"><i class="fas fa-chevron-left"></i><span>'.esc_html__( 'Prev', 'stadium' ).'</span></a></li>';
                        } else {
                            $output .= '<li class="prev-page inactive"></li>';
                        }
                        if ( $page + 1 <= $pages ) {
                            $output .= '<li class="next-page"><a href="' . get_pagenum_link( $page + 1 ) . '"><i class="fas fa-chevron-right"></i><span>'.esc_html__( 'Next', 'stadium' ).'</span></a></li>';
                        } else {
                            $output .= '<li class="next-page inactive"></li>';
                        }
                        $output .= '</ul>';
                        $output .= '</nav>';
                    }

                    if ( $paginated == 'numeric' ) {
                        $numeric_links = paginate_links( array(
                            'foramt' => '',
                            'add_args' => '',
                            'current' => $paged,
                            'total' => $pages,
                            'prev_text' => esc_html__( 'Prev', 'stadium' ),
                            'next_text' => esc_html__( 'Next', 'stadium' ),
                        ) );
                        $output .= '<nav class="vlt-pagination vlt-pagination--numeric">';
                        $output .= $numeric_links;
                        $output .= '</nav>';
                    }

                    if ( $paginated == 'load-more' ) {
                        $output .= '<nav class="vlt-pagination vlt-pagination--load-more"><a href="#" class="sky-main-btn"><i class="fas fa-spinner icofont-rotate"></i><span></span></a></nav>';
                        $output .= stadium_load_more_btn( $query );
                    }
                }
                return apply_filters('stadium/pagination', $output);
            }
        }

        // Custom Comment Functions
        require STADIUM_REQUIRE_DIRECTORY .'inc/custom-comment.php';

	    # Sanitize class
	    if ( ! function_exists( 'std_sanitize_class' ) ) {
		    function std_sanitize_class( $class, $fallback = null ) {

			    if ( is_string( $class ) ) {
				    $class = explode( ' ', $class );
			    }
			    if ( is_array( $class ) && count( $class ) > 0 ) {
				    $class = array_map( 'sanitize_html_class', $class );
				    return implode( ' ', $class );
			    } else {
				    return sanitize_html_class( $class, $fallback );
			    }

		    }
	    }

	    /**
	     * Register widget area.
	     *
	     * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	     */
	    function stadium_widgets_init() {
		    $footer_columns = get_theme_mod( 'footer_columns', 4 );
		    for ( $i = 1; $i < $footer_columns + 1; $i++ ) {
			    register_sidebar(array(
				    'name' => sprintf( esc_html__( 'Footer Sidebar: %s Column', 'stadium' ), $i ),
				    'id' => sanitize_key( 'footer_sidebar_' . $i ),
				    'description' => esc_html__( 'Footer Widget Area', 'stadium' ),
				    'before_widget' => '<div id="%1$s" class="vlt-widget %2$s">',
				    'after_widget' => '</div>',
				    'before_title' => '<h5 class="vlt-widget__title">',
				    'after_title' => '</h5>'
			    ));
		    }
	    }
	    add_action( 'widgets_init', 'stadium_widgets_init' );

	    // Prevent update for WPBakery Page Builder
        global $pagenow;
	    if ($pagenow == 'plugins.php') {
		    function filter_plugin_updates($value) {
			    if( isset( $value->response['wp-bakery/js_composer.php'] ) )
				    unset( $value->response['wp-bakery/js_composer.php'] );

			    return $value;
		    }
		    add_filter('site_transient_update_plugins', 'filter_plugin_updates');
	    }

    }
endif;
add_action( 'after_setup_theme', 'stadium_setup' );
