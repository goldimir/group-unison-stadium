<?php

/*Import content data*/
if ( ! function_exists( 'stadium_import_files' ) ) :
    function stadium_import_files() {
        return array(
            array(
                'import_file_name'             => 'Default Demo',
                'local_import_file'            => STADIUM_REQUIRE_DIRECTORY . 'inc/demo/default/stadium-content.xml',
                'local_import_widget_file'     => STADIUM_REQUIRE_DIRECTORY . 'inc/demo/default/stadium-widgets.wie',
                'local_import_customizer_file' => STADIUM_REQUIRE_DIRECTORY . 'inc/demo/default/stadium-customizer-export.dat',
                'import_preview_image_url'     => STADIUM_THEME_DIRECTORY . 'inc/demo/default/screenshot.png',
                'import_notice'                => __( 'Please waiting for a few minutes, do not close the window or refresh the page until the data is imported.', 'stadium' ),
            ),
            array(
                'import_file_name'             => 'only for thumbnails view',
            ),
        );
    }
    add_filter( 'pt-ocdi/import_files', 'stadium_import_files' );
endif;

//Assign menus to theme locations & set the front page
if ( ! function_exists( 'stadium_after_import' ) ) :
    function stadium_after_import( $selected_import ) {

        if ( 'Default Demo' === $selected_import['import_file_name'] ) {

            //Set Menu
            $main_top_menu = get_term_by('name', 'Main Top Menu', 'nav_menu');
            $footer_top_menu = get_term_by('name', 'Footer Top Menu', 'nav_menu');
            $footer_bot_menu = get_term_by('name', 'Footer Bottom Menu', 'nav_menu');
            set_theme_mod( 'nav_menu_locations' , array(
                    'main-top-menu' => $main_top_menu->term_id,
                    'footer-top-menu' => $footer_top_menu->term_id,
                    'footer-bot-menu' => $footer_bot_menu->term_id
                )
            );

            //Set Front page
            $page = get_page_by_title( 'Home');
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }

            //Set default WP settings->discussion for comments list
            update_option( 'show_comments_cookies_opt_in', 0 );
            update_option( 'page_comments', 1 );
            update_option( 'comments_per_page', 10 );
            update_option( 'comment_order', 'desc' );
            update_option( 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/' );

		    //Import Revolution Slider
		    if ( class_exists( 'RevSlider' ) ) {
			    $slider_array = array(
				    STADIUM_REQUIRE_DIRECTORY . "inc/demo/default/revslider/classic-carousel1.zip",
				    STADIUM_REQUIRE_DIRECTORY . "inc/demo/default/revslider/classic-slider-stadium.zip",
				    STADIUM_REQUIRE_DIRECTORY . "inc/demo/default/revslider/vimeo-hero.zip"
			    );

			    $slider = new RevSlider();

			    foreach($slider_array as $filepath){
				    $slider->importSliderFromPost(true,true,$filepath);
			    }

			    echo ' Slider processed';

		    }
        }
    }
    add_action( 'pt-ocdi/after_import', 'stadium_after_import' );
endif;

//Unset default widgets
if ( ! function_exists( 'ocdi_before_widgets_import' ) ) :
    function ocdi_before_widgets_import( $selected_import ) {

        $sidebar_widgets = wp_get_sidebars_widgets();
        foreach($sidebar_widgets['footer_sidebar_1'] as $i => $widget) {
            unset($sidebars_widgets['footer_sidebar_1'][$i]);
        }
        wp_set_sidebars_widgets($sidebars_widgets);
    }
    add_action( 'pt-ocdi/before_widgets_import', 'ocdi_before_widgets_import' );
endif;

// Information for Customer
function demo_import_notice(){
    // Early exit if the user has dismissed the consent.
    if ( get_option( 'str_dismiss_notice' ) ) {
        return;
    }
    echo sprintf(
        '<div class="notice notice-info std-import-notice" style="font-weight: 600;">
                    <p>Please import DEMO content to see all predefined Pages, Posts, Sliders, Images, Menus and Widgets on the site.<br/>
                    <a style="font-size: 22px;" href="/wp-admin/themes.php?page=pt-one-click-demo-import">IMPORT DEMO CONTENT</a><br>
                    After that, you can completely remove "One Click Demo Import" plugin.</p>
                    <p><a href="%1$s" class="button button-secondary dismiss">%2$s</a></p>
                </div>',
        esc_url( wp_nonce_url( add_query_arg( 'std-hide-notice', 'telemetry' ) ) ),
        'Close'
    );
};
add_action('admin_notices', 'demo_import_notice');

function demo_import_notice_dismissed() {
    // Check if this is the request we want.
    if ( isset( $_GET['_wpnonce'] ) && isset( $_GET['std-hide-notice'] ) ) {
        if ( 'telemetry' === sanitize_text_field( wp_unslash( $_GET['std-hide-notice'] ) ) ) {
            // Check the wp-nonce.
            if ( wp_verify_nonce( sanitize_text_field( wp_unslash( $_GET['_wpnonce'] ) ) ) ) {
                // All good, we can save the option to dismiss this notice.
                update_option( 'str_dismiss_notice', true, false );
            }
        }
    }
}
add_action( 'admin_init', 'demo_import_notice_dismissed' );

// Clear data after plugin has been deactivated
$file = WP_PLUGIN_DIR . '/one-click-demo-import/one-click-demo-import.php';
function demo_import_deactivate(){
	delete_option('str_dismiss_notice');
}
register_deactivation_hook( $file, 'demo_import_deactivate' );

// Prevent .htaccess update
function ocdi_plugin_active() {
	if ( is_plugin_active( 'one-click-demo-import/one-click-demo-import.php' ) )
		add_filter('flush_rewrite_rules_hard','__return_false');
}
add_action( 'admin_init', 'ocdi_plugin_active' );