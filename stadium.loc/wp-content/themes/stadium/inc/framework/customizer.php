<?php

# Update Kirki config

# Remove some basic sections
function stadium_customize_register( $wp_customize ) {
	$wp_customize->remove_section( 'title_tagline' );
}
add_action( 'customize_register', 'stadium_customize_register' );

# Add specific JS/CSS files for Customizer Preview
function unyson_stadium_customize_preview_js() {
	wp_enqueue_script( 'unyson-stadium-kirki-customizer', get_template_directory_uri() . '/assets/js/kirki-customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'unyson_stadium_customize_preview_js' );

function unyson_stadium_customize_preview_styles() {
	wp_enqueue_style( 'stadium-customizer-preview-style', get_template_directory_uri() . '/assets/css/customizer-preview.css', array() );
}
add_action( 'customize_preview_init', 'unyson_stadium_customize_preview_styles' );

function get_rev_slides() {
	global $wpdb;
	$rev_slides = $wpdb->get_results( "SELECT * FROM wp_revslider_sliders WHERE type=''" );
	$select_arr = [];
	foreach ( $rev_slides as $rev_slide ) {
		$select_arr += [$rev_slide->alias => $rev_slide->title];
	}
	return $select_arr;
}

Kirki::add_config( 'stadium_customize', array(
	'capability'  => 'edit_theme_options',
	'option_type' => 'theme_mod',
) );

$first_level  = 0;
$second_level = 0;

# General
Kirki::add_section( 'section_general', array(
	'title'    => esc_html__( 'General Options', 'stadium' ),
	'priority' => $first_level ++,
	'icon'     => 'dashicons-admin-generic',
) );

require_once STADIUM_REQUIRE_DIRECTORY . 'inc/framework/setup/section-general.php';

# Header
Kirki::add_section( 'section_header_general', array(
	'title'    => esc_html__( 'Header Options', 'stadium' ),
	'priority' => $first_level ++,
	'icon'     => 'dashicons-arrow-up-alt',
) );

require_once STADIUM_REQUIRE_DIRECTORY . 'inc/framework/setup/section-header.php';

# Footer
Kirki::add_section( 'section_footer_general', array(
	'title'    => esc_html__( 'Footer Options', 'stadium' ),
	'priority' => $first_level ++,
	'icon'     => 'dashicons-arrow-down-alt',
) );

require_once STADIUM_REQUIRE_DIRECTORY . 'inc/framework/setup/section-footer.php';

# Single Post
Kirki::add_section( 'section_single_post_general', array(
	'title'    => esc_html__( 'Single Post', 'stadium' ),
	'priority' => $first_level ++,
	'icon'     => 'dashicons-text-page',
) );

require_once STADIUM_REQUIRE_DIRECTORY . 'inc/framework/setup/section-single-post.php';