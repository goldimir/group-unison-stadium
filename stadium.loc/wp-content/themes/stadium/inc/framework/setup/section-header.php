<?php

# Header

$priority = 0;

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-header' . $priority++,
    'section' => 'section_header_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Logo & Company Name', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'show_logo',
    'section' => 'section_header_general',
    'label' => esc_html__( 'Logo', 'stadium' ),
    'description' => esc_html__( 'Switch "Show" if you want to show Logo.', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'image',
    'settings' => 'header_default_logo',
    'section' => 'section_header_general',
    'label' => esc_html__( 'Logo', 'stadium' ),
    'description' => esc_html__( 'Choose a logo image to display for header.', 'stadium' ),
    'priority' => $priority++,
    'default' => '',
    'transport' => 'auto',
    'partial_refresh' => [
        'header_site_title_logo' => [
            'selector'        => '.logo',
            'render_callback' => function() {
                if ( get_theme_mod( 'header_default_logo' ) ) {
                    return '<img src="' . get_theme_mod( 'header_default_logo' ) . '" alt="' . bloginfo( 'name' ) . '">';
                }else{
                    return '<h2 class="site-title">' . get_theme_mod( 'company_name' ) . '</h2>';
                }
            },
        ],
    ],
    'active_callback' => array(
        array(
            'setting' => 'show_logo',
            'operator' => '==',
            'value' => '1'
        )
    )
) );

Kirki::add_field( 'stadium_customize', [
    'type' => 'text',
    'settings' => 'company_name',
    'label' => esc_html__( 'Company Name', 'stadium' ),
    'description' => esc_html__( 'Remove Logo to show Company Name.', 'stadium' ),
    'section' => 'section_header_general',
    'default' => esc_html__( 'Stadium', 'stadium' ),
    'priority' => $priority++,
    'partial_refresh' => [
        'header_site_title' => [
            'selector'        => '.site-title',
            'render_callback' => function() {
                return get_theme_mod( 'company_name', 'Stadium' );
            },
        ],
    ],
    'active_callback' => array(
        array(
            'setting' => 'show_logo',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-header' . $priority++,
    'section' => 'section_header_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Sticky Header', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'sticky_header',
    'section' => 'section_header_general',
    'label' => esc_html__( 'Sticky Header', 'stadium' ),
    'description' => esc_html__( 'Switch "On" for sticky Header at the top.', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'On', 'stadium' ),
        'off' => esc_html__( 'Off', 'stadium' )
    ),
    'default' => 0
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-header' . $priority++,
    'section' => 'section_header_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Search', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'show_search',
    'section' => 'section_header_general',
    'label' => esc_html__( 'Search', 'stadium' ),
    'description' => esc_html__( 'Switch "Show" if you want to show Search.', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1
) );