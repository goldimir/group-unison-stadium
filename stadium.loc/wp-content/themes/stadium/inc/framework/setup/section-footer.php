<?php

# Header

$priority = 0;

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-footer' . $priority++,
    'section' => 'section_footer_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Company Info', 'stadium' ).'</div>',
    'priority' => $priority++,
    'partial_refresh' => [
        'cmp_info' => [
            'selector'        => '.footer-address',
            'render_callback' => function() {
                return;
            },
        ],
    ],
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'show_cmp_info',
    'section' => 'section_footer_general',
    'label' => esc_html__( 'Show Company Info Section', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1
) );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'text',
    'settings' => 'company_address',
    'label'    => esc_html__( 'Address', 'stadium' ),
    'section'  => 'section_footer_general',
    'priority' => $priority++,
    'transport' => 'postMessage',
    'active_callback' => array(
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'text',
    'settings' => 'company_email',
    'label'    => esc_html__( 'Email', 'stadium' ),
    'section'  => 'section_footer_general',
    'priority' => $priority++,
    'transport' => 'postMessage',
    'active_callback' => array(
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'text',
    'settings' => 'company_phone',
    'label'    => esc_html__( 'Phone', 'stadium' ),
    'section'  => 'section_footer_general',
    'priority' => $priority++,
    'transport' => 'postMessage',
    'active_callback' => array(
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'text',
    'settings' => 'copy',
    'label'    => esc_html__( 'Copyright', 'stadium' ),
    'section'  => 'section_footer_general',
    'priority' => $priority++,
    'partial_refresh' => [
        'copy' => [
            'selector'        => '.copy',
            'render_callback' => function() {
                return get_theme_mod( 'copy', 'Stadium' );
            },
        ],
    ],
] );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'toggle',
    'settings' => 'add_f_btn',
    'section' => 'section_footer_general',
    'label' => esc_html__( 'Add Button', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1,
    'active_callback' => array(
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
) );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'text',
    'settings' => 'f_btn_name',
    'label'    => esc_html__( 'Button Name', 'stadium' ),
    'section'  => 'section_footer_general',
    'default' => 'Contact Us',
    'priority' => $priority++,
    'transport' => 'postMessage',
    'active_callback' => array(
        array(
            'setting' => 'add_f_btn',
            'operator' => '==',
            'value' => '1'
        ),
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'link',
    'settings' => 'f_btn_link',
    'label'    => __( 'Link', 'stadium' ),
    'section'  => 'section_footer_general',
    'priority' => $priority++,
    'active_callback' => array(
        array(
            'setting' => 'add_f_btn',
            'operator' => '==',
            'value' => '1'
        ),
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', [
    'type'     => 'checkbox',
    'settings' => 'f_btn_link_nw',
    'label'    => __( 'Open in a New Window', 'stadium' ),
    'section'  => 'section_footer_general',
    'default'  => true,
    'priority' => $priority++,
    'active_callback' => array(
        array(
            'setting' => 'add_f_btn',
            'operator' => '==',
            'value' => '1'
        ),
        array(
            'setting' => 'show_cmp_info',
            'operator' => '==',
            'value' => '1'
        )
    )
] );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-footer' . $priority++,
    'section' => 'section_footer_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Footer Widgets', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
	'type' => 'select',
	'settings' => 'footer_columns',
	'section' => 'section_footer_general',
	'label' => esc_html__( 'Widget Columns', 'stadium' ),
	'description' => esc_html__( 'Select number of columns.', 'stadium' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		1 => esc_html__( 'One Column', 'stadium' ),
		2 => esc_html__( 'Two Columns', 'stadium' ),
		3 => esc_html__( 'Three Columns', 'stadium' ),
		4 => esc_html__( 'Four Columns', 'stadium' ),
	),
	'default' => 4
) );