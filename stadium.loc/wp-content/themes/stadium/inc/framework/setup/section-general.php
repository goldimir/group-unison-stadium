<?php

# General

$priority = 0;

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-general' . $priority++,
    'section' => 'section_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Site Layout', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type'        => 'switch',
    'settings'    => 'switch_bullet_setting',
    'label'       => __( 'Theme Bullet List', 'kirki' ),
    'description' => esc_html__( 'Use theme bullet for all lists on site', 'kirki' ),
    'section'     => 'section_general',
    'default'     => 1,
    'transport'   => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Yes', 'stadium' ),
        'off' => esc_html__( 'No', 'stadium' )
    ),
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'select',
    'settings' => 'header_container_width',
    'section' => 'section_general',
    'label'       => esc_html__( 'Header Width', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        '-fluid' => esc_html__( 'Full Width', 'stadium' ),
        '' => esc_html__( 'Box', 'stadium' ),
    ),
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'select',
    'settings' => 'content_container_width',
    'section' => 'section_general',
    'label'       => esc_html__( 'Content Width', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        '-fluid' => esc_html__( 'Full Width', 'stadium' ),
        '' => esc_html__( 'Box', 'stadium' ),
    ),
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'select',
    'settings' => 'footer_container_width',
    'section' => 'section_general',
    'label'       => esc_html__( 'Footer Width', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        '-fluid' => esc_html__( 'Full Width', 'stadium' ),
        '' => esc_html__( 'Box', 'stadium' ),
    ),
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-general' . $priority++,
    'section' => 'section_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Additional', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

if ( class_exists( 'RevSlider' ) ) {

	Kirki::add_field( 'stadium_customize', array(
		'type'        => 'switch',
		'settings'    => 'show_rev_slider',
		'section'     => 'section_general',
		'label'       => esc_html__( 'Slider Revolution', 'stadium' ),
		'description' => esc_html__( 'Show Slider Revolution on all pages.', 'stadium' ),
		'priority'    => $priority ++,
		'transport'   => 'auto',
		'choices'     => array(
			'on'  => esc_html__( 'Show', 'stadium' ),
			'off' => esc_html__( 'Hide', 'stadium' )
		),
		'default'     => 1
	) );

	Kirki::add_field( 'stadium_customize', array(
		'type'            => 'select',
		'settings'        => 'slider_revolution_setting',
		'section'         => 'section_general',
		'label'           => esc_html__( 'Choose Slider', 'stadium' ),
		'priority'        => $priority ++,
		'transport'       => 'auto',
		'placeholder' => esc_html__( 'Select an slider', 'stadium' ),
		'choices'         => get_rev_slides(),
		'active_callback' => [
			[
				'setting'  => 'show_rev_slider',
				'operator' => '==',
				'value'    => '1'
			]
		]
	) );

}

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'show_back_to_top',
    'section' => 'section_general',
    'label' => esc_html__( 'Back to Top Button', 'stadium' ),
    'description' => esc_html__( 'Switch "Show" if you want to show "Back to Top" button.', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'show_preloader',
    'section' => 'section_general',
    'label' => esc_html__( 'Show Preloader', 'stadium' ),
    'description' => esc_html__( 'Switch "Show" if you want to show preloader.', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 0
) );
