<?php

# Header

$priority = 0;

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-single-post' . $priority++,
    'section' => 'section_single_post_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Single Post Navigation', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'select',
    'settings' => 'comment_nav',
    'section' => 'section_single_post_general',
    'label'       => esc_html__( 'Comments Navigation', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'default'     => 'load-more-nav',
    'choices' => array(
        'none' => esc_html__( 'None', 'stadium' ),
        'load-more-nav' => esc_html__( 'Load More', 'stadium' ),
        'paged-nav' => esc_html__( 'Paged', 'stadium' ),
    ),
    'active_callback' => function() {
        $none = get_theme_mod( 'comment_nav', 'none' );
        $load_more_nav = get_theme_mod( 'comment_nav', 'load-more-nav' );
        $paged_nav     = get_theme_mod( 'comment_nav', 'paged-nav' );
        ( 'none' == $none ) ? update_option( 'page_comments', 0 ) : update_option( 'page_comments', 1 );
        ( 'load-more-nav' == $load_more_nav ) ? update_option( 'comment_order', 'desc' ) : '';
        ( 'paged-nav' == $paged_nav ) ? update_option( 'comment_order', 'asc' ) : '';
    },
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'number',
    'settings' => 'comment_count',
    'section' => 'section_single_post_general',
    'label'       => esc_html__( 'Count of comments', 'stadium' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'default'     => 20,
    'choices' => array(
        'min'  => 1,
        'max'  => PHP_INT_MAX,
        'step' => 1,
    ),
    'active_callback' => function() {
        $none = get_theme_mod( 'comment_nav', 'none' );
        if ( 'none' != $none ) {
            update_option( 'comments_per_page', get_theme_mod( 'comment_count' ) );
            return true;
        }
    },
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'custom',
    'settings' => 'sep-single-post' . $priority++,
    'section' => 'section_single_post_general',
    'default' => '<div class="kirki-separator">'.esc_html__( 'Share Buttons', 'stadium' ).'</div>',
    'priority' => $priority++,
) );

Kirki::add_field( 'stadium_customize', array(
    'type' => 'switch',
    'settings' => 'share-btn',
    'section' => 'section_single_post_general',
    // 'label'       => esc_html__( 'Share Buttons', 'stadium' ),
    'description' => 'Show buttons to share the post<br> (Facebook, Twitter etc.)',
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Show', 'stadium' ),
        'off' => esc_html__( 'Hide', 'stadium' )
    ),
    'default' => 1
) );
