<?php

# Theme Bullet List
function stadium_bullet_list() {

    if ( get_theme_mod( 'switch_bullet_setting' ) ) {
        echo '<style>.entry-content > ul,.wpb_wrapper > ul{padding-left: 0;}.entry-content > ul li,.wpb_wrapper > ul li{overflow: hidden;}.entry-content > ul li:before,.wpb_wrapper > ul li:before{content:"\f054";font-size: 12px;font-family:"Font Awesome 5 Free";float: left;margin-right: 7px;color: #c5161d;}</style>';
    }

}
add_action( 'wp_head', 'stadium_bullet_list' );

# Preloader
function stadium_preloader() {

    if ( get_theme_mod( 'show_preloader', false ) ) {
        echo '<div class="site-preloader"><div class="loader"><div class="loader__bar"></div><div class="loader__bar"></div><div class="loader__bar"></div><div class="loader__bar"></div><div class="loader__bar"></div><div class="loader__ball"></div></div></div>';
    }

}
add_action( 'stadium/before_site', 'stadium_preloader' );

# Back to top
function stadium_site_backtotop() {

    if ( get_theme_mod( 'show_back_to_top', true ) ) {
        echo '<a href="#" class="back-to-top hidden"><i class="fas fa-chevron-up"></i></a>';
    }

}
add_action( 'stadium/after_site', 'stadium_site_backtotop' );

# Search
function stadium_search() {

    if ( get_theme_mod( 'show_search', true ) ) {
        echo '<div class="search">
                <input type="search" class="search-box" placeholder="Search..." />
                <span class="search-button">
                    <span class="search-icon"></span>
                </span>
                <ul class="codyshop-ajax-search"></ul>
            </div>';
    }

}
add_action( 'stadium/top_of_header', 'stadium_search' );

# Slider Revolution
function global_slider_revolution() {
	if ( class_exists( 'RevSlider' ) && get_theme_mod( 'show_rev_slider' ) ) {
		echo '<div class="wr-rev-slider">';
		add_revslider( get_theme_mod( 'slider_revolution_setting' ) );
		echo '</div>';
	}
}
add_action( 'stadium/under_header', 'global_slider_revolution' );