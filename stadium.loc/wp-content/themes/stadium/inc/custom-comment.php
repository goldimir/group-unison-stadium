<?php

# AJAX Button Load More comments
add_action('wp_ajax_cloadmore', 'comments_loadmore_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_cloadmore', 'comments_loadmore_handler'); // wp_ajax_nopriv_{action}
function comments_loadmore_handler(){
    global $post;
    $post = get_post( $_POST['post_id'] );
    setup_postdata( $post );
    
    // actually we must copy the params from wp_list_comments() used in our theme
    wp_list_comments( array(
        'style'      => 'ol',
        'short_ping' => true,
        'page' => $_POST['cpage'], // current comment page
        'per_page' => get_option('comments_per_page'),
    ) );
    die;
}

# AJAX Comment form submit
if ( ! function_exists( 'submit_ajax_comment' ) ) {

    add_action( 'wp_ajax_ajaxcomments', 'submit_ajax_comment' ); // wp_ajax_{action} for registered user
    add_action( 'wp_ajax_nopriv_ajaxcomments', 'submit_ajax_comment' ); // wp_ajax_nopriv_{action} for not registered users
    function submit_ajax_comment(){

        $comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
        if ( is_wp_error( $comment ) ) {
            $error_data = intval( $comment->get_error_data() );
            if ( ! empty( $error_data ) ) {
                wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure' ), array( 'response' => $error_data, 'back_link' => true ) );
            } else {
                wp_die( 'Unknown error' );
            }
        }
     
        // Set Cookies
        $user = wp_get_current_user();
        do_action('set_comment_cookies', $comment, $user);
     
        $comment_depth = 1;
        $comment_parent = $comment->comment_parent;
        while( $comment_parent ){
            $comment_depth++;
            $parent_comment = get_comment( $comment_parent );
            $comment_parent = $parent_comment->comment_parent;
        }
     
        $GLOBALS['comment'] = $comment;
        $GLOBALS['comment_depth'] = $comment_depth;
     
        $comment_html = '<li ' . comment_class('', null, null, false ) . ' id="comment-' . get_comment_ID() . '">
            <article class="comment-body" id="div-comment-' . get_comment_ID() . '">
                <footer class="comment-meta">
                    <div class="comment-author vcard">
                        ' . get_avatar( $comment, 100 ) . '
                        <b class="fn">' . get_comment_author_link() . '</b> <span class="says">says:</span>
                    </div>
                    <div class="comment-metadata">
                        <a href="' . esc_url( get_comment_link( $comment->comment_ID ) ) . '">' . sprintf('%1$s at %2$s', get_comment_date(),  get_comment_time() ) . '</a>';
     
                        if( $edit_link = get_edit_comment_link() )
                            $comment_html .= '<span class="edit-link"><a class="comment-edit-link" href="' . $edit_link . '">Edit</a></span>';
     
                    $comment_html .= '</div>';
                    if ( $comment->comment_approved == '0' )
                        $comment_html .= '<p class="comment-awaiting-moderation">Your comment is awaiting moderation.</p>';

                $phone_html = (get_comment_meta( get_comment_ID(), 'phone', true )) ? "<strong>Phone:</strong> " . apply_filters( 'comment_phone', get_comment_meta( get_comment_ID(), 'phone', true ), $comment ) : "";

                $comment_html .= '</footer>
                <div class="comment-content">'. $phone_html . apply_filters( 'comment_text', get_comment_text( $comment ), $comment ) . '</div>
            </article>
        </li>';
        echo $comment_html;
        die();
    }
}

// Move Comment field to bottom
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
function wpb_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_action( 'comment_post', 'save_extend_comment_meta_data' );
function save_extend_comment_meta_data( $comment_id ){
    if( !empty( $_POST['phone'] ) ){
        $phone = sanitize_text_field($_POST['phone']);
        add_comment_meta( $comment_id, 'phone', $phone );
    }
}

// add Metabox for custom fields
add_action( 'add_meta_boxes_comment', 'extend_comment_add_meta_box' );
function extend_comment_add_meta_box(){
    add_meta_box( 'title', __( 'Additional Information' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
}

function extend_comment_meta_box( $comment ){
    $phone  = get_comment_meta( $comment->comment_ID, 'phone', true );

    wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
    ?>
    <p>
        <label for="phone"><?php _e( 'Phone' ); ?></label>
        <input type="text" name="phone" value="<?php echo esc_attr( $phone ); ?>" class="widefat" />
    </p>
    <?php
}

add_action( 'edit_comment', 'extend_comment_edit_meta_data' );
function extend_comment_edit_meta_data( $comment_id ) {
    if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) )
    return;

    if( !empty($_POST['phone']) ){
        $phone = sanitize_text_field($_POST['phone']);
        update_comment_meta( $comment_id, 'phone', $phone );
    }
    else{
        delete_comment_meta( $comment_id, 'phone');
    }
}