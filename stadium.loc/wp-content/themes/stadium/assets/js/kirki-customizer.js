/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

    // Site title and description.
    wp.customize( 'company_name', function( value ) {
        value.bind( function( to ) {
            $( '.site-title' ).text( to );
        } );
    } );

    // Site Footer section.
    wp.customize( 'company_address', function( value ) {
        value.bind( function( to ) {
            $( '.footer-address .company_address' ).text( to );
        } );
    } );
	wp.customize( 'company_email', function( value ) {
	        value.bind( function( to ) {
	            $( '.footer-address .company_email' ).text( to );
	        } );
	    } );
	wp.customize( 'company_phone', function( value ) {
	        value.bind( function( to ) {
	            $( '.footer-address .company_phone' ).text( to );
	        } );
	    } );
	wp.customize( 'copy', function( value ) {
	        value.bind( function( to ) {
	            $( '.copy' ).text( to );
	        } );
	    } );
	wp.customize( 'f_btn_name', function( value ) {
	        value.bind( function( to ) {
	            $( '.foot-btn' ).text( to );
	        } );
	    } );

} )( jQuery );
