document.addEventListener('touchstart', function () {
}, false);

jQuery(document).ready(function ($) {

    /**
     * Main Menu
     *
     * Handles toggling the navigation menu for small screens and enables TAB key
     * navigation support for dropdown menus.
     */

    $('.skydropdown').append($('<a class="skydropdown-anim-arrw"><span></span><span></span><span></span><span></span><span></span><span></span></a>'));

    $('.skydropdown').append($('<div class="skydropdown-text">Navigation</div>'));

    $('.skydropdown-list > li').has('.skydropdown-submenu').prepend('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

    $('.skydropdown-submenu > li').has('ul').prepend('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

    $('.skydropdown-submenu-sub > li').has('ul').prepend('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

    $('.skydropdown-submenu-sub-sub > li').has('ul').prepend('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

    $('.skydropdown-list li').has('.menu-big').prepend('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');

    $('.skydropdown-anim-arrw').click(function () {
        $('.skydropdown-list').slideToggle('fast');
        $(this).toggleClass('skydropdown-lines')
    });

    $('.skydropdown-click').click(function () {
        $(this).toggleClass('skydropdownarrow-rotate').parent().siblings().children().removeClass('skydropdownarrow-rotate');
        $('.skydropdown-submenu, .menu-big').not($(this).siblings('.skydropdown-submenu, .menu-big')).slideUp('fast');
        $(this).siblings('.skydropdown-submenu').slideToggle('fast');
        $(this).siblings('.menu-big').slideToggle('fast')
    });

    $('.skydropdown-clk-two').click(function () {
        $(this).toggleClass('skydropdownarrow-rotate').parent().siblings().children().removeClass('skydropdownarrow-rotate');
        $(this).siblings('.skydropdown-submenu').slideToggle('fast');
        $(this).siblings('.skydropdown-submenu-sub').slideToggle('fast');
        $(this).siblings('.skydropdown-submenu-sub-sub').slideToggle('fast')
    });

    if ($(window).width() > 767) {
        $('.skydropdown-list .skydropdown-submenu li.menu-item-has-children').mouseover(function () {
            $(this).addClass('permahover');
        });

        $('.skydropdown-list .skydropdown-submenu li.menu-item-has-children').mouseout(function () {
            $(this).removeClass('permahover');
        });
    }

// This is just for the case that the browser window is resized
    window.onresize = function () {
        if ($(window).width() > 767) {
            $('.skydropdown-submenu').removeAttr('style');
            $('.skydropdown-list').removeAttr('style');
        }
    }

    /**
     * Search
     */
    let searchBtn = $('.search');
    let searchVal = $('.codyshop-ajax-search');
    $('html').on('click', function () {
        searchBtn.removeClass('open');
    });

    searchBtn.on('click', function (event) {
        event.stopPropagation();
    });

    searchBtn.on('click', '.search-button', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
        $(this).prev().trigger('focus');
    });

    $('.search-box').on('keyup', function (eventObject) {
        let searchTerm = $(this).val();
        if (searchTerm.length > 1) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'fly_ajax_search',
                    'term': searchTerm
                },
                success: function (result) {
                    searchVal.fadeIn(300).html(result);
                }
            });
        } else {
            searchVal.fadeOut(300, function () {
                searchVal.html('');
            });
        }
    });

    /**
     * Sticky Header
     */
    $('.header-sticky').sticky({
        topSpacing:0
    });

    // let revSlider = $( '#sticky-wrapper header' ).hasClass('rev-slider');
    // if ( revSlider === true ) {
    //     $( '#sticky-wrapper' ).addClass('rev-slider');
    // }

    /**
     * Preloader
     */
    setTimeout(function () {
        $('.site-preloader').fadeOut(500);
    }, 1000);

    /**
     * Back to top
     */
    let $window = $(window),
        offset = 300,
        lastScrollTop = 0;

    $window.on('scroll', function () {
        let st = $(this).scrollTop();
        if ($window.scrollTop() > offset) {
            if (st > lastScrollTop) {
                hide_btn();
            } else {
                show_btn();
            }
            lastScrollTop = st;
        } else {
            hide_btn();
        }
    }).trigger('scroll');

    function show_btn() {
        $('.back-to-top').removeClass('hidden').addClass('visible');
    }

    function hide_btn() {
        $('.back-to-top').removeClass('visible').addClass('hidden');
    }

    $(document).on('click', '.back-to-top', function (e) {
        e.preventDefault();
        $('html').scrollTo(0, 500);
    });

    /*
     * AJAX Single Post Comment Validation
     */
    jQuery.extend(jQuery.fn, {
        /*
         * check if field value lenth more than 3 symbols ( for name and comment )
         */
        validate_comment_form: function (data) {

            let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                emailToValidate = $(this).val();

            let phoneReg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                phoneToValidate = $(this).val();

            if ($(this).val().length < 3 && data === 'min-symbol') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Please type something</div>').find('div').addClass('error').hide().fadeIn(600);
            } else if ((!emailReg.test(emailToValidate) || emailToValidate === "") && data === 'email') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Type valid Email address</div>').find('div').addClass('error').hide().fadeIn(600);
            } else if (!phoneReg.test(phoneToValidate) && phoneToValidate && data === 'phone') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Type valid number of phone</div>').find('div').addClass('error').hide().fadeIn(600);
            } else {
                $(this).removeClass('error');
                $(this).removeClass('valid');
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('valid');
                $(this).parent().append('<div>Valid</div>').find('div').addClass('valid').hide().fadeIn(600);
                return true;
            }
        },
    });

    /*
     * On comment form submit
     */
    // define some vars
    let commentform = $('#commentform'),
        button = $('#submit'), // submit button
        respond = $('#respond'), // comment form container
        commentlist = $('.comment-list'), // comment list container
        cancelreplylink = $('#cancel-comment-reply-link'),
        author = $('#author'),
        email = $('#email'),
        phone = $('#phone'),
        comment = $('#comment');

    //Valid on Blur event
    // commentform.on('blur', '#author', function () {
    //     $(this).validate_comment_form('min-symbol');
    // });
    // commentform.on('blur', '#email', function () {
    //     $(this).validate_comment_form('email');
    // });
    // commentform.on('blur', '#phone', function () {
    //     $(this).validate_comment_form('phone');
    // });
    // commentform.on('blur', '#comment', function () {
    //     $(this).validate_comment_form('min-symbol');
    // });

    commentform.on('submit', function () {

        if ( author.length )
            author.validate_comment_form('min-symbol');
        if ( email.length )
            email.validate_comment_form('email');
        if ( phone.length )
            phone.validate_comment_form('phone');
        if ( comment.length )
            comment.validate_comment_form('min-symbol');

        // if comment form isn't in process, submit it
        if (!button.hasClass('loadingform') && !author.hasClass('error') && !email.hasClass('error') && !phone.hasClass('error') && !comment.hasClass('error')) {

            // ajax request
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: $(this).serialize() + '&action=ajaxcomments', // send form data + action parameter
                beforeSend: function (xhr) {
                    // what to do just after the form has been submitted
                    button.addClass('loadingform').val('Loading...');
                },
                error: function (request, status, error) {
                    if (status === 500) {
                        alert('Error while adding comment');
                    } else if (status === 'timeout') {
                        alert('Error: Server doesn\'t respond.');
                    } else {
                        // process WordPress errors
                        let wpErrorHtml = request.responseText.split("<p>"),
                            wpErrorStr = wpErrorHtml[1].split("</p>");

                        alert(wpErrorStr[0]);
                    }
                },
                success: function (addedCommentHTML) {

                    // if this post already has comments
                    if (commentlist.length > 0) {

                        // if in reply to another comment
                        if (respond.parent().hasClass('comment')) {

                            // if the other replies exist
                            if (respond.parent().children('.children').length) {
                                respond.parent().children('.children').append(addedCommentHTML);
                            } else {
                                // if no replies, add <ol class="children">
                                addedCommentHTML = '<ol class="children">' + addedCommentHTML + '</ol>';
                                respond.parent().append(addedCommentHTML);
                            }
                            // close respond form
                            cancelreplylink.trigger("click");
                        } else {
                            // simple comment
                            commentlist.append(addedCommentHTML);
                        }
                    } else {
                        // if no comments yet
                        addedCommentHTML = '<ol class="comment-list">' + addedCommentHTML + '</ol>';
                        respond.before($(addedCommentHTML));
                    }
                    // clear textarea field
                    $('#comment').val('');
                },
                complete: function () {
                    // what to do after a comment has been added
                    button.removeClass('loadingform').val('Post Comment');
                    commentform.closest('form').find("input[type=text], textarea").val("");
                }
            });
        }
        return false;
    });

    // load more comments button click event
    $('.comment_loadmore').on('click', function () {
        if (typeof load_more_btn === 'undefined') {
            return;
        }
        let noMore = load_more_btn.noMore,
            text = load_more_btn.text,
            loading = load_more_btn.loading;

        let button = $(this);

        // decrease the current comment page value
        cpage--;

        if (!button.hasClass('disabled')) {
            button.addClass('loading');
            button.find('span').text(loading);
        }

        $.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {
                'action': 'cloadmore', // wp_ajax_cloadmore
                'post_id': parent_post_id, // the current post
                'cpage': cpage, // current comment page
            },
            beforeSend: function (xhr) {
                button.find('span').text(loading); // preloader here
            },
            success: function (data) {
                if (data) {
                    $('ol.comment-list').append(data);
                    button.removeClass('loading');
                    button.find('span').text(text);
                    // if the last page, disable the button
                    if (cpage === 1)
                        button.addClass('disabled').find('span').text(noMore);
                } else {
                    button.find('span').text(noMore);
                    button.addClass('disabled');
                }
            }
        });
        return false;
    });

});


/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
    let isIe = /(trident|msie)/i.test(navigator.userAgent);

    if (isIe && document.getElementById && window.addEventListener) {
        window.addEventListener('hashchange', function () {
            let id = location.hash.substring(1),
                element;

            if (!(/^[A-z0-9_-]+$/.test(id))) {
                return;
            }

            element = document.getElementById(id);

            if (element) {
                if (!(/^(?:a|select|input|button|textarea)$/i.test(element.tagName))) {
                    element.tabIndex = -1;
                }

                element.focus();
            }
        }, false);
    }
})();