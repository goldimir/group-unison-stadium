<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stadium
 */

$footer_columns = get_theme_mod( 'footer_columns', 4 );
switch ( $footer_columns ) {
	case 1:
		$column_class = 'col-md-12';
		break;
	case 2:
		$column_class = 'col-md-6';
		break;
	case 3:
		$column_class = 'col-md-4';
		break;
	case 4:
		$column_class = 'col-lg-3 col-md-6 col-sm-6';
		break;
}
?>

    </div><!-- #content -->

    <footer class="footer">
        <div class="container">
            <div class="footer-widgets">
                <div class="row">
				    <?php
				    for ( $i = 1; $i < $footer_columns + 1; $i++ ) {
					    if ( is_active_sidebar( 'footer_sidebar_' . $i ) ) {
						    echo '<div class="wdg '.std_sanitize_class( $column_class ).'">';
						    dynamic_sidebar( 'footer_sidebar_' . $i );
						    echo '</div>';
					    }
				    }
				    ?>
                </div>
            </div>
        </div>
        <div class="container<?php echo get_theme_mod( 'footer_container_width' ); ?>">
            <div class="row">
                <div class="col-sm-12">
                    <?php if ( has_nav_menu( 'footer-top-menu' ) ): ?>
                        <nav class="footer-nav">
                            <?php wp_nav_menu(array( 'theme_location' => 'footer-top-menu' )); ?>
                        </nav>
                    <?php endif ?>

                    <?php if ( true == get_theme_mod( 'show_cmp_info' ) ): ?>
                        <div class="footer-address">
                            <?php if ( true == get_theme_mod( 'add_f_btn' ) ): ?>
                                <a class="foot-btn" <?php echo ( true == get_theme_mod( 'f_btn_link_nw' ) ) ? "target='_blank'" : '' ;?> href="<?php echo get_theme_mod( 'f_btn_link' ) ?>"><?php echo get_theme_mod( 'f_btn_name', 'Contact Us' ) ?></a>
                            <?php endif ?>
                            <p class="company_address"><?php echo get_theme_mod( 'company_address' ) ?></p>
                            <p><a class="company_email" href="mailto:<?php echo get_theme_mod( 'company_email' ) ?>"><?php echo get_theme_mod( 'company_email' ) ?></a></p>
                            <p><a class="company_phone" href="tel:+<?php echo get_theme_mod( 'company_phone' ) ?>"><?php echo get_theme_mod( 'company_phone' ) ?></a></p>
                        </div>
                    <?php endif ?>

                    <div class="row footer-bottom-menu">
                        <?php if ( has_nav_menu( 'footer-bot-menu' ) ): ?>
                            <div class="col-sm-4">
                                <p class="copy"><?php echo get_theme_mod( 'copy' ) ?></p>
                            </div>
                            <div class="col-sm-8">
                                <nav class="footer-bottom-nav">
                                    <?php wp_nav_menu(array( 'theme_location' => 'footer-bot-menu' )); ?>
                                </nav>
                            </div>
                        <?php else: ?>
                            <div class="col-sm-12 center">
                                <p class="copy"><?php echo get_theme_mod( 'copy' ) ?></p>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<?php do_action( 'stadium/after_site' ); ?>

<?php wp_footer(); ?>

</body>
</html>
