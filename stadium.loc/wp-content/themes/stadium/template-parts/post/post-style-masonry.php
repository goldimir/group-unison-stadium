<?php

/**
 *
 * @version: 1.0
 */

$size = 'stadium-masonry';

?>

<article <?php post_class( 'vlt-post vlt-post--style-masonry' ); ?> id="post-<?php the_ID(); ?>">


	<?php if ( has_post_thumbnail() ) : ?>

		<div class="vlt-post-thumbnail clearfix">
			<?php echo stadium_get_post_thumbnail( $size ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'thumbnail-link' ); ?>
		</div>
		<!-- /.vlt-post-thumbnail -->

	<?php endif; ?>

	<div class="vlt-post-content">

		<header class="vlt-post-header">

			<?php get_template_part( 'template-parts/post/particles/particle', 'post-title' ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'post-meta-small' ); ?>

		</header>
		<!-- /.vlt-post-header -->

		<div class="vlt-post-excerpt">
			<?php echo stadium_limit_text( get_the_content(), 23 ); ?>
		</div>
		<!-- /.vlt-post-excerpt -->

	<?php if ( $read_more_btn ) : ?>

		<footer class="vlt-post-footer">
			<?php get_template_part( 'template-parts/post/particles/particle', 'post-read-more' ); ?>
		</footer>
		<!-- /.vlt-post-footer -->

	<?php endif; ?>

	</div>
	<!-- /.vlt-post-content -->

</article>
<!-- /.vlt-post -->