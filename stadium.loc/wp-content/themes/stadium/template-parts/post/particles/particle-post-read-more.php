<?php

/**
 *
 * @version: 1.0
 */

?>

<a class="sky-main-btn post-btn" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Read More', 'stadium' ); ?></a>