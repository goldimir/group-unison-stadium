<?php

/**
 *
 * @version: 1.0
 */

?>

<div class="vlt-post-meta vlt-post-meta--small">
	<!-- <span><i class="icofont icofont-user-male"></i><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></span> -->
	<span><i class="far fa-clock"></i> <time datetime="<?php the_time( 'c' ); ?>"><?php echo get_the_date('M j, Y G:i'); ?></time></span>
</div>
<!-- /.vlt-post-meta -->