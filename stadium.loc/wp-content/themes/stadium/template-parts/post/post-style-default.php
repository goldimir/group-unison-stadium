<?php

/**
 *
 * @version: 1.0
 */

$size = 'stadium-standard';

?>

<article <?php post_class( 'vlt-post vlt-post--style-default' ); ?> id="post-<?php the_ID(); ?>">


	<?php if ( has_post_thumbnail() ) : ?>

		<div class="vlt-post-thumbnail clearfix">
			<?php echo stadium_get_post_thumbnail( $size ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'thumbnail-link' ); ?>
		</div>
		<!-- /.vlt-post-thumbnail -->

	<?php endif; ?>

	<div class="vlt-post-content">

		<header class="vlt-post-header">

			<?php get_template_part( 'template-parts/post/particles/particle', 'post-meta-large' ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'post-title' ); ?>

		</header>
		<!-- /.vlt-post-header -->

		<div class="vlt-post-excerpt">
			<?php echo stadium_limit_text( get_the_content(), 55 ); ?>
		</div>
		<!-- /.vlt-post-excerpt -->

	<?php
	if ( isset($read_more_btn) ) {
        if ( $read_more_btn === NULL ) {
            $read_more_btn = 1;
        }
        if ( $read_more_btn ) { ?>

            <footer class="vlt-post-footer">
                <?php get_template_part( 'template-parts/post/particles/particle', 'post-read-more' ); ?>
            </footer>
            <!-- /.vlt-post-footer -->

        <?php
        }
    }
	?>

	</div>
	<!-- /.vlt-post-content -->

</article>
<!-- /.vlt-post -->