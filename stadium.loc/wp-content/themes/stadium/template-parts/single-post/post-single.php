<?php

/**
 *
 * @version: 1.0
 */

?>
<!--post-single.php-->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php stadium_post_thumbnail(); ?>
    <?php get_template_part( 'template-parts/single-post/particles/particle-single-post', 'title' ); ?>
    <?php get_template_part( 'template-parts/single-post/particles/particle-single-post', 'meta' ); ?>

    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'stadium' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Edit <span class="screen-reader-text">%s</span>', 'stadium' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

<footer class="vlt-post-footer">
    <?php get_template_part( 'template-parts/single-post/particles/particle-single-post', 'footer' );  ?>
</footer>