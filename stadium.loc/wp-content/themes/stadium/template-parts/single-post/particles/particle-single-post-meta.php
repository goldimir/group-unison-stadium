<?php

/**
 *
 * @version: 1.0
 */

?>

<div class="date-post"><i class="far fa-clock"></i> <time><?php the_time('M j, Y G:i'); ?></time></div>