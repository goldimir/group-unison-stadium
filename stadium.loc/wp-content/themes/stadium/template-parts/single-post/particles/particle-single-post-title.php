<?php

/**
 *
 * @version: 1.0
 */

?>

<header class="entry-header">
	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header> <!-- .entry-header -->