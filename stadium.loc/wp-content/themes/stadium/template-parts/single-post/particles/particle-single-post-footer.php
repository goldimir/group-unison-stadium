<?php

/**
 *
 * @version: 1.0
 */

?>

<?php if ( true == get_theme_mod( 'share-btn' ) ): ?>
    <div class="share-buttons">
    	<?php echo stthemes_get_post_share_buttons( get_the_ID() ); ?>
    </div>
<?php endif ?>

<?php echo stadium_get_pagination( null, 'paged-posts' ); ?>