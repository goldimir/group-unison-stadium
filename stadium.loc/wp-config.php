<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stadium');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5x@}^!i|mFUJ$ylV|)4:b;WDVP|9BtZIJPMH):u0|%h`sf-+c:+}|fAgO2s$S;}K');
define('SECURE_AUTH_KEY',  'MB+nwWrvkRRQmG|J-=}EO?=>gr<NJe{z-% ~?WJRdI5{_[+[$U|4qGL2e+e3+It`');
define('LOGGED_IN_KEY',    ';Ok-oL{O<Fj;?^x?CS.|NV+|0_-rZcC$kv$hY0gP}d-_fcZV`<Cjcg$e%es*q2Xt');
define('NONCE_KEY',        'fF/, |h&)6fD7EPxeTod@duiN^haP^zi}AxJ|0=U!N+I([c]Q(^D ;/yG;=+0<S-');
define('AUTH_SALT',        '6lQvq!1_dQ3YA7kw+ 6RJ?~,^xFVZ`J{;,`XojJvpXY*`&&Vs^s.hF7<WK2v[qdJ');
define('SECURE_AUTH_SALT', '9UuC9ky]8K(RVQ$Tt-h~Yt.#wy<e]GCe++8Uh* |UZu`lX+,%zPohuNAM&BgR1Ya');
define('LOGGED_IN_SALT',   '[(+z+wR3#~PxUw+xAbp4+Y9.pPC;9[Hgy*P9xx*<4}l[lj-0aNy{yU3BuEh=N.nV');
define('NONCE_SALT',       'q[Gh`13(x`NV8FIM}{y-doZZ3z%BdDa]uYe80R!E5SP-.*g-O.-M=8=B[G4}Cbx~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
