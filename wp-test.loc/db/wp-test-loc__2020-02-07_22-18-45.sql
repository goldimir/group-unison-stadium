-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2020 at 10:18 PM
-- Server version: 5.7.20
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wp-test-loc`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-01-09 15:42:13', '2019-01-09 15:42:13', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_create_map`
--

CREATE TABLE `wp_create_map` (
  `map_id` int(11) NOT NULL,
  `map_title` varchar(255) DEFAULT NULL,
  `map_width` varchar(255) DEFAULT NULL,
  `map_height` varchar(255) DEFAULT NULL,
  `map_zoom_level` varchar(255) DEFAULT NULL,
  `map_type` varchar(255) DEFAULT NULL,
  `map_scrolling_wheel` varchar(255) DEFAULT NULL,
  `map_visual_refresh` varchar(255) DEFAULT NULL,
  `map_45imagery` varchar(255) DEFAULT NULL,
  `map_street_view_setting` text,
  `map_route_direction_setting` text,
  `map_all_control` text,
  `map_info_window_setting` text,
  `style_google_map` text,
  `map_locations` longtext,
  `map_layer_setting` text,
  `map_polygon_setting` longtext,
  `map_polyline_setting` longtext,
  `map_cluster_setting` text,
  `map_overlay_setting` text,
  `map_geotags` text,
  `map_infowindow_setting` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_create_map`
--

INSERT INTO `wp_create_map` (`map_id`, `map_title`, `map_width`, `map_height`, `map_zoom_level`, `map_type`, `map_scrolling_wheel`, `map_visual_refresh`, `map_45imagery`, `map_street_view_setting`, `map_route_direction_setting`, `map_all_control`, `map_info_window_setting`, `style_google_map`, `map_locations`, `map_layer_setting`, `map_polygon_setting`, `map_polyline_setting`, `map_cluster_setting`, `map_overlay_setting`, `map_geotags`, `map_infowindow_setting`) VALUES
(2, 'usa', '', '400', '5', 'ROADMAP', 'false', NULL, NULL, 'a:2:{s:11:\"pov_heading\";s:0:\"\";s:9:\"pov_pitch\";s:0:\"\";}', 'b:0;', 'a:91:{s:17:\"map_minzoom_level\";s:1:\"0\";s:17:\"map_maxzoom_level\";s:2:\"19\";s:23:\"zoom_level_after_search\";s:2:\"10\";s:15:\"doubleclickzoom\";s:4:\"true\";s:7:\"gesture\";s:4:\"auto\";s:7:\"screens\";a:3:{s:11:\"smartphones\";a:3:{s:16:\"map_width_mobile\";s:0:\"\";s:17:\"map_height_mobile\";s:0:\"\";s:21:\"map_zoom_level_mobile\";s:1:\"5\";}s:5:\"ipads\";a:3:{s:16:\"map_width_mobile\";s:0:\"\";s:17:\"map_height_mobile\";s:0:\"\";s:21:\"map_zoom_level_mobile\";s:1:\"5\";}s:13:\"large-screens\";a:3:{s:16:\"map_width_mobile\";s:0:\"\";s:17:\"map_height_mobile\";s:0:\"\";s:21:\"map_zoom_level_mobile\";s:1:\"5\";}}s:19:\"map_center_latitude\";s:0:\"\";s:20:\"map_center_longitude\";s:0:\"\";s:10:\"fit_bounds\";s:4:\"true\";s:12:\"current_post\";s:4:\"true\";s:23:\"center_circle_fillcolor\";s:7:\"#8CAEF2\";s:25:\"center_circle_fillopacity\";s:0:\"\";s:25:\"center_circle_strokecolor\";s:7:\"#8CAEF2\";s:27:\"center_circle_strokeopacity\";s:0:\"\";s:26:\"center_circle_strokeweight\";s:0:\"\";s:20:\"center_circle_radius\";s:0:\"\";s:29:\"show_center_marker_infowindow\";s:0:\"\";s:18:\"marker_center_icon\";s:86:\"http://wp-bedrock.loc/app/plugins/wp-google-map-gold/assets/images//default_marker.png\";s:19:\"gm_radius_dimension\";s:5:\"miles\";s:9:\"gm_radius\";s:0:\"\";s:21:\"zoom_control_position\";s:8:\"TOP_LEFT\";s:18:\"zoom_control_style\";s:5:\"LARGE\";s:25:\"map_type_control_position\";s:9:\"TOP_RIGHT\";s:22:\"map_type_control_style\";s:14:\"HORIZONTAL_BAR\";s:28:\"full_screen_control_position\";s:9:\"TOP_RIGHT\";s:28:\"street_view_control_position\";s:8:\"TOP_LEFT\";s:23:\"search_control_position\";s:8:\"TOP_LEFT\";s:25:\"locateme_control_position\";s:8:\"TOP_LEFT\";s:20:\"map_control_settings\";a:0:{}s:21:\"infowindow_openoption\";s:5:\"click\";s:19:\"marker_default_icon\";s:86:\"http://wp-bedrock.loc/app/plugins/wp-google-map-gold/assets/images//default_marker.png\";s:16:\"infowindow_close\";s:4:\"true\";s:27:\"infowindow_bounce_animation\";s:5:\"click\";s:25:\"infowindow_drop_animation\";s:4:\"true\";s:20:\"infowindow_zoomlevel\";s:1:\"9\";s:19:\"infowindow_iscenter\";s:4:\"true\";s:16:\"infowindow_width\";s:0:\"\";s:23:\"infowindow_border_color\";s:1:\"#\";s:24:\"infowindow_border_radius\";s:0:\"\";s:19:\"infowindow_bg_color\";s:1:\"#\";s:24:\"location_infowindow_skin\";a:3:{s:4:\"name\";s:4:\"ojas\";s:4:\"type\";s:10:\"infowindow\";s:10:\"sourcecode\";s:924:\"<div class=\"fc-item-box fc-item-no-padding\">\r\n    <div class=\"fc-item-title fc-item-primary-text-color\">{marker_title}</div>\r\n    <div class=\"fc-itemcontent-padding\">\r\n        <div class=\"fc-item-content fc-item-body-text-color fc-space-bottom\">\r\n            {marker_message}\r\n        </div>\r\n        <div class=\"fc-item-content fc-item-body-text-color fc-space-bottom\">\r\n            <p>{marker_email}</p>\r\n            <a id=\"broker-form-btn\" data-toggle=\"modal\" data-id=\"{marker_id}\" data-target=\"#broker-modal\" href=\"#\">Send Message</a>\r\n        </div>\r\n        <div class=\"fc-divider fc-item-content\">\r\n            <div class=\"fc-6 fc-css\"><strong><i>Address:</i></strong><br>\r\n                {marker_address}\r\n            </div>\r\n            <div class=\"fc-6 fc-css\"><strong><i>Contact:</i></strong><br>\r\n                <strong>Phone:</strong><br> 056-50438-4574\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\";}s:20:\"post_infowindow_skin\";a:3:{s:4:\"name\";s:4:\"aare\";s:4:\"type\";s:4:\"post\";s:10:\"sourcecode\";s:442:\"<div class=\"fc-item-box fc-item-no-padding \">\r\n    {post_featured_image}\r\n    <div class=\"fc-itemcontent-padding\">\r\n        <div class=\"fc-itemcontent-padding fc-item-no-padding\">\r\n            <div class=\"fc-item-title fc-item-primary-text-color fc-item-top-space\">{post_title}</div>\r\n            <div class=\"fc-item-content fc-item-body-text-color\">{marker_address}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"fc-clear\"></div>\r\n</div>\";}s:20:\"wpgmp_acf_field_name\";s:0:\"\";s:12:\"custom_style\";s:0:\"\";s:13:\"from_latitude\";s:0:\"\";s:14:\"from_longitude\";s:0:\"\";s:11:\"to_latitude\";s:0:\"\";s:12:\"to_longitude\";s:0:\"\";s:10:\"zoom_level\";s:0:\"\";s:24:\"wpgmp_category_tab_title\";s:0:\"\";s:20:\"wpgmp_category_order\";s:5:\"title\";s:34:\"wpgmp_category_location_sort_order\";s:3:\"asc\";s:25:\"wpgmp_direction_tab_title\";s:0:\"\";s:19:\"wpgmp_unit_selected\";s:2:\"km\";s:25:\"wpgmp_direction_tab_start\";s:7:\"textbox\";s:33:\"wpgmp_direction_tab_start_default\";s:0:\"\";s:23:\"wpgmp_direction_tab_end\";s:7:\"textbox\";s:31:\"wpgmp_direction_tab_end_default\";s:0:\"\";s:22:\"wpgmp_nearby_tab_title\";s:0:\"\";s:23:\"nearby_circle_fillcolor\";s:7:\"#8CAEF2\";s:25:\"nearby_circle_fillopacity\";s:0:\"\";s:25:\"nearby_circle_strokecolor\";s:7:\"#8CAEF2\";s:27:\"nearby_circle_strokeopacity\";s:0:\"\";s:26:\"nearby_circle_strokeweight\";s:0:\"\";s:18:\"nearby_circle_zoom\";s:1:\"8\";s:21:\"wpgmp_route_tab_title\";s:0:\"\";s:14:\"custom_filters\";a:0:{}s:20:\"display_reset_button\";s:4:\"true\";s:21:\"map_reset_button_text\";s:5:\"Reset\";s:15:\"display_listing\";s:4:\"true\";s:20:\"wpgmp_search_display\";s:4:\"true\";s:22:\"wpgmp_radius_dimension\";s:5:\"miles\";s:20:\"wpgmp_radius_options\";s:0:\"\";s:20:\"wpgmp_listing_number\";s:0:\"\";s:20:\"wpgmp_before_listing\";s:17:\"Locations Listing\";s:15:\"wpgmp_list_grid\";s:18:\"wpgmp_listing_list\";s:25:\"wpgmp_categorydisplaysort\";s:5:\"title\";s:27:\"wpgmp_categorydisplaysortby\";s:3:\"asc\";s:20:\"wpgmp_default_radius\";s:0:\"\";s:30:\"wpgmp_default_radius_dimension\";s:5:\"miles\";s:9:\"item_skin\";a:3:{s:4:\"name\";s:6:\"acerra\";s:4:\"type\";s:4:\"item\";s:10:\"sourcecode\";s:1260:\"<div class=\"fc-item-box fc-component-2 wpgmp_locations\">\r\n    <div class=\"fc-component-block\">\r\n        <div class=\"fc-component-content\">\r\n            <ul>\r\n                <li class=\"fc-item-featured fc-component-thumb fc-item-top_space\">\r\n                    <div class=\"fc-featured-hoverdiv\">\r\n                        <div class=\"fc-featured-hoverinner\"><a href=\"https://www.facebook.com/sharer/sharer.php?u={post_link}\" class=\"facebook wpgmp-social-share\"></a><a href=\"https://twitter.com/intent/tweet/?text={post_title}&url={post_link}\" class=\"twitter wpgmp-social-share\"></a></div>\r\n                        {marker_image}\r\n                    </div>\r\n                </li>\r\n\r\n                <li class=\"fc-component-text\">\r\n                    <div class=\"fc-item-padding-content_15\">\r\n                        <div class=\"fc-item-meta fc-item-secondary-text-color\">{marker_category}</div>\r\n                        <div class=\"fc-item-title fc-item-primary-text-color\"> {marker_title} </div>\r\n                        <div class=\"fc-item-content fc-item-body-text-color\">\r\n                            {marker_message}\r\n                        </div>\r\n                    </div>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>\";}s:16:\"filters_position\";s:7:\"default\";s:11:\"geojson_url\";s:0:\"\";s:16:\"wpgmp_custom_css\";s:0:\"\";s:20:\"wpgmp_base_font_size\";s:0:\"\";s:19:\"wpgmp_primary_color\";s:1:\"#\";s:21:\"wpgmp_secondary_color\";s:1:\"#\";s:16:\"fc_custom_styles\";s:7514:\"{\"0\":{\"infowindow-ojas\":{\"fc-item-box.fc-item-no-padding\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:400;font-size:15px;color:rgba(0, 0, 0, 0.87);line-height:21.4333px;background-color:rgb(255, 255, 255);font-style:normal;text-align:start;text-decoration:rgba(0, 0, 0, 0.87);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"1\":{\"infowindow-ojas\":{\"fc-item-title.fc-item-primary-text-color\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:700;font-size:16px;color:rgb(255, 255, 255);line-height:21.4333px;background-color:rgb(244, 67, 54);font-style:normal;text-align:start;text-decoration:rgb(255, 255, 255);margin-top:0px;margin-bottom:5px;margin-left:0px;margin-right:0px;padding-top:8px;padding-bottom:8px;padding-left:10px;padding-right:30px;\"}},\"2\":{\"infowindow-ojas\":{\"fc-item-content.fc-item-body-text-color.fc-space-bottom\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:20px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"3\":{\"infowindow-ojas\":{\"fc-item-content.fc-item-body-text-color.fc-space-bottom\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:20px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"4\":{\"infowindow-ojas\":{\"fc-divider.fc-item-content\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:0px;margin-left:-15px;margin-right:-15px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"5\":{\"infowindow-ojas\":{\"fc-6.fc-css\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:15px;padding-right:15px;\"}},\"6\":{\"infowindow-ojas\":{\"fc-6.fc-css\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:15px;padding-right:15px;\"}},\"7\":{\"post-aare\":{\"fc-item-box.fc-item-no-padding\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:400;font-size:15px;color:rgba(0, 0, 0, 0.87);line-height:21.4333px;background-color:rgb(255, 255, 255);font-style:normal;text-align:start;text-decoration:rgba(0, 0, 0, 0.87);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"8\":{\"post-aare\":{\"fc-item-title.fc-item-primary-text-color.fc-item-top-space\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:700;font-size:16px;color:rgb(68, 68, 68);line-height:21.4333px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(68, 68, 68);margin-top:10px;margin-bottom:5px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"9\":{\"post-aare\":{\"fc-item-content.fc-item-body-text-color\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:start;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"10\":{\"item-acerra\":{\"fc-item-box.fc-component-2.wpgmp_locations\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:400;font-size:15px;color:rgba(0, 0, 0, 0.87);line-height:21.4333px;background-color:rgb(255, 255, 255);font-style:normal;text-align:start;text-decoration:rgba(0, 0, 0, 0.87);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"11\":{\"item-acerra\":{\"fc-item-meta.fc-item-secondary-text-color\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:400;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:left;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:7px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"12\":{\"item-acerra\":{\"fc-item-title.fc-item-primary-text-color\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:700;font-size:16px;color:rgb(68, 68, 68);line-height:21.4333px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:left;text-decoration:rgb(68, 68, 68);margin-top:0px;margin-bottom:5px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}},\"13\":{\"item-acerra\":{\"fc-item-content.fc-item-body-text-color\":\"background-image:none;font-family:-apple-system, BlinkMacSystemFont, \\\"Segoe UI\\\", Roboto, Oxygen-Sans, Ubuntu, Cantarell, \\\"Helvetica Neue\\\", sans-serif;font-weight:300;font-size:13px;color:rgb(119, 119, 119);line-height:23px;background-color:rgba(0, 0, 0, 0);font-style:normal;text-align:left;text-decoration:rgb(119, 119, 119);margin-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;padding-top:0px;padding-bottom:0px;padding-left:0px;padding-right:0px;\"}}}\";s:18:\"infowindow_setting\";s:924:\"<div class=\"fc-item-box fc-item-no-padding\">\r\n    <div class=\"fc-item-title fc-item-primary-text-color\">{marker_title}</div>\r\n    <div class=\"fc-itemcontent-padding\">\r\n        <div class=\"fc-item-content fc-item-body-text-color fc-space-bottom\">\r\n            {marker_message}\r\n        </div>\r\n        <div class=\"fc-item-content fc-item-body-text-color fc-space-bottom\">\r\n            <p>{marker_email}</p>\r\n            <a id=\"broker-form-btn\" data-toggle=\"modal\" data-id=\"{marker_id}\" data-target=\"#broker-modal\" href=\"#\">Send Message</a>\r\n        </div>\r\n        <div class=\"fc-divider fc-item-content\">\r\n            <div class=\"fc-6 fc-css\"><strong><i>Address:</i></strong><br>\r\n                {marker_address}\r\n            </div>\r\n            <div class=\"fc-6 fc-css\"><strong><i>Contact:</i></strong><br>\r\n                <strong>Phone:</strong><br> 056-50438-4574\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\";s:26:\"infowindow_geotags_setting\";s:442:\"<div class=\"fc-item-box fc-item-no-padding \">\r\n    {post_featured_image}\r\n    <div class=\"fc-itemcontent-padding\">\r\n        <div class=\"fc-itemcontent-padding fc-item-no-padding\">\r\n            <div class=\"fc-item-title fc-item-primary-text-color fc-item-top-space\">{post_title}</div>\r\n            <div class=\"fc-item-content fc-item-body-text-color\">{marker_address}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"fc-clear\"></div>\r\n</div>\";s:27:\"wpgmp_categorydisplayformat\";s:1260:\"<div class=\"fc-item-box fc-component-2 wpgmp_locations\">\r\n    <div class=\"fc-component-block\">\r\n        <div class=\"fc-component-content\">\r\n            <ul>\r\n                <li class=\"fc-item-featured fc-component-thumb fc-item-top_space\">\r\n                    <div class=\"fc-featured-hoverdiv\">\r\n                        <div class=\"fc-featured-hoverinner\"><a href=\"https://www.facebook.com/sharer/sharer.php?u={post_link}\" class=\"facebook wpgmp-social-share\"></a><a href=\"https://twitter.com/intent/tweet/?text={post_title}&url={post_link}\" class=\"twitter wpgmp-social-share\"></a></div>\r\n                        {marker_image}\r\n                    </div>\r\n                </li>\r\n\r\n                <li class=\"fc-component-text\">\r\n                    <div class=\"fc-item-padding-content_15\">\r\n                        <div class=\"fc-item-meta fc-item-secondary-text-color\">{marker_category}</div>\r\n                        <div class=\"fc-item-title fc-item-primary-text-color\"> {marker_title} </div>\r\n                        <div class=\"fc-item-content fc-item-body-text-color\">\r\n                            {marker_message}\r\n                        </div>\r\n                    </div>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>\";}', 'b:0;', 'a:4:{s:14:\"mapfeaturetype\";a:10:{i:0;s:20:\"Select Featured Type\";i:1;s:20:\"Select Featured Type\";i:2;s:20:\"Select Featured Type\";i:3;s:20:\"Select Featured Type\";i:4;s:20:\"Select Featured Type\";i:5;s:20:\"Select Featured Type\";i:6;s:20:\"Select Featured Type\";i:7;s:20:\"Select Featured Type\";i:8;s:20:\"Select Featured Type\";i:9;s:20:\"Select Featured Type\";}s:14:\"mapelementtype\";a:10:{i:0;s:19:\"Select Element Type\";i:1;s:19:\"Select Element Type\";i:2;s:19:\"Select Element Type\";i:3;s:19:\"Select Element Type\";i:4;s:19:\"Select Element Type\";i:5;s:19:\"Select Element Type\";i:6;s:19:\"Select Element Type\";i:7;s:19:\"Select Element Type\";i:8;s:19:\"Select Element Type\";i:9;s:19:\"Select Element Type\";}s:5:\"color\";a:10:{i:0;s:1:\"#\";i:1;s:1:\"#\";i:2;s:1:\"#\";i:3;s:1:\"#\";i:4;s:1:\"#\";i:5;s:1:\"#\";i:6;s:1:\"#\";i:7;s:1:\"#\";i:8;s:1:\"#\";i:9;s:1:\"#\";}s:10:\"visibility\";a:10:{i:0;s:2:\"on\";i:1;s:2:\"on\";i:2;s:2:\"on\";i:3;s:2:\"on\";i:4;s:2:\"on\";i:5;s:2:\"on\";i:6;s:2:\"on\";i:7;s:2:\"on\";i:8;s:2:\"on\";i:9;s:2:\"on\";}}', 'a:14:{i:0;s:1:\"7\";i:1;s:2:\"17\";i:2;s:2:\"18\";i:3;s:2:\"12\";i:4;s:2:\"13\";i:5;s:2:\"11\";i:6;s:2:\"10\";i:7;s:2:\"15\";i:8;s:2:\"14\";i:9;s:1:\"6\";i:10;s:1:\"8\";i:11;s:1:\"9\";i:12;s:2:\"16\";i:13;s:2:\"19\";}', 'a:4:{s:9:\"map_links\";s:0:\"\";s:13:\"fusion_select\";s:0:\"\";s:11:\"fusion_from\";s:0:\"\";s:16:\"fusion_icon_name\";s:0:\"\";}', 'b:0;', NULL, 'a:5:{s:4:\"grid\";s:0:\"\";s:8:\"max_zoom\";s:1:\"1\";s:13:\"location_zoom\";s:2:\"10\";s:4:\"icon\";s:5:\"4.png\";s:10:\"hover_icon\";s:5:\"4.png\";}', 'a:6:{s:20:\"overlay_border_color\";s:1:\"#\";s:13:\"overlay_width\";s:0:\"\";s:14:\"overlay_height\";s:0:\"\";s:16:\"overlay_fontsize\";s:0:\"\";s:20:\"overlay_border_width\";s:0:\"\";s:20:\"overlay_border_style\";s:6:\"dotted\";}', 'a:2:{s:16:\"video-catalog-mb\";a:4:{s:7:\"address\";s:0:\"\";s:8:\"latitude\";s:0:\"\";s:9:\"longitude\";s:0:\"\";s:8:\"category\";s:0:\"\";}s:4:\"post\";a:4:{s:7:\"address\";s:0:\"\";s:8:\"latitude\";s:0:\"\";s:9:\"longitude\";s:0:\"\";s:8:\"category\";s:0:\"\";}}', 'b:0;');

-- --------------------------------------------------------

--
-- Table structure for table `wp_group_map`
--

CREATE TABLE `wp_group_map` (
  `group_map_id` int(11) NOT NULL,
  `group_map_title` varchar(255) DEFAULT NULL,
  `group_marker` text,
  `extensions_fields` text,
  `group_parent` int(11) DEFAULT '0',
  `group_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_group_map`
--

INSERT INTO `wp_group_map` (`group_map_id`, `group_map_title`, `group_marker`, `extensions_fields`, `group_parent`, `group_added`) VALUES
(3, 'Brokers', 'http://wp-test.loc/wp-content/themes/menu-jquery/vendor/wp-google-map-gold/assets/images//default_marker.png', 'a:1:{s:9:\"cat_order\";s:0:\"\";}', 0, '2020-01-29 17:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_map_locations`
--

CREATE TABLE `wp_map_locations` (
  `location_id` int(11) NOT NULL,
  `location_title` varchar(255) DEFAULT NULL,
  `location_address` varchar(255) DEFAULT NULL,
  `location_draggable` varchar(255) DEFAULT NULL,
  `location_infowindow_default_open` varchar(255) DEFAULT NULL,
  `location_animation` varchar(255) DEFAULT NULL,
  `location_latitude` varchar(255) DEFAULT NULL,
  `location_longitude` varchar(255) DEFAULT NULL,
  `location_city` varchar(255) DEFAULT NULL,
  `location_state` varchar(255) DEFAULT NULL,
  `location_country` varchar(255) DEFAULT NULL,
  `location_postal_code` varchar(255) DEFAULT NULL,
  `location_author` int(11) DEFAULT NULL,
  `location_messages` text,
  `location_settings` text,
  `location_group_map` text,
  `location_extrafields` text,
  `location_email` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_map_locations`
--

INSERT INTO `wp_map_locations` (`location_id`, `location_title`, `location_address`, `location_draggable`, `location_infowindow_default_open`, `location_animation`, `location_latitude`, `location_longitude`, `location_city`, `location_state`, `location_country`, `location_postal_code`, `location_author`, `location_messages`, `location_settings`, `location_group_map`, `location_extrafields`, `location_email`) VALUES
(6, 'Met Dimon', '261 Moore St, Brooklyn, NY 10007, United States', '', '', 'DROP', '40.7050886', '-73.93357420000001', 'New York', 'NY', 'USA', '10007', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(7, 'Zion Arabella', '3140 Fulton St, Brooklyn, NY 11208, United States', '', '', 'BOUNCE1', '40.681584', '-73.878901', 'New York', 'NY', 'USA', '11208', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(8, 'John Carter', '265 Ashford St, Brooklyn, NY 11207, United States', '', '', 'DROP', '40.676185', '-73.885567', 'New York', 'NY', 'USA', '11207', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(9, 'Cora Ashan', '390 Crescent St, Brooklyn, NY 11208, United States', '', '', 'DROP', '40.679403', '-73.871130', 'New York', 'NY', 'USA', '11208', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(10, 'Sasha Lucky', 'Jamaica, NY 11417, USA', '', '', 'BOUNCE1', '40.680003', '-73.846598', 'New York', 'NY', 'USA', '11417', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(11, 'Tamara Ohana', '1515 S Garden Ave, Fresno, CA 93727, USA', '', '', 'DROP', '36.727550', '-119.734531', 'Fresno', 'CA', 'USA', '93727', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(12, 'Vika Canjucia', '1100-1298 E Terrace Ave Fresno, CA 93704, USA', '', '', 'DROP', '36.772963', '-119.796691', 'Fresno', 'CA', 'USA', '93704', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(13, 'Timur Viconsa', '1800 Tulare St, Fresno, CA 93721, USA', '', '', 'DROP', '36.731435', '-119.792200', 'Fresno', 'CA', 'USA', '93721', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(14, 'Olha Hanna', '1519 Beaumont St, Dallas, TX 75215, USA', '', '', 'DROP', '32.769199', '-96.788918', 'Dallas', 'TX', 'USA', '75215', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(15, 'Omar Tokar', '1519 Beaumont St, Dallas, TX 75215, USA', '', '', 'DROP', '32.772154', '-96.765343', 'Dallas', 'TX', 'USA', '75215', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(16, 'Bob Hammer', '1519 Beaumont St, Dallas, TX 75215, USA', '', '', 'DROP', '32.760791', '-96.779537', 'Dallas', 'TX', 'USA', '75215', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(17, 'Vova Champion', '1599-1501 33rd Ave, Seattle, WA 98122, USA', '', '', 'DROP', '47.615058', '-122.290103', 'Seattle', 'WA', 'USA', '98122', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', 'lvasoul1982@gmail.com'),
(18, 'Viktor Bradley', '1900 5th Ave, Seattle, WA 98101, USA', '', '', 'DROP', '47.613217', '-122.338998', 'Seattle', 'WA', 'USA', '98101', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL),
(19, 'Alkin Bosshin', '1801-1899 S Holgate St, Seattle, WA 98144, USA', '', '', 'DROP', '47.586621', '-122.308398', 'Seattle', 'WA', 'USA', '98144', 1, '', 'a:4:{s:7:\"onclick\";s:6:\"marker\";s:13:\"redirect_link\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:20:\"redirect_link_window\";s:3:\"yes\";}', 'a:1:{i:0;s:1:\"3\";}', 's:0:\"\";', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_map_routes`
--

CREATE TABLE `wp_map_routes` (
  `route_id` int(11) NOT NULL,
  `route_title` varchar(255) DEFAULT NULL,
  `route_stroke_color` varchar(255) DEFAULT NULL,
  `route_stroke_opacity` varchar(255) DEFAULT NULL,
  `route_stroke_weight` int(11) DEFAULT NULL,
  `route_travel_mode` varchar(255) DEFAULT NULL,
  `route_unit_system` varchar(255) DEFAULT NULL,
  `route_marker_draggable` varchar(255) DEFAULT NULL,
  `route_optimize_waypoints` varchar(255) DEFAULT NULL,
  `route_start_location` int(11) DEFAULT NULL,
  `route_end_location` int(11) DEFAULT NULL,
  `route_way_points` text,
  `extensions_fields` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wp-test.loc', 'yes'),
(2, 'home', 'http://wp-test.loc', 'yes'),
(3, 'blogname', 'wp-test.loc', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'lvasoul1982@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:129:{s:19:\"video-catalog-mb/?$\";s:36:\"index.php?post_type=video-catalog-mb\";s:49:\"video-catalog-mb/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?post_type=video-catalog-mb&feed=$matches[1]\";s:44:\"video-catalog-mb/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?post_type=video-catalog-mb&feed=$matches[1]\";s:36:\"video-catalog-mb/page/([0-9]{1,})/?$\";s:54:\"index.php?post_type=video-catalog-mb&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:44:\"video-catalog-mb/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:54:\"video-catalog-mb/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:74:\"video-catalog-mb/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"video-catalog-mb/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"video-catalog-mb/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:50:\"video-catalog-mb/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"video-catalog-mb/([^/]+)/embed/?$\";s:49:\"index.php?video-catalog-mb=$matches[1]&embed=true\";s:37:\"video-catalog-mb/([^/]+)/trackback/?$\";s:43:\"index.php?video-catalog-mb=$matches[1]&tb=1\";s:57:\"video-catalog-mb/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?video-catalog-mb=$matches[1]&feed=$matches[2]\";s:52:\"video-catalog-mb/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?video-catalog-mb=$matches[1]&feed=$matches[2]\";s:45:\"video-catalog-mb/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?video-catalog-mb=$matches[1]&paged=$matches[2]\";s:52:\"video-catalog-mb/([^/]+)/comment-page-([0-9]{1,})/?$\";s:56:\"index.php?video-catalog-mb=$matches[1]&cpage=$matches[2]\";s:41:\"video-catalog-mb/([^/]+)(?:/([0-9]+))?/?$\";s:55:\"index.php?video-catalog-mb=$matches[1]&page=$matches[2]\";s:33:\"video-catalog-mb/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"video-catalog-mb/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"video-catalog-mb/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"video-catalog-mb/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"video-catalog-mb/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"video-catalog-mb/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:55:\"mb-manufacture/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?mb-manufacture=$matches[1]&feed=$matches[2]\";s:50:\"mb-manufacture/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?mb-manufacture=$matches[1]&feed=$matches[2]\";s:31:\"mb-manufacture/([^/]+)/embed/?$\";s:47:\"index.php?mb-manufacture=$matches[1]&embed=true\";s:43:\"mb-manufacture/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?mb-manufacture=$matches[1]&paged=$matches[2]\";s:25:\"mb-manufacture/([^/]+)/?$\";s:36:\"index.php?mb-manufacture=$matches[1]\";s:49:\"mb-color/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?mb-color=$matches[1]&feed=$matches[2]\";s:44:\"mb-color/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?mb-color=$matches[1]&feed=$matches[2]\";s:25:\"mb-color/([^/]+)/embed/?$\";s:41:\"index.php?mb-color=$matches[1]&embed=true\";s:37:\"mb-color/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?mb-color=$matches[1]&paged=$matches[2]\";s:19:\"mb-color/([^/]+)/?$\";s:30:\"index.php?mb-color=$matches[1]\";s:54:\"mb-categories/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?mb-categories=$matches[1]&feed=$matches[2]\";s:49:\"mb-categories/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?mb-categories=$matches[1]&feed=$matches[2]\";s:30:\"mb-categories/([^/]+)/embed/?$\";s:46:\"index.php?mb-categories=$matches[1]&embed=true\";s:42:\"mb-categories/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?mb-categories=$matches[1]&paged=$matches[2]\";s:24:\"mb-categories/([^/]+)/?$\";s:35:\"index.php?mb-categories=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:40:\"index.php?&page_id=180&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:1;s:43:\"mb-custom-post-type/mb-custom-post-type.php\";i:2;s:21:\"meta-box/meta-box.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'menu-jquery', 'yes'),
(41, 'stylesheet', 'menu-jquery', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '180', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:75:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpgmp_admin_overview\";b:1;s:19:\"wpgmp_form_location\";b:1;s:21:\"wpgmp_manage_location\";b:1;s:21:\"wpgmp_import_location\";b:1;s:14:\"wpgmp_form_map\";b:1;s:16:\"wpgmp_manage_map\";b:1;s:20:\"wpgmp_form_group_map\";b:1;s:22:\"wpgmp_manage_group_map\";b:1;s:20:\"wpgmp_manage_drawing\";b:1;s:16:\"wpgmp_form_route\";b:1;s:18:\"wpgmp_manage_route\";b:1;s:24:\"wpgmp_manage_permissions\";b:1;s:21:\"wpgmp_manage_settings\";b:1;s:18:\"wpgmp_manage_tools\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:3:{i:2;a:1:{s:5:\"title\";s:0:\"\";}i:3;a:1:{s:5:\"title\";s:6:\"Search\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:8:\"big-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"yspl_win\";a:2:{i:0;s:13:\"custom_html-2\";i:1;s:8:\"search-3\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:321:\"<div class=\"row\">\r\n	<div class=\"container\">\r\n		<div class=\"col-sm-4\">\r\n			<h2>Contact</h2>\r\n			<p>Phone: 123.123.123</p>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<h2>Contact</h2>\r\n			<p>Phone: 123.123.123</p>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<h2>Contact</h2>\r\n			<p>Phone: 123.123.123</p>\r\n		</div>\r\n	</div>\r\n</div>\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:7:{i:1581108133;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1581133333;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1581157268;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1581160043;a:1:{s:28:\"ots_extensions_license_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1581176547;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1581176842;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentynineteen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1547117601;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}}', 'yes'),
(138, 'recently_activated', 'a:1:{s:36:\"contact-form-7/wp-contact-form-7.php\";i:1580838202;}', 'yes'),
(142, 'vc_version', '5.5.2', 'yes'),
(151, 'current_theme', 'Menu jQuery', 'yes'),
(152, 'theme_mods_hill', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1569256109;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"sidebar-shop\";a:0:{}s:12:\"sidebar-page\";a:0:{}s:12:\"footer_col_1\";a:0:{}s:12:\"footer_col_2\";a:0:{}s:12:\"footer_col_3\";a:0:{}}}}', 'yes'),
(153, 'theme_switched', '', 'yes'),
(154, 'widget_hill_products_carousel', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(155, 'widget_widget_latest_posts_tab', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(164, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:21:\"lvasoul1982@gmail.com\";s:7:\"version\";s:5:\"5.0.3\";s:9:\"timestamp\";i:1547117241;}', 'no'),
(167, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1581091224;s:7:\"checked\";a:4:{s:4:\"hill\";s:3:\"1.5\";s:11:\"menu-jquery\";s:5:\"1.0.0\";s:8:\"my-theme\";s:5:\"1.0.0\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(171, 'theme_mods_twentysixteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1547117511;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(174, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(178, '_transient_twentysixteen_categories', '1', 'yes'),
(183, 'theme_mods_twentyseventeen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548677585;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(188, 'widget_smatcat_team_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(189, 'widget_ots_main_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(191, 'ots-extension-notices', 'a:0:{}', 'yes'),
(228, 'theme_mods_my-theme', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:9:\"main-menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1579879325;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:13:\"custom_html-2\";i:1;s:8:\"search-3\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(303, 'category_children', 'a:0:{}', 'yes'),
(525, 'faqcat_children', 'a:0:{}', 'yes'),
(614, 'acf_version', '5.8.3', 'yes'),
(670, 'catalog_children', 'a:0:{}', 'yes'),
(736, 'manufacture_children', 'a:0:{}', 'yes'),
(797, 'color_children', 'a:0:{}', 'yes'),
(834, 'WPLANG', '', 'yes'),
(835, 'new_admin_email', 'lvasoul1982@gmail.com', 'yes'),
(1036, 'theme_mods_menu-jquery', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1579007727;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:8:\"yspl_win\";a:2:{i:0;s:13:\"custom_html-2\";i:1;s:8:\"search-3\";}s:8:\"big-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(1094, 'mb-categories_children', 'a:0:{}', 'yes'),
(1095, 'mb-manufacture_children', 'a:0:{}', 'yes'),
(1102, 'mb-color_children', 'a:0:{}', 'yes'),
(1187, 'widget_wpgmp_google_map_widget_class', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1189, 'admin_email_lifespan', '1595861293', 'yes'),
(1190, 'db_upgraded', '', 'yes'),
(1192, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1581091223;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}', 'no'),
(1194, 'recovery_keys', 'a:0:{}', 'yes'),
(1195, 'can_compress_scripts', '1', 'no'),
(1217, 'wpgmp_settings', 'a:11:{s:14:\"wpgmp_language\";s:2:\"en\";s:13:\"wpgmp_api_key\";s:39:\"AIzaSyCU4AqnyWXeWX1nygAyUd3p-Gy1YELV4ZQ\";s:19:\"wpgmp_scripts_place\";s:6:\"footer\";s:20:\"wpgmp_scripts_minify\";s:2:\"no\";s:16:\"wpgmp_allow_meta\";s:6:\"a:0:{}\";s:17:\"wpgmp_metabox_map\";s:0:\"\";s:14:\"wpgmp_auto_fix\";s:0:\"\";s:16:\"wpgmp_debug_mode\";s:0:\"\";s:10:\"wpgmp_gdpr\";s:0:\"\";s:14:\"wpgmp_gdpr_msg\";s:0:\"\";s:22:\"wpgmp_country_specific\";s:0:\"\";}', 'yes'),
(1218, 'wpgmp_location_extrafields', 's:6:\"a:0:{}\";', 'yes'),
(1240, 'recovery_mode_email_last_sent', '1580397763', 'yes'),
(1336, '_site_transient_timeout_theme_roots', '1581093024', 'no'),
(1337, '_site_transient_theme_roots', 'a:4:{s:4:\"hill\";s:7:\"/themes\";s:11:\"menu-jquery\";s:7:\"/themes\";s:8:\"my-theme\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no'),
(1338, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1581091225;s:7:\"checked\";a:3:{s:43:\"mb-custom-post-type/mb-custom-post-type.php\";s:5:\"1.9.1\";s:21:\"meta-box/meta-box.php\";s:5:\"5.2.3\";s:39:\"our-team-enhanced/our-team-showcase.php\";s:5:\"4.4.2\";}s:8:\"response\";a:2:{s:43:\"mb-custom-post-type/mb-custom-post-type.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/mb-custom-post-type\";s:4:\"slug\";s:19:\"mb-custom-post-type\";s:6:\"plugin\";s:43:\"mb-custom-post-type/mb-custom-post-type.php\";s:11:\"new_version\";s:5:\"1.9.2\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/mb-custom-post-type/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/mb-custom-post-type.1.9.2.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/mb-custom-post-type/assets/icon-128x128.png?rev=1697794\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:74:\"https://ps.w.org/mb-custom-post-type/assets/banner-772x250.png?rev=1697795\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:5:\"5.2.7\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/meta-box.5.2.7.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:0:{}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 1, '_edit_lock', '1570032871:1'),
(6, 2, '_edit_lock', '1548764076:1'),
(16, 14, '_menu_item_type', 'post_type'),
(17, 14, '_menu_item_menu_item_parent', '0'),
(18, 14, '_menu_item_object_id', '2'),
(19, 14, '_menu_item_object', 'page'),
(20, 14, '_menu_item_target', ''),
(21, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(22, 14, '_menu_item_xfn', ''),
(23, 14, '_menu_item_url', ''),
(25, 15, '_menu_item_type', 'post_type'),
(26, 15, '_menu_item_menu_item_parent', '0'),
(27, 15, '_menu_item_object_id', '2'),
(28, 15, '_menu_item_object', 'page'),
(29, 15, '_menu_item_target', ''),
(30, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(31, 15, '_menu_item_xfn', ''),
(32, 15, '_menu_item_url', ''),
(34, 16, '_menu_item_type', 'post_type'),
(35, 16, '_menu_item_menu_item_parent', '0'),
(36, 16, '_menu_item_object_id', '2'),
(37, 16, '_menu_item_object', 'page'),
(38, 16, '_menu_item_target', ''),
(39, 16, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(40, 16, '_menu_item_xfn', ''),
(41, 16, '_menu_item_url', ''),
(43, 17, '_menu_item_type', 'post_type'),
(44, 17, '_menu_item_menu_item_parent', '15'),
(45, 17, '_menu_item_object_id', '2'),
(46, 17, '_menu_item_object', 'page'),
(47, 17, '_menu_item_target', ''),
(48, 17, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(49, 17, '_menu_item_xfn', ''),
(50, 17, '_menu_item_url', ''),
(52, 18, '_menu_item_type', 'post_type'),
(53, 18, '_menu_item_menu_item_parent', '17'),
(54, 18, '_menu_item_object_id', '2'),
(55, 18, '_menu_item_object', 'page'),
(56, 18, '_menu_item_target', ''),
(57, 18, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(58, 18, '_menu_item_xfn', ''),
(59, 18, '_menu_item_url', ''),
(61, 19, '_menu_item_type', 'post_type'),
(62, 19, '_menu_item_menu_item_parent', '15'),
(63, 19, '_menu_item_object_id', '2'),
(64, 19, '_menu_item_object', 'page'),
(65, 19, '_menu_item_target', ''),
(66, 19, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(67, 19, '_menu_item_xfn', ''),
(68, 19, '_menu_item_url', ''),
(70, 20, 'sc_member_order', '9223372036854775807'),
(71, 20, '_edit_last', '1'),
(72, 20, '_edit_lock', '1547118650:1'),
(73, 22, '_menu_item_type', 'post_type'),
(74, 22, '_menu_item_menu_item_parent', '18'),
(75, 22, '_menu_item_object_id', '2'),
(76, 22, '_menu_item_object', 'page'),
(77, 22, '_menu_item_target', ''),
(78, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(79, 22, '_menu_item_xfn', ''),
(80, 22, '_menu_item_url', ''),
(81, 23, '_menu_item_type', 'post_type'),
(82, 23, '_menu_item_menu_item_parent', '17'),
(83, 23, '_menu_item_object_id', '2'),
(84, 23, '_menu_item_object', 'page'),
(85, 23, '_menu_item_target', ''),
(86, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(87, 23, '_menu_item_xfn', ''),
(88, 23, '_menu_item_url', ''),
(90, 24, '_menu_item_type', 'post_type'),
(91, 24, '_menu_item_menu_item_parent', '15'),
(92, 24, '_menu_item_object_id', '2'),
(93, 24, '_menu_item_object', 'page'),
(94, 24, '_menu_item_target', ''),
(95, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 24, '_menu_item_xfn', ''),
(97, 24, '_menu_item_url', ''),
(99, 25, '_menu_item_type', 'post_type'),
(100, 25, '_menu_item_menu_item_parent', '18'),
(101, 25, '_menu_item_object_id', '2'),
(102, 25, '_menu_item_object', 'page'),
(103, 25, '_menu_item_target', ''),
(104, 25, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(105, 25, '_menu_item_xfn', ''),
(106, 25, '_menu_item_url', ''),
(108, 26, '_edit_lock', '1579091992:1'),
(109, 28, '_menu_item_type', 'post_type'),
(110, 28, '_menu_item_menu_item_parent', '0'),
(111, 28, '_menu_item_object_id', '26'),
(112, 28, '_menu_item_object', 'page'),
(113, 28, '_menu_item_target', ''),
(114, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(115, 28, '_menu_item_xfn', ''),
(116, 28, '_menu_item_url', ''),
(172, 106, '_wp_attached_file', '2019/09/SampleVideo_1280x720_1mb.mp4'),
(173, 106, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:1055736;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:5;s:16:\"length_formatted\";s:4:\"0:05\";s:5:\"width\";i:1280;s:6:\"height\";i:720;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:0;}'),
(174, 107, '_wp_attached_file', '2019/09/SampleVideo_1280x720_1mb.mkv'),
(175, 107, '_wp_attachment_metadata', 'a:9:{s:8:\"filesize\";i:1052413;s:9:\"mime_type\";s:16:\"video/x-matroska\";s:6:\"length\";i:4;s:16:\"length_formatted\";s:4:\"0:04\";s:5:\"width\";i:1280;s:6:\"height\";i:720;s:10:\"fileformat\";s:8:\"matroska\";s:10:\"dataformat\";s:5:\"mpeg4\";s:5:\"audio\";a:4:{s:10:\"dataformat\";s:3:\"aac\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:6;s:8:\"language\";s:3:\"und\";}}'),
(176, 108, '_wp_attached_file', '2019/09/SampleVideo_1280x720_1mb.flv'),
(177, 108, '_wp_attachment_metadata', 'a:12:{s:8:\"lossless\";b:0;s:7:\"bitrate\";i:1171;s:8:\"filesize\";i:1051185;s:9:\"mime_type\";s:11:\"video/x-flv\";s:6:\"length\";i:5;s:16:\"length_formatted\";s:4:\"0:05\";s:5:\"width\";i:1280;s:6:\"height\";i:720;s:10:\"fileformat\";s:3:\"flv\";s:10:\"dataformat\";s:3:\"flv\";s:5:\"codec\";s:14:\"Sorenson H.263\";s:5:\"audio\";a:9:{s:10:\"dataformat\";s:3:\"flv\";s:7:\"bitrate\";i:375000;s:5:\"codec\";s:3:\"AAC\";s:11:\"sample_rate\";i:44100;s:15:\"bits_per_sample\";i:16;s:8:\"channels\";i:2;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";s:17:\"compression_ratio\";d:0.2657312925170068;}}'),
(184, 109, '_wp_attached_file', '2019/09/2SampleVideo_1280x720_1mb.mp4'),
(185, 109, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:1055736;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:5;s:16:\"length_formatted\";s:4:\"0:05\";s:5:\"width\";i:1280;s:6:\"height\";i:720;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:0;}'),
(197, 110, '_wp_attached_file', '2019/09/3SampleVideo_1280x720_1mb.mp4'),
(198, 110, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:1055736;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:5;s:16:\"length_formatted\";s:4:\"0:05\";s:5:\"width\";i:1280;s:6:\"height\";i:720;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:0;}'),
(225, 110, '_edit_lock', '1569933817:1'),
(226, 110, '_edit_last', '1'),
(248, 26, '_wp_page_template', 'template-parts/content-acf.php'),
(274, 143, '_edit_last', '1'),
(275, 143, '_edit_lock', '1579090094:1'),
(279, 144, '_edit_last', '1'),
(280, 144, '_edit_lock', '1579090029:1'),
(284, 146, '_edit_last', '1'),
(285, 146, '_edit_lock', '1579089982:1'),
(287, 147, '_edit_last', '1'),
(288, 147, '_edit_lock', '1579089931:1'),
(291, 148, '_edit_last', '1'),
(292, 148, '_edit_lock', '1579089849:1'),
(294, 150, '_edit_last', '1'),
(295, 150, '_edit_lock', '1579091482:1'),
(307, 148, 'tax_cat_videos_id', '106'),
(358, 147, 'tax_cat_videos_id', '109'),
(360, 144, 'tax_cat_videos_id', '106'),
(361, 143, 'tax_cat_videos_id', '110'),
(362, 143, 'tax_cat_videos_id', '109'),
(363, 150, 'tax_cat_videos_id', '109'),
(364, 150, 'tax_cat_videos_id', '106'),
(365, 150, 'tax_cat_videos_id', '110'),
(366, 146, 'tax_cat_videos_id', '110'),
(448, 159, '_edit_lock', '1580748386:1'),
(449, 159, '_wp_page_template', 'default'),
(450, 162, '_menu_item_type', 'post_type'),
(451, 162, '_menu_item_menu_item_parent', '0'),
(452, 162, '_menu_item_object_id', '159'),
(453, 162, '_menu_item_object', 'page'),
(454, 162, '_menu_item_target', ''),
(455, 162, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(456, 162, '_menu_item_xfn', ''),
(457, 162, '_menu_item_url', ''),
(467, 164, '_menu_item_type', 'yspl_win'),
(468, 164, '_menu_item_menu_item_parent', '16'),
(469, 164, '_menu_item_object_id', '2'),
(470, 164, '_menu_item_object', 'yspl_win'),
(471, 164, '_menu_item_target', ''),
(472, 164, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(473, 164, '_menu_item_xfn', 'custom_html-2'),
(474, 164, '_menu_item_url', ''),
(475, 165, '_edit_lock', '1579188732:1'),
(480, 167, '_edit_lock', '1579089539:1'),
(481, 165, '_edit_last', '1'),
(482, 165, 'label_name', 'Video Catalog with MB post'),
(483, 165, 'label_singular_name', 'Video Catalog with MB post'),
(484, 165, 'args_post_type', 'video-catalog-mb'),
(485, 165, 'label_menu_name', 'Video Catalog with MB post'),
(486, 165, 'label_name_admin_bar', 'Video Catalog with MB post'),
(487, 165, 'label_all_items', 'All Video Catalog with MB post'),
(488, 165, 'label_add_new', 'Add new'),
(489, 165, 'label_add_new_item', 'Add new Video Catalog with MB post'),
(490, 165, 'label_edit_item', 'Edit Video Catalog with MB post'),
(491, 165, 'label_new_item', 'New Video Catalog with MB post'),
(492, 165, 'label_view_item', 'View Video Catalog with MB post'),
(493, 165, 'label_search_items', 'Search Video Catalog with MB post'),
(494, 165, 'label_not_found', 'No Video Catalog with MB post found'),
(495, 165, 'label_not_found_in_trash', 'No Video Catalog with MB post found in Trash'),
(496, 165, 'label_parent_item_colon', 'Parent Video Catalog with MB post'),
(497, 165, 'args_public', '1'),
(498, 165, 'args_exclude_from_search', '0'),
(499, 165, 'args_publicly_queryable', '1'),
(500, 165, 'args_show_ui', '1'),
(501, 165, 'args_show_in_nav_menus', '1'),
(502, 165, 'args_show_in_admin_bar', '1'),
(503, 165, 'args_show_in_rest', '1'),
(504, 165, 'args_menu_position', '101'),
(505, 165, 'args_capability_type', 'post'),
(506, 165, 'args_hierarchical', '0'),
(507, 165, 'args_has_archive', '1'),
(508, 165, 'args_query_var', '1'),
(509, 165, 'args_can_export', '1'),
(510, 165, 'args_rewrite_no_front', '0'),
(514, 165, 'function_name', 'your_prefix_register_post_type'),
(515, 165, 'text_domain', 'text-domain'),
(519, 165, 'args_menu_icon', 'dashicons-editor-video'),
(523, 167, '_edit_last', '1'),
(524, 167, 'label_name', 'MB Categories'),
(525, 167, 'label_singular_name', 'MB Categories'),
(526, 167, 'args_taxonomy', 'mb-categories'),
(527, 167, 'label_menu_name', 'MB Categories'),
(528, 167, 'label_all_items', 'All MB Categories'),
(529, 167, 'label_edit_item', 'Edit MB Categories'),
(530, 167, 'label_view_item', 'View MB Categories'),
(531, 167, 'label_update_item', 'Update MB Categories'),
(532, 167, 'label_add_new_item', 'Add new MB Categories'),
(533, 167, 'label_new_item_name', 'New MB Categories'),
(534, 167, 'label_parent_item', 'Parent MB Categories'),
(535, 167, 'label_parent_item_colon', 'Parent MB Categories:'),
(536, 167, 'label_search_items', 'Search MB Categories'),
(537, 167, 'label_popular_items', 'Popular MB Categories'),
(538, 167, 'label_separate_items_with_commas', 'Separate MB Categories with commas'),
(539, 167, 'label_add_or_remove_items', 'Add or remove MB Categories'),
(540, 167, 'label_choose_from_most_used', 'Choose most used MB Categories'),
(541, 167, 'label_not_found', 'No MB Categories found'),
(542, 167, 'args_public', '1'),
(543, 167, 'args_show_ui', '1'),
(544, 167, 'args_show_in_menu', '1'),
(545, 167, 'args_show_in_nav_menus', '1'),
(546, 167, 'args_meta_box_cb', '1'),
(547, 167, 'args_show_tagcloud', '1'),
(548, 167, 'args_show_in_quick_edit', '1'),
(549, 167, 'args_show_admin_column', '0'),
(550, 167, 'args_show_in_rest', '1'),
(551, 167, 'args_hierarchical', '1'),
(552, 167, 'args_query_var', '1'),
(553, 167, 'args_sort', '0'),
(554, 167, 'args_rewrite_no_front', '0'),
(555, 167, 'args_rewrite_hierarchical', '0'),
(556, 167, 'function_name', 'your_prefix_register_taxonomy'),
(557, 167, 'text_domain', 'text-domain'),
(558, 168, '_edit_lock', '1579089540:1'),
(559, 168, '_edit_last', '1'),
(560, 168, 'label_name', 'MB Color'),
(561, 168, 'label_singular_name', 'MB Color'),
(562, 168, 'args_taxonomy', 'mb-color'),
(563, 168, 'label_menu_name', 'MB Color'),
(564, 168, 'label_all_items', 'All MB Color'),
(565, 168, 'label_edit_item', 'Edit MB Color'),
(566, 168, 'label_view_item', 'View MB Color'),
(567, 168, 'label_update_item', 'Update MB Color'),
(568, 168, 'label_add_new_item', 'Add new MB Color'),
(569, 168, 'label_new_item_name', 'New MB Color'),
(570, 168, 'label_parent_item', 'Parent MB Color'),
(571, 168, 'label_parent_item_colon', 'Parent MB Color:'),
(572, 168, 'label_search_items', 'Search MB Color'),
(573, 168, 'label_popular_items', 'Popular MB Color'),
(574, 168, 'label_separate_items_with_commas', 'Separate MB Color with commas'),
(575, 168, 'label_add_or_remove_items', 'Add or remove MB Color'),
(576, 168, 'label_choose_from_most_used', 'Choose most used MB Color'),
(577, 168, 'label_not_found', 'No MB Color found'),
(578, 168, 'args_public', '1'),
(579, 168, 'args_show_ui', '1'),
(580, 168, 'args_show_in_menu', '1'),
(581, 168, 'args_show_in_nav_menus', '1'),
(582, 168, 'args_meta_box_cb', '1'),
(583, 168, 'args_show_tagcloud', '1'),
(584, 168, 'args_show_in_quick_edit', '1'),
(585, 168, 'args_show_admin_column', '0'),
(586, 168, 'args_show_in_rest', '1'),
(587, 168, 'args_hierarchical', '1'),
(588, 168, 'args_query_var', '1'),
(589, 168, 'args_sort', '0'),
(590, 168, 'args_rewrite_no_front', '0'),
(591, 168, 'args_rewrite_hierarchical', '0'),
(592, 168, 'function_name', 'your_prefix_register_taxonomy'),
(593, 168, 'text_domain', 'text-domain'),
(614, 165, 'args_supports', 'title'),
(615, 165, 'args_supports', 'editor'),
(616, 165, 'args_supports', 'thumbnail'),
(619, 169, '_edit_lock', '1579188741:1'),
(620, 169, '_edit_last', '1'),
(621, 169, 'label_name', 'MB Manufacture'),
(622, 169, 'label_singular_name', 'MB Manufacture'),
(623, 169, 'args_taxonomy', 'mb-manufacture'),
(624, 169, 'label_menu_name', 'MB Manufacture'),
(625, 169, 'label_all_items', 'All MB Manufacture'),
(626, 169, 'label_edit_item', 'Edit MB Manufacture'),
(627, 169, 'label_view_item', 'View MB Manufacture'),
(628, 169, 'label_update_item', 'Update MB Manufacture'),
(629, 169, 'label_add_new_item', 'Add new MB Manufacture'),
(630, 169, 'label_new_item_name', 'New MB Manufacture'),
(631, 169, 'label_parent_item', 'Parent MB Manufacture'),
(632, 169, 'label_parent_item_colon', 'Parent MB Manufacture:'),
(633, 169, 'label_search_items', 'Search MB Manufacture'),
(634, 169, 'label_popular_items', 'Popular MB Manufacture'),
(635, 169, 'label_separate_items_with_commas', 'Separate MB Manufacture with commas'),
(636, 169, 'label_add_or_remove_items', 'Add or remove MB Manufacture'),
(637, 169, 'label_choose_from_most_used', 'Choose most used MB Manufacture'),
(638, 169, 'label_not_found', 'No MB Manufacture found'),
(639, 169, 'args_public', '1'),
(640, 169, 'args_show_ui', '1'),
(641, 169, 'args_show_in_menu', '1'),
(642, 169, 'args_show_in_nav_menus', '1'),
(643, 169, 'args_meta_box_cb', '1'),
(644, 169, 'args_show_tagcloud', '1'),
(645, 169, 'args_show_in_quick_edit', '1'),
(646, 169, 'args_show_admin_column', '0'),
(647, 169, 'args_show_in_rest', '1'),
(648, 169, 'args_hierarchical', '1'),
(649, 169, 'args_query_var', '1'),
(650, 169, 'args_sort', '0'),
(651, 169, 'args_rewrite_no_front', '0'),
(652, 169, 'args_rewrite_hierarchical', '0'),
(653, 169, 'function_name', 'your_prefix_register_taxonomy'),
(654, 169, 'text_domain', 'text-domain'),
(657, 171, '_edit_lock', '1579092760:1'),
(658, 172, '_edit_lock', '1579092765:1'),
(659, 172, '_edit_last', '1'),
(660, 169, 'args_post_types', 'video-catalog-mb'),
(661, 167, 'args_post_types', 'video-catalog-mb'),
(662, 168, 'args_post_types', 'video-catalog-mb'),
(669, 172, 'mb_tax_cat_videos_id', '106'),
(670, 172, 'mb_tax_cat_videos_id', '110'),
(671, 172, 'mb_tax_cat_videos_id', '109'),
(672, 171, '_edit_last', '1'),
(673, 171, 'mb_tax_cat_videos_id', '110'),
(674, 174, '_edit_lock', '1579092751:1'),
(675, 174, '_edit_last', '1'),
(676, 174, 'mb_tax_cat_videos_id', '109'),
(677, 175, '_edit_lock', '1579092747:1'),
(678, 175, '_edit_last', '1'),
(679, 175, 'mb_tax_cat_videos_id', '110'),
(680, 176, '_edit_lock', '1579092738:1'),
(681, 176, '_edit_last', '1'),
(682, 176, 'mb_tax_cat_videos_id', '110'),
(683, 177, '_edit_lock', '1580315327:1'),
(684, 177, '_edit_last', '1'),
(685, 177, 'mb_tax_cat_videos_id', '110'),
(686, 177, 'mb_tax_cat_videos_id', '106'),
(687, 178, '_edit_lock', '1579090962:1'),
(692, 180, '_edit_lock', '1580317268:1'),
(693, 180, '_wp_page_template', 'default'),
(694, 182, '_menu_item_type', 'post_type'),
(695, 182, '_menu_item_menu_item_parent', '0'),
(696, 182, '_menu_item_object_id', '180'),
(697, 182, '_menu_item_object', 'page'),
(698, 182, '_menu_item_target', ''),
(699, 182, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(700, 182, '_menu_item_xfn', ''),
(701, 182, '_menu_item_url', ''),
(702, 159, '_edit_last', '1'),
(703, 159, '_wpgmp_location_address', ''),
(704, 159, '_wpgmp_location_city', ''),
(705, 159, '_wpgmp_location_state', ''),
(706, 159, '_wpgmp_location_country', ''),
(707, 159, '_wpgmp_metabox_latitude', ''),
(708, 159, '_wpgmp_metabox_longitude', ''),
(709, 159, '_wpgmp_metabox_location_redirect', 'marker'),
(710, 159, '_wpgmp_metabox_custom_link', ''),
(711, 159, '_wpgmp_map_id', 's:2:\"N;\";'),
(712, 159, '_wpgmp_metabox_marker_id', 's:2:\"N;\";'),
(713, 159, '_wpgmp_metabox_taxomomies_terms', 's:2:\"N;\";'),
(714, 159, '_wpgmp_extensions_fields', 's:2:\"N;\";'),
(715, 180, '_edit_last', '1'),
(716, 180, '_wpgmp_location_address', ''),
(717, 180, '_wpgmp_location_city', ''),
(718, 180, '_wpgmp_location_state', ''),
(719, 180, '_wpgmp_location_country', ''),
(720, 180, '_wpgmp_metabox_latitude', ''),
(721, 180, '_wpgmp_metabox_longitude', ''),
(722, 180, '_wpgmp_metabox_location_redirect', 'marker'),
(723, 180, '_wpgmp_metabox_custom_link', ''),
(724, 180, '_wpgmp_map_id', 's:2:\"N;\";'),
(725, 180, '_wpgmp_metabox_marker_id', 's:2:\"N;\";'),
(726, 180, '_wpgmp_metabox_taxomomies_terms', 's:2:\"N;\";'),
(727, 180, '_wpgmp_extensions_fields', 's:2:\"N;\";');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-01-09 15:42:13', '2019-01-09 15:42:13', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-01-09 15:44:49', '2019-01-09 15:44:49', '', 0, 'http://wp-test.loc/?p=1', 0, 'post', '', 1),
(2, 1, '2019-01-09 15:42:13', '2019-01-09 15:42:13', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:columns {\"columns\":3} -->\n<div class=\"wp-block-columns has-3-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://wp-test.loc/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-01-09 15:46:22', '2019-01-09 15:46:22', '', 0, 'http://wp-test.loc/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-01-09 15:42:13', '2019-01-09 15:42:13', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://wp-test.loc.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-01-09 15:42:13', '2019-01-09 15:42:13', '', 0, 'http://wp-test.loc/?page_id=3', 0, 'page', '', 0),
(6, 1, '2019-01-09 15:44:49', '2019-01-09 15:44:49', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-09 15:44:49', '2019-01-09 15:44:49', '', 1, 'http://wp-test.loc/2019/01/09/1-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-01-09 15:46:22', '2019-01-09 15:46:22', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:columns {\"columns\":3} -->\n<div class=\"wp-block-columns has-3-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://wp-test.loc/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-01-09 15:46:22', '2019-01-09 15:46:22', '', 2, 'http://wp-test.loc/2019/01/09/2-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-01-10 10:50:24', '2019-01-10 10:50:24', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=14', 3, 'nav_menu_item', '', 0),
(15, 1, '2019-01-10 10:50:24', '2019-01-10 10:50:24', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=15', 5, 'nav_menu_item', '', 0),
(16, 1, '2019-01-10 10:50:24', '2019-01-10 10:50:24', ' ', '', '', 'publish', 'closed', 'closed', '', '16', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=16', 13, 'nav_menu_item', '', 0),
(17, 1, '2019-01-10 10:50:24', '2019-01-10 10:50:24', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=17', 7, 'nav_menu_item', '', 0),
(18, 1, '2019-01-10 10:51:27', '2019-01-10 10:51:27', ' ', '', '', 'publish', 'closed', 'closed', '', '18', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=18', 9, 'nav_menu_item', '', 0),
(19, 1, '2019-01-10 10:51:27', '2019-01-10 10:51:27', ' ', '', '', 'publish', 'closed', 'closed', '', '19', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=19', 12, 'nav_menu_item', '', 0),
(20, 1, '2019-01-10 11:10:50', '0000-00-00 00:00:00', 'sdfsdfsdfsdfsdf', 'etest sfsdf', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-10 11:10:50', '2019-01-10 11:10:50', '', 0, 'http://wp-test.loc/?post_type=team_member&#038;p=20', 0, 'team_member', '', 0),
(22, 1, '2019-01-28 16:03:21', '2019-01-28 16:03:21', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=22', 11, 'nav_menu_item', '', 0),
(23, 1, '2019-01-29 11:51:39', '2019-01-29 11:51:39', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=23', 8, 'nav_menu_item', '', 0),
(24, 1, '2019-01-29 11:51:39', '2019-01-29 11:51:39', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=24', 6, 'nav_menu_item', '', 0),
(25, 1, '2019-01-29 11:51:39', '2019-01-29 11:51:39', ' ', '', '', 'publish', 'closed', 'closed', '', '25', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=25', 10, 'nav_menu_item', '', 0),
(26, 1, '2019-01-29 12:17:15', '2019-01-29 12:17:15', '<!-- wp:paragraph -->\n<p>\nThis is an example page. It\'s different from a blog post because it will\n stay in one place and will show up in your site navigation (in most \nthemes). Most people start with an About page that introduces them to \npotential site visitors. It might say something like this:\n\n</p>\n<!-- /wp:paragraph -->', 'Meta box plugin', '', 'publish', 'closed', 'closed', '', 'acf', '', '', '2019-10-15 16:09:53', '2019-10-15 16:09:53', '', 0, 'http://wp-test.loc/?page_id=26', 0, 'page', '', 0),
(27, 1, '2019-01-29 12:17:15', '2019-01-29 12:17:15', '<!-- wp:paragraph -->\n<p>\nThis is an example page. It\'s different from a blog post because it will\n stay in one place and will show up in your site navigation (in most \nthemes). Most people start with an About page that introduces them to \npotential site visitors. It might say something like this:\n\n</p>\n<!-- /wp:paragraph -->', 'Sample 2', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-29 12:17:15', '2019-01-29 12:17:15', '', 26, 'http://wp-test.loc/2019/01/29/26-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2019-01-29 12:18:32', '2019-01-29 12:18:32', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=28', 1, 'nav_menu_item', '', 0),
(34, 1, '2019-09-23 16:06:56', '2019-09-23 16:06:56', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2019-09-23 16:06:56', '2019-09-23 16:06:56', '', 1, 'http://wp-test.loc/2019/09/23/1-autosave-v1/', 0, 'revision', '', 0),
(106, 1, '2019-09-25 16:27:47', '2019-09-25 16:27:47', '', 'Green Table', 'Green Table', 'inherit', 'open', 'closed', '', 'samplevideo_1280x720_1mb', '', '', '2019-09-27 11:49:40', '2019-09-27 11:49:40', '', 0, 'http://wp-test.loc/wp-content/uploads/2019/09/SampleVideo_1280x720_1mb.mp4', 0, 'attachment', 'video/mp4', 0),
(107, 1, '2019-09-25 16:28:05', '2019-09-25 16:28:05', '', 'SampleVideo_1280x720_1mb', '', 'inherit', 'open', 'closed', '', 'samplevideo_1280x720_1mb-2', '', '', '2019-09-25 16:28:05', '2019-09-25 16:28:05', '', 0, 'http://wp-test.loc/wp-content/uploads/2019/09/SampleVideo_1280x720_1mb.mkv', 0, 'attachment', 'video/x-matroska', 0),
(108, 1, '2019-09-25 16:28:24', '2019-09-25 16:28:24', '', 'SampleVideo_1280x720_1mb', '', 'inherit', 'open', 'closed', '', 'samplevideo_1280x720_1mb-3', '', '', '2019-09-25 16:28:24', '2019-09-25 16:28:24', '', 0, 'http://wp-test.loc/wp-content/uploads/2019/09/SampleVideo_1280x720_1mb.flv', 0, 'attachment', 'video/x-flv', 0),
(109, 1, '2019-09-25 16:33:38', '2019-09-25 16:33:38', '', '2SampleVideo_1280x720_1mb', '', 'inherit', 'open', 'closed', '', '2samplevideo_1280x720_1mb', '', '', '2019-09-25 16:33:38', '2019-09-25 16:33:38', '', 0, 'http://wp-test.loc/wp-content/uploads/2019/09/2SampleVideo_1280x720_1mb.mp4', 0, 'attachment', 'video/mp4', 0),
(110, 1, '2019-09-25 16:43:22', '2019-09-25 16:43:22', '', 'Red Table', '', 'inherit', 'open', 'closed', '', '3samplevideo_1280x720_1mb', '', '', '2019-10-02 16:21:58', '2019-10-02 16:21:58', '', 0, 'http://wp-test.loc/wp-content/uploads/2019/09/3SampleVideo_1280x720_1mb.mp4', 0, 'attachment', 'video/mp4', 0),
(119, 1, '2019-10-02 13:05:07', '2019-10-02 13:05:07', '<!-- wp:paragraph -->\n<p>\nThis is an example page. It\'s different from a blog post because it will\n stay in one place and will show up in your site navigation (in most \nthemes). Most people start with an About page that introduces them to \npotential site visitors. It might say something like this:\n\n</p>\n<!-- /wp:paragraph -->', 'ACF plugin', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-10-02 13:05:07', '2019-10-02 13:05:07', '', 26, 'http://wp-test.loc/2019/10/02/26-revision-v1/', 0, 'revision', '', 0),
(143, 1, '2019-10-03 12:59:45', '2019-10-03 12:59:45', '', 'Red Bath 2 | Axent', '', 'publish', 'closed', 'closed', '', 'red-bath', '', '', '2019-10-15 09:35:12', '2019-10-15 09:35:12', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=143', 0, 'tax_cat_videos', '', 0),
(144, 1, '2019-10-03 13:07:23', '2019-10-03 13:07:23', '', 'Green Bath | Puma', '', 'publish', 'closed', 'closed', '', 'green-bath', '', '', '2019-10-15 09:01:05', '2019-10-15 09:01:05', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=144', 0, 'tax_cat_videos', '', 0),
(146, 1, '2019-10-03 13:09:00', '2019-10-03 13:09:00', '', 'Red Kitchen', '', 'publish', 'closed', 'closed', '', 'red-kitchen', '', '', '2019-10-16 13:15:08', '2019-10-16 13:15:08', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=146', 0, 'tax_cat_videos', '', 0),
(147, 1, '2019-10-03 13:09:18', '2019-10-03 13:09:18', '', 'Yellow Kitchen | Nike', '', 'publish', 'closed', 'closed', '', 'yellow-kitchen', '', '', '2019-10-11 12:34:28', '2019-10-11 12:34:28', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=147', 0, 'tax_cat_videos', '', 0),
(148, 1, '2019-10-03 13:10:18', '2019-10-03 13:10:18', '', 'Black Kitchen', '', 'publish', 'closed', 'closed', '', 'black-kitchen', '', '', '2019-10-05 20:21:51', '2019-10-05 20:21:51', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=148', 0, 'tax_cat_videos', '', 0),
(150, 1, '2019-10-05 19:54:03', '2019-10-05 19:54:03', '', 'Blue Kitchen 3 | Adidas - Axent - Nike - Puma', '', 'publish', 'closed', 'closed', '', 'blue-kitchen', '', '', '2019-10-15 09:35:42', '2019-10-15 09:35:42', '', 0, 'http://wp-test.loc/?post_type=tax_cat_videos&#038;p=150', 0, 'tax_cat_videos', '', 0),
(153, 1, '2019-10-15 16:09:53', '2019-10-15 16:09:53', '<!-- wp:paragraph -->\n<p>\nThis is an example page. It\'s different from a blog post because it will\n stay in one place and will show up in your site navigation (in most \nthemes). Most people start with an About page that introduces them to \npotential site visitors. It might say something like this:\n\n</p>\n<!-- /wp:paragraph -->', 'Meta box plugin', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-10-15 16:09:53', '2019-10-15 16:09:53', '', 26, 'http://wp-test.loc/2019/10/15/26-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2019-12-06 15:48:29', '2019-12-06 15:48:29', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'publish', 'closed', 'closed', '', 'map-google-api', '', '', '2020-02-03 16:46:25', '2020-02-03 16:46:25', '', 0, 'http://wp-test.loc/?page_id=159', 0, 'page', '', 0),
(160, 1, '2019-12-06 15:48:29', '2019-12-06 15:48:29', '', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2019-12-06 15:48:29', '2019-12-06 15:48:29', '', 159, 'http://wp-test.loc/2019/12/06/159-revision-v1/', 0, 'revision', '', 0),
(161, 1, '2019-12-06 15:48:53', '2019-12-06 15:48:53', '<!-- wp:paragraph -->\n<p>JS Google Map</p>\n<!-- /wp:paragraph -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2019-12-06 15:48:53', '2019-12-06 15:48:53', '', 159, 'http://wp-test.loc/2019/12/06/159-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2019-12-06 16:02:13', '2019-12-06 16:02:13', '', 'Map', '', 'publish', 'closed', 'closed', '', 'map', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=162', 4, 'nav_menu_item', '', 0),
(164, 1, '2019-12-27 16:35:52', '2019-12-27 16:35:52', '', 'Custom HTML', '', 'publish', 'closed', 'closed', '', 'custom-html', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=164', 14, 'nav_menu_item', '', 0),
(165, 1, '2020-01-15 09:52:34', '2020-01-15 09:52:34', '', 'Video Catalog with MB post', '', 'publish', 'closed', 'closed', '', 'auto-draft', '', '', '2020-01-15 10:01:18', '2020-01-15 10:01:18', '', 0, 'http://wp-test.loc/?post_type=mb-post-type&#038;p=165', 0, 'mb-post-type', '', 0),
(167, 1, '2020-01-15 09:55:24', '2020-01-15 09:55:24', '', 'MB Categories', '', 'publish', 'closed', 'closed', '', 'auto-draft', '', '', '2020-01-15 12:01:18', '2020-01-15 12:01:18', '', 0, 'http://wp-test.loc/?post_type=mb-taxonomy&#038;p=167', 0, 'mb-taxonomy', '', 0),
(168, 1, '2020-01-15 09:57:43', '2020-01-15 09:57:43', '', 'MB Color', '', 'publish', 'closed', 'closed', '', 'auto-draft-2', '', '', '2020-01-15 12:01:22', '2020-01-15 12:01:22', '', 0, 'http://wp-test.loc/?post_type=mb-taxonomy&#038;p=168', 0, 'mb-taxonomy', '', 0),
(169, 1, '2020-01-15 10:02:31', '2020-01-15 10:02:31', '', 'MB Manufacture', '', 'publish', 'closed', 'closed', '', 'auto-draft-3', '', '', '2020-01-15 12:00:42', '2020-01-15 12:00:42', '', 0, 'http://wp-test.loc/?post_type=mb-taxonomy&#038;p=169', 0, 'mb-taxonomy', '', 0),
(171, 1, '2020-01-15 12:06:04', '2020-01-15 12:06:04', '', 'MB Orange Kitchen', '', 'publish', 'closed', 'closed', '', 'orange-kitchen', '', '', '2020-01-15 12:52:40', '2020-01-15 12:52:40', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=171', 0, 'video-catalog-mb', '', 0),
(172, 1, '2020-01-15 10:20:27', '2020-01-15 10:20:27', '', 'MB Blue Kitchen 3 | Adidas - Axent - Nike - Puma', '', 'publish', 'closed', 'closed', '', 'blue-kitchen-3-adidas-axent-nike-puma', '', '', '2020-01-15 12:52:45', '2020-01-15 12:52:45', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=172', 0, 'video-catalog-mb', '', 0),
(174, 1, '2020-01-15 12:07:05', '2020-01-15 12:07:05', '', 'MB Red Kitchen | Nike', '', 'publish', 'closed', 'closed', '', 'red-kitchen-nike', '', '', '2020-01-15 12:52:31', '2020-01-15 12:52:31', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=174', 0, 'video-catalog-mb', '', 0),
(175, 1, '2020-01-15 12:08:29', '2020-01-15 12:08:29', '', 'MB Red | Blue Kitchen', '', 'publish', 'closed', 'closed', '', 'red-blue-kitchen', '', '', '2020-01-15 12:52:27', '2020-01-15 12:52:27', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=175', 0, 'video-catalog-mb', '', 0),
(176, 1, '2020-01-15 12:09:15', '2020-01-15 12:09:15', '', 'MB Blue Bath | Puma', '', 'publish', 'closed', 'closed', '', 'blue-bath-puma', '', '', '2020-01-15 12:52:18', '2020-01-15 12:52:18', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=176', 0, 'video-catalog-mb', '', 0),
(177, 1, '2020-01-15 12:10:03', '2020-01-15 12:10:03', '', 'MB Red Bath 2 | Axent', '', 'publish', 'closed', 'closed', '', 'red-bath-2-axent', '', '', '2020-01-15 12:51:52', '2020-01-15 12:51:52', '', 0, 'http://wp-test.loc/?post_type=video-catalog-mb&#038;p=177', 0, 'video-catalog-mb', '', 0),
(178, 1, '2020-01-15 12:24:30', '2020-01-15 12:24:30', '', 'Test post', '', 'publish', 'open', 'open', '', 'test-post', '', '', '2020-01-15 12:25:01', '2020-01-15 12:25:01', '', 0, 'http://wp-test.loc/?p=178', 0, 'post', '', 0),
(179, 1, '2020-01-15 12:24:30', '2020-01-15 12:24:30', '', 'Test post', '', 'inherit', 'closed', 'closed', '', '178-revision-v1', '', '', '2020-01-15 12:24:30', '2020-01-15 12:24:30', '', 178, 'http://wp-test.loc/2020/01/15/178-revision-v1/', 0, 'revision', '', 0),
(180, 1, '2020-01-15 12:43:03', '2020-01-15 12:43:03', '<!-- wp:html -->\n<p>Loremus Ipsimus</p>\n<!-- /wp:html -->', 'MB plugin and Post Type with plugin', '', 'publish', 'closed', 'closed', '', 'mb-plugin-and-post-type-with-plugin', '', '', '2020-01-29 17:03:21', '2020-01-29 17:03:21', '', 0, 'http://wp-test.loc/?page_id=180', 0, 'page', '', 0),
(181, 1, '2020-01-15 12:43:03', '2020-01-15 12:43:03', '<!-- wp:paragraph -->\n<p>Loremus Ipsimus</p>\n<!-- /wp:paragraph -->', 'MB plugin and Post Type with plugin', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-01-15 12:43:03', '2020-01-15 12:43:03', '', 180, 'http://wp-test.loc/2020/01/15/180-revision-v1/', 0, 'revision', '', 0),
(182, 1, '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 'MB & Post Type with plugin', '', 'publish', 'closed', 'closed', '', 'mb-post-type-with-plugin', '', '', '2020-01-15 12:44:13', '2020-01-15 12:44:13', '', 0, 'http://wp-test.loc/?p=182', 2, 'nav_menu_item', '', 0),
(184, 1, '2020-01-29 16:32:08', '2020-01-29 16:32:08', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=1]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-01-29 16:32:08', '2020-01-29 16:32:08', '', 159, 'http://wp-test.loc/2020/01/29/159-revision-v1/', 0, 'revision', '', 0),
(186, 1, '2020-01-29 17:02:54', '2020-01-29 17:02:54', '<!-- wp:paragraph -->\n<p>Loremus Ipsimus</p>\n[put_wpgm id=2]\n<!-- /wp:paragraph -->', 'MB plugin and Post Type with plugin', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-01-29 17:02:54', '2020-01-29 17:02:54', '', 180, 'http://wp-test.loc/2020/01/29/180-revision-v1/', 0, 'revision', '', 0),
(187, 1, '2020-01-29 17:03:05', '2020-01-29 17:03:05', '<!-- wp:html -->\n<p>Loremus Ipsimus</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'MB plugin and Post Type with plugin', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-01-29 17:03:05', '2020-01-29 17:03:05', '', 180, 'http://wp-test.loc/2020/01/29/180-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2020-01-29 17:03:20', '2020-01-29 17:03:20', '<!-- wp:html -->\n<p>Loremus Ipsimus</p>\n<!-- /wp:html -->', 'MB plugin and Post Type with plugin', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-01-29 17:03:20', '2020-01-29 17:03:20', '', 180, 'http://wp-test.loc/2020/01/29/180-revision-v1/', 0, 'revision', '', 0),
(189, 1, '2020-01-29 17:03:41', '2020-01-29 17:03:41', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-01-29 17:03:41', '2020-01-29 17:03:41', '', 159, 'http://wp-test.loc/2020/01/29/159-revision-v1/', 0, 'revision', '', 0),
(190, 1, '2020-01-30 12:35:15', '2020-01-30 12:35:15', '<!-- wp:html -->\n<p>JS Google Map</p>[contact]\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-01-30 12:35:15', '2020-01-30 12:35:15', '', 159, 'http://wp-test.loc/2020/01/30/159-revision-v1/', 0, 'revision', '', 0),
(191, 1, '2020-01-30 12:42:50', '2020-01-30 12:42:50', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-01-30 12:42:50', '2020-01-30 12:42:50', '', 159, 'http://wp-test.loc/2020/01/30/159-revision-v1/', 0, 'revision', '', 0),
(193, 1, '2020-02-03 14:34:00', '2020-02-03 14:34:00', '<!-- wp:html -->\n<p>JS Google Map</p>[contact-form-7 id=\"192\" title=\"Broker Form\"]\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 14:34:00', '2020-02-03 14:34:00', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0),
(194, 1, '2020-02-03 14:34:21', '2020-02-03 14:34:21', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 14:34:21', '2020-02-03 14:34:21', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0),
(195, 1, '2020-02-03 15:09:15', '2020-02-03 15:09:15', '<!-- wp:html -->\n<p>JS Google Map</p><a id=\"broker-form\" href=\"http://google.com\">Send Message</a>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 15:09:15', '2020-02-03 15:09:15', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0),
(196, 1, '2020-02-03 16:42:53', '2020-02-03 16:42:53', '<!-- wp:html -->\n<p>JS Google Map</p><a id=\"broker-form\" href=\"#\">Send Message</a>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 16:42:53', '2020-02-03 16:42:53', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0),
(197, 1, '2020-02-03 16:46:17', '2020-02-03 16:46:17', '<!-- wp:html -->\n<p>JS Google Map</p><a id=\"broker-form2\" href=\"#\">Send Message</a>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 16:46:17', '2020-02-03 16:46:17', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0),
(198, 1, '2020-02-03 16:46:25', '2020-02-03 16:46:25', '<!-- wp:html -->\n<p>JS Google Map</p>\n[put_wpgm id=2]\n<!-- /wp:html -->', 'Map Google API', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-03 16:46:25', '2020-02-03 16:46:25', '', 159, 'http://wp-test.loc/2020/02/03/159-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(4, 'Kitchens', 'kitchens', 0),
(5, 'Baths', 'baths', 0),
(7, 'Adidas', 'adidas', 0),
(8, 'Empty', 'empty', 0),
(9, 'Puma', 'puma', 0),
(10, 'Nike', 'nike', 0),
(11, 'Axent', 'axent', 0),
(12, 'Red', 'red', 0),
(13, 'MB Bath', 'mb-bath', 0),
(14, 'MB Kitchens', 'mb-kitchens', 0),
(15, 'MB Empty', 'mb-empty', 0),
(16, 'MB Red', 'mb-red', 0),
(17, 'MB Blue', 'mb-blue', 0),
(18, 'MB Orange', 'mb-orange', 0),
(19, 'MB Adidas', 'mb-adidas', 0),
(20, 'MB Nike', 'mb-nike', 0),
(21, 'MB Axent', 'mb-axent', 0),
(22, 'MB Puma', 'mb-puma', 0),
(23, 'news', 'news', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(14, 2, 0),
(15, 2, 0),
(16, 2, 0),
(17, 2, 0),
(18, 2, 0),
(19, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(25, 2, 0),
(28, 2, 0),
(143, 5, 0),
(143, 11, 0),
(144, 5, 0),
(144, 9, 0),
(146, 4, 0),
(146, 12, 0),
(147, 4, 0),
(147, 10, 0),
(148, 4, 0),
(150, 4, 0),
(150, 7, 0),
(150, 9, 0),
(150, 10, 0),
(150, 11, 0),
(162, 2, 0),
(164, 2, 0),
(171, 14, 0),
(171, 18, 0),
(172, 14, 0),
(172, 17, 0),
(172, 19, 0),
(172, 20, 0),
(172, 21, 0),
(172, 22, 0),
(174, 14, 0),
(174, 16, 0),
(174, 20, 0),
(175, 14, 0),
(175, 16, 0),
(175, 17, 0),
(176, 13, 0),
(176, 17, 0),
(176, 22, 0),
(177, 13, 0),
(177, 16, 0),
(177, 21, 0),
(178, 1, 0),
(178, 23, 0),
(182, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'nav_menu', '', 0, 14),
(4, 4, 'catalog', '', 0, 4),
(5, 5, 'catalog', '', 0, 2),
(7, 7, 'manufacture', '', 0, 1),
(8, 8, 'catalog', '', 0, 0),
(9, 9, 'manufacture', '', 0, 2),
(10, 10, 'manufacture', '', 0, 2),
(11, 11, 'manufacture', '', 0, 2),
(12, 12, 'color', '', 0, 1),
(13, 13, 'mb-categories', '', 0, 2),
(14, 14, 'mb-categories', '', 0, 4),
(15, 15, 'mb-categories', '', 0, 0),
(16, 16, 'mb-color', '', 0, 3),
(17, 17, 'mb-color', '', 0, 3),
(18, 18, 'mb-color', '', 0, 1),
(19, 19, 'mb-manufacture', '', 0, 1),
(20, 20, 'mb-manufacture', '', 0, 2),
(21, 21, 'mb-manufacture', '', 0, 2),
(22, 22, 'mb-manufacture', '', 0, 2),
(23, 23, 'post_tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,text_widget_custom_html'),
(15, 1, 'default_password_nag', ''),
(16, 1, 'show_welcome_panel', '1'),
(18, 1, 'wp_dashboard_quick_press_last_post_id', '183'),
(19, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(22, 1, 'nav_menu_recently_edited', '2'),
(23, 1, 'closedpostboxes_post', 'a:0:{}'),
(24, 1, 'metaboxhidden_post', 'a:0:{}'),
(25, 1, 'wp_user-settings', 'editor=html&libraryContent=browse&mfold=o'),
(26, 1, 'wp_user-settings-time', '1577464099'),
(27, 1, 'closedpostboxes_videos', 'a:0:{}'),
(28, 1, 'metaboxhidden_videos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(29, 1, 'wp_media_library_mode', 'grid'),
(30, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(31, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(32, 1, 'closedpostboxes_acf_cat_videos', 'a:0:{}'),
(33, 1, 'metaboxhidden_acf_cat_videos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(34, 1, 'closedpostboxes_tax_cat_videos', 'a:0:{}'),
(35, 1, 'metaboxhidden_tax_cat_videos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(36, 1, 'meta-box-order_tax_cat_videos', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:35:\"submitdiv,catalogdiv,manufacturediv\";s:6:\"normal\";s:28:\"videos_tax,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(37, 1, 'screen_layout_tax_cat_videos', '2'),
(52, 3, 'nickname', 'Sunny'),
(53, 3, 'first_name', ''),
(54, 3, 'last_name', ''),
(55, 3, 'description', ''),
(56, 3, 'rich_editing', 'true'),
(57, 3, 'syntax_highlighting', 'true'),
(58, 3, 'comment_shortcuts', 'false'),
(59, 3, 'admin_color', 'fresh'),
(60, 3, 'use_ssl', '0'),
(61, 3, 'show_admin_bar_front', 'false'),
(62, 3, 'locale', ''),
(63, 3, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(64, 3, 'wp_user_level', '0'),
(65, 3, 'default_password_nag', ''),
(67, 4, 'nickname', 'Sunny2'),
(68, 4, 'first_name', ''),
(69, 4, 'last_name', ''),
(70, 4, 'description', ''),
(71, 4, 'rich_editing', 'true'),
(72, 4, 'syntax_highlighting', 'true'),
(73, 4, 'comment_shortcuts', 'false'),
(74, 4, 'admin_color', 'fresh'),
(75, 4, 'use_ssl', '0'),
(76, 4, 'show_admin_bar_front', 'true'),
(77, 4, 'locale', ''),
(78, 4, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(79, 4, 'wp_user_level', '0'),
(80, 4, 'default_password_nag', '1'),
(81, 1, 'session_tokens', 'a:1:{s:64:\"8c4628a5a56ff575b3d8a3f50e2adbdc3bd462121c8710d5fde0edc37ceeb710\";a:4:{s:10:\"expiration\";i:1581086700;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0\";s:5:\"login\";i:1580913900;}}'),
(82, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(83, 1, 'closedpostboxes_video-catalog-mb', 'a:0:{}'),
(84, 1, 'metaboxhidden_video-catalog-mb', 'a:0:{}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bf6mImLQLUqpmEzO3SHth32Q0OIDQ61', 'admin', 'lvasoul1982@gmail.com', '', '2019-01-09 15:42:13', '', 0, 'admin'),
(3, 'Sunny', '$P$B.Vm3XB8s7H2KfCCnQPudLkey2lw.p0', 'sunny', 'void2@jvlicenses.com', '', '2019-11-11 13:08:43', '', 0, 'Sunny'),
(4, 'Sunny2', '$P$B2qZWE5h60NES8LBmegFvJeUEl5D//0', 'sunny2', 'void@jvlicenses.com', '', '2019-11-11 13:35:39', '1573479341:$P$BtWYmZAnNTtyILVzGjhGfXT5Q7rsGd0', 0, 'Sunny2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_create_map`
--
ALTER TABLE `wp_create_map`
  ADD PRIMARY KEY (`map_id`);

--
-- Indexes for table `wp_group_map`
--
ALTER TABLE `wp_group_map`
  ADD PRIMARY KEY (`group_map_id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_map_locations`
--
ALTER TABLE `wp_map_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `wp_map_routes`
--
ALTER TABLE `wp_map_routes`
  ADD PRIMARY KEY (`route_id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_create_map`
--
ALTER TABLE `wp_create_map`
  MODIFY `map_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_group_map`
--
ALTER TABLE `wp_group_map`
  MODIFY `group_map_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_map_locations`
--
ALTER TABLE `wp_map_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `wp_map_routes`
--
ALTER TABLE `wp_map_routes`
  MODIFY `route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1343;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=728;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
