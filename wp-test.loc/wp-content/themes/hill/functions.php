<?php
define('HILL_THEME_URI',  get_template_directory_uri());
define('HILL_THEME_DIR',  get_template_directory());
define('HILL_CSS_URL',  get_template_directory_uri().'/css');
define('HILL_JS_URL',  get_template_directory_uri().'/js');
define('HILL_FONTS_URL',  get_template_directory_uri().'/fonts/font-awesome/css');
define('HILL_IMG_URL', HILL_THEME_URI.'/images/');
define('HILL_FREAMWORK_DIRECTORY', HILL_THEME_DIR.'/framework/');

/*
 * Enable support TGM features.
*/
require_once(HILL_FREAMWORK_DIRECTORY."class-tgm-plugin-activation.php");

/*
 * Enable support TGM configuration.
*/
require_once(HILL_FREAMWORK_DIRECTORY."config.tgm.php");

/*
 * Load Widgets
*/
require_once(HILL_FREAMWORK_DIRECTORY."widgets/swpf-products-carousel.php");
require_once(HILL_FREAMWORK_DIRECTORY."widgets/smart-latest-post-widget.php");

/*
 * Enable side bar generator.
*/
require_once(HILL_FREAMWORK_DIRECTORY.'sidebar_generator.php');

/*
 * Meta Box Configuration Post Meta Option
 */
require_once(HILL_FREAMWORK_DIRECTORY."config.meta.box.php");

/**
 * radium installer
 */
require_once(HILL_FREAMWORK_DIRECTORY."framework_customize.php");

/**
 * Load Redux Config File
 */
require get_template_directory() . '/framework/redux.config.php';
require get_template_directory() . '/css/custom_style.php';

add_action('init','hill_init');
function hill_init(){
    global $hill_option;
	if(function_exists('YITH_WCQV_Frontend')){
		remove_action( 'woocommerce_after_shop_loop_item', array( YITH_WCQV_Frontend(), 'yith_add_quick_view_button' ), 15 );
		add_action( 'hill_before_shop_loop_item_title_a', array( YITH_WCQV_Frontend(), 'yith_add_quick_view_button' ), 2 );
	}
    if(isset($hill_option['is_shop_enable']) && !$hill_option['is_shop_enable']){
        remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
    }
}

function hill_get_all_options(){
    global $hill_option;
    return $hill_option;
}

/**
 * hill-theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hill-theme
 */


if ( ! function_exists( 'hill_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function hill_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on hill-theme, use a find and replace
	 * to change 'hill' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'hill', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'woocommerce' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'hill' ),
		'mobile' => esc_html__( 'Mobile Menu', 'hill' ),
		'cat_menu' => esc_html__( 'Category Menu', 'hill' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio'
	) );

	/*
	 * Enable support for custom-background.
	 */

	$defaults = array(
		'default-color'          => '',
		'default-image'          => '',
		'default-repeat'         => '',
		'default-position-x'     => '',
		'default-attachment'     => '',
		'wp-head-callback'       => '_custom_background_cb',
		'admin-head-callback'    => '',
		'admin-preview-callback' => ''
	);
	add_theme_support( 'custom-background', $defaults );

	//Add custom thumb size
	add_image_size( 'hill-grid-thumb', 370, 310, true );
}
endif; // hill_setup
add_action( 'after_setup_theme', 'hill_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hill_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hill_content_width', 640 );
}
add_action( 'after_setup_theme', 'hill_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hill_widgets_init() {
	
	register_sidebar( array(
		'name'          => esc_html__( 'Widget Area', 'hill' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'shop', 'hill' ),
		'id'            => 'sidebar-shop',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Page', 'hill' ),
		'id'            => 'sidebar-page',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'hill' ),
		'id'            => 'footer_col_1',
		'description'   => esc_html__( 'User subscription form.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget footer_box %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'hill' ),
		'id'            => 'footer_col_2',
		'description'   => esc_html__( 'User subscription form.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget footer_box %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 3', 'hill' ),
		'id'            => 'footer_col_3',
		'description'   => esc_html__( 'User subscription form.', 'hill' ),
		'before_widget' => '<aside id="%1$s" class="widget footer_box %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );


}
add_action( 'widgets_init', 'hill_widgets_init' );


function hill_front_init_js_var() {
    global $yith_wcwl, $post;
    ?>
    <script type="text/javascript">
        var hill_PRODUCT_PAGE = false;
        var THEMEURL = '<?php echo HILL_THEME_URI ?>';
        var IMAGEURL = '<?php echo HILL_THEME_URI ?>/images';
        var CSSURL = '<?php echo HILL_THEME_URI ?>/css';
    <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) { ?>
            var hill_ADD_TO_WISHLIST_SUCCESS_TEXT = '<?php printf(preg_replace_callback('/(\r|\n|\t)+/',function($match) {return "";}, wp_kses(__('Product successfully added to wishlist. <a href="%s">Browse Wishlist</a>', 'hill'), array( 'a'=> array('href' => array())) )), $yith_wcwl->get_wishlist_url()) ?>';
            var hill_ADD_TO_WISHLIST_EXISTS_TEXT = '<?php printf(preg_replace_callback('/(\r|\n|\t)+/', function($match) {return "";}, wp_kses(__('The product is already in the wishlist! <a href="%s">Browse Wishlist</a>', 'hill'), array( 'a'=> array( 'href' => array())) )), $yith_wcwl->get_wishlist_url()) ?>';
    <?php } ?>
    <?php if (is_singular('product')) { ?>
            hill_PRODUCT_PAGE = true;
    <?php } ?>
    </script>
    <?php
}
add_action('wp_head', 'hill_front_init_js_var', 1);

/**
 * Enqueue scripts and styles.
 */
add_action('wp_enqueue_scripts', 'hill_scripts', 40);
function hill_scripts() {


	// Load Google 
	wp_enqueue_style( 'oswald', "//fonts.googleapis.com/css?family=Oswald%3A300%2Cregular%2C700&#038", '', '4.7.4' );
	wp_enqueue_style( 'bootstrap', HILL_CSS_URL . "/bootstrap.min.css", '', '3.3.5' );
	wp_enqueue_style( 'owl-carousel', HILL_CSS_URL . "/owl.carousel.css", '', '3.3.5' );
	wp_enqueue_style( 'owl-theme', HILL_CSS_URL . "/owl.theme.css", '', '3.3.5' );
	wp_enqueue_style( 'owl-transitions', HILL_CSS_URL . "/owl.transitions.css", '', '3.3.5' );
	wp_enqueue_style( 'bxslider-style', HILL_CSS_URL . "/jquery.bxslider.css", '', '3.3.5' );
	wp_enqueue_style( 'animate', HILL_CSS_URL . "/animate.css", '', '3.3.2' );
	wp_enqueue_style( 'font-awesome', HILL_FONTS_URL . "/font-awesome.min.css", '', '3.3.5' );
	wp_enqueue_style( 'custom_plugin_style', HILL_THEME_URI . '/css/custom_plugin_style.css');

	if (function_exists('vc_asset_url') && defined('WPB_VC_VERSION')) {
		wp_register_style( 'vc_tta_style', vc_asset_url( 'css/js_composer_tta.min.css' ), false, WPB_VC_VERSION );
		wp_enqueue_style('vc_tta_style');
	}
	

	wp_enqueue_style( 'hill-style', get_stylesheet_uri() );
	wp_enqueue_style( 'custome-responsive', HILL_CSS_URL . "/custom_responsive.css", '', '3.3.4' );

	$hill_custom_inline_style = '';

	if (function_exists( 'hill_get_custom_styles' )) {
		$hill_custom_inline_style = hill_get_custom_styles();
	}

	wp_add_inline_style( 'hill-style', $hill_custom_inline_style );

	wp_enqueue_script( 'jquery-ui-spinner' );
	wp_enqueue_script( 'hill-bootstrap', HILL_THEME_URI . '/js/bootstrap.min.js', array('jquery','jquery-ui-core'), '201202124', true );
	wp_enqueue_script( 'bxslider', HILL_THEME_URI . '/js/jquery.bxslider.min.js', array('jquery'), '201202124', true );
	wp_enqueue_script( 'owl-carousel', HILL_THEME_URI . '/js/owl.carousel.min.js', array('jquery'), '201202124', true );
	wp_enqueue_script( 'wow', HILL_THEME_URI . '/js/wow.min.js', array('jquery'), '201202120', true );
	wp_enqueue_script( 'isotope', HILL_THEME_URI . '/js/isotope.pkgd.min.js', array('jquery'), '201202120', true );
	wp_enqueue_script( 'hill-cd-plugin', HILL_THEME_URI . '/js/jquery.plugin.min.js', array('jquery'), '201202127', false );
	wp_enqueue_script( 'hill-cd-main', HILL_THEME_URI . '/js/jquery.countdown.min.js', array('jquery','hill-cd-plugin'), '201202127', false );
	wp_enqueue_script( 'hill-comoon', HILL_THEME_URI . '/js/common.js', array('jquery'), '201202127', true );
	wp_enqueue_script( 'hill-navigation', HILL_THEME_URI . '/js/navigation.js', array(), '20120206', true );
    wp_enqueue_script( 'hill-flexslider', HILL_THEME_URI . '/js/jquery.flexslider-min.js', array('jquery'));
    

	wp_enqueue_script( 'hill-skip-link-focus-fix', HILL_THEME_URI . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
 
/**
 * Enque script in admin header
 */


function hill_pw_load_scripts($hook) {
 
	if( $hook != 'edit.php' && $hook != 'post.php' && $hook != 'post-new.php' ) 
		return;
 
	wp_enqueue_script( 'custom-js',  get_template_directory_uri() . '/js/admin.js' );
	wp_enqueue_style( 'custom-admin', HILL_CSS_URL . "/admin.css", '', '1.0' );
}
add_action('admin_enqueue_scripts', 'hill_pw_load_scripts');


if (function_exists('vc_add_param')) {
	$attributes = array(
		'type' => 'dropdown',
		'heading' => "Tab Style",
		'param_name' => 'h_tab_style',
		'value' => array(
			esc_html__('Default', 'hill') => 'hill_tabs',
			esc_html__('Short Style', 'hill') => 'hill_wc_products_tab',
		),
		'description' => esc_html__( "New style attribute", "hill" )
	);
	vc_add_param( 'vc_tta_tabs', $attributes );
}


function hill_custom_class_vc_tab($class, $tag, $atts)
{
	if ($tag == 'vc_tta_tabs' && isset($atts['h_tab_style']) && !empty($atts['h_tab_style'])) {
		$class .= ' '.esc_attr($atts['h_tab_style']);

	}

	if ($tag == 'vc_text_separator') {
		$class .= ' hill_sec_title';

	}

	return $class;
}
add_filter('vc_shortcodes_css_class', 'hill_custom_class_vc_tab', 10, 3);

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

require get_template_directory() . '/inc/custom-header.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



$sds_post_comment_count = 0;

function hill_comment_cb($comment, $args, $depth) {

	global $sds_post_comment_count;
    $max_depth = $args['max_depth'];
    
    $liclass = ( ++$sds_post_comment_count % 2 == 0 ) ? 'even' : 'odd';
    ?>
    <li class="<?php echo esc_attr($liclass . " depth-{$depth}") ?>">
    	<div class="reply">
			<?php
				$arr = array('reply_text' => '<i class="fa fa-mail-reply"></i>');
				comment_reply_link(array_merge($arr, array('depth' => $depth, 'max_depth' => $max_depth)));
			?>
		</div>
    	<div class="hill-author-comments-box">
	    	<div class="row">
		    	<div class="col-lg-2">
		    		<?php echo get_avatar($comment->user_id, 100, '', $comment->comment_author); ?>
		    	</div>
		    	<div class="col-lg-10">
			    	<div class="hill-author-comments-meta">
			        	<div itemtype="http://schema.org/UserComments" itemscope="" itemprop="comment" id="comment-<?php echo esc_attr($comment->comment_ID) ?>">
				            <div class="created">
				            	<em class="name"><?php echo esc_html($comment->comment_author); ?></em>
				            	<?php
				            		$commenter_data = get_userdata($comment->user_id);
				            		if($commenter_data && isset($commenter_data->caps['administrator']) && $commenter_data->caps['administrator']):
				            	?>
				            		<span class="h_admin_label"><?php esc_html_e('Admin', 'hill'); ?></span>
				            	<?php endif; ?>
				                <span itemprop="commentTime">
				                	<i class="fa fa-calendar"></i> <?php echo esc_html(date(get_option('date_format'), strtotime($comment->comment_date))); ?>
				                	<?php printf("<small>".esc_html__("at %s", 'hill')."</small>", date(get_option('time_format'), strtotime($comment->comment_date))); ?>
				                </span>
				            </div>
				            <p itemprop="commentText"><?php echo get_comment_text($comment->comment_ID); ?></p>
			            </div>
			        </div>
		        </div>
	        </div>
        </div>
    <?php
}


/* Overwrite WooCommerce DOM postion */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_before_subcategory', 'woocommerce_template_loop_category_link_open', 10 );
remove_action( 'woocommerce_after_subcategory', 'woocommerce_template_loop_category_link_close', 10 );

add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display',15 );
add_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );



add_action('woocommerce_cart_collaterals','hill_cart_continue_shopping');

function hill_cart_continue_shopping(){
	printf('<a class="hill_continue_shop" href="%s">%s</a>',get_permalink(wc_get_page_id('shop')),esc_html__('Continue Shopping','hill'));
}


add_filter('woocommerce_form_field_args', 'hill_add_boot_input');

function hill_add_boot_input($args){

	if ($args['type'] == 'text' ||$args['type'] == 'email'||$args['type'] == 'tel'||$args['type'] == 'password' ||$args['type'] == 'textarea'   ) {
		$args['input_class'][]= "form-control";
	}
	

	return $args;
}



add_action('woocommerce_before_shop_loop','hill_grid_list_trigger',11);

function hill_grid_list_trigger(){
    ?>
    <div class="hill_grid_display">
        <a class="list-trigger">
     		<i class="fa fa-th-list"></i>       
        </a>
        <a class="grid-trigger">
            <i class="fa fa-th"></i>
        </a>
    </div>
    <?php
    
}


remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

function hill_hide_page_title() {
	return false;
}
add_filter('woocommerce_show_page_title', 'hill_hide_page_title');

function hill_wc_pricing_html_from_to($price, $from, $to) {
	$price = ' <ins>' . ( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) . '</ins> <del>' . ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) . '</del>';
	return $price;
}
add_filter('woocommerce_get_price_html_from_to', 'hill_wc_pricing_html_from_to', 10, 4 );

remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
add_action('woocommerce_before_shop_loop_item', 'woocommerce_show_product_loop_sale_flash', 10);

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'hill_add_to_cart_fragments' );
function hill_add_to_cart_fragments( $fragments ) {
	ob_start();
	?>
	<div class="h_header_cart_box_cont"> 
        <div class="h_header_cart_box">
          <span class="h_mycart_txt p1">
             <a href="<?php echo esc_url(wc_get_cart_url()); ?>"> <?php echo sprintf (__( '<strong>My cart:</strong> %s', 'hill' ), WC()->cart->get_cart_total() ); ?></a>
          </span>
          <strong class=" h_shoping_crt fa fa-shopping-cart"></strong>
          <span class="h_cart_number_symble"><?php echo esc_html(WC()->cart->cart_contents_count); ?></span>
        </div>
        <div class="hill_min_cart_popup">
          <?php get_template_part('woocommerce/cart/mini', 'cart'); ?>
        </div>
    </div>
	<?php
	
	$fragments['div.h_header_cart_box_cont'] = ob_get_clean();

	ob_start();
	?>
	<div class="h_mycart_box3">
		<div class="my_cart_box2">
			<div class="my_cart_box2_left">
				<span><a href="<?php echo esc_url(wc_get_cart_url()); ?>"><?php echo sprintf (__( '<strong>My cart:</strong> %s', 'hill' ), WC()->cart->get_cart_total() ); ?></a></span>
			</div>
			<div class="my_cart_box2_right">
				<i class="fa fa-shopping-cart"></i>
				<span class="h_cart_number_symble"><?php echo esc_html(WC()->cart->cart_contents_count); ?></span>
			</div>
		</div>
		<div class="hill_min_cart_popup">
			<?php get_template_part('woocommerce/cart/mini', 'cart'); ?>
		</div>
	</div>
	<?php

	$fragments['div.h_mycart_box3'] = ob_get_clean();

	ob_start();
	?>
	<div class="h_mycart_box2">
		<div class="h_header_cart_box2">
			<div class="h_mycart_box2_left">
				<span><a href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"><?php echo sprintf (__( 'My cart: %s', 'hill' ), WC()->cart->get_cart_total() ); ?></a></span>
			</div>
			<div class="h_mycart_box2_right">
				<i class="fa fa-shopping-cart"></i>
				<span class="cart_number_con"><?php echo esc_html(WC()->cart->cart_contents_count); ?></span>
			</div>
		</div>
		<div class="hill_min_cart_popup">
			<?php get_template_part('woocommerce/cart/mini', 'cart'); ?>
		</div>
	</div>
	<?php

	$fragments['div.h_mycart_box2'] = ob_get_clean();

	ob_start();
	?>
	<span class="h_f_number_box1"> <?php echo WC()->cart->cart_contents_count; ?> </span>
	<?php

	$fragments['span.h_f_number_box1'] = ob_get_clean();
	
	return $fragments;
}


// hill_pass_scb
function hill_pass_scb($value){
	return $value;
}

/* Mobile Menu Filter */
add_filter('walker_nav_menu_start_el', 'hill_filter_tomobile_nav', 10, 4);
function hill_filter_tomobile_nav( $item_output, $item, $depth, $args )
{
	if ($args->theme_location == 'mobile') {
		$children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
		if (!empty($children)) {
			$item_output .= '<i class="fa fa-angle-right"></i>';
		}
	}
	return $item_output;
}

add_filter('wp_get_attachment_image_attributes', 'hill_alter_wp_get_attachment_image_attrs');

function hill_alter_wp_get_attachment_image_attrs($attr)
{
    if (isset($attr['sizes'])) {
        unset($attr['sizes']);
    }
    if (isset($attr['srcset'])) {
        unset($attr['srcset']);
    }
    return $attr;
}

function woocommerce_template_loop_product_title() {
		echo '<h3 class="woocommerce-loop-product__title">' . get_the_title() . '</h3>';
	}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );

if ( ! function_exists( 'woocommerce_output_upsells' ) ) {
	function woocommerce_output_upsells() {
	    woocommerce_upsell_display( 4,4 ); // Display 4 products in rows of 4
	}
}

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action( 'woocommerce_cart_collaterals', 'woocommerce_output_cross_sell_display', 15 );

if ( ! function_exists( 'woocommerce_output_cross_sell_display' ) ) {
	function woocommerce_output_cross_sell_display() {
	    woocommerce_cross_sell_display( 4,4 ); // Display 4 products in rows of 4
	}
}


add_action( 'after_setup_theme', 'view_product_design' );
 
function view_product_design() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

/*
add_filter( 'woocommerce_product_single_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text' );
add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
	global $product;
	$product_type = $product->get_type();
	switch ( $product_type ) {
		case 'external':
			return __( 'Kupi', 'hill' );
		break;
		case 'grouped':
			return __( 'Odaberi opciju', 'hill' );
		break;
		case 'simple':
			return __( 'Add to cart', 'hill' );
		break;
		case 'variable':
			return __( 'Odaberi opciju', 'hill' );
		break;
		default:
			return __( 'Kupi', 'hill' );
	}
}
*/
