<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.4.0
 */

 
defined( 'ABSPATH' ) || exit;

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="row">
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );


		$time_limit = get_post_meta(get_the_ID(),'_sale_price_dates_to',true);

		if(!empty($time_limit)):
			$datestring = date('Y, n-1 ,d', $time_limit);
		?>

		<div class="countdown-box">
			<div class="conutdown-txt-box">
				<h4><i class="fa fa-exclamation-circle"></i> <?php esc_html_e('Don\'t miss:', 'hill'); ?></h4>
				<p><?php esc_html_e('This special event, offer time will end when the timer stops.', 'hill'); ?></p>
			</div>

			<div class="hill_conutdown_box" id="hill_cdb_<?php $uniq_id = rand(0000000,9999999); echo esc_attr($uniq_id); ?>"></div>
			<script type="text/javascript">
			jQuery(document).ready(function () {	

				//Day Murkup
				var layout_dicount_cd_day = '<div class="countdown-digit-box"><div class="countdown-digit-box-top"><span class="cdb-digit">{dn}</span></div><div class="countdown-digit-box-bottom"><span class="cdb-txt">{dl}</span></div></div>',

					// Hrs Markup
					layout_dicount_cd_hrs = '<div class="countdown-digit-box"><div class="countdown-digit-box-top"><span class="cdb-digit">{hn}</span></div><div class="countdown-digit-box-bottom"><span class="cdb-txt">{hl}</span></div></div>',

					// Min Markup
					layout_dicount_cd_min = '<div class="countdown-digit-box"><div class="countdown-digit-box-top"><span class="cdb-digit">{mn}</span></div><div class="countdown-digit-box-bottom"><span class="cdb-txt">{ml}</span></div></div>',

					// Sec Markup
					layout_dicount_cd_sec = '<div class="countdown-digit-box"><div class="countdown-digit-box-top"><span class="cdb-digit">{sn}</span></div><div class="countdown-digit-box-bottom"><span class="cdb-txt">{sl}</span></div></div>';


				jQuery('#hill_cdb_<?php echo esc_attr($uniq_id); ?>').countdown({
					until: new Date(<?php echo esc_attr($datestring); ?>),
					compact: false,
					layout: layout_dicount_cd_day+layout_dicount_cd_hrs+layout_dicount_cd_min+layout_dicount_cd_sec,
					labels:['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Min', 'Sec'],
					labels1:['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Min', 'Sec']
				});
			});
			</script> 
		</div><!-- end: countdown-box -->
		<?php endif; ?>
	</div>

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		?>
		<div style="clear:both"></div>
		<?php
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
</div>