<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php do_action( 'woocommerce_before_mini_cart' ); ?>
<?php if ( ! WC()->cart->is_empty() ) : ?>
<ul class="cart_list product_list_widget <?php echo isset($args['list_class']) ? $args['list_class'] : '' ; ?>">
	
		<?php
		do_action( 'woocommerce_before_mini_cart_contents' );
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

					$product_name  = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
					$thumbnail     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<li class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item clearfix', $cart_item, $cart_item_key ) ); ?> ">
						
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">&times;</a>',
							esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'hill' ),
							esc_attr( $product_id ),
							esc_attr( $cart_item_key ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>
						<div class="hill_wc_mini_cart_img">
							<?php if ( ! $_product->is_visible() ) : ?>
								<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ); ?>
							<?php else : ?>
								<a href="<?php echo esc_url( $product_permalink ); ?>">
									<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ); ?>
								</a>
							<?php endif; ?>
						</div>
						
						<div class="hill_wc_mini_cart_p_info">
							<?php if ( ! $_product->is_visible() ) : ?>
								<span class="hill_mci_link"><?php echo wp_kses_post($product_name); ?></span>
							<?php else : ?>
								<a href="<?php echo esc_url(get_permalink($product_id)); ?>" class="hill_mci_link"><?php echo wp_kses_post($product_name); ?></a>
							<?php endif; ?>
							<?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
							<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
						</div>
					</li>
					<?php
				}
			}
	do_action( 'woocommerce_mini_cart_contents' );
	?>
</ul>
	<div class="hill_mini_cart_footer">
		<!-- <p class="total"><strong><?php esc_html_e( 'Subtotal', 'hill' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p> -->
		<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>
		<p class="buttons">
			<a class="hill-wish-list" href="<?php echo esc_url(wc_get_cart_url()); ?>" class="button wc-forward"><i class="fa fa-shopping-cart"></i></a>
			<?php if( function_exists( 'YITH_WCWL' ) ): ?>
				<a class="hill-wish-list" href="<?php if( function_exists( 'YITH_WCWL' ) ){ echo esc_url(YITH_WCWL()->get_wishlist_url()); } ?>"><i class="fa fa-star"></i></a>
			<?php endif; ?>
			<?php if (class_exists('YITH_Woocompare')): ?>
				<a class="hill-compare hill_wcc_minicart" href="<?php echo esc_url(hill_get_yith_compare()->obj->view_table_url()) ?>"><i class="fa fa-retweet"></i></i></a>
			<?php endif; ?>
			<a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="button checkout wc-forward"><?php esc_html_e( 'Go to Checkout', 'hill' ); ?></a>
		</p>
	</div>
<?php else : ?>
	<ul class="cart_list product_list_widget ">
		<li class="empty"><?php esc_html_e( 'No products in the cart.', 'hill' ); ?></li>
    </ul>
<?php endif; ?>
<?php do_action( 'woocommerce_after_mini_cart' ); ?>

