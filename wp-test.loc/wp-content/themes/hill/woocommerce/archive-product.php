<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );


// how many column will be showed left side and right side

$design ='1';

 switch ($design) {

        case 1:
            $leftbar  = 'col-lg-3 col-md-12 col-sm-12 col-xs-12';
            $rightbar = 'col-lg-9 col-md-12 col-sm-12 col-xs-12';
            break;
        case 2:
            $leftbar  = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
            $rightbar = 'col-lg-8 col-md-8 col-sm-8 col-xs-12';
            break;
        default:
            $leftbar  = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
            $rightbar = 'col-lg-8 col-md-8 col-sm-8 col-xs-12';
            break;
    }



	$shop_page_setting = get_post( wc_get_page_id( 'shop' ) );
	$view_sidebar=true;
	if($shop_page_setting->framework_page_style =='leftsidebar'){
		$extra_class="";
	}elseif($shop_page_setting->framework_page_style =='rightsidebar'){
		$extra_class="rightslideclass";
	}else{
		$extra_class="noslideclass";
		$rightbar = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
		$view_sidebar=false;
	}

 ?>

<div class="woo-archives-container">
  <div class="container">
    <div class="row"> 
      <script type="text/javascript">
    jQuery(document).ready(function(){

    	 jQuery("a.grid-trigger").addClass('h-grid-current');
        
        jQuery("a.list-trigger").click(function() {
            jQuery(".products").addClass('h-list');
            jQuery(this).addClass('h-list-current');
            jQuery(".products").removeClass('h-grid');
            jQuery("a.grid-trigger").removeClass('h-grid-current');

            jQuery(".products li").each(function () {
            	var main_title = jQuery(this).find('h3').clone();
				main_title.addClass('hill_list_view_title');
				jQuery(this).find('.hill-wocommerce-row').prepend(main_title);
            });
        });

        jQuery("a.grid-trigger").click(function() {
            jQuery(".products").addClass('h-grid');
            jQuery(this).addClass('h-grid-current');
            jQuery(".products").removeClass('h-list');
            jQuery("a.list-trigger").removeClass('h-list-current');

            jQuery(".products li").each(function () {
            	jQuery(this).find('h3.hill_list_view_title').remove();

            });
        });
    })
</script>
      <div class="<?php echo esc_attr($rightbar); ?> <?php echo esc_attr($extra_class); ?>  hill_product_cat_page">
        <?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
        <h1 class="page-title">
          <?php woocommerce_page_title(); ?>
        </h1>
        <?php endif; ?>
        <?php
			/**
			 * woocommerce_archive_description hook
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>
        <div  class="product-filter">
          <div class="sort">
            <?php
                /**
                 * woocommerce_pagination hook
                 *
                 * @hooked woocommerce_pagination - 10
                 * @hooked woocommerce_catalog_ordering - 20
                 */
                do_action('woocommerce_pagination', 12);
                ?>
            <?php do_action('woocommerce_before_shop_loop', 13); ?>
          </div>
        </div>
        <?php if ( woocommerce_product_loop() ) : ?>
        <?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
			?>
        <?php woocommerce_product_loop_start(); 
			
				if ( wc_get_loop_prop( 'total' ) ) {
					while ( have_posts() ) {
						the_post();
			
						/**
						 * Hook: woocommerce_shop_loop.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );
			
						wc_get_template_part( 'content', 'product' );
					}
				}

			woocommerce_product_loop_end(); ?>
        <?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>
        <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
        <?php wc_get_template( 'loop/no-products-found.php' ); ?>
        <?php endif; ?>
        <?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
      </div>
      <?php if($view_sidebar){ ?>
      <div class="<?php echo esc_attr($leftbar); ?> hill_wc_left_sidebar">
        <?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
		$sidebar_name=$shop_page_setting->framework_page_left_sidebar;
		$sidebar_name = ($sidebar_name && $sidebar_name !== "0") ? $sidebar_name : 'sidebar-page' ;
		if ( is_active_sidebar( $sidebar_name) ) {
			dynamic_sidebar( $sidebar_name );
		}
?>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
</div>
<?php get_footer( 'shop' ); ?>
