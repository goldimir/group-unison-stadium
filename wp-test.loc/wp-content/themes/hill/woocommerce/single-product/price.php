<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>
<div class="hill_price_offers_container price-box product-info__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price hill-price"><?php echo wp_kses_post($product->get_price_html()); ?></p>
	<?php if( $product->is_in_stock()  ): ?>
		<div class="hill_wc_product_stock_status hill_instock"><?php esc_html_e('In Stock', 'hill'); ?></div>
	<?php else: ?>
		<div class="hill_wc_product_stock_status hill_outofstock"><?php esc_html_e('Out of Stock', 'hill'); ?></div>
	<?php endif; ?>
	<hr>

	<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo esc_url($product->is_in_stock() ? 'InStock' : 'OutOfStock'); ?>" />

</div>


