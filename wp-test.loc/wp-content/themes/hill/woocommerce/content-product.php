<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

//how column will be shown on shop archive page
$woo_con_pro_cols = '';
$woo_con_pro_cols = $woocommerce_loop['columns'];

if (!empty($hill_wc_col)) {
	$woo_con_pro_cols = $hill_wc_col;
}

$colclass = 'columns';

switch ($woo_con_pro_cols) {
	case 1:
		$colclass = 'col-md-12';
		break;
	case 2:
		$colclass = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
		break;

    case 3:
        $colclass = 'col-lg-4 col-md-4 col-sm-6 col-xs-6';
        break;
    case 4:
        $colclass = 'col-hill col-lg-3 col-md-3 col-sm-6';
        break;
    case 6:
        $colclass = 'col-lg-2 col-md-2 col-sm-6';
        break;
    default:
        $colclass = 'col-lg-4 col-md-4 col-sm-6';
        break;
}

if (!empty($box_type)) {
	$colclass = '';
}

if (!empty($hill_wc_col_class)) {
	$colclass = $hill_wc_col_class;
}

$product_terms = get_the_terms(get_the_ID(), 'product_cat');
$pt_class = '';

if (is_array($product_terms) && count($product_terms)) {
	foreach ($product_terms as $product_term) {
		$pt_class .= esc_attr($product_term->slug).' ';
	}
}

?>


<li class="<?php echo esc_attr($colclass).' '.esc_attr($pt_class); ?> hill_wc_product_item  <?php wc_product_class(); ?>">


	<div class="hillpro-single-box clearfix">
		

		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		
		<div class="hill-proImg-container">
			
			<?php if(!empty($box_type)): ?><div class="hill_small_buttons_cont"><?php endif; ?>
			<?php
				
				do_action( 'hill_before_shop_loop_item_title_a' );

				if(!empty($box_type)):
				?>
				<?php if(defined( 'YITH_WCWL' ) && YITH_WCWL): ?>
					<a class="hill-wish-list add_to_wishlist" href="#" data-product-id="<?php echo esc_attr($product->get_id())?>" data-product-type="<?php echo esc_attr($product->get_type())?>"><i class="fa fa-star"></i></a>
				<?php endif; ?>
				<?php if(class_exists('YITH_Woocompare_Frontend')): $hill_yith_cmp = new YITH_Woocompare_Frontend; ?><a class="hill-compare compare add_to_compare_small" data-product_id="<?php echo esc_attr($product->get_id())?>" href="<?php echo esc_url($hill_yith_cmp->add_product_url($product->get_id())) ?>"><i class="fa fa-retweet"></i></i></a><?php endif; ?>
				<?php
				endif;

				hill_share_buttons();
			?>
			<?php if(!empty($box_type)): ?></div><?php endif; ?>

			<?php
				if(!empty($box_type)) {
					/**
					 * woocommerce_after_shop_loop_item hook
					 *
					 * @hooked woocommerce_template_loop_add_to_cart - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item' );
				}
			?>

			<a href="<?php the_permalink(); ?>" class="hill_product_thumb_link">

			<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			</a>

		</div>

		<div class="hill-wocommerce-right-col clearfix">
			<div class="hill-wocommerce-row">
				<div class="hill_wc_rating_price clearfix">
					<?php
						/**
						 * woocommerce_after_shop_loop_item_title hook
						 *
						 * @hooked woocommerce_template_loop_rating - 5
						 * @hooked woocommerce_template_loop_price - 10
						 */
						do_action( 'woocommerce_after_shop_loop_item_title' );
					?>
				</div>
				<a href="<?php the_permalink(); ?>">
				<?php
					/**
					 * woocommerce_shop_loop_item_title hook
					 *
					 * @hooked woocommerce_template_loop_product_title - 10
					 */
					do_action( 'woocommerce_shop_loop_item_title' );
				?>
				</a>
				<div class="h-excerpt">
					<?php the_excerpt(); ?>
				</div>
			</div>
			<div class="hill-cart2-box p1">
				<?php if(empty($box_type)): ?>
					<?php if(defined( 'YITH_WCWL' ) && YITH_WCWL): ?>
						<a class="hill-wish-list add_to_wishlist" href="#" data-product-id="<?php echo esc_attr($product->get_id())?>" data-product-type="<?php echo esc_attr($product->get_type())?>"><i class="fa fa-star"></i></a>
					<?php endif; ?>
					<?php if(class_exists('YITH_Woocompare_Frontend')): $hill_yith_cmp = new YITH_Woocompare_Frontend; ?><a class="hill-compare compare add_to_compare_small" data-product_id="<?php echo esc_attr($product->get_id())?>" href="<?php echo esc_url($hill_yith_cmp->add_product_url($product->get_id())) ?>"><i class="fa fa-retweet"></i></i></a><?php endif; ?>
				<?php
				endif;
					if(empty($box_type)) {
						/**
						 * woocommerce_after_shop_loop_item hook
						 *
						 * @hooked woocommerce_template_loop_add_to_cart - 10
						 */
						do_action( 'woocommerce_after_shop_loop_item' );
					}
				?>
			</div>
		</div>
	</div>	
</li>

