<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

//how column will be shown on shop archive page
$woo_con_pro_cols = 4;

switch ($woo_con_pro_cols) {
    case 3:
        $colclass = 'col-lg-4 col-md-4 col-sm-6';
        break;
    case 4:
        $colclass = 'col-lg-3 col-md-3 col-sm-6';
        break;
    case 6:
        $colclass = 'col-lg-2 col-md-2 col-sm-6';
        break;
    default:
        $colclass = 'col-lg-4 col-md-4 col-sm-6';
        break;
}

?>


<div class="<?php echo esc_attr($colclass); ?>">

<div class="hillpro-single-box">
	

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	<a class="h-pro-share-btn" href=""><i class="fa fa-share-alt"></i>
</a>

	<a href="<?php the_permalink(); ?>">

		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );

			/**
			 * woocommerce_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */


		?>

			<div class="hill-wocommerce-row">
				<?php
				do_action( 'woocommerce_after_shop_loop_item_title' );
				?>
			</div>
			<?php	
			

			do_action( 'woocommerce_shop_loop_item_title' );

			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */

			

		?>

	</a>

	<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		
		?>
		<div class="hill-cart2-box p2">
			<a class="hill-wish-list add_to_wishlist" href="#" data-product-id="<?php echo esc_attr($product->get_id())?>" data-product-type="<?php echo esc_attr($product->get_type())?>"><i class="fa fa-star"></i></a>
			<a class="hill-compare compare" data-product_id="<?php echo esc_attr($product->get_id())?>" href="#"><i class="fa fa-retweet"></i></i></a>
		<?php
		do_action( 'woocommerce_after_shop_loop_item' );

		?>
		</div>
	</div>	
</div>
