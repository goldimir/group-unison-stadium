<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.1
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Increase loop count
$woocommerce_loop['loop']++;

$col_class = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
$cats_per_row = '';

	if (isset($woocommerce_loop['columns']) && !empty($woocommerce_loop['columns'])) {
		switch($woocommerce_loop['columns']){
	        case 1:
	            $col_class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
	            break;
	        case 2:
	            $col_class = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
	            break;        
	        case 3:
	            $col_class = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
	            break;   
	        case 4:
	            $col_class = 'col-lg-3 col-md-3 col-sm-3 col-xs-12';
	            break;   
	      
	        default:
	            $col_class = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
	            break;
	    }
	}
    

?>
<div class="<?php echo esc_attr($col_class); ?> hill_wc_cat_item">
	<?php do_action( 'woocommerce_before_subcategory', $category ); ?>

	<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">

		<?php
			/**
			 * woocommerce_before_subcategory_title hook
			 *
			 * @hooked woocommerce_subcategory_thumbnail - 10
			 */
			do_action( 'woocommerce_before_subcategory_title', $category );
		?>

		<h3>
			<?php
				echo esc_html($category->name);

				if ( $category->count > 0 )
					echo apply_filters( 'woocommerce_subcategory_count_html', wp_kses_post(' <mark class="count">(' . $category->count . ')</mark>'), $category );
			?>
		</h3>

		<?php
			/**
			 * woocommerce_after_subcategory_title hook
			 */
			do_action( 'woocommerce_after_subcategory_title', $category );
		?>

	</a>

	<?php do_action( 'woocommerce_after_subcategory', $category ); ?>
</div>
