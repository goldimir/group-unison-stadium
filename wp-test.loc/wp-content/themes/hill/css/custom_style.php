<?php 
/*
 * print css with cheking value is empty or not
 *
 */
function hill_print_css($props = '', $values = array(), $vkey = '', $pre_fix = '', $post_fix = '')
{
    if (isset($values[$vkey]) && !empty($values[$vkey])) {
        print wp_kses_post($props.":".$pre_fix.$values[$vkey].$post_fix.";\n");
    }
    
}

function hill_color_brightness($colourstr, $steps, $darken = false) {
    $colourstr = str_replace('#','',$colourstr);
    $rhex = substr($colourstr,0,2);
    $ghex = substr($colourstr,2,2);
    $bhex = substr($colourstr,4,2);

    $r = hexdec($rhex);
    $g = hexdec($ghex);
    $b = hexdec($bhex);

    if ($darken) {
        $steps = $steps*-1;
    }

    $r = max(0,min(255,$r + $steps));
    $g = max(0,min(255,$g + $steps));  
    $b = max(0,min(255,$b + $steps));

    $hex = "#";
    $hex .= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
    $hex .= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
    $hex .= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);

    return $hex;
}

function hill_get_custom_styles(){
    global $hill_option;
    $hill_colors = get_theme_mod('hill_colors', array());
    $hill_css = get_theme_mod('hill_css', array());

    ob_start();
    ?>
    body{
        <?php
            hill_print_css('font-family', $hill_option['body_typography'], 'font-family');
            hill_print_css('font-weight', $hill_option['body_typography'], 'font-weight');
            hill_print_css('font-size', $hill_option['body_typography'], 'font-size');
            hill_print_css('line-height', $hill_option['body_typography'], 'line-height');
            hill_print_css('color', $hill_option['body_typography'], 'color');
        ?>
    }
    a{ <?php hill_print_css('color', $hill_colors, 'lnk_color'); ?> }
    a:hover{ <?php hill_print_css('color', $hill_colors, 'lnk_color_hover'); ?> }
    .hill_wc_products_tab.vc_tta.vc_tta-style-classic .vc_tta-tab a{ <?php hill_print_css('font-family', $hill_option['body_typography'], 'font-family'); ?> }
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li, #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link, #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link, #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link{
        <?php
            hill_print_css('font-family', $hill_option['nav_typography'], 'font-family');
            hill_print_css('font-weight', $hill_option['nav_typography'], 'font-weight');
            hill_print_css('font-size', $hill_option['nav_typography'], 'font-size');
            hill_print_css('color', $hill_option['nav_typography'], 'color');
        ?>
    }
    #mega_main_menu > .menu_holder > .menu_inner > ul > li.default_dropdown .mega_dropdown,
    #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown > .mega_dropdown{
        <?php
            hill_print_css('font-family', $hill_option['nav_drodown_typography'], 'font-family');
            hill_print_css('font-weight', $hill_option['nav_drodown_typography'], 'font-weight');
            hill_print_css('font-size', $hill_option['nav_drodown_typography'], 'font-size');
            hill_print_css('color', $hill_option['nav_drodown_typography'], 'color');
        ?>
    }

    #mega_main_menu ul.mega_main_menu_ul > li.multicolumn_dropdown > ul.mega_dropdown >li > .item_link,
    #mega_main_menu ul.mega_main_menu_ul > li.multicolumn_dropdown > ul.mega_dropdown >li > .item_link:hover,
    #mega_main_menu ul.mega_main_menu_ul > li.multicolumn_dropdown > ul.mega_dropdown >li > .item_link .link_content,
    #mega_main_menu ul.mega_main_menu_ul > li.multicolumn_dropdown > ul.mega_dropdown >li > .item_link .link_text{
        <?php
            hill_print_css('font-family', $hill_option['nav_mega_title_typography'], 'font-family');
            hill_print_css('font-weight', $hill_option['nav_mega_title_typography'], 'font-weight');
            hill_print_css('font-size', $hill_option['nav_mega_title_typography'], 'font-size');
            hill_print_css('color', $hill_option['nav_mega_title_typography'], 'color');
        ?>
    }

    .hill_banner_cont .hill_banner_title{
        <?php
            hill_print_css('font-family', $hill_option['banner_title_typography'], 'font-family');
            hill_print_css('font-weight', $hill_option['banner_title_typography'], 'font-weight');
        ?>
    }

    .widget-title, .title-contact-info, .widgettitle{
        <?php hill_print_css('font-family', $hill_option['widget_title_typography'], 'font-family'); ?>
        <?php hill_print_css('font-weight', $hill_option['widget_title_typography'], 'font-weight'); ?>
        <?php hill_print_css('font-size', $hill_option['widget_title_typography'], 'font-size'); ?>
        <?php hill_print_css('color', $hill_option['widget_title_typography'], 'color'); ?>
    }
    .footer_box h4{
        <?php hill_print_css('font-family', $hill_option['footer_widget_title_typography'], 'font-family'); ?>
        <?php hill_print_css('font-weight', $hill_option['footer_widget_title_typography'], 'font-weight'); ?>
        <?php hill_print_css('font-size', $hill_option['footer_widget_title_typography'], 'font-size'); ?>
        <?php hill_print_css('color', $hill_option['footer_widget_title_typography'], 'color'); ?>
    }
    .framwork-hdr-title{
        <?php hill_print_css('font-family', $hill_option['page_title_typography'], 'font-family'); ?>
        <?php hill_print_css('font-weight', $hill_option['page_title_typography'], 'font-weight'); ?>
        <?php hill_print_css('font-size', $hill_option['page_title_typography'], 'font-size'); ?>
        <?php hill_print_css('color', $hill_option['page_title_typography'], 'color'); ?>
    }
    .h_best_seller_left h4{
        <?php hill_print_css('font-family', $hill_option['slider_title_typography'], 'font-family'); ?>
        <?php hill_print_css('font-weight', $hill_option['slider_title_typography'], 'font-weight'); ?>
        <?php hill_print_css('font-size', $hill_option['slider_title_typography'], 'font-size'); ?>
    }

    .h_banner_1{
        <?php hill_print_css('background-image', $hill_option['hill_header_bg'], 'url', 'url(', ')'); ?>
    }

    /* Common Color */

    .h_cat_plus i.fa-times-circle,
    .h_header_utility_menu li i,
    .h_telephone_box i,
    .widget_categories ul li a:hover,
    .hill-title-comments span,
    .hill-notice-box::after,
    .woocommerce .products h3:hover,
    .h-get-support p,
    .h-get-support i{
        <?php hill_print_css('color', $hill_colors, 'common_color_main'); ?>
    }

    .h_pro_box_all_products.woocommerce .products .hill_small_buttons_cont > a:hover,
    .h_pro_box_all_products.woocommerce .products .hill_small_buttons_cont > .framwork-share-this:hover,
    .button.yith-wcqv-button:hover,
    .products li .framwork-share-this:hover,
    .tagcloud a:hover{
        <?php hill_print_css('background-color', $hill_colors, 'common_color_main', '', ' !important'); ?>
        <?php hill_print_css('border-color', $hill_colors, 'common_color_main', '', ' !important'); ?>
    }

    .hill_mini_cart_footer .hill-wish-list:hover,
    .hill_mini_cart_footer .hill-compare:hover,
    .hill_pro_carousel .owl-theme .owl-controls .owl-page.active span,
    .hill_pro_carousel .owl-theme .owl-controls.clickable .owl-page:hover span,
    .hr_list li a:hover,
    .h_admin_label,
    .hill-blockquate-fat-box,
    .hill-single-post-slider-box .owl-theme .owl-controls .owl-page.active span,
    .hill-single-post-slider-box .owl-theme .owl-controls.clickable .owl-page:hover span,
    .social_media_footer_pan,
    .hill-woocommerce-pagination li a:hover,
    .hill_wc_hot_product .hill_wc_hp_header,
    .hill_cm_title,
    .hill-about-author-box,
    .woocommerce .hill_wc_cat_item h3,
    .btn-main,
    .h_best_seller_left{
        <?php hill_print_css('background-color', $hill_colors, 'common_color_main'); ?>
    }
    .hill_wc_hot_product .hill_hp_cont{
        <?php hill_print_css('border-color', $hill_colors, 'common_color_main'); ?>
    }
    .hill_mini_cart_footer .button.checkout,
    #submit,
    .hill-contact-form .wpcf7-submit{
        <?php hill_print_css('background-color', $hill_colors, 'common_color_main', '', ' !important'); ?>
    }

    .commentList .children,
    .hill-notice-box{
        <?php hill_print_css('border-left-color', $hill_colors, 'common_color_main'); ?>
    }


    .h_header_bottom,
    .h_header_bottom #navbar{
        <?php hill_print_css('background-color', $hill_colors, 'menu_bar_color'); ?>
    }

    .hill_header_cont{
        <?php
        hill_print_css('background-color', $hill_colors, 'header_bg_color');
        if (isset($hill_colors['header_bg_color']) && !empty($hill_colors['header_bg_color'])) {
            echo 'padding:30px;';
        }
        ?>
    }
    .nav > li > a:hover{
        <?php hill_print_css('border-top-color', $hill_colors, 'menu_hover_border_top'); ?>
    }
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover{
        <?php hill_print_css('border-top-color', $hill_colors, 'menu_hover_border_top'); ?>
        <?php hill_print_css('background-color', $hill_colors, 'menu_hover_bg'); ?>
    }
    .nav_mofdifier2 #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover{
        <?php hill_print_css('border-top-color', $hill_colors, 'menu_hover_border_top'); ?>
    }

    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > a{
        <?php hill_print_css('border-color', $hill_colors, 'menu_hover_border'); ?>
    }

    #mega_main_menu > .menu_holder > .menu_inner > ul > li.default_dropdown .mega_dropdown,
    #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown > .mega_dropdown,
    .hill-mobile-nav-container,
    .nav > li ul.children{
        <?php hill_print_css('border-bottom-color', $hill_colors, 'menu_dropdown_bottom'); ?>
    }

    #mega_main_menu > .menu_holder > .menu_inner > ul > li .mega_dropdown .item_link:hover, 
    #mega_main_menu > .menu_holder > .menu_inner > ul > li .mega_dropdown .item_link:focus, 
    #mega_main_menu ul li.default_dropdown .mega_dropdown > li:hover > .item_link,
    #mega_main_menu ul li.default_dropdown .mega_dropdown > li.current-menu-item > .item_link,
    #mega_main_menu ul li.multicolumn_dropdown .mega_dropdown > li > .item_link:hover,
    #mega_main_menu ul li.multicolumn_dropdown .mega_dropdown > li.current-menu-item > .item_link,
    #mega_main_menu ul li.post_type_dropdown .mega_dropdown > li:hover > .item_link,
    #mega_main_menu ul li.post_type_dropdown .mega_dropdown > li > .item_link:hover,
    #mega_main_menu ul li.post_type_dropdown .mega_dropdown > li.current-menu-item > .item_link,
    #mega_main_menu ul li.grid_dropdown .mega_dropdown > li:hover > .processed_image,
    #mega_main_menu ul li.grid_dropdown .mega_dropdown > li:hover > .item_link,
    #mega_main_menu ul li.grid_dropdown .mega_dropdown > li > .item_link:hover,
    #mega_main_menu ul li.grid_dropdown .mega_dropdown > li.current-menu-item > .item_link,
    #mega_main_menu ul li.post_type_dropdown .mega_dropdown > li > .processed_image:hover,
    .hill-mobile-nav-container li:hover > a,
    .nav > li ul.children a:hover{
        <?php hill_print_css('background-color', $hill_colors, 'menu_dropdown_link_hover_bg', '', ' !important'); ?>
    }

    .footer_search_box{
        <?php hill_print_css('background-image', $hill_option['footer_logo'], 'url', 'url(', ')'); ?>
    }

    .hill-about-author-box{
        <?php hill_print_css('background-image', $hill_option['hill_blog_author_bg'], 'url', 'url(', ')'); ?>
    }

    .h_header_cart_box_cont:hover .h_header_cart_box{
        <?php hill_print_css('background-color', $hill_colors, 'mini_cart_hover_bg'); ?>
    }
    .h_header_cart_box_cont:hover .h_mycart_txt{
        <?php hill_print_css('border-color', $hill_colors, 'mini_cart_hover_bg'); ?>
    }
    .h_header_cart_box_cont:hover .h_shoping_crt{
        <?php hill_print_css('border-color', $hill_colors, 'mini_cart_icon_hover_bg'); ?>
        <?php hill_print_css('background-color', $hill_colors, 'mini_cart_icon_hover_bg'); ?>
    }

    .h_cart_number_symble{
        <?php hill_print_css('background-color', $hill_colors, 'mini_cart_count_bg'); ?>
    }

    .hill_min_cart_popup{
        <?php hill_print_css('border-color', $hill_colors, 'mini_cart_items_border_color'); ?>
    }

    .woocommerce span.onsale{
        <?php hill_print_css('background-image', $hill_css, 'onsale_badge_bg', 'url(', ')'); ?>
    }

    .woocommerce .star-rating{
        <?php hill_print_css('color', $hill_colors, 'rated_star_color'); ?>
    }

    .h_telephone_box,
    .h_telephone_box a,
    .h_header_utility_menu,
    .h_header_utility_menu li a,
    .h_header_utility_menu li{
        <?php hill_print_css('color', $hill_colors, 'topbar_color'); ?>
    }

    .h_telephone_box a{
        <?php hill_print_css('border-color', $hill_colors, 'topbar_color'); ?>
    }

    .add_to_cart_button, .product_type_simple,
    .woocommerce div.product form.cart .single_add_to_cart_button,
    .hill_wc_hot_product .add_to_cart_button{
        <?php hill_print_css('background', $hill_colors, 'add_to_cart_btn_bg_color', '', ' !important'); ?>
    }
    .products li .add_to_cart_button::before,
    .single_add_to_cart_button::after,
    .hill_wc_hot_product .add_to_cart_button::after{
        <?php hill_print_css('background', $hill_colors, 'add_to_cart_btn_icon_bg_color', '', ' !important'); ?>
    }
    .add_to_cart_button:hover,
    .product_type_simple:hover,
    .woocommerce div.product form.cart .single_add_to_cart_button:hover,
    .hill_wc_hot_product .add_to_cart_button:hover{
        <?php hill_print_css('background', $hill_colors, 'add_to_cart_btn_hvr_bg_color', '', ' !important'); ?>
    }

    .hill_instock{
        <?php hill_print_css('background-color', $hill_colors, 'in_stock_bg_color'); ?>
    }

    .hill_brands_carousel ul li a:hover {
        <?php hill_print_css('background-color', $hill_colors, 'brand_bg_color'); ?>
        <?php hill_print_css('box-shadow', $hill_colors, 'brand_top_border_color', 'inset 0 2px '); ?>
    }

    .hill_brands_carousel ul li a span{
        <?php hill_print_css('background-color', $hill_colors, 'brand_lnk_bg_color'); ?>
    }

    .entry_categories > span > a{
        <?php hill_print_css('background-color', $hill_colors, 'cat_bg_color'); ?>
    }

    .hill_brands_carousel{
        <?php
            hill_print_css('background-color', $hill_colors, 'brand_slider_bg');
            hill_print_css('border-color', $hill_colors, 'brand_slider_border');
        ?>
    }

    .hill_brands_carousel .owl-theme .owl-controls .owl-buttons div:hover{
        <?php hill_print_css('background-color', $hill_colors, 'brand_nav_bg_color'); ?>
    }

    .woocommerce div.product .woocommerce-tabs ul.tabs li.active{
        <?php hill_print_css('box-shadow', $hill_colors, 'common_color_main', 'inset 0 2px '); ?>
    }

    .sort .hill_grid_display a:hover,
    .sort .hill_grid_display .h-grid-current,
    .sort .hill_grid_display .h-list-current,
    .h_float_utiliy_box > a:hover{
        <?php
            hill_print_css('background-color', $hill_colors, 'common_color_main');
            hill_print_css('border-color', $hill_colors, 'common_color_main');
        ?>
    }

    .hill_wc_products_tab.vc_tta.vc_general .vc_tta-tab > a,
    .hill_wc_products_tab.vc_tta.vc_general .vc_tta-tab > a:hover,
    .hill_wc_products_tab.vc_tta.vc_general .vc_tta-tab.vc_active::before,
    .hill_wc_products_tab.vc_tta.vc_general .vc_tta-tab::before{
        <?php hill_print_css('color', $hill_colors, 'tab_short_txt'); ?>
    }

    .hill_wc_products_tab.vc_tta.vc_general .vc_tta-tab.vc_active > a{
        <?php hill_print_css('color', $hill_colors, 'tab_short_txt_active'); ?>
    }


    .vc_tta.vc_tta-style-classic .vc_tta-tab a{
        <?php hill_print_css('color', $hill_colors, 'tabs_nav_txt_color'); ?>
    }

    .vc_tta.vc_tta-style-classic .vc_tta-tab.vc_active > a,
    .vc_tta.vc_tta-style-classic .vc_tta-tab.vc_active > a:hover{
        <?php hill_print_css('background-color', $hill_colors, 'tabs_nav_active_bg'); ?>
        <?php hill_print_css('color', $hill_colors, 'tabs_nav_txt_color_active'); ?>
    }
    .vc_tta.vc_tta-style-classic .vc_tta-tab.vc_active > a{
        <?php hill_print_css('border-color', $hill_colors, 'tabs_nav_active_bg'); ?>
    }

    .vc_tta.vc_tta-style-classic .vc_tta-tab > a:hover{
        <?php hill_print_css('color', $hill_colors, 'tabs_nav_txt_color_hover'); ?>
    }

    .vc_tta.vc_tta-style-classic .vc_tta-tab.vc_active > a::after{
        <?php hill_print_css('background-color', $hill_colors, 'tabs_nav_active_bottom_border'); ?>
    }

    .hill_discount_product .hill_dp_percent{
        <?php hill_print_css('background-color', $hill_colors, 'woo_discount_label'); ?>
    }

    .hill_discount_product .hill_dp_percent::before{
        <?php if(isset($hill_colors['woo_discount_label']) && !empty($hill_colors['woo_discount_label'])): ?>
        border-color: transparent <?php echo hill_color_brightness($hill_colors['woo_discount_label'], 50, true); ?> <?php echo hill_color_brightness($hill_colors['woo_discount_label'], 50, true); ?> transparent;
        <?php endif; ?>
    }


    .hill_pf_button{
        <?php
            hill_print_css('color', $hill_colors, 'shorting_nav_color');
            hill_print_css('border-color', $hill_colors, 'shorting_nav_border');
        ?>
    }
    .hill_pf_button:active,
    .hill_pf_button:focus,
    .hill_pf_button:hover{
        <?php
            hill_print_css('color', $hill_colors, 'srt_nav_hvr_color');
            hill_print_css('border-color', $hill_colors, 'srt_nav_hvr_border');
            hill_print_css('background-color', $hill_colors, 'srt_nav_hvr_bg');
        ?>
    }
    .hill_pf_active{
        <?php
            hill_print_css('color', $hill_colors, 'srt_nav_active_color');
            hill_print_css('border-color', $hill_colors, 'srt_nav_active_border');
            hill_print_css('background-color', $hill_colors, 'srt_nav_active_bg');
        ?>
    }

    .hill_pro_carousel .owl-theme .owl-controls .owl-buttons div,
    .hill_popup_close::after{
        <?php hill_print_css('background-color', $hill_colors, 'common_color_main'); ?>
    }

    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
        <?php hill_print_css('background', $hill_colors, 'common_color_main'); ?>
    }

    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
        <?php hill_print_css('box-shadow', $hill_colors, 'common_color_main', '0 0 0 1px '); ?>
        <?php hill_print_css('background', $hill_colors, 'common_color_main'); ?>
    }



    .btn-hill-subscribe,
    .hill_popup .hill-subscribe-form input[type="submit"]{
        <?php hill_print_css('background', $hill_colors, 'sc_btn_bg_color', '', ' !important'); ?>
        <?php hill_print_css('color', $hill_colors, 'sc_btn_txt_color'); ?>
    }

    .btn-hill-subscribe:hover,
    .hill_popup .hill-subscribe-form input[type="submit"]:hover{
        <?php hill_print_css('background', $hill_colors, 'sc_btn_bg_color_hover', '', ' !important'); ?>
        <?php hill_print_css('color', $hill_colors, 'sc_btn_txt_color_hover'); ?>
    }

    .hill_popup .hill_sp_fill_bg{
        <?php hill_print_css('background-color', $hill_option['popup_bg_img'], 'background-color'); ?>
        <?php hill_print_css('background-image', $hill_option['popup_bg_img'], 'background-image', 'url(', ')'); ?>
        <?php hill_print_css('background-size', $hill_option['popup_bg_img'], 'background-size'); ?>
        <?php hill_print_css('background-position', $hill_option['popup_bg_img'], 'background-position'); ?>
        <?php hill_print_css('background-attachment', $hill_option['popup_bg_img'], 'background-attachment'); ?>
    }
    #navbar .sub-menu li a{color:#000;}
     #navbar .sub-menu li a:hover{color:#fff;}
    <?php
	
	if(isset($hill_option['extra_css'])){
        print esc_html($hill_option['extra_css']);
    }

    $hill_custom_css = ob_get_clean();

    return $hill_custom_css;
}