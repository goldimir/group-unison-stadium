<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package hill-theme
 */

if ( ! function_exists( 'hill_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function hill_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'hill' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'hill' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'hill_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function hill_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'hill' ) );
		if ( $categories_list && hill_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'hill' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'hill' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'hill' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'hill' ), esc_html__( '1 Comment', 'hill' ), esc_html__( '% Comments', 'hill' ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'hill' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function hill_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'hill_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'hill_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so hill_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so hill_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in hill_categorized_blog.
 */
function hill_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'hill_categories' );
}
add_action( 'edit_category', 'hill_category_transient_flusher' );
add_action( 'save_post',     'hill_category_transient_flusher' );


function hill_entry_meta(){

	

	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';


		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			get_the_date(),
			esc_attr( get_the_modified_date( 'c' ) ),
			get_the_modified_date()
		);

		printf( '<span class="framwork posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark"><i class="fa fa-calendar">&nbsp;&nbsp;</i>%3$s<span></span></a></span>',
			_x( 'Posted on', 'Used before publish date.', 'hill' ),
			esc_url( get_permalink() ),
			$time_string
		);
	}
	?>

	 
		<span class="framwork-commnet-number">
			<a href="<?php echo esc_url( get_permalink() ) ?>">
				&nbsp;<i class="fa fa-comment-o"></i>&nbsp;
				<?php 
				printf( _nx( '1 <span class="hill_hide_on_grid">Comment</span>', '%1$s <span class="hill_hide_on_grid">Comments</span>', get_comments_number(), 'comments title', 'hill' ), number_format_i18n( get_comments_number() ) );
				?>
			</a>
		</span>

		<?php if(function_exists('hill_love_it')) { echo hill_love_it(); } ?> 

		<?php echo hill_share_buttons(); ?>


<?php
	if ( 'post' == get_post_type() ) {
		if ( is_singular() || is_multi_author() ) {
			printf( '<span class="byline"><span class="author vcard"><span class="screen-reader-text">%1$s </span><a class="url fn n" href="%2$s">%3$s</a></span></span>',
				_x( 'Author', 'Used before post author name.', 'hill' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author()
			);
		}

		

		$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'hill' ) );
		
	}

	if ( is_attachment() && wp_attachment_is_image() ) {
		// Retrieve attachment metadata.
		$metadata = wp_get_attachment_metadata();

		printf( '<span class="full-size-link"><span class="screen-reader-text">%1$s </span><a href="%2$s">%3$s &times; %4$s</a></span>',
			_x( 'Full size', 'Used before full size attachment link.', 'hill' ),
			esc_url( wp_get_attachment_url() ),
			$metadata['width'],
			$metadata['height']
		);
	}





}

function hill_share_buttons()
{
	global $post;
	?>
	<span class="framwork-share-this">	
		<i class="fa fa-share-alt"></i>
		<span  class="framwork-share-this-popup">
			<ul>
				<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_permalink() ) ?>"><i class="fa fa-facebook"></i></a></li>
				<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://twitter.com/home?status=<?php echo esc_html( get_the_title() ) ?> - <?php echo esc_url( get_permalink() ) ?>"><i class="fa fa-twitter"></i></a></li>
				<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?php echo esc_url( get_permalink() ) ?>"><i class="fa fa-google-plus"></i></a></li>
				<li><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://pinterest.com/pin/create/button/?url=<?php echo esc_url( get_permalink() ) ?>&media=<?php echo esc_url(hill_get_post_thumb_src($post->ID)); ?>&description="><i class="fa fa-pinterest"></i></a></li>
			</ul>
		</span>
	</span>
	<?php
}

function hill_get_post_thumb_src($postID)
{
	return wp_get_attachment_url( get_post_thumbnail_id($postID) );;
}

function hill_post_thumbnail($thumb_size = 'post-thumbnail') {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	global $hill_option;
	
	$hill_grid_styled_post = ((isset($hill_option['hill_blog_grid_type']) && $hill_option['hill_blog_grid_type'] == 'on') || (isset($_GET['grid']) && $_GET['grid'] == 'on')) ? true : false;

	$hill_grid_thumb_size = ($hill_grid_styled_post && !is_sticky()) ? 'hill-grid-thumb' : $thumb_size;

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( $hill_grid_thumb_size, array( 'alt' => get_the_title() ) );
		?>
	</a>

	<?php endif; // End is_singular()
}


function hill_page_menu( $args = array() ) {
    $defaults = array('sort_column' => 'menu_order, post_title', 'menu_class' => 'menu', 'echo' => true, 'link_before' => '', 'link_after' => '');
    $args = wp_parse_args( $args, $defaults );
 
    $menu = '';
 
    $list_args = $args;
 
    // Show Home in the menu
    if ( ! empty($args['show_home']) ) {
        if ( true === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] )
            $text = esc_html__('Home', 'hill');
        else
            $text = $args['show_home'];
        $class = '';
        if ( is_front_page() && !is_paged() )
            $class = 'class="current_page_item"';
        $menu .= '<li ' . $class . '><a href="' . home_url( '/' ) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';
        // If the front page is a page, add it to the exclude list
        if (get_option('show_on_front') == 'page') {
            if ( !empty( $list_args['exclude'] ) ) {
                $list_args['exclude'] .= ',';
            } else {
                $list_args['exclude'] = '';
            }
            $list_args['exclude'] .= get_option('page_on_front');
        }
    }
 
    $list_args['echo'] = false;
    $list_args['title_li'] = '';
    $menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_pages($list_args) );
 
    if ( $menu ){
        $menu = '<ul class="' . esc_attr($args['menu_class']) . '">' . $menu . '</ul>';
    }
 

    if ( $args['echo'] )
        print wp_kses_post($menu);
    else
        return $menu;
}





// Related post function
function hill_related_posts() {
	?>
	
	<?php
	$post_categories = wp_get_post_terms( get_the_ID(), 'category' );
	$post_tags = wp_get_post_terms( get_the_ID(), 'post_tag' );


	$all_related_posts = array();

	$not___in = array();
	$not___in[] = get_the_ID();

	foreach ($post_categories as $post_categorie) {

		$rp_args = array(
			'posts_per_page' => 4,
			'exclude' => get_the_ID(),
			'cat' => $post_categorie->term_id,
			'post__not_in' => $not___in
		);

		$related_posts_temp = new WP_Query( $rp_args );

		if($related_posts_temp->have_posts()){
			$all_related_posts[] = $related_posts_temp;
		}
	}

	foreach ($post_tags as $post_tag) {
		$rp_args = array(
			'posts_per_page' => 4,
			'exclude' => get_the_ID(),
			'tag_id' => $post_tag->term_id,
			'post__not_in' => $not___in
		);

		$related_posts_temp = new WP_Query( $rp_args );

		if($related_posts_temp->have_posts()){
			$all_related_posts[] = $related_posts_temp;
		}
		
	}

	$already_in = array();
	
	if(count($all_related_posts)):
		$pcount = 0;
	?>
	<h3 class="title-related-post"><?php esc_html_e('Related Posts', 'hill'); ?></h3>
	<div class="hill-single-post-slider-box">	
		<ul class="hill-rated-post-slider-item">
			<?php foreach($all_related_posts as $related_posts): ?>
				<?php if ($related_posts->have_posts()): ?>
					<?php while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
						<?php

						if(!isset($already_in[get_the_ID()])): 
							$pcount++;
							$already_in[get_the_ID()] = get_the_ID();
						
						?>
						<li>
							<div class="hill-single-post-box framework-grid-post framework-per-single-post-no-thumb">
								<div class="entry_categories <?php if( has_post_thumbnail() ) { echo "entry_categories-per-single-post"; } ?>">
									<?php
									$categories = get_the_category();
									if (count($categories)) {
										foreach ($categories as $categorie) {
											?>
											<span><a href="<?php echo esc_url(get_term_link($categorie)); ?>"><?php echo esc_html($categorie->cat_name); ?></a></span>
											<?php
										}
									}
									?>
								</div>
								<?php if ( has_post_thumbnail() ): ?>
									<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
										<?php the_post_thumbnail( 'hill-grid-thumb', array( 'alt' => get_the_title() ) ); ?>
									</a>
								<?php endif; ?>
								<div class="hill-single-post-meta-box entry-meta">
									<?php hill_entry_meta(); ?>
								</div>
								<h4 class="title-hill-single-post"><?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?></h4>	
							</div>
						</li>
						<?php endif; ?>
						<?php if ($pcount >= 6) { break; } ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if ($pcount >= 6) { break; } ?>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</ul>	
	</div>
<?php
	endif;
}

if ( ! function_exists( 'hill_get_option' ) ) {
	function hill_get_option( $id ){
		global $hill_option;

		if (isset($hill_option[$id])) {
			return $hill_option[$id];
		}

		return '';
	}
}

if ( ! function_exists( 'hill_get_yith_compare' ) ) {
	function hill_get_yith_compare(){
		global $yith_woocompare;

		return $yith_woocompare;
	}
}


if ( ! function_exists( 'hill_wpml_wc') ) {
	function hill_wpml_wc(){
		global $woocommerce_wpml;

		return $woocommerce_wpml;
	}
}

if ( ! function_exists( 'hill_wpml') ) {
	function hill_wpml(){
		global $sitepress;

		return $sitepress;
	}
}