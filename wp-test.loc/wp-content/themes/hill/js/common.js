
// dom.query = jQuery.noConflict( true );
jQuery(document).ready(function($){
    
    if ($.fn.flexslider) {
        $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: true,
            pausePlay: false,
            controlNav: false,
            smoothHeight: true,
            start: function(slider) {
                $('body').removeClass('loading');
            }
        });
        $('.carousel0').flexslider({
            animation: "slide",
            animationLoop: true,
            slideshow: true,
            controlNav: false,
            itemWidth: 180,
            maxItems: 4,
            minItems: 1
        });
        $('.product-right-sm-related-flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false
        });
    }

$(document).off( 'click', '.product a.compare');
        /*$(document).on( 'click', 'a.compare', function(e){
            e.preventDefault();
            console.log(yith_woocompare)
            var button = $(this),
                data = {
                    _yitnonce_ajax: yith_woocompare.nonceadd,
                    action: yith_woocompare.actionadd,
                    id: button.data('product-id'),
                    context: 'frontend'
                },
                widget_list = $('.yith-woocompare-widget ul.products-list');

            // add ajax loader
            button.block({message: null, overlayCSS: {background: '#fff url(' + woocommerce_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});
            widget_list.block({message: null, overlayCSS: {background: '#fff url(' + woocommerce_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});

            $.ajax({
                type: 'post',
                url: yith_woocompare.ajaxurl,
                data: data,
                dataType: 'json',
                success: function(response){
                    //button.unblock().addClass('added').text( yith_woocompare.added_label );
                    button.unblock().html('<i class="fa fa-tasks"></i>');
                    // add the product in the widget
                    widget_list.unblock().html( response.widget_table );

                    if (yith_woocompare.auto_open == 'yes') $('body').trigger( 'yith_woocompare_open_popup', { response: response.table_url, button: button } );
                }
            });
        });*/
        $(document).on( 'click', '.product a.compare, .products a.compare', function(e){
            e.preventDefault();

            var button = $(this),
                data = {
                    action: yith_woocompare.actionadd,
                    id: button.data('product_id'),
                    context: 'frontend'
                },
                widget_list = $('.yith-woocompare-widget ul.products-list');

            // add ajax loader
            if( typeof $.fn.block != 'undefined' ) {
                button.block({message: null, overlayCSS: { background: '#fff url(' + yith_woocompare.loader + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});
                widget_list.block({message: null, overlayCSS: { background: '#fff url(' + yith_woocompare.loader + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});
            }

            $.ajax({
                type: 'post',
                url: yith_woocompare.ajaxurl.toString().replace( '%%endpoint%%', yith_woocompare.actionadd ),
                data: data,
                dataType: 'json',
                success: function(response){

                    if( typeof $.fn.block != 'undefined' ) {
                        button.unblock();
                        widget_list.unblock()
                    }


                    button.addClass('added')
                            .attr( 'href', response.table_url )
                            .html( '<i class="fa fa-tasks"></i>' );

                    $('a.h_float_box2').attr( 'href', response.table_url );

                    $('a.h_float_box2').find('.h_f_number_box2').text(parseInt($('a.h_float_box2').find('.h_f_number_box2').text()) + 1);

                    // add the product in the widget
                    widget_list.html( response.widget_table );

                    if ( yith_woocompare.auto_open == 'yes')
                        $('body').trigger( 'yith_woocompare_open_popup', { response: response.table_url, button: button } );
                }
            });
        });

        // remove from table
        $(document).off( 'click', '.remove a');
        $(document).on( 'click', '.remove a', function(e){
            e.preventDefault();

            var button = $(this),
                data = {
                    action: yith_woocompare.actionremove,
                    id: button.data('product_id'),
                    context: 'frontend'
                },
                product_cell = $( 'td.product_' + data.id + ', th.product_' + data.id );

            // add ajax loader
            if( typeof $.fn.block != 'undefined' ) {
                button.block({
                    message: null,
                    overlayCSS: {
                        background: '#fff url(' + yith_woocompare.loader + ') no-repeat center',
                        backgroundSize: '16px 16px',
                        opacity: 0.6
                    }
                });
            }

            $.ajax({
                type: 'post',
                url: yith_woocompare.ajaxurl.toString().replace( '%%endpoint%%', yith_woocompare.actionremove ),
                data: data,
                dataType:'html',
                success: function(response){

                    // in compare table
                    var table = $(response).filter('table.compare-list');
                    $('body > table.compare-list').replaceWith( table );

                    $('a.h_float_box2', window.parent.document).find('.h_f_number_box2').text(parseInt($('a.h_float_box2', window.parent.document).find('.h_f_number_box2').text()) - 1);

                    $('.compare[data-product_id="' + button.data('product_id') + '"]', window.parent.document).removeClass('added').html( '<i class="fa fa-retweet"></i>' );

                    // removed trigger
                    $(window).trigger('yith_woocompare_product_removed');
                }
            });
        });

        $(document).on('click', 'a.h_float_box2, .hill_mini_cart_footer .hill-compare.hill_wcc_minicart', function (ev) {
            ev.preventDefault();

            var table_url = this.href;

            if (typeof table_url == 'undefined')
                return;

            $('body').trigger('yith_woocompare_open_popup', {response: table_url, button: $(this)});
        });
        

var removeNft = function(){
            if($("#notification") !== undefined){
                $('body div#notification').removeClass('notify-poped');
                setTimeout(function () {
                    $("#notification").remove();
                }, 600);
            }
        };
  hill_yith_ajax_wish_list = function(obj,ajaxurl,opts){
            $.ajax({
                type:"POST",
                url:ajaxurl,
                data:"product_id="+opts.add_to_wishlist+"&"+jQuery.param(opts),
                success : function(resp){
//                    var response = resp.split('##');
                    var response = resp;
                    $('body .wrapper div#notification').remove();
                    var ntop = $('#wpadminbar') !== undefined ? $('#wpadminbar').height() : 10;
                    if(response.result == 'true'){                        
                        if(hill_ADD_TO_WISHLIST_SUCCESS_TEXT !== undefined)
                        $('<div id="notification" class="hill-wishlist-notify"><div class="success">'+hill_ADD_TO_WISHLIST_SUCCESS_TEXT+'<a class="close" href="#"><i class="fa fa-times-circle"></i></a></div></div>').prependTo('body');
                        //$('body div#notification').css('top',ntop+'px');
                        // $('body div#notification > div').fadeIn('show');
                        setTimeout(function(){
                            $('body div#notification').addClass('notify-poped');

                            $('body div#notification').on('click', '.close', function (e) {
                                e.preventDefault();

                                removeNft();
                            });
                        },10);
                        
                        //alert('i am here...');
                        // $('html,body').animate({scrollTop:0},300);
                    }                    
                    else if(response.result == 'exists'){
                        if(hill_ADD_TO_WISHLIST_EXISTS_TEXT !== undefined)
                        $('<div id="notification" class="hill-wishlist-notify"><div class="success">'+hill_ADD_TO_WISHLIST_EXISTS_TEXT+'<a class="close" href="#"><i class="fa fa-times-circle"></i></a></div></div>').prependTo('body');
                        //$('body div#notification').css('top',ntop+'px');
                        setTimeout(function(){
                            $('body div#notification').addClass('notify-poped');
                            $('body div#notification').on('click', '.close', function (e) {
                                e.preventDefault();

                                removeNft();
                            });
                        },10);
                        // $('body div#notification > div').fadeIn('show');
                        // $('html,body').animate({scrollTop:0},300);
                         //alert('i am here...');
                        
                    }
                    setTimeout(function(){
                        removeNft();                        
                    },10000);
                    
                }
            });
        
};

$(document).off("click",".add_to_wishlist");
$(document).on("click",".add_to_wishlist",function(){
    var b = yith_wcwl_plugin_ajax_web_url;
    var opts={add_to_wishlist:$(this).data("product-id"),product_type:$(this).data("product-type"),action:"add_to_wishlist"};
    hill_yith_ajax_wish_list($(this),b,opts);            
    return false;
});

  jQuery(function($) {
            "use strict";

            //alert('hello data...');
            $("ul.product-categories li").each(function() {

                if ($(this).children('ul.children').length > 0) {

                    $(this).addClass('parent-cat');

                    $(this).prepend('<span class="expand plus"></span>');

                    $('ul.children li').removeClass('parent-cat');

                }

            });
            $(document).on('click','ul.product-categories li.parent-cat .expand',function(e) {

                e.preventDefault();
                
                if($(this).hasClass('plus')){
                    $(this).removeClass('plus').addClass('minus');
                    $(this).parent().children('ul.children').slideDown(250);
                }else{
                    $(this).removeClass('minus').addClass('plus');
                    $(this).parent().children('ul.children').slideUp(250);                    
                }

            });

            $('.btn-navbar').toggle(function() {

                $(this).next('.nav-collapse').slideDown(200);

            }, function() {

                $(this).next('.nav-collapse').slideUp(200);

            })

            $(".navbar .nav > li").each(function() {

                if ($(this).find('ul').length > 0) {

                    $(this).prepend('<span class="expand">+</span>');

                }

            });



            $(".navbar .nav > li > span.expand").on('click', function() {

                var elem = $(this).parent().find('ul');

                if (elem.length > 0 && elem.css('display') == 'none') {

                    elem.slideDown(200);

                    $(this).html('-');

                }
                else if (elem.length > 0 && elem.css('display') == 'block') {
                    elem.slideUp(200);
                    $(this).html('+');
                }
            });
            
            $('.mobile-nav .nav li').each(function(){
                if($(this).children('ul.sub-menu').length>0 || $(this).children('.brands-wall').length>0){
                   $(this).children('a').append('<span class="menu-expander dashicons-arrow-down-alt2"><span>'); 
                }
                else if($(this).find('.large-10 .five-nb').length>0){                   
                   $(this).find('.large-10 .five-nb > span > a').append('<span class="menu-expander dashicons-arrow-down-alt2"><span>');
                }
                
            });

            $('.mobile-nav .nav').css({height:'0px',visibility:'hidden'});

            $('a.mobile_menu_trigger').on('click',function(){
                
                var elem = $(this).next('.mobile-nav').children('ul.nav');
                if(elem.css('visibility') === 'hidden')
                    elem.css({visibility:'visible',height:'auto'});
                else
                    elem.css({height:'0px',visibility:'hidden'});
                
                return false;        
            }
        );


            $(document).on('click','.menu-expander',function(e){    
                e.preventDefault();
                if($(this).hasClass('dashicons-arrow-down-alt2')){                    
                    $(this).removeClass('dashicons-arrow-down-alt2').addClass('dashicons-arrow-up-alt2');
                    $(this).parent().next('ul.sub-menu').css({visibility:'visible',opacity:'1'}).slideDown(200);

                    $(this).parents('.five-nb').children('ul').css({visibility:'visible',opacity:'1'}).slideDown(200);
                    $(this).parent().next('.brands-wall').css({visibility:'visible',opacity:'1'}).slideDown(200);                
                }else{
                    $(this).removeClass('dashicons-arrow-up-alt2').addClass('dashicons-arrow-down-alt2');;
                    $(this).parent().next('ul.sub-menu').slideUp(200);

                    $(this).parents('.five-nb').children('ul').slideUp(200);
                    $(this).parent().next('.brands-wall').slideUp(200);
                }
                      
            });
            
        });

  //alert('I am here..');



  //console.log('i am here...');
  //HANDLES CLEARING THE SEARCHBOX
  

  /*jQuery(".input_search").focus(function(){
    jQuery(this).addClass('black-color');
    if(jQuery(this).val() === 'Search'){
      jQuery(this).val('');
    }
  }); 
  jQuery(".input_search").blur(function(){
     jQuery(this).removeClass('black-color');
    if(jQuery(this).val() === ''){
      jQuery(this).val('Search');
    }
  });*/



  jQuery(".h_input_search_cat").focus(function(){
    jQuery(this).addClass('black-color');
    if(jQuery(this).val() === 'Enter your keyword'){
      jQuery(this).val('');
    }
  }); 
  jQuery(".h_input_search_cat").blur(function(){
     jQuery(this).removeClass('black-color');
    if(jQuery(this).val() === ''){
      jQuery(this).val('Enter your keyword');
    }
  });



    jQuery(".h_input_fotter_search").focus(function(){
    jQuery(this).addClass('white-color');
    if(jQuery(this).val() === 'Search'){
      jQuery(this).val('');
    }
  }); 
  jQuery(".h_input_fotter_search").blur(function(){
     jQuery(this).removeClass('white-color');
    if(jQuery(this).val() === ''){
      jQuery(this).val('Search');
    }
  });
 

  

   // jQuery(window).scroll(function() {

   // 		var iCurScrollPos = jQuery(this).scrollTop();

   // 		//console.log(iCurScrollPos);

   //     jQuery(".h_float_utiliy_box").css({"top": iCurScrollPos+300})


   //  });


	 jQuery(".h_footer h4").click(function(){
        var widget_parent = $(this).parent(),
            toggleElem = widget_parent.find('ul.menu');

        if ($(window).width() >= 481) {
            return;
        }

        toggleElem.slideToggle();
        $(this).toggleClass('h_active');
	});




    // Blog post category
    $('.widget_categories ul li').each(function () {
        var hasChildern = $(this).has('ul.children');
        if (hasChildern.length) {
            $(this).children('ul.children').before('<span class="h_cat_plus"><i class="fa fa-plus-circle"></i></span>');
        }
    });

    $('body').on('click', '.h_cat_plus', function () {
        var parentSelector = $(this).parent('li'),
            childElement = parentSelector.children('ul.children');

        childElement.slideToggle(200);

        $(this).find('i').toggleClass('fa-times-circle');
    });


    $('.hill-rated-post-slider-item').owlCarousel({
        items: 2,
        itemsDesktop: [1199,2],
        itemsDesktopSmall: [991,2],
        itemsTablet: [768,1],
        itemsMobile: [479,1],
    });


    /*
     * Hill Love It Plugin
     */
    $('.hill_love_it').click(function (e) {
        var post_id = parseInt( $(this).attr('data-post-id') );
        var $this = $(this);

        if( post_id > 0){
            jQuery.ajax({
                type : "post",
                dataType : "json",
                url : hill_love_it.ajaxurl,
                data : {action: "hill_love_it_ajax", post_id : post_id, nonce: hill_love_it.nonce},
                success: function(response){
                    if(response.type == 'success'){
                        $this.find('span').html( response.count );
                        $this.find('i').removeClass('fa-heart-o').addClass( 'fa-heart' );
                    }
                }
            });
        }

        e.preventDefault();
        e.stopPropagation();
    });

    //$('.input-text.qty.text').spinner();
    $( ".input-text.qty.text" ).spinner({
            spin: function( event, ui ) {
				/*if(ui.value<1){
					return false;
				}*/
				$( '.woocommerce-cart-form input[name="update_cart"]' ).prop( 'disabled', false );
			}
    });
	

    // $('.hill_wc_single_thumbnails').bxSlider({
    //     mode: 'vertical',
    //     slideMargin: 5
    // });

    $('.hill_wc_single_thumbnails').owlCarousel({
        items: 3,
        navigation: true,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });

    


    

    $('.hill_wc_top_sale_slider ul.products').owlCarousel({
        items: 1,
    });

    function float_utility_box ($this) {
        if ($this.scrollTop() >= 300) {
            $('.h_float_utiliy_box').addClass('hiil_fu_box_sticky');
        } else if($this.scrollTop() <= 299){
            $('.h_float_utiliy_box').removeClass('hiil_fu_box_sticky');
        }
    }
    float_utility_box($(window));
    $(window).on('scroll', function () {
        float_utility_box($(this));
    });
    $('a.gotto_f_top_box').on('click', function (e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
    $('.hill_wc_top_sale_slider ul.products li .hillpro-single-box').each(function () {
        var product_image_container = $(this).find('.hill-proImg-container'),
            main_right_col_html = $('<div class="hill-wc-title-desc-col"></div>'),
            title_of_product = $(this).find('.hill-wocommerce-right-col h3').clone(),
            desc_of_product = $(this).find('.hill-wocommerce-right-col .h-excerpt').clone(),
            pricing_of_product = $(this).find('.hill-wocommerce-right-col .hill_wc_rating_price'),
            rating_of_product = $(this).find('.hill-wocommerce-right-col .star-rating');

        product_image_container.wrap('<div class="hill_single_slider_img_cont clearfix"></div>')

        main_right_col_html.append(title_of_product);
        main_right_col_html.append(desc_of_product);
        $(main_right_col_html).insertAfter(product_image_container);
        rating_of_product.insertAfter(pricing_of_product);
    });


    $('.hill_carousel_product_3_col ul.products').owlCarousel({
        items: 3,
    });

    // Sticky Menu
    $(window).on('scroll', function () {
        var $this = $(this),
            containerHeight = $('.h_banner_1_modify').outerHeight();

        if ($this.outerWidth() >= 1025) {
            if ($this.scrollTop() >= (containerHeight + 50)) {
                $('.h_banner_1_modify').addClass('hill_sticky_navbar');
                $('.h_banner_1_modify').css({'height': containerHeight});
            } else if( $this.scrollTop() <= (containerHeight + 49) ){
                $('.h_banner_1_modify').removeClass('hill_sticky_navbar');
                $('.h_banner_1_modify').css({'height': 'auto'});
            }
        };
        
    });

    // .h_upper_nav_pan
    /*$(window).on('scroll', function () {
        var $this = $(this),
            containerHeight = $('.h_upper_nav_pan').outerHeight();

        if ($this.outerWidth() >= 1025) {
            if ($this.scrollTop() >= (containerHeight + 50)) {
                $('.h_upper_nav_pan').addClass('hill_sticky_navbar');
                $('.h_upper_nav_pan').css({'height': containerHeight});
                $('.hill_header_catnav .hill_cm_cont .hill_cm_holder_cont').slideUp();
            } else if( $this.scrollTop() <= (containerHeight + 49) ){
                $('.h_upper_nav_pan').removeClass('hill_sticky_navbar');
                $('.h_upper_nav_pan').css({'height': 'auto'});
                $('.hill_header_catnav .hill_cm_cont .hill_cm_holder_cont').slideDown();
            }
        };
        
    });*/

    // $('.hill_pro_slider_nav ul.products').owlCarousel({
    //     items: 4,
    //     navigation: true,
    //     pagination: false,
    //     navigationText: ['<i class="fa fa-chevron-circle-left"></i>', '<i class="fa fa-chevron-circle-right"></i>']
    // });

    


    // Categories Menu

    $('.hill_catmenu_more_less').on('click', function (e) {

        e.preventDefault();

        var menu_container = $(this).parents('.hill_cm_cont'),
            less_cats_txt = $(this).data('lesscat'),
            more_cats_txt = $(this).data('morecat');

        if ($(this).hasClass('expanded')) {
            hill_hide_catmenu_more_items(menu_container, false);
            $(this).text(more_cats_txt);
            $(this).removeClass('expanded');
        } else {
            hill_hide_catmenu_more_items(menu_container, true);
            $(this).text(less_cats_txt);
            $(this).addClass('expanded');
        }

    });
    $('.hill_cm_cont').each(function () {

        hill_hide_catmenu_more_items($(this), false);
    });
    

    function hill_hide_catmenu_more_items (cm_container, show_full) {
        var item_limit = cm_container.data('limit'),
            menu_colps = cm_container.find('.hill_cat_menu_holder'),
            menu_ul = cm_container.find('.mega_main_menu_ul'),
            itemHeight = menu_ul.children().outerHeight(),
            menu_real_height = 44*menu_ul.children().length,
            menu_closed_height = 44*item_limit,
            display_css = 'none',
            animate_height = menu_closed_height;

        cm_container.css({'height': menu_closed_height+cm_container.find('.hill_catmenu_more_less').outerHeight()+cm_container.find('.hill_cm_title').outerHeight()});

        if (show_full) {
            display_css = 'block';

            menu_ul.children().each(function (i) {
                if ( i >= item_limit) {
                    $(this).css({'display': display_css});
                };
            });

            animate_height = menu_real_height;
        }

        menu_colps.css('overflow', 'hidden');
        menu_colps.animate({'height': animate_height}, function () {
            menu_colps.css('overflow', 'visible');
            if (!show_full) {
                menu_ul.children().each(function (i) {
                    if ( i >= item_limit) {
                        $(this).css({'display': display_css});
                    };
                });
            }
        });
    }

    $('.hill_cm_holder_cont.hill_cm_holder_cont_uped').slideUp();

    $('.hill_header_catnav .hill_cm_title').on('click', function (e) {

        e.preventDefault();

        var menu_container = $(this).parent('.hill_cm_cont'),
            menu_holder = menu_container.find('.hill_cm_holder_cont');

        menu_holder.slideToggle();

    });


    $('.hill-mn-toggle').on('click', function () {
        var parent = $(this).parent('.hill-mobile-nav'),
            nav_container = parent.find('.hill-mobile-nav-container');

        nav_container.slideToggle();
    });

    $('.hill-mobile-nav-container li i').on('click', function () {
        var parent = $(this).parent('li'),
            child_container = parent.children('ul.sub-menu');

        $(this).toggleClass('hill_icon_active');
        child_container.slideToggle();
    });


    $('.error404').css({'height': $(window).height()});

});


jQuery(document).ready(function($) {

    var hillCookie = {
        setCookie: function ( cname,cvalue,exdays ) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname+"="+cvalue+"; "+expires;
        },
        getCookie: function ( cname ) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return false;
        },
        checkCookie: function ( cname ) {
            var uCookie = this.getCookie(cname);

            if (uCookie) {
                return true;
            }

            return false;
        }
    };

    function hill_newslatter_popup () {
        var is_enable = !hillCookie.checkCookie('nlpopup');

        if(jQuery(window).width() <= 767){
            is_enable = false;
        }

        if (is_enable) {
            setTimeout(function () {
                jQuery('#hill_popup').modal('show');
            }, 1000);
        }


    }

    hill_newslatter_popup();

    jQuery('#hill_popup').on('hidden.bs.modal', function (e) {
        var is_checked = jQuery('#dont_show_again:checked').length;

        if (is_checked) {
            hillCookie.setCookie('nlpopup', 'true', 90);
        };
    });

    jQuery('#hill_popup').on('shown.bs.modal', function (e) {
        var modal = jQuery(this).find('.hill-subscrib-pan.hill_sp_fill_bg'),
            is_add_able = modal.outerHeight() % 2;

        if (is_add_able) {
            modal.css({'height':modal.outerHeight()+1});
        };

    });

    


    var fix_product_box_title_height = function  () {
        jQuery('.h_banner_pro_utility_box_wrapper').each(function () {
            jQuery(this).find('.h_best_seller_left').css({'height': '0px'});
            
            var height = jQuery(this).height();

            jQuery(this).find('.h_best_seller_left').css({'height': height+'px'});
        });
    }

    jQuery('.h_pro_box_all_products ul.products').owlCarousel({
        items: 3,
        itemsDesktop: [1199,3],
        itemsDesktopSmall: [991,2],
        itemsTablet: [768,2],
        itemsMobile: [479,1],
        afterAction: fix_product_box_title_height
        // pagination: false,
    });

    fix_product_box_title_height();

    setTimeout(fix_product_box_title_height, 3000);


    jQuery(window).resize(fix_product_box_title_height);

    jQuery('.hill_product_filter img').ready(function () {
        var $grid = jQuery('.hill_product_filter .woocommerce').isotope({
            // options
            itemSelector: '.col-hill',
            layoutMode: 'fitRows'
        });

        jQuery('.hill_pf_button').on('click', function (e) {

            e.preventDefault();


            $grid.isotope({filter: jQuery(this).attr('data-filter')});

            jQuery('.hill_pf_active').removeClass('hill_pf_active');
            jQuery(this).addClass('hill_pf_active');
        });
    });

    if (jQuery.fn.yith_wcan_ajax_filters) {
        jQuery( ".price_slider" ).on( "slidechange", function( e, ui ) {
            jQuery(this).yith_wcan_ajax_filters(e, this);
        } );
    }
	
	
	
	
    // variable product image handling
    $(window).on('load', function(){
        
        var $form = $('form.variations_form');

        if ($form.length === 1) {

            var variations = $form.data('product_variations');

            var $product_img = $('.product div.image img.attachment-shop_single').eq(0);
		
            var src = $product_img.attr('src');
			
			var fullsrc = $product_img.attr('data-src');
			var data_largei_w=$product_img.attr('data-large_image_width') 
			var data_largei_h=$product_img.attr('data-large_image_height') 
			var data_maini_w=$product_img.attr('width') 
			var data_maini_h=$product_img.attr('height') 

            $product_img.attr('data-main', src);
			//console.log(variations)
           /*
            if(!$product_img.parent().is('a')){
                var $product_img_clone = $product_img.clone();
                $product_img.parent().prepend('<a href="#" data-rel="prettyPhoto" data-href="'+src+'" class="woocommerce-main-image zoom"></a>');
                $product_img.remove();
                $product_img_clone.appendTo('.product div.image a.zoom');
                $('a.zoom').prettyPhoto({social_tools:!1,theme:"pp_woocommerce",horizontal_padding:20,opacity:.8,deeplinking:!1});
            }else{
                $product_img.closest('a.zoom').attr('data-href', $product_img.closest('a.zoom').attr('href'));
            }*/


            $.each(variations, function(k, v) {
				
                var match = 0;

                var count = 0;
	

                $.each(v.attributes, function(name, value) {

                    count++;

                    var vval = $form.find('[name="' + name + '"] > option:selected').val();

                    if (value === vval || (value === '' && vval !== undefined && vval !== '')){
                         match++;
                    }

                });



                if (match === count) {

                    if (v.image.src != '' && v.image.url != '') {
					
                        $product_img.parent().attr('href', v.image.url);
                        $product_img.attr('src', v.image.src);
						$product_img.parent().next('.zoomImg').attr('src', v.image.url)
						
						
						$product_img.attr('data-src', v.image.url);
						$product_img.attr('data-large_image', v.image.url);
						$product_img.attr('data-main', v.image.url);
						var img = new Image();
						img.src = v.image.full_src;
						$product_img.attr('data-large_image_width', img.width);
						$product_img.attr('data-large_image_height', img.height);
						$product_img.attr('width', v.image.src_w);
						$product_img.attr('height', v.image.src_h);
						
						$('.first-img-thum').attr('src',v.image.url)

                    }

                }else{
					
				}

            });

        }


        $form.on('found_variation', function(event, variation) {
            var $product = $(this).closest('.product');
            var cloud_zoom = $product.find('a.zoom').eq(0);
			
            if (variation.variation_id != 'undefined' && variation.image.src) {

                cloud_zoom.attr("href", variation.image.src);
                cloud_zoom.find('img').attr("src", variation.image.src);
				cloud_zoom.next('.zoomImg').attr('src', variation.image.url)
				
				
				cloud_zoom.find('img').attr('data-src', variation.image.url);
				cloud_zoom.find('img').attr('data-large_image', variation.image.url);
				cloud_zoom.find('img').attr('data-main', variation.image.url);
				var img = new Image();
				img.src = variation.image.full_src;
				cloud_zoom.find('img').attr('data-large_image_width', img.width);
				cloud_zoom.find('img').attr('data-large_image_height', img.height);
				cloud_zoom.find('img').attr('width', variation.image.src_w);
				cloud_zoom.find('img').attr('height', variation.image.src_h);
				$('.first-img-thum').attr('src',variation.image.url)
				$('.first-img-thum').click();
				$('body').click()
				$('.flex-active').removeClass('flex-active');
				$('.flex-control-nav li:first-child img').click();
				$('.flex-control-nav li:first-child img').addClass('flex-active');
            } else {
                $(this).trigger('reset_image');
            }

        }).on('reset_image', function(event) {
			$product_img.attr('src', src);
			$product_img.parent().next('.zoomImg').attr('src', fullsrc)
			
			$product_img.attr('data-src', fullsrc);
			$product_img.attr('data-large_image', fullsrc);
			$product_img.attr('data-main', fullsrc);
			$product_img.attr('data-large_image_width', data_largei_w);
			$product_img.attr('data-large_image_height', data_largei_h);
			$product_img.attr('width', data_maini_w);
			$product_img.attr('height', data_maini_h);
			
			$('.first-img-thum').attr('src',fullsrc)
			$('.first-img-thum').click();
			$('body').click()
						 
			$('.flex-active').removeClass('flex-active');
			$('.flex-control-nav li:first-child img').click();
			$('.flex-control-nav li:first-child img').addClass('flex-active');
        });
    });
	
$(window).load(function() {
    if($('#carousel-for-p-thum').length){

      $('#carousel-for-p-thum').flexslider({
    	animation: "slide",
    	controlNav: true,
    	animationLoop: true,
    	slideshow: false,
    	itemWidth: 150,
    	minItems  :3,
    	maxItems:3,
    	directionNav: false,
    	asNavFor: '.woocommerce-product-gallery'
      });
      $('#carousel-for-p-thum').show()
    }
});
	
	
	
});