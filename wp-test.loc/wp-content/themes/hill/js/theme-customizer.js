( function( $ ) {

	wp.customize( 'blogname', function( value ) {
		value.bind( function( newval ) {
			$( '#site-title a' ).html( newval );
		} );
	} );
	
	//Update the site description in real time...
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( newval ) {
			$( '.site-description' ).html( newval );
		} );
	} );

//        Common

        wp.customize( 'illusion_colors[illusion_header_color]', function( value ) {
        value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }
                        
                        var newRule = '.framwork-hdr-title{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
            $('style#ilscustomize_css').html( newRule );
        } );
    } );


        wp.customize( 'illusion_colors[illusion_link_hover_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }
                        
                        var newRule = 'a:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_alt_link_hover_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }
                        
                        var newRule = 'a:not([class*="_hover"]):hover, blockquote.type_2:before, .thumbnails_container > ul > .active article > p, .thumbnails_container > ul > li:hover article > p, blockquote.type_2 > p:last-of-type:after, .main_menu li:hover > a i[class|="icon"], a:not([class*="_hover"]):hover [class*="icon_wrap"][class*="color_"], a:not([class*="_hover"]):hover [class|="icon"][class*="color_"], .categories_list li:hover > a, .categories_list li:hover > a > *, .color_scheme, .color_scheme_hover:hover, .color_scheme_hover:hover [class*="color_"], .dropdown_2_container.login:hover > [class*="icon_wrap_"] > [class|="icon"], .woocommerce.widget_products .product_list_widget li a:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );

        wp.customize( 'illusion_colors[illusion_bg_gradient_color]', function( value ) {
		value.bind( function( newval ) {

			$('.bg_gradient, .divider_type_2, .gradient_line, #qLbar').css( 'background',newval );
			$('#qLpercentage').css( 'color',newval+' !important' );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_color_dark]', function( value ) {
		value.bind( function( newval ) {

			$('.color_dark, .tweet_text a:hover, .color_dark_hover:hover, .select_list li:hover, [class*="button_type_"].color_dark, .breadcrumbs li a.color_default:hover, .woocommerce.widget_products .product_list_widget li a').css( 'color',newval );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_color_dark_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.item_services a.color_dark:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_text_color_blue]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.color_blue, .color_blue_hover:hover, [class*="button_type_"].color_blue:not(.transparent):hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_bg_color_blue]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '[class*="button_type_"].transparent.color_blue:hover, [class*="button_type_"].color_blue:not(.transparent), .animation_fill.color_blue:before{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_text_color_pink]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.color_pink, .color_pink_hover:hover, .color_pink_hover:hover [class*="color_"], [class*="button_type_"].color_pink:not(.transparent):hover, .dropdown_2_container.shoppingcart:hover > [class*="icon_wrap_"] > [class|="icon"]{'+
                                'color: '+newval+' !important;'+                                
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_border_color_pink]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.border_color_pink, [class*="icon_wrap_"].color_pink, .color_pink_hover:hover [class*="icon_wrap_"], [class*="button_type_"].color_pink, .p_table.bg_color_pink_hover:hover, .p_table.bg_color_pink_hover.active, [class*="button_type_"].color_pink_hover:not(.color_light):hover, [class*="icon_wrap_"].color_pink_hover:hover, .dropdown_2_container.shoppingcart:hover > [class*="icon_wrap_"]{'+ 
                                'border-color: '+newval+' !important;'+                                
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_bg_color_pink]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.bg_color_pink, .ui-slider-range, .bg_color_pink_hover:hover, .p_table.bg_color_pink_hover.active, .animation_fill.color_pink:before, [class*="button_type_"].transparent.color_pink:hover, [class*="button_type_"].color_pink:not(.transparent){'+ 
                                'background-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_text_color_green]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.color_green, .color_green_hover:hover, [class*="button_type_"].color_green_hover:hover, [class*="button_type_"].color_green:not(.transparent):hover{'+
                                'color: '+newval+' !important;'+                                
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_border_color_green]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.select_button_left, input:not([type="submit"]).success, [class*="button_type_"].color_green_hover:hover, [class*="button_type_"].color_green{'+
                                'border-color: '+newval+' !important;'+                                
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_bg_color_green]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.bg_color_green, [class*="button_type_"].color_green.transparent:hover, [class*="button_type_"].color_green:not(.transparent){'+
                                'background-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        
        wp.customize( 'illusion_colors[illusion_bg_color_purple]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.bg_color_purple, .paginations .active a, .paginations li a:hover, .step:hover .step_counter, .title_counter_type:before, .bg_color_purple_hover:hover, .animation_fill.color_purple:before, .p_table.bg_color_purple_hover.active, [class*="button_type_"].transparent.color_purple:hover, [class*="button_type_"].color_purple:not(.transparent), .shop_table.cart .actions input.checkout-button:hover, .shop_table.cart .actions input[name="update_cart"]:hover, .shop_table.cart .coupon input[type="submit"]:hover, .shipping-calculator-form button[name="calc_shipping"]:hover, .place-order input[name="woocommerce_checkout_place_order"]:hover, .woocommerce .login .form-row input[name="login"]:hover, .woocommerce .lost_reset_password input[name="wc_reset_password"]:hover, .woocommerce-account .woocommerce input[name="save_account_details"]:hover, .woocommerce-account .woocommerce input[name="save_address"]:hover, a.added_to_cart:hover{'+
                                'background-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_text_color_purple]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.checkout_video:hover .video_button i[class|="icon"], .color_purple, .step:hover h3, blockquote.type_3:before, blockquote.type_3 > p:last-of-type:after, .color_purple_hover:hover, .category_link:hover .category_title, .color_purple [class*="icon_wrap_"], [class*="button_type_"]:not(.transparent).color_purple:hover, .shop_table.cart .coupon input[type="submit"], .shop_table.cart .actions input.checkout-button, .shop_table.cart .actions input[name="update_cart"], .shipping-calculator-form button[name="calc_shipping"], .place-order input[name="woocommerce_checkout_place_order"], .woocommerce .login .form-row input[name="login"], .woocommerce .lost_reset_password input[name="wc_reset_password"], .woocommerce-account .woocommerce input[name="save_account_details"], .woocommerce-account .woocommerce input[name="save_address"], a.added_to_cart, .tp-caption a, .widget_search form.searchform input[type="submit"], .woocommerce.widget_price_filter .price_slider_amount button, .woocommerce.widget_product_search form input[type="submit"], .yith-wcan-reset-navigation.button, .woocommerce .widget_layered_nav ul.yith-wcan-label li a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a, .wishlist_table .product-add-to-cart .add_to_cart.button{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_border_color_purple]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.border_color_purple, .paginations .active a, .paginations .active:last-child a, .paginations li a:hover, .paginations li:last-child a:hover, .step:hover .step_counter, [class*="icon_wrap_"].color_purple, .color_purple [class*="icon_wrap_"], [class*="button_type"].color_purple, .bg_color_purple_hover:hover, .p_table.bg_color_purple_hover:hover, .p_table.bg_color_purple_hover.active, [class*="icon_wrap_"].color_purple_hover:hover, [class*="button_type"].color_purple_hover:not(.color_light):hover, .shop_table.cart .coupon input[type="submit"], .shop_table.cart .actions input.checkout-button, .shop_table.cart .actions input[name="update_cart"], .shipping-calculator-form button[name="calc_shipping"], .place-order input[name="woocommerce_checkout_place_order"], .woocommerce .login .form-row input[name="login"], .woocommerce .lost_reset_password input[name="wc_reset_password"], .woocommerce-account .woocommerce input[name="save_account_details"], .woocommerce-account .woocommerce input[name="save_address"], a.added_to_cart{'+
                                'border-color: '+newval+' !important;'+
                        "}\n";
                        newRule += '.paginations .active + li a, .paginations li:hover + li a{'+
                                'border-left-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_text_color_light]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }
                        
                        var newRule = 'mark, .color_light, .social_icons a:hover, .tabs_nav .ui-state-active a, .tabs_nav .active a, .checkout_video:hover *, .paginations .active a, .color_light_hover:hover, .paginations li a:hover, .step:hover .step_counter, .link_container:hover [class*="icon_wrap_"], .p_table:hover > *:not([class*="button_type"]), .p_table.active > *:not([class*="button_type"]), .steps_nav li .animation_fill.type_2, [class*="button_type_"].transparent:not(.color_light):hover, [class*="button_type_"]:not(.transparent), .animation_fill[class*="color_"]:hover, .item_services h6:hover .animation_fill, .shop_table.cart .coupon input[type="submit"]:hover, .shop_table.cart .actions input.checkout-button:hover, .shop_table.cart .actions input[name="update_cart"]:hover, .shipping-calculator-form button[name="calc_shipping"]:hover, .place-order input[name="woocommerce_checkout_place_order"]:hover, .woocommerce .login .form-row input[name="login"]:hover, .woocommerce .lost_reset_password input[name="wc_reset_password"]:hover, .woocommerce-account .woocommerce input[name="save_account_details"]:hover, .woocommerce-account .woocommerce input[name="save_address"]:hover, a.added_to_cart:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_woo_spc_dropdown_border_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.dropdown_2_container.shoppingcart .dropdown_2{'+
                                'border-top-color: '+newval+' !important;'+
                        "}\n";
                        newRule += '.dropdown_2_container.shoppingcart .dropdown_2:before{'+
                                'border-bottom-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_yith_wcwl_share_fb_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '.yith-wcwl-share ul li a.facebook:hover{'+
                                'color: '+newval+' !important;'+
                                'border-color: '+newval+' !important;'+
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_yith_wcwl_share_tw_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '.yith-wcwl-share ul li a.twitter:hover{'+
                                'color: '+newval+' !important;'+
                                'border-color: '+newval+' !important;'+
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_yith_wcwl_share_pin_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '.yith-wcwl-share ul li a.pinterest:hover{'+
                                'color: '+newval+' !important;'+
                                'border-color: '+newval+' !important;'+
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_yith_wcwl_share_gp_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '.yith-wcwl-share ul li a.googleplus:hover{'+
                                'color: '+newval+' !important;'+
                                'border-color: '+newval+' !important;'+
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_yith_wcwl_email_hover]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '.yith-wcwl-share ul li a.email:hover{'+
                                'color: '+newval+' !important;'+
                                'border-color: '+newval+' !important;'+
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_scrollbar_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');                            
                        }                        
                        var newRule = '::-webkit-scrollbar-thumb{'+
                                'background-color: '+newval+' !important;'+                                
                        "}\n";                        
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        
//        Main Menu
        wp.customize( 'illusion_colors[illusion_mm_p_text_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu > li > a').css( 'color',newval );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_mm_p_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu > li > a').css( 'background',newval );
		} );
	} );
        
	
        
	wp.customize( 'illusion_colors[illusion_mm_p_hover_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.main_menu li:hover > a, .main_menu li:hover > a i[class|="icon"]{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_p_hover_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.main_menu li:hover > a, .main_menu li:hover > a i[class|="icon"]{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        
        wp.customize( 'illusion_colors[illusion_mm_active_text_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu .current-menu-ancestor > a, .main_menu .current-menu-item > a').css( 'color',newval );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_p_after_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.main_menu > li > a:after{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_p_border_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu > li > a, #menu_button.color_blue, [class*="icon_wrap_"].color_blue, [class*="button_type_"].color_blue, [class*="icon_wrap_"].color_blue_hover:hover, [class*="button_type_"].color_blue_hover:hover').css( 'border-color',newval );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_sub_active_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu .sub_menu .current-menu-ancestor > a, .main_menu .sub_menu .current-menu-item > a').css( 'background',newval );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_sub_text_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu .sub_menu li > a').css( 'color',newval );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_mm_sub_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu .sub_menu li > a').css( 'background',newval );
		} );
	} );

        wp.customize( 'illusion_colors[illusion_mm_sub_hover_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.main_menu .sub_menu li > a:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_mm_sub_hover_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.main_menu .sub_menu li > a:hover{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );

	wp.customize( 'illusion_colors[illusion_mm_mm_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('.main_menu .ilsmegamenu > ul.sub_menu').css( 'background',newval );
		} );
	} );
//        Side Menu
	wp.customize( 'illusion_colors[illusion_sd_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('#side_menu').css( 'background',newval );
		} );
	} );
        
	wp.customize( 'illusion_colors[illusion_sd_link_color]', function( value ) {
		value.bind( function( newval ) {

			$('#side_menu ul li a.color_light_2').css( 'color',newval );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_sd_link_hover_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '#side_menu ul li a.color_light_2.color_blue_hover:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
	wp.customize( 'illusion_colors[illusion_sd_current_link_color]', function( value ) {
		value.bind( function( newval ) {

			$('#side_menu ul li.current-menu-item > a.color_light_2').css( 'color',newval );
		} );
	} );
        
        
        
//        Buttons
	wp.customize( 'illusion_colors[illusion_button_text_color]', function( value ) {
		value.bind( function( newval ) {

			$('.widget_search form.searchform input[type="submit"], .woocommerce.widget_price_filter .price_slider_amount button, .woocommerce.widget_product_search form input[type="submit"], .yith-wcan-reset-navigation.button, .woocommerce .widget_layered_nav ul.yith-wcan-label li a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a, .wishlist_table .product-add-to-cart .add_to_cart.button').css( 'color',newval );
		} );
	} );
	wp.customize( 'illusion_colors[illusion_button_bg_color]', function( value ) {
		value.bind( function( newval ) {

			$('.widget_search form.searchform input[type="submit"], .woocommerce.widget_price_filter .price_slider_amount button, .woocommerce.widget_product_search form input[type="submit"], .yith-wcan-reset-navigation.button, .woocommerce .widget_layered_nav ul.yith-wcan-label li a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a, .wishlist_table .product-add-to-cart .add_to_cart.button').css( 'background',newval );
		} );
	} );
	wp.customize( 'illusion_colors[illusion_button_border_color]', function( value ) {
		value.bind( function( newval ) {

			$('.widget_search form.searchform input[type="submit"], .woocommerce.widget_price_filter .price_slider_amount button, .woocommerce.widget_product_search form input[type="submit"], .yith-wcan-reset-navigation.button, .woocommerce .widget_layered_nav ul.yith-wcan-label li a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a, .wishlist_table .product-add-to-cart .add_to_cart.button').css( 'border-color',newval );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_button_hover_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.widget_search form.searchform input[type="submit"]:hover, .woocommerce.widget_price_filter .price_slider_amount button:hover, .woocommerce.widget_product_search form input[type="submit"]:hover, .yith-wcan-reset-navigation.button:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a, .wishlist_table .product-add-to-cart .add_to_cart.button:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_button_hover_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.widget_search form.searchform input[type="submit"]:hover, .woocommerce.widget_price_filter .price_slider_amount button:hover, .woocommerce.widget_product_search form input[type="submit"]:hover, .yith-wcan-reset-navigation.button:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a, .wishlist_table .product-add-to-cart .add_to_cart.button:hover{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_button_hover_border_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.widget_search form.searchform input[type="submit"]:hover, .woocommerce.widget_price_filter .price_slider_amount button:hover, .woocommerce.widget_product_search form input[type="submit"]:hover, .yith-wcan-reset-navigation.button:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a, .wishlist_table .product-add-to-cart .add_to_cart.button:hover{'+
                                'border-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );

//         tabs & accordion
        wp.customize( 'illusion_colors[illusion_tab_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.section_offset .wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav li.ui-tabs-active a, mark, #open_switcher, .bg_color_blue, .tabs_nav .ui-state-active a, .tabs_nav .active a, .owl-pagination > .active, .bg_color_blue_hover:hover, .p_table.bg_color_blue_hover.active, [class*="icon_wrap_"].animation_fill.color_scheme:before{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_tab_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.section_offset .wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav li.ui-tabs-active a, .tabs_nav .active a,.tabs_nav .active a:hover{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_tab_border_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.section_offset .wpb_content_element .wpb_tabs_nav li.ui-tabs-active, .section_offset .wpb_tabs .wpb_tabs_nav li.ui-tabs-active{'+
                                'border-color: '+newval+' !important;'+
                        "}\n";
                
                        newRule += '.tabs_nav .ui-state-active + li a, .tabs_nav .active + li a, .paginations .active + li a, .paginations li:hover + li a{'+
                                'border-left-color: '+newval+' !important;'+
                        "}\n";
                        
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
//        Widgets
        wp.customize( 'illusion_colors[illusion_widgets_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.widget_nav_menu ul li a, .widget_pages ul li a, .widget_meta ul li a, .widget_recent_comments ul li, .widget_recent_comments ul li a, .widget_recent_entries ul li a, .woocommerce.widget_product_categories ul.categories_list > li > ul li a{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_widgets_text_hover_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.widget_nav_menu ul li a:hover, .widget_pages ul li a:hover, .widget_meta ul li a:hover, .widget_recent_comments ul li a:hover, .widget_recent_entries ul li a:hover, .widget_categories ul li a:hover, .widget_archive ul li a:hover, .woocommerce.widget_product_categories ul.categories_list > li > a:hover, .woocommerce.widget_product_categories ul.categories_list > li > ul li a:hover, .widget_categories ul li a:hover:before, .widget_archive ul li a:hover:before, .woocommerce.widget_product_categories ul.categories_list > li > a:hover:before{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                
                        newRule += '.tagcloud a:hover, .paginations li div[class*="rc_"]:hover{'+
                                'border-color: '+newval+' !important;'+
                        "}\n";
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
//        Paginations
        wp.customize( 'illusion_colors[illusion_active_pagination_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.paginations li a.current{'+
                                'background: '+newval+' !important;'+
                        "}\n";
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_active_pagination_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.paginations li a.current{'+
                                'color: '+newval+' !important;'+
                        "}\n";
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
//        Plugins elements
        wp.customize( 'illusion_colors[illusion_woo_tt_before_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.woocommerce-billing-fields > h3:before, .woocommerce-shipping-fields > h3:before, #order_review_heading:before, .woocommerce-checkout .woocommerce > h2:before, .woocommerce-checkout .woocommerce header h2:before, .woocommerce-checkout .woocommerce header.title h3:before, .woocommerce-account .woocommerce > h2:before, .woocommerce-account .woocommerce header.title h3:before, .woocommerce-account .woocommerce header h2:before, .woocommerce-account .woocommerce h3:before, .woocommerce .cart-collaterals .cart_totals > h2:before, .woocommerce .cart-collaterals .shipping_calculator > h2:before, .woocommerce .cart-collaterals .cross-sells > h2:before, .woocommerce-wishlist.woocommerce #yith-wcwl-form > h2:before, .woocommerce-wishlist.woocommerce .yith-wcwl-share > h4:before{'+
                                'background-color: '+newval+' !important;'+
                        "}\n";
                
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_woo_yith_nav_border_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.woocommerce .widget_layered_nav ul.yith-wcan-label li a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a{'+
                                'border-color: '+newval+' !important;'+
                        "}\n";
                
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        
        wp.customize( 'illusion_colors[illusion_woo_yith_nav_hover_bg_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a{'+
                                'border-color: '+newval+' !important;'+
                                'background-color: '+newval+' !important;'+
                        "}\n";
                
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );
        wp.customize( 'illusion_colors[illusion_woo_yith_nav_hover_text_color]', function( value ) {
		value.bind( function( newval ) {
                    
                        if($('html > head style#ilscustomize_css').length < 1){
                            
                            $('<style id="ilscustomize_css" type="text/css"></style>').appendTo('head');
                            
                        }
                        
                        var newRule = '.woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, .woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, .woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a{'+
                                'color: '+newval+' !important;'+                                
                        "}\n";
                
                
//                        var val = $('style#ilscustomize_css').text();
			$('style#ilscustomize_css').html( newRule );
		} );
	} );


} )( jQuery );