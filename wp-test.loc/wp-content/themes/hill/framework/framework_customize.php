<?php


function hill_customizer_live_preview()
{
	wp_enqueue_script( 
		  'ils-themecustomizer',			//Give the script an ID
		  HILL_JS_URL.'/theme-customizer.js',//Point to file
		  array( 'jquery','customize-preview' ),	//Define dependencies
		  '',						//Define a version (optional) 
		  true						//Put script in footer?
	);
}
add_action( 'customize_preview_init', 'hill_customizer_live_preview' );

/*Customizer Code HERE*/



function hill_theme_customizer( $wp_customize ) {

    // Customizer Title Control
    class WP_Customize_Title_Control extends WP_Customize_Control {
        public function __construct( $manager, $id, $args = array() ) {
            parent::__construct( $manager, $id, $args );
        }
        public function render_content() {
            ?><h3><?php echo esc_html( $this->label ); ?></h3><?php
        }
    }


    $wp_customize->add_panel( 'all_styling_panel', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'title'          => esc_html__('Styling Settings', 'hill'),
        'description'    => esc_html__('All Styling Settings', 'hill'),
    ) );
    

    // Woocommerce Colors
    $wp_customize->add_section( 'hill_wc_color' , array(
        'title'      => esc_html__( 'Woocommerce Colors', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    $wp_customize->add_setting( 'hill_css[onsale_badge_bg]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'hill_css[onsale_badge_bg]', array(
        'label'    => esc_html__( 'On Sale Badge', 'hill' ),
        'section'  => 'hill_wc_color',
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[rated_star_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[rated_star_color]', array(
    'label'        => esc_html__( 'Rated Star Color', 'hill' ),
    'section'    => 'hill_wc_color',
    'priority'   => 18,
    ) ) );

    // Title: Add To Cart Button
    $wp_customize->add_section( 'hill_wc_atbtn_color' , array(
        'title'      => esc_html__( 'Add To Cart Button', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );


    $wp_customize->add_setting ( 'hill_colors[add_to_cart_btn_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[add_to_cart_btn_bg_color]', array(
    'label'        => esc_html__( 'Background', 'hill' ),
    'section'    => 'hill_wc_atbtn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[add_to_cart_btn_icon_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[add_to_cart_btn_icon_bg_color]', array(
    'label'        => esc_html__( 'Icon Background', 'hill' ),
    'section'    => 'hill_wc_atbtn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[add_to_cart_btn_hvr_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[add_to_cart_btn_hvr_bg_color]', array(
    'label'        => esc_html__( 'Hover Background', 'hill' ),
    'section'    => 'hill_wc_atbtn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[woo_discount_label]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[woo_discount_label]', array(
    'label'        => esc_html__( 'Discount Label Background', 'hill' ),
    'section'    => 'hill_wc_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[in_stock_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[in_stock_bg_color]', array(
    'label'        => esc_html__( 'In Stock Label Background', 'hill' ),
    'section'    => 'hill_wc_color',
    'priority'   => 18,
    ) ) );

    // Title: Shorting

    $wp_customize->add_section( 'hill_wc_sn_color' , array(
        'title'      => esc_html__( 'Shorting Nav', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );


    $wp_customize->add_setting ( 'hill_titles[hill_nrml_str_nav]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_nrml_str_nav]', array(
    'label'        => esc_html__( 'Normal', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[shorting_nav_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[shorting_nav_color]', array(
    'label'        => esc_html__( 'Text Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[shorting_nav_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[shorting_nav_border]', array(
    'label'        => esc_html__( 'Border Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_titles[hill_hvr_str_nav]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_hvr_str_nav]', array(
    'label'        => esc_html__( 'Hover', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_hvr_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_hvr_color]', array(
    'label'        => esc_html__( 'Text Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_hvr_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_hvr_bg]', array(
    'label'        => esc_html__( 'Background', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_hvr_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_hvr_border]', array(
    'label'        => esc_html__( 'Border Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    //Active Stage
    $wp_customize->add_setting ( 'hill_titles[hill_atcv_str_nav]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_atcv_str_nav]', array(
    'label'        => esc_html__( 'Active', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_active_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_active_color]', array(
    'label'        => esc_html__( 'Text Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_active_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_active_bg]', array(
    'label'        => esc_html__( 'Background', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[srt_nav_active_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[srt_nav_active_border]', array(
    'label'        => esc_html__( 'Border Color', 'hill' ),
    'section'    => 'hill_wc_sn_color',
    'priority'   => 18,
    ) ) );



    // All Color Settings
    $wp_customize->add_section( 'hill_header_color' , array(
        'title'      => esc_html__( 'Common Color', 'hill' ),
        'priority'   => 1,
        'panel'      => 'all_styling_panel'
    ) );


    $wp_customize->add_setting ( 'hill_colors[lnk_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[lnk_color]', array(
        'label'        => esc_html__( 'Link Color', 'hill' ),
        'section'    => 'hill_header_color',
        'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[lnk_color_hover]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[lnk_color_hover]', array(
        'label'        => esc_html__( 'Link Color hover', 'hill' ),
        'section'    => 'hill_header_color',
        'priority'   => 18,
    ) ) );






    $wp_customize->add_setting ( 'hill_colors[common_color_main]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[common_color_main]', array(
    'label'        => esc_html__( 'Main Color', 'hill' ),
    'section'    => 'hill_header_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[header_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[header_bg_color]', array(
    'label'        => esc_html__( 'Header Background', 'hill' ),
    'section'    => 'hill_header_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_section( 'hill_menu_color' , array(
        'title'      => esc_html__( 'Menu Style', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    $wp_customize->add_setting ( 'hill_colors[menu_bar_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_bar_color]', array(
    'label'        => esc_html__( 'Menubar Background', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    // Title: Menu
    $wp_customize->add_setting ( 'hill_titles[hill_menu]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_menu]', array(
    'label'        => esc_html__( 'Menu', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );


    $wp_customize->add_setting ( 'hill_colors[menu_hover_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_hover_border]', array(
    'label'        => esc_html__( 'Hover Border Color', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[menu_hover_border_top]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_hover_border_top]', array(
    'label'        => esc_html__( 'Hover Top Border Color', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[menu_hover_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));
    
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_hover_bg]', array(
    'label'        => esc_html__( 'Hover Background Color', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    // Title: Menu Dropdown
    $wp_customize->add_setting ( 'hill_titles[hill_menu_dropdown]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_menu_dropdown]', array(
    'label'        => esc_html__( 'Menu Dropdown', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[menu_dropdown_bottom]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_dropdown_bottom]', array(
    'label'        => esc_html__( 'Bottom Border Color', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[menu_dropdown_link_hover_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[menu_dropdown_link_hover_bg]', array(
    'label'        => esc_html__( 'Hover Background', 'hill' ),
    'section'    => 'hill_menu_color',
    'priority'   => 18,
    ) ) );

    // Title: Mini Cart

    $wp_customize->add_section( 'hill_minicart_color' , array(
        'title'      => esc_html__( 'Mini Cart', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    $wp_customize->add_setting ( 'hill_colors[mini_cart_hover_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[mini_cart_hover_bg]', array(
    'label'        => esc_html__( 'Hover Background', 'hill' ),
    'section'    => 'hill_minicart_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[mini_cart_icon_hover_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[mini_cart_icon_hover_bg]', array(
    'label'        => esc_html__( 'Icon Hover Background', 'hill' ),
    'section'    => 'hill_minicart_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[mini_cart_count_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[mini_cart_count_bg]', array(
    'label'        => esc_html__( 'Count Background', 'hill' ),
    'section'    => 'hill_minicart_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[mini_cart_items_border_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[mini_cart_items_border_color]', array(
    'label'        => esc_html__( 'Items Border Color', 'hill' ),
    'section'    => 'hill_minicart_color',
    'priority'   => 18,
    ) ) );


    

    // Title: Brand Slider

    $wp_customize->add_section( 'hill_brand_slider_color' , array(
        'title'      => esc_html__( 'Brand Slider', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    

    $wp_customize->add_setting ( 'hill_colors[brand_slider_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_slider_bg]', array(
    'label'        => esc_html__( 'Brand Background', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[brand_slider_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_slider_border]', array(
    'label'        => esc_html__( 'Border Color', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[brand_top_border_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_top_border_color]', array(
    'label'        => esc_html__( 'Top Border Color', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[brand_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_bg_color]', array(
    'label'        => esc_html__( 'Item Background (Hover)', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[brand_lnk_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_lnk_bg_color]', array(
    'label'        => esc_html__( 'Link Background Color', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[brand_nav_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[brand_nav_bg_color]', array(
    'label'        => esc_html__( 'Nav Background Hover', 'hill' ),
    'section'    => 'hill_brand_slider_color',
    'priority'   => 18,
    ) ) );



    $wp_customize->add_setting ( 'hill_colors[cat_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[cat_bg_color]', array(
    'label'        => esc_html__( 'Category ', 'hill' ),
    'section'    => 'hill_header_color',
    'priority'   => 18,
    ) ) );


    // Title: Tabs
    $wp_customize->add_section( 'hill_basic_tab_color' , array(
        'title'      => esc_html__( 'Basic Tab Style', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    $wp_customize->add_setting ( 'hill_titles[hill_tabs_short_title]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_tabs_short_title]', array(
    'label'        => esc_html__( 'Normal', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tabs_nav_txt_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tabs_nav_txt_color]', array(
    'label'        => esc_html__( 'Nav Text Color', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );


    $wp_customize->add_setting ( 'hill_titles[hill_tabs_short_title_hover]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_tabs_short_title_hover]', array(
    'label'        => esc_html__( 'Hover', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tabs_nav_txt_color_hover]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tabs_nav_txt_color_hover]', array(
    'label'        => esc_html__( 'Nav Text Color Hover', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_titles[hill_tabs_short_title_active]');

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_tabs_short_title_active]', array(
    'label'        => esc_html__( 'Active', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tabs_nav_txt_color_active]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tabs_nav_txt_color_active]', array(
    'label'        => esc_html__( 'Nav Text Color Active', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tabs_nav_active_bg]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tabs_nav_active_bg]', array(
    'label'        => esc_html__( 'Nav Active Background', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tabs_nav_active_bottom_border]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tabs_nav_active_bottom_border]', array(
    'label'        => esc_html__( 'Nav Active Bottom Border', 'hill' ),
    'section'    => 'hill_basic_tab_color',
    'priority'   => 18,
    ) ) );


    // Title: Short Type Tabs

    $wp_customize->add_section( 'hill_srt_tabs_color' , array(
        'title'      => esc_html__( 'Short Type Tabs', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );



    $wp_customize->add_setting ( 'hill_colors[tab_short_txt]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tab_short_txt]', array(
    'label'        => esc_html__( 'Text Color ', 'hill' ),
    'section'    => 'hill_srt_tabs_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[tab_short_txt_active]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[tab_short_txt_active]', array(
    'label'        => esc_html__( 'Text Color Active', 'hill' ),
    'section'    => 'hill_srt_tabs_color',
    'priority'   => 18,
    ) ) );


    // Title: Subscribe Button
    $wp_customize->add_section( 'hill_mc_btn_color' , array(
        'title'      => esc_html__( 'Subscribe Button', 'hill' ),
        'priority'   => 2,
        'panel'      => 'all_styling_panel'
    ) );

    $wp_customize->add_setting ( 'hill_titles[hill_subscribe_btn-title]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_subscribe_btn-title]', array(
    'label'        => esc_html__( 'Normal', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[sc_btn_bg_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[sc_btn_bg_color]', array(
    'label'        => esc_html__( 'Background Color', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[sc_btn_txt_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[sc_btn_txt_color]', array(
    'label'        => esc_html__( 'Text Color', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_titles[hill_subscribe_btn_hvr-title]', array(
        'sanitize_callback' => 'hill_pass_scb',
    ));

    $wp_customize->add_control( new WP_Customize_Title_Control( $wp_customize, 'hill_titles[hill_subscribe_btn_hvr-title]', array(
    'label'        => esc_html__( 'Hover', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[sc_btn_bg_color_hover]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[sc_btn_bg_color_hover]', array(
    'label'        => esc_html__( 'Background Color', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );

    $wp_customize->add_setting ( 'hill_colors[sc_btn_txt_color_hover]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hill_colors[sc_btn_txt_color_hover]', array(
    'label'        => esc_html__( 'Text Color', 'hill' ),
    'section'    => 'hill_mc_btn_color',
    'priority'   => 18,
    ) ) );



    $wp_customize->add_setting ( 'hill_colors[topbar_color]', array (
        'default' => '',
        'sanitize_callback' => 'sanitize_hex_color',
        'type' => 'theme_mod', 
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_color', array(
    'label'        => esc_html__( 'Header Topbar Color', 'hill' ),
    'section'    => 'hill_header_color',
    'settings' => 'hill_colors[topbar_color]',
    'priority'   => 18,
    ) ) );



}
add_action( 'customize_register', 'hill_theme_customizer' );









