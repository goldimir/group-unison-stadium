<?php

	if ( ! class_exists( 'Redux' ) ) {
		return;
	}

	// This is your option name where all the Redux data is stored.
    $opt_name = "hill_option";

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
		// TYPICAL -> Change these values as you need/desire
		'opt_name'             => $opt_name,
		// This is where your data is stored in the database and also becomes your global variable name.
		'display_name'         => $theme->get( 'Name' ),
		// Name that appears at the top of your panel
		'display_version'      => $theme->get( 'Version' ),
		// Version that appears at the top of your panel
		'menu_type'            => 'menu',
		//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
		'allow_sub_menu'       => true,
		// Show the sections below the admin menu item or not
		'menu_title'           => esc_html__( 'Hill Options', 'hill' ),
		'page_title'           => esc_html__( 'Hill Options', 'hill' ),
		// You will need to generate a Google API key to use this feature.
		// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
		'google_api_key'       => '',
		// Set it you want google fonts to update weekly. A google_api_key value is required.
		'google_update_weekly' => false,
		// Must be defined to add google fonts to the typography module
		'async_typography'     => false,
		// Use a asynchronous font on the front end or font string
		//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
		'admin_bar'            => true,
		// Show the panel pages on the admin bar
		'admin_bar_icon'       => 'dashicons-portfolio',
		// Choose an icon for the admin bar menu
		'admin_bar_priority'   => 50,
		// Choose an priority for the admin bar menu
		'global_variable'      => '',
		// Set a different name for your global variable other than the opt_name
		'dev_mode'             => false,
		// Show the time the page took to load, etc
		'update_notice'        => true,
		// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
		'customizer'           => false,
		// Enable basic customizer support
		//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
		//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
		// OPTIONAL -> Give you extra features
		'page_priority'        => null,
		// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
		'page_parent'          => 'themes.php',
		// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
		'page_permissions'     => 'manage_options',
		// Permissions needed to access the options panel.
		'menu_icon'            => '',
		// Specify a custom URL to an icon
		'last_tab'             => '',
		// Force your panel to always open to a specific tab (by id)
		'page_icon'            => 'icon-themes',
		// Icon displayed in the admin panel next to your menu_title
		'page_slug'            => '',
		// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
		'save_defaults'        => true,
		// On load save the defaults to DB before user clicks save or not
		'default_show'         => false,
		// If true, shows the default value next to each field that is not the default value.
		'default_mark'         => '',
		// What to print by the field's title if the value shown is default. Suggested: *
		'show_import_export'   => true,
		// Shows the Import/Export panel when not used as a field.
		// CAREFUL -> These options are for advanced use only
		'transient_time'       => 60 * MINUTE_IN_SECONDS,
		'output'               => true,
		// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
		'output_tag'           => true,
		// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
		// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
		// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
		'database'             => '',
		// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
		'use_cdn'              => true,
		// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
		// HINTS
		'hints'                => array(
			'icon'          => 'el el-question-sign',
			'icon_position' => 'right',
			'icon_color'    => 'lightgray',
			'icon_size'     => 'normal',
			'tip_style'     => array(
				'color'   => 'red',
				'shadow'  => true,
				'rounded' => false,
				'style'   => '',
			),
			'tip_position'  => array(
				'my' => 'top left',
				'at' => 'bottom right',
			),
			'tip_effect'    => array(
				'show' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'mouseover',
				),
				'hide' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'click mouseleave',
				),
			),
		)
	);


	Redux::setArgs( $opt_name, $args );

	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Header Settings', 'hill' ),
		'id'               => 'header_settings',
		'desc'             => esc_html__( 'All header settings', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-website',
		'fields'           => array(
			array(
				'id'       => 'hill_logo',
				'type'     => 'media',
				'title'    => esc_html__( 'Site Logo', 'hill' ),
				'default'     => array(
					'url'       => get_template_directory_uri().'/images/interface/logo_hill.png',
				),
			),
			array(
				'id'       => 'hill_header_type',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Site Header', 'hill' ),
				'subtitle' => esc_html__( 'Select which type of header you want to show', 'hill' ),
				'default'     => 'one',
				'options' => array(
					'one' => array(
						'alt'   => 'Header One',
						'img'   => get_template_directory_uri().'/images/admin/header_one.jpg'
					),
					'two' => array(
						'alt'   => 'Header Two',
						'img'   => get_template_directory_uri().'/images/admin/header_two.jpg'
					),
					'three' => array(
						'alt'   => 'Header Three',
						'img'   => get_template_directory_uri().'/images/admin/header_three.jpg'
					)
				)
			),
			array(
				'id'       => 'hill_header_bg',
				'type'     => 'media',
				'title'    => esc_html__( 'Header Background Image', 'hill' ),
				'default'     => array(),
			),
			array(
				'id'       => 'hill_header_left_txt',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Header Left Text', 'hill' ),
				'subtitle' => esc_html__( 'It will show at top left.' , 'hill' ),
				'default'     => wp_kses(__('<i class="fa fa-phone"></i> <a href="tel:88006564827">8 800 656 48 27</a> Free Shipping every Friday', 'hill'), array('i' => array( 'class' => array() ), 'a' => array('href' => array()))),
			),
			array(
				'id'       => 'hill_header_after_nav',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Header After nav Features', 'hill' ),
				'subtitle' => esc_html__( 'It will be show at only header type 3, just after navbar', 'hill' ),
				'default'     => '<ul>
                  <li><a href="#"><i class="fa fa-truck"></i> Free Shipping</a></li>
                  <li><a href="#"><i class="fa fa-retweet"></i> Money Back Guarantee</a></li>
                  <li><a href="#"><i class="fa fa-gift"></i> Free Gifts Every Friday</a></li>
                  <li><a href="#"><i class="fa fa-clock-o"></i> Daily Lightning Deals</a></li>
                </ul>',
			),
			array(
				'id'       => 'mini_cart_bottom_border_only',
				'type'     => 'select',
				'title'    => esc_html__( 'Mini Cart Popup Border', 'hill' ),
				'subtitle' => esc_html__( 'At minicart only border bottom', 'hill' ),
				'default'     => 'off',
				'options' => array(
					'on' => esc_html__('Bottom Only', 'hill'),
        			'off' => esc_html__('All Side', 'hill'),
				)
			),
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Blog Settings', 'hill' ),
		'id'               => 'blog_settings',
		'desc'             => esc_html__( 'These are really basic fields!', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-th-large',
		'fields'           => array(
			array(
				'id'       => 'hill_blog_grid_type',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Post Grid Style', 'hill' ),
				'subtitle' => esc_html__( 'Blog style grid or full-width', 'hill' ),
				'default'     => 'off',
				'options' => array(
					'off' => array(
						'alt'   => esc_html__('Fullwidth Style', 'hill'),
						'img'   => get_template_directory_uri().'/images/admin/full.jpg'
					),
					'on' => array(
						'alt'   => esc_html__('Grid Style', 'hill'),
						'img'   => get_template_directory_uri().'/images/admin/grid.jpg'
					),
				)
			),
			array(
				'id'       => 'hill_blog_author_bg',
				'type'     => 'media',
				'title'    => esc_html__( 'Author Bio Background Image', 'hill' ),
				'subtitle' => esc_html__( 'Author Bio background image at single blog post.', 'hill' ),
				'default'     => array(),
			),
		)
	));


	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Typography', 'hill' ),
		'id'               => 'typography',
		'desc'             => esc_html__( 'Theme all font options', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-font',
		'fields'           => array(
			array(
				'id'       => 'body_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Body Typography', 'hill' ),
				'subtitle' => esc_html__( 'Select body font family, size, line height, color and weight.', 'hill' ),
				'text-align' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Open Sans', 
					'google'      => true,
					'font-size'   => '14px', 
					'line-height' => '22px'
				),
			),
			array(
				'id'       => 'nav_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Nav Menu', 'hill' ),
				'subtitle' => esc_html__( 'Nave menu typography settings', 'hill' ),
				'line-height' => false,
				'text-align' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '15px'
				),
			),
			array(
				'id'       => 'nav_drodown_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Nav Dropdown', 'hill' ),
				'subtitle' => esc_html__( 'Nav menu dropdown typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Open Sans', 
					'google'      => true,
					'font-size'   => '13px', 
				),
			),
			array(
				'id'       => 'nav_mega_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Mega Menu Title', 'hill' ),
				'subtitle' => esc_html__( 'Mega menu title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '20px', 
				),
			),
			array(
				'id'       => 'banner_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Banner Title', 'hill' ),
				'subtitle' => esc_html__( 'Banner title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'font-size' => false,
				'subsets' => false,
				'color' => false,
				'default'     => array(
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
				),
			),
			array(
				'id'       => 'widget_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Widget Title', 'hill' ),
				'subtitle' => esc_html__( 'Widget title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '24px', 
				),
			),
			array(
				'id'       => 'footer_widget_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Footer Widget Title', 'hill' ),
				'subtitle' => esc_html__( 'Footer widget title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#fff', 
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '24px', 
				),
			),
			array(
				'id'       => 'page_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Page Title', 'hill' ),
				'subtitle' => esc_html__( 'Page title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'default'     => array(
					'color'       => '#000', 
					'font-weight'  => '400', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '24px', 
				),
			),
			array(
				'id'       => 'slider_title_typography',
				'type'     => 'typography',
				'title'    => esc_html__( 'Slider Title', 'hill' ),
				'subtitle' => esc_html__( 'Slider title typography settings', 'hill' ),
				'text-align' => false,
				'line-height' => false,
				'subsets' => false,
				'color' => false,
				'default'     => array(
					'font-weight'  => '300', 
					'font-family' => 'Oswald', 
					'google'      => true,
					'font-size'   => '36px', 
				),
			),
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Woocommerce Settings', 'hill' ),
		'id'               => 'wc_settings',
		'desc'             => esc_html__( 'These are really basic fields!', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-shopping-cart-sign',
		'fields'           => array(
            array(
				'id'       => 'is_shop_enable',
				'type'     => 'switch', 
				'title'    => esc_html__('Shop Status', 'hill'),
				'subtitle' => esc_html__('Enable or Disable shopping on site', 'hill'),
				'default'  => true,
				'on'	   => esc_html__('Enable', 'hill'),
				'off'	   => esc_html__('Disable', 'hill'),
			),
			array(
				'id'       => 'products_per_page',
				'type'     => 'spinner', 
				'title'    => esc_html__('Products per page', 'hill'),
				'subtitle' => esc_html__('Number of products you want to show at shop and category page', 'hill'),
				'default'  => 9,
			),
		)
	));

	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Popup Settings', 'hill' ),
		'id'               => 'popup_settings',
		'desc'             => esc_html__( 'Popup for email subscription', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-website-alt',
		'fields'           => array(
			array(
				'id'       => 'is_popup_enable',
				'type'     => 'switch', 
				'title'    => esc_html__('Popup', 'hill'),
				'subtitle' => esc_html__('Enable or Disable popup on site', 'hill'),
				'default'  => true,
				'on'	   => esc_html__('Enable', 'hill'),
				'off'	   => esc_html__('Disable', 'hill'),
			),
			array(
				'id'       => 'popup_title',
				'type'     => 'text',
				'title'    => esc_html__('Popup Title', 'hill'),
				'subtitle' => esc_html__('Popup title which one will be shown at top of popup', 'hill'),
				'default'  => 'Newsletter signup'
			),
			array(
				'id'       => 'popup_subtitle',
				'type'     => 'editor',
				'title'    => esc_html__('Popup Sub-Title', 'hill'),
				'subtitle' => esc_html__('It will be shown at just below title', 'hill'),
				'default'  => 'Subscribe to the <strong style="color:#da2127;text-decoration: underline;">HILL</strong> mailing list to receive updates on new arrivals, offers and other discount information.'
			),
			array(
				'id'       => 'popup_nomore_text',
				'type'     => 'text',
				'title'    => esc_html__('Don\'t show it again', 'hill'),
				'subtitle' => esc_html__('Don\'t show it again checkbox title', 'hill'),
				'default'  => 'Don\'t show it again'
			),

			array(
				'id'       => 'popup_mc_form',
				'type'     => 'textarea',
				'title'    => esc_html__('MailChimp Form', 'hill'),
				'subtitle' => esc_html__('MailChimp form murkup', 'hill'),
				'default'  => ''
			),
			array(
				'id'       => 'popup_bg_img',
				'type'     => 'background',
				'title'    => esc_html__('Popup Background Image', 'hill'),
				'subtitle' => esc_html__('Popup backgrund settings', 'hill'),
				'default'  => array(
					'background-color' => '#ffffff'
				)
			),
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Footer Settings', 'hill' ),
		'id'               => 'footer_settings',
		'desc'             => esc_html__( 'These are really basic fields!', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-share',
		'fields'           => array(
			array(
				'id'       => 'footer_logo',
				'type'     => 'media',
				'title'    => esc_html__( 'Footer Logo', 'hill' ),
				'subtitle' => esc_html__( 'It will be show just after widgets and left side of search box', 'hill' ),
				'default'     => array(
					'url'       => get_template_directory_uri().'/images/interface/logo_hill.png',
				),
			),
			array(
				'id'       => 'hill_footer_copyright',
				'type'     => 'editor',
				'title'    => esc_html__( 'Copyright Text', 'hill' ),
				'subtitle' => esc_html__( 'Set Copyright Text', 'hill' ),
				'default'     => ''
			),
			array(
				'id'       => 'hill_footer_phn_label',
				'type'     => 'text',
				'title'    => esc_html__( 'Phone Label', 'hill' ),
				'subtitle' => esc_html__( 'Footer phone number label', 'hill' ),
				'default'     => 'Our Phones:'
			),
			array(
				'id'       => 'hill_footer_phn_number',
				'type'     => 'text',
				'title'    => esc_html__( 'Phone Number', 'hill' ),
				'subtitle' => esc_html__( 'Footer phone number', 'hill' ),
				'default'     => '44 800 123 45 67   /  38 240 123 45 89'
			),
			array(
				'id'       => 'hill_footer_email',
				'type'     => 'text',
				'title'    => esc_html__( 'Email/Contact Page URL', 'hill' ),
				'subtitle' => esc_html__( 'Email address or Contact page link here', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_map_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Google Map URL', 'hill' ),
				'subtitle' => esc_html__( 'Your address google map link', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_facebook',
				'type'     => 'text',
				'title'    => esc_html__( 'Facebook URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_twitter',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_google_plus',
				'type'     => 'text',
				'title'    => esc_html__( 'Google Plus URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_youtube',
				'type'     => 'text',
				'title'    => esc_html__( 'YouTube URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_pinterest',
				'type'     => 'text',
				'title'    => esc_html__( 'Pinterest URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_flickr',
				'type'     => 'text',
				'title'    => esc_html__( 'Flickr URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_dribbble',
				'type'     => 'text',
				'title'    => esc_html__( 'Dribbble URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_behance',
				'type'     => 'text',
				'title'    => esc_html__( 'Behance URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_instagram',
				'type'     => 'text',
				'title'    => esc_html__( 'Instagram URL', 'hill' ),
				'default'     => '#'
			),
			array(
				'id'       => 'hill_footer_social_git',
				'type'     => 'text',
				'title'    => esc_html__( 'Git URL', 'hill' ),
				'default'     => '#'
			),
		)
	));


	Redux::setSection( $opt_name, array(
		'title'            => esc_html__( 'Extra Settings', 'hill' ),
		'id'               => 'extra_settings',
		'desc'             => esc_html__( 'These are really basic fields!', 'hill' ),
		'customizer_width' => '400px',
		'icon'             => 'el el-share',
		'fields'           => array(
			array(
				'id'       => 'extra_css',
				'type'     => 'ace_editor',
				'title'    => esc_html__( 'Extra CSS', 'hill' ),
				'subtitle' => esc_html__( 'Extra CSS just after theme styles', 'hill' ),
				'mode' => 'css'
			),
		)
	));

	