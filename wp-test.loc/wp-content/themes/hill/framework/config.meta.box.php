<?php
add_filter( 'rwmb_meta_boxes', 'hill_register_framework_post_meta_box' );
/**
* Register meta boxes
*
* Remember to change "your_prefix" to actual prefix in your project
*
* @return void
*/
function hill_register_framework_post_meta_box( $meta_boxes )
{
        global $wp_registered_sidebars;

        // Revslider Lists
        $revsliders = array(
            '0' => esc_html__('No slider', 'hill'),
        );
        if (class_exists('RevSlider')) {
            $slider = new RevSlider();
            $arrSliders = $slider->getArrSliders();

            if ( $arrSliders ) {
                foreach ( $arrSliders as $slider ) {
                    $revsliders[ $slider->getAlias() ] = $slider->getTitle();
                }
            }
        }


        /**
        * prefix of meta keys (optional)
        * Use underscore (_) at the beginning to make keys hidden
        * Alt.: You also can make prefix empty to disable it
        */
        // Better has an underscore as last sign
        $prefix = 'framework';
        
        $sidebars = array(
            '0' => esc_html__('Default widget', 'hill')
        );
        
        foreach($wp_registered_sidebars as $key=>$value){
            $sidebars[$key] = $value['name'];            
        }
        
        $opacities = array();

        for($o = 0.0, $n = 0; $o <= 1.0; $o += 0.1, $n++){
            $opacities[$n] = $o;
        }
        

        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-quote',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(
                array(
                    'name' => esc_html__( 'Quote Author', 'hill' ),
                    'desc' => esc_html__( 'Insert quote author name.', 'hill' ),
                    'id' => "{$prefix}-quote-author",
                    'type' => 'text',                    
                ),
                array(
                    'name' => esc_html__( 'Quote Author Url', 'hill' ),
                    'desc' => esc_html__( 'Insert author url.', 'hill' ),
                    'id' => "{$prefix}-quote-author-link",
                    'type' => 'text',                    
                ),
                array(
                    'name' => esc_html__( 'Quote Text', 'hill' ),
                    'desc' => esc_html__( 'Insert Quote Text.', 'hill' ),
                    'id' => "{$prefix}-quote",
                    'type' => 'textarea',                    
                ),
        ));
                    

        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-video',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(        
                array(
                    'name' => esc_html__( 'Video Markup', 'hill' ),
                    'desc' => esc_html__( 'Put embed src of video. i.e. youtube, vimeo', 'hill' ),
                    'id' => "{$prefix}-video-markup",
                    'type' => 'textarea',
                    'cols' => 20,
                    'rows' => 3,                    
                    // 'clone' => true                            
                ),
        ));

        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-audio',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(        
                array(
                    'name' => esc_html__( 'Audio Markup', 'hill' ),
                    'desc' => esc_html__( 'Put embed src of video. i.e. youtube, vimeo', 'hill' ),
                    'id' => "{$prefix}-audio-markup",
                    'type' => 'textarea',
                    'cols' => 20,
                    'rows' => 3,                    
                    // 'clone' => true                            
                ),
        ));


        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-link',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(
                array(
                    'name' => esc_html__( 'Link', 'hill' ),
                    'desc' => esc_html__( 'Works with link post format.', 'hill' ),
                    'id' => "{$prefix}-link",
                    'type' => 'text',                    
                ),
                array(
                    'name' => esc_html__( 'Link title', 'hill' ),
                    'desc' => esc_html__( 'Works with link post format.', 'hill' ),
                    'id' => "{$prefix}-link-title",
                    'type' => 'text',                    
                ),
        ));

        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-image',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(        
                
                 array(
                    'name' => esc_html__('Upload Gallery Images', 'hill'),
                    'id' => "{$prefix}-image",
                    'desc' => '',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 50,
                    
                ),
        )); 




        $meta_boxes[] = array(
            'id' => 'framework-meta-box-post-format-gallery',
            'title' => esc_html__('Post Format Data', 'hill'),
            'pages' => array(
                'post',
            ),            
            'context' => 'normal',
            'priority' => 'high',
            'tab_style' => 'left',
            
            'fields' => array(        
                

                 array(
                    'name' => esc_html__('Upload Gallery Images', 'hill'),
                    'id' => "{$prefix}-gallery",
                    'desc' => '',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 50,
                    
                ),
        ));   
                    
        $posts_page = get_option('page_for_posts');

        if(!isset($_GET['post']) || intval($_GET['post']) != $posts_page){

            $meta_boxes[] = array(
                'id' => 'framework_page_meta_box',
                'title' => esc_html__('Page Design Settings', 'hill'),
                'pages' => array(
                    'page',
                ),
                'context' => 'normal',
                'priority' => 'core',
                'fields' => array(



                    array(
                        'id' => "framework_show_page_title",
                        'name' => esc_html__('Show page titlebar', 'hill'),
                        'desc' => '',
                        'type' => 'radio',
                        'std' => "on",
                        'options' => array('on'=>'Yes','off'=>'No'),
                    ), 
                    array(
                        'id' => "framework_revslider",
                        'name' => esc_html__('RevSlider', 'hill'),
                        'desc' => '',
                        'type' => 'select_advanced',
                        'std' => "",
                        'options' => $revsliders,
                        'allowClear' => true,
                        'placeholder' => esc_html__('Select','hill'),
                    ),
                    array(
                        'id' => "framework_page_style",
                        'name' => esc_html__('Page Style', 'hill'),
                        'desc' => '',
                        'type' => 'image_select',
                        'std' => "rightsidebar",
                        'options' => array(
                            'leftsidebar' => HILL_THEME_URI.'/images/admin/left.jpg',
                            'nosidebar' => HILL_THEME_URI.'/images/admin/no.jpg',
                            'rightsidebar' => HILL_THEME_URI.'/images/admin/right.jpg',
                        ),
                        'allowClear' => true,
                        'placeholder' => esc_html__('Select','hill'),
                    ),
                    array(
                        'id' => "framework_page_left_sidebar",
                        'name' => esc_html__('Page Sidebar', 'hill'),
                        'desc' => '',
                        'type' => 'select_advanced',
                        'std' => "",
                        'options' => $sidebars,
                        'allowClear' => true,
                        'placeholder' => esc_html__('Select','hill'),
                    )
                )
            );
        
       }
        return $meta_boxes;
}


