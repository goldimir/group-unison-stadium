<?php

/**
* 
*/
class Hill_Products_Carousel extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct('Hill_Products_Carousel', 'Hill Products Carousel', array( 'description' => esc_html__('This widget will show product carousel','hill') ) );
	}

	public function form($instance){
		$defaults = array(
			'title' => 'Products', 
			'number' => 6,
			'order' => 'desc',
			'orderby' => '',
			'show_hidden' => false,
			'hide_free' => true,
			'show' => 'top',
			'show_desc' => 0,
			'cat_ids' => '',
			'per_slider' => 4,
			'singleitem' => 'false',
			'autoplay' => 0,
			'stop_on_hover' => 0,
			'navigation' => 0,
			'pagination' => 1,
			'slide_speed' => 200,
			'items_desktop' => 4,
			'items_desktop_small' => 3,
			'items_tablet' => 2,
			'items_mobile' => 1,
			'boxed_mod' => 0,
			'vertical_mode' => false
		); 

		$instance = wp_parse_args((array) $instance, $defaults);

		?>
		<p>
			<label for="<?php print esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'hill'); ?></label>  
			<input type="text" class="widefat" id="<?php print esc_attr($this->get_field_id('title')); ?>" name="<?php print esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number of Products:', 'hill'); ?></label>  
			<input type="number" class="widefat" id="<?php print esc_attr($this->get_field_id('number')); ?>" name="<?php print esc_attr($this->get_field_name('number')); ?>" value="<?php echo esc_attr($instance['number']); ?>" /> 
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('show')); ?>"><?php esc_html_e('Show:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('show')); ?>" name="<?php print esc_attr($this->get_field_name('show')); ?>">
				<option value="recent" <?php selected( $instance['show'], 'recent' ); ?>><?php esc_html_e('Recent', 'hill'); ?></option>
				<option value="featured" <?php selected( $instance['show'], 'featured' ); ?>><?php esc_html_e('Featured', 'hill'); ?></option>
				<option value="onsale" <?php selected( $instance['show'], 'onsale' ); ?>><?php esc_html_e('On Sale', 'hill'); ?></option>
				<option value="top" <?php selected( $instance['show'], 'top' ); ?>><?php esc_html_e('Top Rated', 'hill'); ?></option>
			</select>  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('orderby')); ?>"><?php esc_html_e('Order By:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('orderby')); ?>" name="<?php print esc_attr($this->get_field_name('orderby')); ?>">
				<option value="date" <?php selected( $instance['orderby'], 'date' ); ?>><?php esc_html_e('Date', 'hill'); ?></option>
				<option value="rand" <?php selected( $instance['orderby'], 'rand' ); ?>><?php esc_html_e('Random', 'hill'); ?></option>
				<option value="sales" <?php selected( $instance['orderby'], 'sales' ); ?>><?php esc_html_e('Sales', 'hill'); ?></option>
				<option value="price" <?php selected( $instance['orderby'], 'price' ); ?>><?php esc_html_e('Price', 'hill'); ?></option>
			</select>  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('order')); ?>"><?php esc_html_e('Sort Order:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('order')); ?>" name="<?php print esc_attr($this->get_field_name('order')); ?>">
				<option value="DESC" <?php selected( $instance['order'], 'DESC' ); ?>><?php esc_html_e('Descending', 'hill'); ?></option>
				<option value="ASC" <?php selected( $instance['order'], 'ASC' ); ?>><?php esc_html_e('Ascending', 'hill'); ?></option>
			</select>  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('boxed_mod')); ?>"><?php esc_html_e('Boxed Mode:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('boxed_mod')); ?>" name="<?php print esc_attr($this->get_field_name('boxed_mod')); ?>">
				<option value="0" <?php selected( $instance['boxed_mod'], '0' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="1" <?php selected( $instance['boxed_mod'], '1' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('vertical_mode')); ?>"><?php esc_html_e('Vertical Mode:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('vertical_mode')); ?>" name="<?php print esc_attr($this->get_field_name('vertical_mode')); ?>">
				<option value="0" <?php selected( $instance['vertical_mode'], '0' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="1" <?php selected( $instance['vertical_mode'], '1' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>
		<h3><?php esc_html_e('Carousel Settings', 'hill'); ?></h3>
		<p>
			<label for="<?php print esc_attr($this->get_field_id('per_slider')); ?>"><?php esc_html_e('Items Per Slide:', 'hill'); ?></label>  
			<input type="number" class="widefat" id="<?php print esc_attr($this->get_field_id('per_slider')); ?>" name="<?php print esc_attr($this->get_field_name('per_slider')); ?>" value="<?php echo esc_attr($instance['per_slider']); ?>" />  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('autoplay')); ?>"><?php esc_html_e('AutoPlay:', 'hill'); ?></label>  
			<input type="number" class="widefat" id="<?php print esc_attr($this->get_field_id('autoplay')); ?>" name="<?php print esc_attr($this->get_field_name('autoplay')); ?>" value="<?php echo esc_attr($instance['autoplay']); ?>" />  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('slide_speed')); ?>"><?php esc_html_e('Slide Speed:', 'hill'); ?></label>  
			<input type="number" class="widefat" id="<?php print esc_attr($this->get_field_id('slide_speed')); ?>" name="<?php print esc_attr($this->get_field_name('slide_speed')); ?>" value="<?php echo esc_attr($instance['slide_speed']); ?>" />  
		</p>

		<p>
			<label for="<?php print esc_attr($this->get_field_id('stop_on_hover')); ?>"><?php esc_html_e('Stop on Hover:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('stop_on_hover')); ?>" name="<?php print esc_attr($this->get_field_name('stop_on_hover')); ?>">
				<option value="0" <?php selected( $instance['stop_on_hover'], '0' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="1" <?php selected( $instance['stop_on_hover'], '1' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>
		<p>
			<label for="<?php print esc_attr($this->get_field_id('navigation')); ?>"><?php esc_html_e('Navigation:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('navigation')); ?>" name="<?php print esc_attr($this->get_field_name('navigation')); ?>">
				<option value="0" <?php selected( $instance['navigation'], '0' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="1" <?php selected( $instance['navigation'], '1' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>
		<p>
			<label for="<?php print esc_attr($this->get_field_id('pagination')); ?>"><?php esc_html_e('Pagination:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('pagination')); ?>" name="<?php print esc_attr($this->get_field_name('pagination')); ?>">
				<option value="0" <?php selected( $instance['pagination'], '0' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="1" <?php selected( $instance['pagination'], '1' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>
		<p>
			<label for="<?php print esc_attr($this->get_field_id('singleitem')); ?>"><?php esc_html_e('Single Item:', 'hill'); ?></label>  
			<select class="widefat" id="<?php print esc_attr($this->get_field_id('singleitem')); ?>" name="<?php print esc_attr($this->get_field_name('singleitem')); ?>">
				<option value="false" <?php selected( $instance['singleitem'], 'false' ); ?>><?php esc_html_e('Disable', 'hill'); ?></option>
				<option value="true" <?php selected( $instance['singleitem'], 'true' ); ?>><?php esc_html_e('Enable', 'hill'); ?></option>
			</select>  
		</p>
		<?php
	}

	public function widget($args, $instance){

		extract( $args );

		$title = $instance['title'];

		print wp_kses_post($before_widget);
		/* If a title was input by the user, display it. */
		if ( !empty( $title ) ){ 
			print wp_kses_post($before_title . apply_filters( 'hill_pc_widget_title', $title, $instance, $this->id_base ) . $after_title);
		}
		print do_shortcode( '[hill_products_carousel number="'.esc_attr($instance['number']).'" order="'.esc_attr($instance['order']).'" orderby="'.esc_attr($instance['orderby']).'" show="'.esc_attr($instance['show']).'" per_slider="'.esc_attr($instance['per_slider']).'" singleitem="'.esc_attr($instance['singleitem']).'" autoplay="'.esc_attr($instance['autoplay']).'" stop_on_hover="'.esc_attr($instance['stop_on_hover']).'" navigation="'.esc_attr($instance['navigation']).'" pagination="'.esc_attr($instance['pagination']).'" slide_speed="'.esc_attr($instance['slide_speed']).'" boxed_mod="'.esc_attr($instance['boxed_mod']).'" vertical_mode="'.esc_attr($instance['vertical_mode']).'"]' );
		print wp_kses_post($after_widget);
	}
}
function Hill_Products_Carousel_widget() {
    register_widget( 'Hill_Products_Carousel' );
}
add_action( 'widgets_init', 'Hill_Products_Carousel_widget' );
