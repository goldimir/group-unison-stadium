<?php

add_action( 'widgets_init', 'hill_latest_post' );

function hill_latest_post() {
	register_widget( 'Hill_Latest_Post');
}

class Hill_Latest_Post extends WP_Widget {

	private $defaults = array();

	function __construct(){
		include_once HILL_FREAMWORK_DIRECTORY.'widgets/get-the-image.php';

		$this->defaults = array(
			'title' => esc_html__('Latest Post', 'hill'),
			'number' => 5,
			'per_row' => 1,
			'first_full' => 0,
			'meta_on_thumb' => 0
		);
		parent::__construct( 'widget_latest_posts_tab', esc_html__('Hill Latest Posts','hill') );
	}

   /*----------------------------------------
	  update()
	  ----------------------------------------
	
	* Function to update the settings from
	* the form() function.
	
	* Params:
	* - Array $new_instance
	* - Array $old_instance
	----------------------------------------*/
	
	function update ( $new_instance, $old_instance ) {		
		$defaults = $this->defaults;
		$instance = $old_instance;		
		$instance['title'] = esc_attr( $new_instance['title'] );
		$instance['number'] = intval( $new_instance['number'] );
		$instance['per_row'] = intval( $new_instance['per_row'] );
		$instance['first_full'] = $new_instance['first_full'];
		$instance['meta_on_thumb'] = $new_instance['meta_on_thumb'];

		return $instance;
		
	} // End update()

   /*----------------------------------------
	 form()
	 ----------------------------------------
	  
	  * The form on the widget control in the
	  * widget administration area.
	  
	  * Make use of the get_field_id() and 
	  * get_field_name() function when creating
	  * your form elements. This handles the confusing stuff.
	  
	  * Params:
	  * - Array $instance
	----------------------------------------*/

   function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$title = isset( $instance['title'] ) ? esc_attr($instance['title']) : '';
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'hill'); ?></label>
			<input type="text" name="<?php echo esc_attr($this->get_field_name('title')); ?>"  value="<?php echo esc_attr($title); ?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
		</p>
		<p>
		   <label for="<?php print esc_attr($this->get_field_id( 'number' )); ?>"><?php esc_html_e('Number of posts:', 'hill'); ?>
		   <input class="widefat" id="<?php print esc_attr($this->get_field_id( 'number' )); ?>" name="<?php print esc_attr($this->get_field_name( 'number' )); ?>" type="text" value="<?php echo isset( $instance['number'] ) ? esc_attr($instance['number']) : ''; ?>" />
		   </label>
		</p>
		<p>
		   <label for="<?php print esc_attr($this->get_field_id( 'per_row' )); ?>"><?php esc_html_e('Posts Per Row', 'hill'); ?>
		   <input class="widefat" id="<?php print esc_attr($this->get_field_id( 'per_row' )); ?>" name="<?php print esc_attr($this->get_field_name( 'per_row' )); ?>" type="text" value="<?php echo isset( $instance['per_row'] ) ? esc_attr($instance['per_row']) : ''; ?>" />
		   </label>
		</p>

		<p>
		   <label for="<?php print esc_attr($this->get_field_id( 'first_full' )); ?>"><?php esc_html_e('1st Item Full Row', 'hill'); ?>
		   <input class="widefat" id="<?php print esc_attr($this->get_field_id( 'first_full' )); ?>" name="<?php print esc_attr($this->get_field_name( 'first_full' )); ?>" type="checkbox" <?php checked($instance['first_full'], 'on'); ?> />
		   </label>
		</p>

		<p>
		   <label for="<?php print esc_attr($this->get_field_id( 'meta_on_thumb' )); ?>"><?php esc_html_e('Title &amp; Meta on Thumbnail', 'hill'); ?>
		   <input class="widefat" id="<?php print esc_attr($this->get_field_id( 'meta_on_thumb' )); ?>" name="<?php print esc_attr($this->get_field_name( 'meta_on_thumb' )); ?>" type="checkbox" <?php checked($instance['meta_on_thumb'], 'on'); ?> />
		   </label>
		</p>

		<?php

	} // End form()


	function widget($args, $instance) {


		$instance = wp_parse_args( (array) $instance, $this->defaults );

		extract( $args );
	
		$number = isset( $instance['number'] ) ? $instance['number'] : 5;
		$per_row = isset( $instance['per_row'] ) ? $instance['per_row'] : 1;
		$first_full = isset( $instance['first_full'] ) ? $instance['first_full'] : null;
		$meta_on_thumb = isset( $instance['meta_on_thumb'] ) ? $instance['meta_on_thumb'] : null;

		$title = $instance['title'];

		print wp_kses_post($before_widget);
		/* If a title was input by the user, display it. */
		if ( !empty( $title ) ){ 
			print wp_kses_post($before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title);
		}
		$this->hill_all_latest_posts($number,$per_row,$first_full,$meta_on_thumb);
		print wp_kses_post($after_widget);
	}


	function hill_all_latest_posts($number = 5,$per_row = 1,$first_full = null,$meta_on_thumb = null) {

		$query_args = array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		);



		$sds_r = new WP_Query( $query_args );
		?>
		<div class="hill_widget_latest_post_container">
			<div class="row">
			<?php
			$sds_r_i = 0;
			$first_gone = false;
			if ($sds_r->have_posts()):
				while ( $sds_r->have_posts() ) : $sds_r->the_post();

				$sds_r_i++;

				$imageArgs = array(
					'width' => (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ? 1200 : 370,
					'height' => (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ? 560 : 310,
					'image_class' => 'thumbnail',
					'format' => 'array',
					'default_image' => '',
					'size' => (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ?  'post-thumbnail' : 'hill-grid-thumb'
				);

				$postImage = pptwj_get_the_image($imageArgs, get_the_ID());

				$post_col_size = (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ? 'col-md-12' : 'col-md-'.(12/$per_row).' col-sm-'.(12/$per_row);

				$post_title_meta_class = '';
				$post_title_meta_class = ( has_post_thumbnail() &&  ((!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) || (!empty($meta_on_thumb) && $meta_on_thumb == 'on')) ) ? 'framework-per-single-post' : 'framework-per-single-post-no-thumb';

				$title_class = (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ? 'hill_big_post_title' : '' ;

				?>
				<div class="<?php echo esc_attr($post_col_size); ?>">
					<article class="clearfix hill_widget_popular_post hill-blog-post">
						<div class="entry_categories <?php if( isset($postImage['src']) && !empty($postImage['src']) ) { echo "entry_categories-per-single-post"; } ?>">
							<?php
							$categories = get_the_category();
							if (count($categories)) {
								foreach ($categories as $categorie) {
									?>
									<span><a href="<?php echo esc_url(get_term_link($categorie)); ?>"><?php echo esc_html($categorie->cat_name); ?></a></span>
									<?php
								}
							}
							?>
						</div>
						<?php if(isset($postImage['src']) && !empty($postImage['src'])): ?>
							<div class="hill-post-thumb-have-hover"><a href="<?php the_permalink()?>"><span class="hill-blog-overlayer"></span><img src="<?php echo esc_url($postImage['src']); ?>" alt="<?php echo esc_attr($postImage['alt']); ?>" /></a></div>
						<?php endif; ?>
						<div class="<?php echo esc_attr($post_title_meta_class); ?>">
							<div class="hill-single-post-meta-box">
								<span><i class="fa fa-calendar"></i> <?php echo get_the_date(); ?></span>
								
								<span class="framwork-commnet-number">
									<a href="<?php echo esc_url( get_permalink() ) ?>">
										&nbsp;<i class="fa fa-comment-o"></i>&nbsp;
										<?php 
										printf( _nx( '0', '%1$s', get_comments_number(), 'comments title', 'hill' ), number_format_i18n( get_comments_number() ) );
										?>
									</a>
								</span>
						
								<?php if(function_exists('hill_love_it')) { echo hill_love_it(); } ?> 
								<?php echo hill_share_buttons(); ?>
							</div>
							<h3 class="entry-title <?php echo esc_attr($title_class); ?>"><a href="<?php  the_permalink()?>"><?php the_title()?></a></h3>
						</div>
					</article>
				</div>
				<?php


				if ((!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone && $number != 1) || ($sds_r_i == $per_row)) {
					
					$first_gone = (!empty($first_full) && $first_full == 'on' && $sds_r_i === 1 && !$first_gone) ? true : false;
					?>
					</div><div class="row">
					<?php

					$sds_r_i = 0;
				}

				

				endwhile;
			endif;
			wp_reset_postdata();
			?>
			</div>
		</div>
		<?php
	}
        
} // End Class
