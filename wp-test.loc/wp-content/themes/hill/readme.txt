A just right amount of lean, well-commented, modern, HTML5 templates.
A helpful 404 template.
An optional sample custom header implementation in inc/custom-header.php
Custom template tags in inc/template-tags.php that keep your templates clean and neat and prevent code duplication.
Some small tweaks in inc/extras.php that can improve your theming experience.
A script at js/navigation.js that makes your menu a toggled dropdown on small screens (like your phone), ready for CSS artistry.
2 sample CSS layouts in layouts/: A sidebar on the right side of your content and a sidebar on the left side of your content.