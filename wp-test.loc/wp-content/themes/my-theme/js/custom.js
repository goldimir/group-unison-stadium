document['addEventListener']('touchstart', function() {}, false);

jQuery(function($) {
    $('.skydropdown')['append']($('<a class="skydropdown-anim-arrw"><span></span><span></span><span></span><span></span><span></span><span></span></a>'));
    $('.skydropdown')['append']($('<div class="skydropdown-text">Navigation</div>'));
    $('.skydropdown-list > li')['has']('.skydropdown-submenu')['prepend']('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
    $('.skydropdown-submenu > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
    $('.skydropdown-submenu-sub > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
    $('.skydropdown-submenu-sub-sub > li')['has']('ul')['prepend']('<span class="skydropdown-clk-two"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
    $('.skydropdown-list li')['has']('.menu-big')['prepend']('<span class="skydropdown-click"><i class="skydropdown-arrow fa fa-angle-down"></i></span>');
    $('.skydropdown-anim-arrw')['click'](function() {
        $('.skydropdown-list')['slideToggle']('slow');
        $(this)['toggleClass']('skydropdown-lines')
    });
    $('.skydropdown-click')['click'](function() {
        $(this)['toggleClass']('skydropdownarrow-rotate')['parent']()['siblings']()['children']()['removeClass']('skydropdownarrow-rotate');
        $('.skydropdown-submenu, .menu-big')['not']($(this)['siblings']('.skydropdown-submenu, .menu-big'))['slideUp']('slow');
        $(this)['siblings']('.skydropdown-submenu')['slideToggle']('slow');
        $(this)['siblings']('.menu-big')['slideToggle']('slow')
    });
    $('.skydropdown-clk-two')['click'](function() {
        $(this)['toggleClass']('skydropdownarrow-rotate')['parent']()['siblings']()['children']()['removeClass']('skydropdownarrow-rotate');
        $(this)['siblings']('.skydropdown-submenu')['slideToggle']('slow');
        $(this)['siblings']('.skydropdown-submenu-sub')['slideToggle']('slow');
        $(this)['siblings']('.skydropdown-submenu-sub-sub')['slideToggle']('slow')
    });

    // This is just for the case that the browser window is resized
    window['onresize'] = function() {
        if ($(window)['width']() > 767) {
            $('.skydropdown-submenu')['removeAttr']('style');
            $('.skydropdown-list')['removeAttr']('style')
        }
    }

    /**
     * Front User Login
     */
    $(".tab_content_login").hide();
    $("ul.tabs_login li:first").addClass("active_login").show();
    $(".tab_content_login:first").show();
    $("ul.tabs_login li").click(function() {
        $("ul.tabs_login li").removeClass("active_login");
        $(this).addClass("active_login");
        $(".tab_content_login").hide();
        var activeTab = $(this).find("a").attr("href");
        if ($.browser.msie) {
            $(activeTab).show();
        }
        else {
            $(activeTab).show();
        }
        return false;
    });

    /**
     * Fancybox Video Gallery
     */
    $('.fancybox').fancybox({
        padding   : 0,
        maxWidth  : '100%',
        maxHeight : '100%',
        width   : 560,
        height    : 315,
        autoSize  : true,
        closeClick  : true,
        openEffect  : 'elastic',
        closeEffect : 'elastic'
    });

    /**
     * Filter Video Gallery
     */

    let searchTaxVal = $('.video-gallery-tax');
    let selectFilter = $('.select-filter');
    let resetBtn = $('.reset-btn');
    let selectObj = {};

    selectFilter.on('change', function () {

        selectFilter.find(':selected').each(function( index ) {
            selectObj[ $(this).data('taxonomy')] = $(this).val();
        });
        resetBtn.fadeIn(600);

        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'fly_ajax_tax_search',
                'term': selectObj,
            },
            success: function (result) {
                searchTaxVal.html(result);
            }
        });
    });

    resetBtn.on('click', function (e) {
        selectFilter.val('').trigger('change');
        $(this).fadeOut(600);
        e.preventDefault();
    });

});