<?php
/**
 * Template Name: MB plugin and Post Type with plugin
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package My_Theme
 */

get_header();
?>
    <!--content-mb-plugin.php-->
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="container">
                <div class="row">

                    <?php
                    while ( have_posts() ) :
                        the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <!--acf-->
                            <header class="entry-header">
                                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                            </header><!-- .entry-header -->

                            <?php my_theme_post_thumbnail(); ?>

                            <div class="entry-content">
                                <div class="wrap-vg">
                                    <div class="filter-video-gallery">
                                        <form class="vg-filter">
                                            <?php

                                            $taxonomies = get_object_taxonomies('video-catalog-mb', 'object' );
                                            $taxonomies_name = array_keys( $taxonomies );
                                            $terms = get_terms($taxonomies_name);

                                            foreach ( $taxonomies as $taxonomy ) {
                                                ?>
                                                <select class="form-control select-filter">
                                                    <option selected data-taxonomy="<?php echo $taxonomy->name ?>" value=""><?php echo $taxonomy->label ?></option>
                                                    <?php
                                                    foreach ( $terms as $term ) {
                                                        if ( $term->taxonomy == $taxonomy->name ) {
                                                            printf(
                                                                '<option data-taxonomy="%2$s" value="%1$s">%3$s</option>',
                                                                $term->slug,
                                                                $term->taxonomy,
                                                                $term->name
                                                            );
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <?php
                                            }
                                            ?>
                                            <input type="submit" value="Reset Filter" class="form-control reset-btn">
                                        </form>
                                    </div>
                                    <div class="video-gallery-tax">
                                        <!--FOREACH-->
                                        <?php

                                        $args = array(
                                            'post_status' => 'publish',
                                            'orderby'     => 'date',
                                            'order'       => 'DESC',
                                            'numberposts' => -1,
                                            'post_type'   => 'video-catalog-mb',
                                        );
                                        $posts = get_posts( $args );

                                        foreach ( $posts as $post ) {
                                            ?>
                                            <!--                                            <div class="gallery-box">-->
                                            <?php
                                            $videos = rwmb_meta( 'mb_tax_cat_videos_id', '', $post->ID );
                                            foreach ( $videos as $video ) {
                                                ?>

                                                <article class="video">
                                                    <figure><video controls src="<?php echo $video['src']; ?>"></video></figure>
                                                    <?php the_title( '<h2 class="title">', '</h2>' ); ?>
                                                </article>

                                                <?php
                                            }
                                            ?>
                                            <!--                                            </div>-->
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>

                                <?php

                                the_content();

                                wp_link_pages( array(
                                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'my-theme' ),
                                    'after'  => '</div>',
                                ) );
                                ?>
                            </div><!-- .entry-content -->

                            <?php if ( get_edit_post_link() ) : ?>
                                <footer class="entry-footer">
                                    <?php
                                    edit_post_link(
                                        sprintf(
                                            wp_kses(
                                            /* translators: %s: Name of current post. Only visible to screen readers */
                                                __( 'Edit <span class="screen-reader-text">%s</span>', 'my-theme' ),
                                                array(
                                                    'span' => array(
                                                        'class' => array(),
                                                    ),
                                                )
                                            ),
                                            get_the_title()
                                        ),
                                        '<span class="edit-link">',
                                        '</span>'
                                    );
                                    ?>
                                </footer><!-- .entry-footer -->
                            <?php endif; ?>
                        </article>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    wp_reset_postdata();
                    ?>

                </div>
            </div>
        </main>
    </div>

<?php
//get_sidebar();
get_footer();
?>