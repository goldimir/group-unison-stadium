<?php
/**
 * Template part for displaying page content in template-map.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package My_Theme
 */

?>
<style>
    #map{
        width: 100%;
        height: 400px;
    }
</style>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </header><!-- .entry-header -->
    <?php my_theme_post_thumbnail(); ?>
    <div class="entry-content">
        <?php
            the_content();
        ?>


        <div id="map"></div>

        <script>
            function initMap(){
                // Map options
                var options = {
                    zoom:8,
                    center:{lat:42.3601,lng:-71.0589}
                };

                // New map
                var map = new google.maps.Map(document.getElementById('map'), options);

                // Listen for click on map
                google.maps.event.addListener(map, 'click', function(event){
                    // Add marker
                    addMarker({coords:event.latLng});
                });

                // Array of markers
                var markers = [
                    {
                        coords:{lat:42.4668,lng:-70.9495},
                        iconImage:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                        content:'<h1>Lynn MA</h1>'
                    },
                    {
                        coords:{lat:42.8584,lng:-70.9300},
                        content:'<h1>Amesbury MA</h1>'
                    },
                    {
                        coords:{lat:42.7762,lng:-71.0773}
                    }
                ];

                // Loop through markers
                for(var i = 0;i < markers.length;i++){
                    // Add marker
                    addMarker(markers[i]);
                }

                // Add Marker Function
                function addMarker(props){
                    var marker = new google.maps.Marker({
                        position:props.coords,
                        map:map,
                        //icon:props.iconImage
                    });

                    // Check for customicon
                    if(props.iconImage){
                        // Set icon image
                        marker.setIcon(props.iconImage);
                    }

                    // Check content
                    if(props.content){
                        var infoWindow = new google.maps.InfoWindow({
                            content:props.content
                        });

                        marker.addListener('click', function(){
                            infoWindow.open(map, marker);
                        });
                    }
                }
            }
        </script>




<?php
        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'my-theme' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                    wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Edit <span class="screen-reader-text">%s</span>', 'my-theme' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU4AqnyWXeWX1nygAyUd3p-Gy1YELV4ZQ&callback=initMap"></script>