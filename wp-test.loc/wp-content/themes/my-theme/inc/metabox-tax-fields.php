<?php

add_action('init', 'tax_video_catalog_init');
function tax_video_catalog_init(){

    register_taxonomy('catalog', array('tax_cat_videos'), array(
        'label'                 => 'Categories',
        'labels'                => array(
            'name'              => 'Category',
            'singular_name'     => 'Раздел вопроса',
            'search_items'      => 'Искать Раздел вопроса',
            'all_items'         => 'Все Разделы вопросов',
            'parent_item'       => 'Родит. раздел вопроса',
            'parent_item_colon' => 'Родит. раздел вопроса:',
            'edit_item'         => 'Ред. Раздел вопроса',
            'update_item'       => 'Обновить Раздел вопроса',
            'add_new_item'      => 'Add new category',
            'new_item_name'     => 'Новый Раздел вопроса',
            'menu_name'         => 'Categories',
        ),
        'description'           => 'Рубрики для раздела вопросов',
        'public'                => true,
        'hierarchical'          => true,
    ) );

    register_taxonomy('color', array('tax_cat_videos'), array(
        'label'                 => 'Colors',
        'labels'                => array(
            'name'              => 'Color',
            'singular_name'     => 'Раздел вопроса',
            'search_items'      => 'Искать Раздел вопроса',
            'all_items'         => 'Все Разделы вопросов',
            'parent_item'       => 'Родит. раздел вопроса',
            'parent_item_colon' => 'Родит. раздел вопроса:',
            'edit_item'         => 'Ред. Раздел вопроса',
            'update_item'       => 'Обновить Раздел вопроса',
            'add_new_item'      => 'Add new category',
            'new_item_name'     => 'Новый Раздел вопроса',
            'menu_name'         => 'Colors',
        ),
        'description'           => 'Рубрики для раздела вопросов',
        'public'                => true,
        'hierarchical'          => true,
    ) );

    register_taxonomy('manufacture', array('tax_cat_videos'), array(
        'label'                 => 'Manufacture',
        'labels'                => array(
            'name'              => 'Manufacture',
            'singular_name'     => 'Раздел вопроса',
            'search_items'      => 'Искать Раздел вопроса',
            'all_items'         => 'Все Разделы вопросов',
            'parent_item'       => 'Родит. раздел вопроса',
            'parent_item_colon' => 'Родит. раздел вопроса:',
            'edit_item'         => 'Ред. Раздел вопроса',
            'update_item'       => 'Обновить Раздел вопроса',
            'add_new_item'      => 'Add new Manufacture',
            'new_item_name'     => 'Новый Раздел вопроса',
            'menu_name'         => 'Manufacture',
        ),
        'description'           => 'Рубрики для раздела вопросов',
        'public'                => true,
        'hierarchical'          => true,
//        'meta_box_cb'          => 'drop_cat',
    ) );

    register_post_type('tax_cat_videos', array(
        'labels'             => array(
            'name'               => 'Taxonomies Video Catalog',
            'singular_name'      => 'Tax Video',
            'add_new'            => 'Add New Video',
            'add_new_item'       => 'Add new video',
            'edit_item'          => 'Edit video',
            'new_item'           => 'Новая книга',
            'view_item'          => 'Посмотреть книгу',
            'search_items'       => 'Find video',
            'not_found'          =>  'No videos',
            'not_found_in_trash' => 'В корзине книг не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Taxonomies Video Catalog'
        ),
        'public'             => true,
        'taxonomies'         => array( 'catalog', 'manufacture' ),
        'menu_position'      => 100,
        'menu_icon'          => 'dashicons-format-video',
        'supports'           => array('title','author'),
    ) );

}


//Change taxonomy from Checkboxes to dropdown list - 'meta_box_cb' => 'drop_cat',
function drop_cat( $post, $box ) {
    $defaults = array('taxonomy' => 'manufacture');
    if ( !isset($box['args']) || !is_array($box['args']) )
        $args = array();
    else
        $args = $box['args'];
    extract( wp_parse_args($args, $defaults), EXTR_SKIP );
    ?>
    <div id="taxonomy-<?php echo $taxonomy; ?>" class="acf-taxonomy-field categorydiv">

        <?php
        $name = ( $taxonomy == 'category' ) ? 'post_category' : 'tax_input[' . $taxonomy . ']';
        echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
        ?>
        <? $term_obj = wp_get_object_terms($post->ID, $taxonomy ); //_log($term_obj[0]->term_id)?>
        <ul id="<?php echo $taxonomy; ?>checklist" data-wp-lists="list:<?php echo $taxonomy?>" class="categorychecklist form-no-clear">
            <?php //wp_terms_checklist($post->ID, array( 'taxonomy' => $taxonomy) ) ?>
        </ul>

        <?php wp_dropdown_categories( array( 'taxonomy' => $taxonomy, 'hide_empty' => 0, 'name' => "{$name}[]", 'selected' => $term_obj[0]->term_id, 'orderby' => 'name', 'hierarchical' => 0, 'show_option_none' => '&mdash;' ) ); ?>

    </div>
    <?php
}

//Create Taxonomy list in Advanced Select Meta box from BD

//function taxonomyList( $tax ) {
//    $terms = get_terms(array(
//        'taxonomy' => array( $tax ),
////        'hide_empty' => false,
//    ));
//
//    $arr = [];
//    foreach ($terms as $term) {
//        $arr += [ $term->slug => $term->name ];
//    }
//    return $arr;
//}

add_filter( 'rwmb_meta_boxes', 'prefix_meta_boxes_tax' );
function prefix_meta_boxes_tax( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'  => 'videos_tax',
        'title'  => 'Tax Videos',
        'post_types' => 'tax_cat_videos',
        'fields' => array(
            array(
                'name'             => 'Tax Video',
                'id'               => 'tax_cat_videos_id',
                'type'             => 'video',
                'max_file_uploads' => 0,
                'force_delete'     => false,
            ),
        ),
    );
    return $meta_boxes;
}

/**
 * AJAX Search Filter Video Gallery
 */
function tax_query_filter( $items ) {

    $query = [];
    foreach ( $items as $taxonomy => $term ){
        $tax_term_query = array(
            'taxonomy' => $taxonomy,
            'field' => 'slug',
            'terms' => $term,
        );
        if( $term == '' ){
            $tax_term_query['operator'] = 'AND';
        };
        array_push($query, $tax_term_query);
    }
    $filter_query = [
        'relation' => 'AND',
        $query
    ];
    return $filter_query;
}

function fly_ajax_tax_search(){

    $list_filters = $_POST['term'];

    $args = array(
        'post_status'      => 'publish',
        'orderby'     => 'date',
        'order'       => 'DESC',
        'numberposts'       => -1,
        'post_type'   => 'tax_cat_videos',
        'tax_query' => tax_query_filter( $list_filters ),
    );
    $posts = get_posts( $args );

    foreach ( $posts as $post ) {
        $videos = rwmb_meta( 'tax_cat_videos_id', '', $post->ID );
        foreach ( $videos as $video ) {
            ?>

            <article class="video">
                <figure><video controls src="<?php echo $video['src']; ?>"></video></figure>
                <h2 class="title"><?php echo $post->post_title ?></h2>
            </article>

            <?php
        }
    }
    wp_reset_postdata();
    exit;
}
add_action('wp_ajax_nopriv_fly_ajax_tax_search','fly_ajax_tax_search');
add_action('wp_ajax_fly_ajax_tax_search','fly_ajax_tax_search');
