<?php

add_action('init', 'video_catalog_init');
function video_catalog_init(){

    register_post_type('cat_videos', array(
        'labels'             => array(
            'name'               => 'Video Catalog',
            'singular_name'      => 'Video',
            'add_new'            => 'Add',
            'add_new_item'       => 'Add new video',
            'edit_item'          => 'Edit video',
            'new_item'           => 'Новая книга',
            'view_item'          => 'Посмотреть книгу',
            'search_items'       => 'Find video',
            'not_found'          =>  'No videos',
            'not_found_in_trash' => 'В корзине книг не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Video Catalog'
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 100,
        'menu_icon'          => 'dashicons-format-video',
        'supports'           => array('title','author')
    ) );
}

add_filter( 'rwmb_meta_boxes', 'prefix_meta_boxes' );
function prefix_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'id'  => 'videos_sss',
        'title'  => 'Videos',
        'post_types' => 'cat_videos',
        'fields' => array(
            array(
                'name'             => 'Video',
                'id'               => 'vid_id',
                'type'             => 'video',
                'max_file_uploads' => 0,
                'force_delete'     => false,
            ),
        ),
    );
    return $meta_boxes;
}

function queryVidPosts( $term = false ){
    $args = array(
        'post_status'      => 'publish',
        'numberposts' => -1,
        'category'    => 0,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'cat_videos',
    );
    if( $term == true ){
        $args = array ('s' => $_POST['term']);
    }
    $query = new WP_Query( $args );
    wp_reset_postdata();
    return $query;
}

/**
 * AJAX Search Filter Video Gallery
 */
function fly_ajax_search(){

    $query = queryVidPosts( true );
    $selCat = $query->query['s'];

    if( $selCat !== 'all' ){

        for( $i = 0; $i < count($selCat); $i++ ){

            $videos = rwmb_meta( 'vid_id', '', $selCat );

            foreach ( $videos as $video ) {
                ?>

                <article class="video">
                    <video controls src="<?php echo $video['src']; ?>"></video>
                </article>

                <?php
            }
        }

    } else {

        $query = queryVidPosts();

        $posts = $query->posts;

        foreach ( $posts as $post ) {

            $videos = rwmb_meta( 'vid_id', '', $post->ID );
            foreach ( $videos as $video ) {
                ?>

                <article class="video">
                    <video controls src="<?php echo $video['src']; ?>"></video>
                </article>

                <?php
            }
        }

    }
    exit;
}
add_action('wp_ajax_nopriv_fly_ajax_search','fly_ajax_search');
add_action('wp_ajax_fly_ajax_search','fly_ajax_search');
