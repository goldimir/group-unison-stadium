<?php

//MB Post Type with plugin
add_filter( 'rwmb_meta_boxes', 'mb_prefix_meta_boxes_tax' );
function mb_prefix_meta_boxes_tax( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'  => 'mb_videos_tax',
        'title'  => 'MB Tax Videos',
        'post_types' => 'video-catalog-mb',
        'fields' => array(
            array(
                'name'             => 'MB Tax Video',
                'id'               => 'mb_tax_cat_videos_id',
                'type'             => 'video',
                'max_file_uploads' => 0,
                'force_delete'     => false,
            ),
        ),
    );
    return $meta_boxes;
}

/**
 * AJAX Search Filter Video Gallery
 */
function tax_query_filter( $items ) {

	$query = [];
	foreach ( $items as $taxonomy => $term ){
		$tax_term_query = array(
			'taxonomy' => $taxonomy,
			'field' => 'slug',
			'terms' => $term,
		);
		if( $term == '' ){
			$tax_term_query['operator'] = 'AND';
		};
		array_push($query, $tax_term_query);
	}
	$filter_query = [
		'relation' => 'AND',
		$query
	];
	return $filter_query;
}

function fly_ajax_tax_search(){

	$list_filters = $_POST['term'];

	$args = array(
		'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'DESC',
		'numberposts' => -1,
		'post_type'   => 'video-catalog-mb',
		'tax_query'   => tax_query_filter( $list_filters ),
	);
	$posts = get_posts( $args );

	foreach ( $posts as $post ) {
		$videos = rwmb_meta( 'mb_tax_cat_videos_id', '', $post->ID );
		foreach ( $videos as $video ) {
			?>

			<article class="video">
				<figure><video controls src="<?php echo $video['src']; ?>"></video></figure>
				<h2 class="title"><?php echo $post->post_title ?></h2>
			</article>

			<?php
		}
	}
	wp_reset_postdata();
	exit;
}
add_action('wp_ajax_nopriv_fly_ajax_tax_search','fly_ajax_tax_search');
add_action('wp_ajax_fly_ajax_tax_search','fly_ajax_tax_search');