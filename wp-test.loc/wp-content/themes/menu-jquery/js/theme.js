jQuery(function($) {

    /*
     * Validation
     */
    jQuery.extend(jQuery.fn, {
        /*
         * check if field value lenth more than 3 symbols ( for name and comment )
         */
        validate_form: function (data) {

            let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                emailToValidate = $(this).val();

            let phoneReg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                phoneToValidate = $(this).val();

            if ($(this).val().length < 3 && data === 'min-symbol') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Please type something</div>').find('div').addClass('error').hide().fadeIn(600);
            } else if ((!emailReg.test(emailToValidate) || emailToValidate === "") && data === 'email') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Type valid Email address</div>').find('div').addClass('error').hide().fadeIn(600);
            } else if (!phoneReg.test(phoneToValidate) && phoneToValidate && data === 'phone') {
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('error');
                $(this).removeClass('valid');
                $(this).parent().append('<div>Type valid number of phone</div>').find('div').addClass('error').hide().fadeIn(600);
            } else {
                $(this).removeClass('error');
                $(this).removeClass('valid');
                $(this).parent().find('div.error').remove();
                $(this).parent().find('div.valid').remove();
                $(this).addClass('valid');
                $(this).parent().append('<div>Valid</div>').find('div').addClass('valid').hide().fadeIn(600);

                // console.log('VALID');

                return true;
            }
        },
    });

});
