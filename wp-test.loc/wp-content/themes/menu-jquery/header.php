<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Menu_jQuery
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <script>
        /**
         * Collect customer urls
         */
        let currUrl = window.location.href;

        let urlOne   = localStorage.getItem('item1'),
            urlTwo   = localStorage.getItem('item2'),
            urlThree = localStorage.getItem('item3');

        if( urlOne == null ){
            localStorage.setItem('item1', currUrl);
        } else if( urlTwo == null ){
            localStorage.setItem('item2', currUrl);
        } else if( urlThree == null ){
            localStorage.setItem('item3', currUrl);
        } else {
            localStorage.setItem('item1', urlTwo);
            localStorage.setItem('item2', urlThree);
            localStorage.setItem('item3', currUrl);
        }
    </script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php

//var_dump(wp_get_sidebars_widgets());

//    dynamic_sidebar( 'big-menu' );
//var_dump( dynamic_sidebar( 'big-menu' ) );
?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'menu-jquery' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$menu_jquery_description = get_bloginfo( 'description', 'display' );
			if ( $menu_jquery_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $menu_jquery_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
            <!-- Menu Toggle btn-->
            <div class="menu-toggle">
                <h3>Menu</h3>
                <button type="button" id="menu-btn">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'respMenu',
                'container'      => '',
                'menu_class'     => 'ace-responsive-menu',
			) );

//			wp_nav_menu( array(
//				'theme_location' => 'big-menu',
//				'menu_id'        => 'big-menu',
//                'container'      => '',
//                'menu_class'     => 'big-menu',
//                'walker'         => new Walker_Big_Menu(),
//			) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
            <p id="inner"></p>
            <script>
                document.getElementById('inner').innerHTML = "<b>First URL:</b> " + localStorage.getItem('item1') + "<br><b>Second URL:</b> " + localStorage.getItem('item2') + "<br><b>Last URL:</b> " + localStorage.getItem('item3');
            </script>

        <!-- Broker Modal -->
        <div class="modal fade" id="broker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Contact to Broker</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
						<?php echo do_shortcode('[broker-contact]') ?>
                    </div>
                </div>
            </div>
        </div>