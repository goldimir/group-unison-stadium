<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

if ( ! class_exists( 'WPGMP_Google_Maps_Pro' ) ) {

	class WPGMP_Google_Maps_Pro {

		/**
		 * List of Modules.
		 *
		 * @var array
		 */
		private $modules = array();
		/**
		 * Intialize variables, files and call actions.
		 *
		 * @var array
		 */
		public function __construct() {

			$this->wpgmp_define_constants();
			$this->wpgmp_load_files();

/* Will create Tables in DB, need to start one time */
//			$this->wpgmp_activation();

			register_activation_hook( __FILE__, array( $this, 'wpgmp_plugin_activation' ) );
			register_deactivation_hook( __FILE__, array( $this, 'wpgmp_plugin_deactivation' ) );
			if ( is_multisite() ) {
				add_action( 'wpmu_new_blog', array( $this, 'wpgmp_on_blog_new_generate' ), 10, 6 );
				add_filter( 'wpmu_drop_tables', array( $this, 'wpgmp_on_blog_delete' ) );
			}
			add_action( 'plugins_loaded', array( $this, 'wpgmp_load_plugin_languages' ) );
			add_action( 'init', array( $this, 'wpgmp_init' ) );
			add_action( 'widgets_init', array( $this, 'wpgmp_google_map_widget' ) );
			add_filter( 'fc-dummy-placeholders', array( $this, 'wpgmp_apply_placeholders' ) );

		}

		function wpgmp_apply_placeholders( $content ) {
			 $data['marker_id']                 = 1;
			 $data['marker_title']              = 'New York, NY, United States';
			 $data['marker_email']              = 'my@email.com';
			 $data['marker_address']            = 'New York, NY, United States';
			 $data['marker_message']            = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.';
			 $data['marker_category']           = 'Real Estate';
			 $data['marker_icon']               = WPGMP_IMAGES . 'default_marker.png';
			 $data['marker_latitude']           = '40.7127837';
			 $data['marker_longitude']          = '-74.00594130000002';
			 $data['marker_city']               = 'New York';
			 $data['marker_state']              = 'NY';
			 $data['marker_country']            = 'United States';
			 $data['marker_zoom']               = '5';
			 $data['marker_postal_code']        = '10002';
			 $data['extra_field_slug']          = 'color';
			 $data['marker_featured_image_src'] = WPGMP_IMAGES . 'sample.jpg';
			 $data['marker_image']              = '<img class="fc-item-featured_image  fc-item-large" src="' . WPGMP_IMAGES . 'sample.jpg' . '" />';
			 $data['marker_featured_image']     = '<img class="fc-item-featured_image  fc-item-large" src="' . WPGMP_IMAGES . 'sample.jpg' . '" />';
			 $data['post_title']                = 'Lorem ipsum dolor sit amet, consectetur';
			 $data['post_link']                 = '#';
			 $data['post_excerpt']              = 'Lorem ipsum dolor sit amet, consectetur';
			 $data['post_content']              = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';
			 $data['post_categories']           = 'city tour';
			 $data['post_tags']                 = 'WordPress, plugins, google maps';
			 $data['post_featured_image']       = '<img class="fc-item-featured_image  fc-item-large" src="' . WPGMP_IMAGES . 'sample.jpg' . '" />';
			 $data['post_author']               = 'FlipperCode';
			 $data['post_comments']             = '<i class="fci fci-comment"></i> 10';
			 $data['view_count']                = '<i class="fci fci-heart"></i> 1';

			foreach ( $data as $key => $value ) {
				if ( strstr( $key, 'marker_featured_image_src' ) === false and strstr( $key, 'marker_icon' ) === false and strstr( $key, 'post_link' ) === false and strstr( $key, 'marker_zoom' ) === false and strstr( $key, 'marker_id' ) === false ) {
					$content = str_replace( "{{$key}}", $value . '<span class="fc-hidden-placeholder">{' . $key . '}</span>', $content );
				} else {
					$content = str_replace( "{{$key}}", $value, $content );
				}
			}
			return $content;
		}

		/**
		 * Call WordPress hooks.
		 */
		function wpgmp_init() {

			global $wpdb;
			// Add VC shortcode
			if ( is_admin() ) {
				$this->wpgmp_check_vc_depenency();
			}

			// Actions.
			add_action( 'admin_menu', array( $this, 'wpgmp_create_menu' ) );
			add_action( 'media_upload_ell_insert_gmap_tab', array( $this, 'wpgmp_google_map_media_upload_tab' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'wpgmp_frontend_scripts' ) );
			add_action( 'admin_print_scripts', array( $this, 'wpgmp_backend_styles' ) );
			add_action( 'admin_init', array( $this, 'wpgmp_export_data' ) );
			add_action( 'add_meta_boxes', array( $this, 'wpgmp_call_meta_box' ) );
			add_action( 'save_post', array( $this, 'wpgmp_save_meta_box_data' ) );
			add_action( 'wp_ajax_wpgmp_ajax_call', array( $this, 'wpgmp_ajax_call' ) );
			add_action( 'wp_ajax_nopriv_wpgmp_ajax_call', array( $this, 'wpgmp_ajax_call' ) );

			// Filters.
			add_filter( 'media_upload_tabs', array( $this, 'wpgmp_google_map_tabs_filter' ) );

			// Shortodes.
			add_shortcode( 'put_wpgm', array( $this, 'wpgmp_show_location_in_map' ) );
			add_shortcode( 'display_map', array( $this, 'wpgmp_display_map' ) );

			add_action( 'admin_head', array($this,'wpgmp_customizer_font_family' ));

		}



		function wpgmp_customizer_font_family() {

				$font_families  = array();
				if ( isset( $_GET['doaction'] ) and 'edit' == $_GET['doaction'] and isset( $_GET['map_id'] ) ) {

				    $modelFactory = new WPGMP_Model();
					$map_obj      = $modelFactory->create_object( 'map' );
					$map_obj = $map_obj->fetch( array( array( 'map_id', '=', intval( wp_unslash( $_GET['map_id'] ) ) ) ) );
					$map     = $map_obj[0];
					if ( ! empty( $map ) ) {
						$map->map_all_control             = unserialize( $map->map_all_control );
					}

					$data = (array) $map;

					if ( isset( $data['map_all_control']['fc_custom_styles'] ) ) {
						$fc_custom_styles = json_decode( $data['map_all_control']['fc_custom_styles'], true );
						if ( ! empty( $fc_custom_styles ) && is_array( $fc_custom_styles ) ) {
							$fc_skin_styles = '';
							foreach ( $fc_custom_styles as $fc_style ) {
								if ( is_array( $fc_style ) ) {
									foreach ( $fc_style as $skin => $class_style ) {
										if ( is_array( $class_style ) ) {
											foreach ( $class_style as $class => $style ) {
												$ind_style         = explode( ';', $style );

												foreach ($ind_style as $css_value) {
													if ( strpos( $css_value, 'font-family' ) !== false ) {
															$font_family_properties   = explode( ':', $css_value );
															if(!empty($font_family_properties['1'])){
																$multiple_family = explode( ',', $font_family_properties['1']);
																if(count($multiple_family)==1){
																	$font_families[] = $font_family_properties['1'];
																}
															}
													}
												}

												if ( strpos( $class, '.' ) !== 0 ) {
													$class = '.' . $class;
												}
												$fc_skin_styles .= ' .fc-' . $skin . ' ' . $class . '{' . $style . '}';
											}
										}
									}
								}
							}
							if ( ! empty( $fc_skin_styles ) ) {
								echo '<style>' . $fc_skin_styles . '</style>';
							}
						}
					}
				}


				if ( ! empty( $font_families ) ) {
					$font_families = array_unique($font_families);
					?>
					<script type="text/javascript">

						var google_customizer_fonts = <?php echo json_encode($font_families,JSON_FORCE_OBJECT);?>;

					</script>
					<?php }
			
		}

		function wpgmp_check_vc_depenency() {

			if ( defined( 'WPB_VC_VERSION' ) ) {
				$this->isVCInstalled = true;
				$this->wpgmp_map_component_vc();
			}
		}

		function wpgmp_map_component_vc() {

			if ( $this->isVCInstalled ) {

				global $wpdb;

				$map_options = array();

				$map_options[ esc_html__( 'Select Map', 'wpgmp-google-map' ) ] = '';
				$map_records = $wpdb->get_results( 'SELECT map_id,map_title FROM ' . TBL_MAP . '' );

				if ( ! empty( $map_records ) ) {
					foreach ( $map_records as $key => $map_record ) {
						$map_options[ $map_record->map_title ] = $map_record->map_id;
					}
				}

				$shortcodeParams = array();

				$shortcodeParams[] = array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Choose Maps', 'wpgmp-google-map' ),
					'param_name'  => 'id',
					'description' => esc_html__( 'Choose here the map you want to show.', 'wpgmp-google-map' ),
					'value'       => $map_options,
				);

				$wpgmp_maps_component = array(
					'name'        => esc_html__( 'WP Google Map Pro', 'wpgmp-google-map' ),
					'base'        => 'put_wpgm',
					'class'       => '',
					'category'    => esc_html__( 'Content', 'wpgmp-google-map' ),
					'description' => esc_html__( 'Google Maps', 'wpgmp-google-map' ),
					'params'      => $shortcodeParams,
					'icon'        => WPGMP_IMAGES . 'flippercode.png',
				);
				vc_map( $wpgmp_maps_component );

			}

		}

		/**
		 * Export data into csv,xml,json or excel file
		 */
		function wpgmp_export_data() {

			if ( isset( $_POST['action'] ) && isset( $_REQUEST['_wpnonce'] ) && $_POST['action'] == 'export_location_csv' ) {
				$nonce = sanitize_text_field( wp_unslash( $_REQUEST['_wpnonce'] ) );

				if ( isset( $nonce ) and ! wp_verify_nonce( $nonce, 'wpgmp-nonce' ) ) {

					die( 'Cheating...' );

				}

				if ( isset( $_POST['action'] ) and false != strstr( $_POST['action'], 'export_' ) ) {
					$export_action = explode( '_', sanitize_text_field( $_POST['action'] ) );
					if ( 3 == count( $export_action ) and 'export' == $export_action[0] ) {
						$model_class = 'WPGMP_Model_' . ucwords( $export_action[1] );
						$entity      = new $model_class();
						$entity->export( $export_action[2] );
					}
				}
			}

		}
		/**
		 * Register WP Google Map Widget
		 */
		function wpgmp_google_map_widget() {

			register_widget( 'WPGMP_Google_Map_Widget_Class' );
		}
		/**
		 * Display WP Google Map meta box on pages/posts and custom post type(s).
		 */
		function wpgmp_call_meta_box() {

			$screens        = array( 'post', 'page' );
			$wpgmp_settings = get_option( 'wpgmp_settings', true );

			$args = array(
				'public'   => true,
				'_builtin' => false,
			);

			$custom_post_types = get_post_types( $args, 'names' );

			$screens = array_merge( $screens, $custom_post_types );

			$screens = apply_filters( 'wpgmp_meta_boxes', $screens );

			$selected_values = unserialize( $wpgmp_settings['wpgmp_allow_meta'] );

			foreach ( $screens as $screen ) {

				if ( is_array( $selected_values ) ) {

					if ( in_array( $screen, $selected_values ) or in_array( 'all', $selected_values ) ) {
						continue;
					}
				}

				add_meta_box(
					'wpgmp_google_map_metabox',
					esc_html__( 'WP Google Map Pro', 'wpgmp-google-map' ),
					array( $this, 'wpgmp_add_meta_box' ),
					$screen
				);
			}
		}
		/**
		 * Callback to display  wp google map pro meta box.
		 *
		 * @param  string $post Post Type.
		 */
		function wpgmp_add_meta_box( $post ) {

			global $wpdb;

			$wpgmp_settings = get_option( 'wpgmp_settings', true );

			$modelFactory                   = new WPGMP_Model();
			$category_obj                   = $modelFactory->create_object( 'group_map' );
			$categories                     = $category_obj->fetch();
			$map_obj                        = $modelFactory->create_object( 'map' );
			$all_maps                       = $map_obj->fetch();
			$wpgmp_location_address         = get_post_meta( $post->ID, '_wpgmp_location_address', true );
			$wpgmp_metabox_location_city    = get_post_meta( $post->ID, '_wpgmp_location_city', true );
			$wpgmp_metabox_location_state   = get_post_meta( $post->ID, '_wpgmp_location_state', true );
			$wpgmp_metabox_location_country = get_post_meta( $post->ID, '_wpgmp_location_country', true );

			$wpgmp_map_ids = get_post_meta( $post->ID, '_wpgmp_map_id', true );
			$wpgmp_map_id  = unserialize( $wpgmp_map_ids );
			if ( ! is_array( $wpgmp_map_id ) ) {
				$wpgmp_map_id = array( $wpgmp_map_ids );
			}
			$wpgmp_metabox_marker_id         = get_post_meta( $post->ID, '_wpgmp_metabox_marker_id', true );
			$wpgmp_metabox_latitude          = get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true );
			$wpgmp_metabox_longitude         = get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true );
			$wpgmp_metabox_location_redirect = get_post_meta( $post->ID, '_wpgmp_metabox_location_redirect', true );
			$wpgmp_metabox_custom_link       = get_post_meta( $post->ID, '_wpgmp_metabox_custom_link', true );
			if ( isset( $_SERVER['HTTPS'] ) && ( 'on' == $_SERVER['HTTPS'] || 1 == $_SERVER['HTTPS'] ) || isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
				$wpgmp_apilocation = 'https';
			} else {
				$wpgmp_apilocation = 'http';
			}

			if ( isset( $wpgmp_settings['wpgmp_country_specific'] ) ) {
				$wpgmp_country_specific = ( $wpgmp_settings['wpgmp_country_specific'] == 'true' );
			} else {
				$wpgmp_country_specific = false;
			}

			if ( isset( $wpgmp_settings['wpgmp_countries'] ) ) {
				$wpgmp_countries = $wpgmp_settings['wpgmp_countries'];
			} else {
				$wpgmp_countries = false;
			}


			$language = $wpgmp_settings['wpgmp_language'];

			if ( $language == '' ) {
				$language = 'en';
			}

			$language = apply_filters( 'wpgmp_map_lang', $language );

			if ( isset( $wpgmp_settings['wpgmp_language'] ) && $wpgmp_settings['wpgmp_language'] != '' ) {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?key=' . $wpgmp_settings['wpgmp_api_key'] . '&libraries=geometry,places,weather,panoramio,drawing&language=' . $language;
			} else {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?libraries=geometry,places,weather,panoramio,drawing&language=' . $language;
			}

			$hide_map = $wpgmp_settings['wpgmp_metabox_map'];

			$center_lat = '38.555475';
			$center_lng = '-95.665';

			if ( $wpgmp_metabox_latitude != '' ) {
				$center_lat = $wpgmp_metabox_latitude;
			}

			if ( $wpgmp_metabox_longitude != '' ) {
				$center_lng = $wpgmp_metabox_longitude;
			}

			$center_lat = apply_filters( 'wpgmp_metabox_lat', $center_lat );
			$center_lng = apply_filters( 'wpgmp_metabox_lng', $center_lng );

			?>
		<script src="<?php echo esc_url( $wpgmp_apilocation ); ?>"></script>
		<script>
		jQuery(document).ready(function($) {
			try {


			  var wpgmp_input = $("#wpgmp_metabox_location").val();
			var wpgmp_geocoder = new google.maps.Geocoder();

			<?php if ( $hide_map != 'true' ) { ?>
			var center = new google.maps.LatLng(<?php echo esc_html( $center_lat ); ?>, <?php echo esc_html( $center_lng ); ?>);
			var wpgmp_map = new google.maps.Map($(".wpgmp_meta_map")[0],{
				zoom: 5,
				center: center,
			});	

			var infowindow = new google.maps.InfoWindow();
			var wpgmp_marker = new google.maps.Marker({
			  map: wpgmp_map,
			  position: center,
			  draggable : true,
			});

			google.maps.event.addListener(wpgmp_marker, 'drag', function() {

				var position = wpgmp_marker.getPosition();

				wpgmp_geocoder.geocode({
					latLng: position
				}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						$("#wpgmp_metabox_location").val(results[0].formatted_address);
						$("#wpgmp_metabox_location_hidden").val(results[0].formatted_address);
						$("#wpgmp_metabox_location_city").val(wpgmp_get_exact_names(results[0], 'administrative_area_level_3') || wpgmp_get_exact_names(results[0], 'locality'));
						$("#wpgmp_metabox_location_state").val(wpgmp_get_exact_names(results[0], "administrative_area_level_1"));
						$("#wpgmp_metabox_location_country").val(wpgmp_get_exact_names(results[0], "country"));
						
					}
				});
				
				

				$(".wpgmp_metabox_latitude").val(position.lat());
				$(".wpgmp_metabox_longitude").val(position.lng());
			});
			<?php } ?>
			
			function wpgmp_get_exact_names(result, type){
					
					var component_name = "";
					for (i = 0; i < result.address_components.length; ++i) {
						var component = result.address_components[i];
						$.each(component.types, function(index, value) {
							if (value == type) {
								component_name = component.long_name;
							}
						});


					}
					return component_name;
				}
				
			var wpgmp_metabox_autocomplete = new google.maps.places.Autocomplete(wpgmp_metabox_location);

			<?php if ($wpgmp_country_specific && $wpgmp_countries != '') { 

					$js_array = json_encode($wpgmp_countries);

				?>
                    wpgmp_metabox_autocomplete.setComponentRestrictions({
                        'country': <?php echo $js_array; ?>
                    });
             <?php } ?>

			<?php if ( $hide_map != 'true' ) { ?>
			wpgmp_metabox_autocomplete.bindTo('bounds', wpgmp_map);
			<?php } ?>
			google.maps.event.addListener(wpgmp_metabox_autocomplete, 'place_changed', function() {
			var metabox_place = wpgmp_metabox_autocomplete.getPlace();
			<?php if ( $hide_map != 'true' ) { ?>
			wpgmp_map.setCenter(metabox_place.geometry.location);
			wpgmp_marker.setPosition(metabox_place.geometry.location);
			
			<?php } ?>
			$(".wpgmp_metabox_latitude").val(metabox_place.geometry.location.lat());
			$(".wpgmp_metabox_longitude").val(metabox_place.geometry.location.lng());
			$("#wpgmp_metabox_location_hidden").val(metabox_place.formatted_address);
			
			$("#wpgmp_metabox_location_city").val(wpgmp_get_exact_names(metabox_place, 'administrative_area_level_3') || wpgmp_get_exact_names(metabox_place, 'locality'));
			$("#wpgmp_metabox_location_state").val(wpgmp_get_exact_names(metabox_place, "administrative_area_level_1"));
			$("#wpgmp_metabox_location_country").val(wpgmp_get_exact_names(metabox_place, "country"));

			
						
			});

			$(".wpgmp_mcurrent_loction").click(function() {

				navigator.geolocation.getCurrentPosition(function(position) {
				
				var position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				<?php if ( $hide_map != 'true' ) { ?>
				wpgmp_map.setCenter(position);
				wpgmp_marker.setPosition(position);
			
				<?php } ?>
				wpgmp_geocoder.geocode({
					latLng: position
				}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						$("#wpgmp_metabox_location").val(results[0].formatted_address);
						$("#wpgmp_metabox_location_hidden").val(results[0].formatted_address);
						$("#wpgmp_metabox_location_city").val(get_exact_names(results[0], 'administrative_area_level_3') || get_exact_names(results[0], 'locality'));
						$("#wpgmp_metabox_location_state").val(get_exact_names(results[0], "administrative_area_level_1"));
						$("#wpgmp_metabox_location_country").val(get_exact_names(results[0], "country"));

					}
				});

				$(".wpgmp_metabox_latitude").val(position.lat());
				$(".wpgmp_metabox_longitude").val(position.lng());

				}, function(ErrorPosition) {

					
				}, {
					enableHighAccuracy: true,
					timeout: 5000,
					maximumAge: 0
				});
			});

			$("select[name='wpgmp_metabox_location_redirect']").change(function() {
			var rval = $(this).val();
			if(rval=="custom_link")
			{
			$("#wpgmp_toggle_custom_link").show("slow");
			}
			else
			{
				$("#wpgmp_toggle_custom_link").hide("slow");
			}
			});

			} catch(err) {
				console.log("wpgmp-exception - " + err.message);
			}
	});
		</script>

		<div class="wpgmp_metabox_container">
			<?php if ( $hide_map != 'true' ) { ?>
		<div class="row_metabox">
			<div class="wpgmp_meta_map"></div>
		</div>
		<?php } ?>
		<div class="row_metabox">
		<div class="wpgmp_metabox_left">
		<label for="wpgmp_metabox_location"><?php esc_html_e( 'Enter Location :', 'wpgmp-google-map' ); ?></label>
	</div>
	<div class="wpgmp_metabox_right">
	<input type="text" id="wpgmp_metabox_location" class="wpgmp_metabox_location wpgmp_auto_suggest" name="wpgmp_metabox_location" value="<?php echo htmlspecialchars( stripslashes( $wpgmp_location_address ) ); ?>" size="25" />
	<input type="hidden" id="wpgmp_metabox_location_hidden" name="wpgmp_metabox_location_hidden" value="<?php echo htmlspecialchars( stripslashes( $wpgmp_location_address ) ); ?>" />
	<input type="hidden" id="wpgmp_metabox_location_city" name="wpgmp_metabox_location_city" value="<?php echo htmlspecialchars( stripslashes( $wpgmp_metabox_location_city ) ); ?>" />
	<input type="hidden" id="wpgmp_metabox_location_state" name="wpgmp_metabox_location_state" value="<?php echo htmlspecialchars( stripslashes( $wpgmp_metabox_location_state ) ); ?>" />
	<input type="hidden" id="wpgmp_metabox_location_country" name="wpgmp_metabox_location_country" value="<?php echo htmlspecialchars( stripslashes( $wpgmp_metabox_location_country ) ); ?>" />
	<span class="wpgmp_mcurrent_loction" title="Take Current Location">&nbsp;</span>
	</div>
	</div>
	<div class="row_metabox">
	<div class="wpgmp_metabox_left">
	<label for="wpgmp_enter_location"><?php esc_html_e( 'Latitude', 'wpgmp-google-map' ); ?>&nbsp;/&nbsp;<?php esc_html_e( 'Longitude', 'wpgmp-google-map' ); ?>&nbsp;:</label>
	</div>
	<div class="wpgmp_metabox_right">
	<input type="text" class="wpgmp_metabox_latitude" id="wpgmp_metabox_latitude" name="wpgmp_metabox_latitude" value="<?php echo esc_attr( $wpgmp_metabox_latitude ); ?>" placeholder="Latitude" />
	<input type="text" class="wpgmp_metabox_longitude" id="wpgmp_metabox_longitude" name="wpgmp_metabox_longitude" value="<?php echo esc_attr( $wpgmp_metabox_longitude ); ?>" placeholder="Longitude" />
	</div>
	</div>
	<div class="row_metabox">
	<div class="wpgmp_metabox_left">
	<label><?php esc_html_e( 'Select Categories:', 'wpgmp-google-map' ); ?></label>
	</div>
	<div class="wpgmp_metabox_right">
			<?php
			$selected_categories = unserialize( $wpgmp_metabox_marker_id );

			if ( ! is_array( $selected_categories ) ) {
				$selected_categories = array( $wpgmp_metabox_marker_id );
			}

			if ( $categories ) {
				foreach ( $categories as $category ) {
					if ( in_array( $category->group_map_id, $selected_categories ) ) {
						$s = "checked='checked'";
					} else {
						$s = '';
					}
					?>
		<span class="wpgmp_check">
		<input type="checkbox" id="wpgmp_location_group_map<?php echo esc_attr( $category->group_map_id ); ?>" <?php echo esc_attr( $s ); ?> name="wpgmp_metabox_marker_id[]" value="<?php echo esc_attr( $category->group_map_id ); ?>">
					<?php echo esc_html( $category->group_map_title ); ?>
	</span>
					<?php
				}
			} else {
				echo '<p class="description">';

				$link = "<a href='" . esc_url( admin_url( 'admin.php?page=wpgmp_form_group_map' ) ) . "' target='_blank'>" . esc_html__( 'Here', 'wpgmp-google-map' ) . '</a>';

				printf(
					/* translators: %s: Add Category Link */
						esc_html__( 'Do you want to assign a category? Please create category %s.', 'wpgmp-google-map' ),
					$link
				);

				echo '</p>';
			}
			?>
		</div>
		</div>
		<div class="row_metabox">
		</div>
		<div class="row_metabox">
		<div class="wpgmp_metabox_left">
		<label for="wpgmp_enter_location"><?php esc_html_e( 'Location Redirect :', 'wpgmp-google-map' ); ?></label>
	</div>
	<div class="wpgmp_metabox_right">
	<select name="wpgmp_metabox_location_redirect" id="wpgmp_metabox_location_redirect">
	<option value="marker"<?php selected( $wpgmp_metabox_location_redirect, 'marker' ); ?>>Marker</option>
	<option value="post"<?php selected( $wpgmp_metabox_location_redirect, 'post' ); ?>>Post</option>
	<option value="custom_link"<?php selected( $wpgmp_metabox_location_redirect, 'custom_link' ); ?>>Custom Link</option>
	</select>
	</div>
	</div>

			<?php
			if ( ! empty( $wpgmp_metabox_custom_link ) && 'custom_link' == $wpgmp_metabox_location_redirect ) {
				$display_custom_link = 'display:block';
			} else {
				$display_custom_link = 'display:none';
			}
			?>

		<div class="row_metabox" style="<?php echo esc_attr( $display_custom_link ); ?>" id="wpgmp_toggle_custom_link">
	<div class="wpgmp_metabox_left">
	<label for="wpgmp_metabox_custom_link">&nbsp;</label>
	</div>
	<div class="wpgmp_metabox_right">
	<input type="textbox" value="<?php echo esc_attr( $wpgmp_metabox_custom_link ); ?>" name="wpgmp_metabox_custom_link" class="wpgmp_metabox_location" />
	<p class="description"><?php esc_html_e( 'Please enter link.', 'wpgmp-google-map' ); ?></p>
	</div>
	</div>
			<?php do_action( 'wpgmp_meta_box_fields' ); ?>
	<div class="row_metabox">
	<div class="wpgmp_metabox_left">
	<label><?php esc_html_e( 'Select Map :', 'wpgmp-google-map' ); ?></label>
	</div>
	<div class="wpgmp_metabox_right">
	
			<?php

			if ( count( $all_maps ) > 0 ) {
				foreach ( $all_maps as $map ) :

					if ( is_array( $wpgmp_map_id ) and in_array( $map->map_id, $wpgmp_map_id ) ) {
						$c = 'checked=checked';
					} else {
						$c = ''; }

					?>
	   
		 <span class="wpgmp_check"><input <?php echo esc_attr( $c ); ?> type="checkbox" name="wpgmp_metabox_mapid[]" value="<?php echo esc_attr( $map->map_id ); ?>">&nbsp; <?php echo esc_html( $map->map_title ); ?></span>
	
					<?php
	endforeach;
			} else {

				$link = "<a href='" . admin_url( 'admin.php?page=wpgmp_create_map' ) . "'>" . esc_html__( 'create a map', 'wpgmp-google-map' ) . '</a>';

				printf(
					/* translators: %s: Add Map Link */
						esc_html__( 'Please %s first.', 'wpgmp-google-map' ),
					$link
				);

			}
			?>
   
	<input type="hidden" name="wpgmp_hidden_flag" value="true" />
	
	</div>
	</div>

	</div>
			<?php
		}
		/**
		 * Save meta box data
		 *
		 * @param  int $post_id Post ID.
		 */
		function wpgmp_save_meta_box_data( $post_id ) {
			if ( isset( $_POST['wpgmp_hidden_flag'] ) ) {

				$wpgmp_enter_location = $_POST['wpgmp_metabox_location_hidden'];

				$wpgmp_enter_city    = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_location_city'] ) );
				$wpgmp_enter_state   = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_location_state'] ) );
				$wpgmp_enter_country = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_location_country'] ) );

				$wpgmp_metabox_latitude          = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_latitude'] ) );
				$wpgmp_metabox_longitude         = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_longitude'] ) );
				$wpgmp_map_id                    = serialize( wp_unslash( $_POST['wpgmp_metabox_mapid'] ) );
				$wpgmp_metabox_marker_id         = serialize( wp_unslash( $_POST['wpgmp_metabox_marker_id'] ) );
				$wpgmp_metabox_location_redirect = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_location_redirect'] ) );
				$wpgmp_metabox_custom_link       = sanitize_text_field( wp_unslash( $_POST['wpgmp_metabox_custom_link'] ) );
				$wpgmp_metabox_taxomomies_terms  = serialize( wp_unslash( $_POST['wpgmp_metabox_taxomomies_terms'] ) );
				$wpgmp_extensions_fields         = serialize( wp_unslash( $_POST['wpgmp_extensions_fields'] ) );

				// Update the meta field in the database.
				update_post_meta( $post_id, '_wpgmp_location_address', $wpgmp_enter_location );
				update_post_meta( $post_id, '_wpgmp_location_city', $wpgmp_enter_city );
				update_post_meta( $post_id, '_wpgmp_location_state', $wpgmp_enter_state );
				update_post_meta( $post_id, '_wpgmp_location_country', $wpgmp_enter_country );

				update_post_meta( $post_id, '_wpgmp_metabox_latitude', $wpgmp_metabox_latitude );
				update_post_meta( $post_id, '_wpgmp_metabox_longitude', $wpgmp_metabox_longitude );
				update_post_meta( $post_id, '_wpgmp_metabox_location_redirect', $wpgmp_metabox_location_redirect );
				update_post_meta( $post_id, '_wpgmp_metabox_custom_link', $wpgmp_metabox_custom_link );
				update_post_meta( $post_id, '_wpgmp_map_id', $wpgmp_map_id );
				update_post_meta( $post_id, '_wpgmp_metabox_marker_id', $wpgmp_metabox_marker_id );
				update_post_meta( $post_id, '_wpgmp_metabox_taxomomies_terms', $wpgmp_metabox_taxomomies_terms );
				update_post_meta( $post_id, '_wpgmp_extensions_fields', $wpgmp_extensions_fields );
			}
		}
		/**
		 * Eneque scripts at frontend.
		 */
		function wpgmp_frontend_scripts() {

			$wpgmp_settings = get_option( 'wpgmp_settings', true );

			$auto_fix = $wpgmp_settings['wpgmp_auto_fix'];

			$scripts = array();

			if ( $auto_fix == 'true' ) {
				wp_enqueue_script( 'jquery' );
			}

			if ( isset( $_SERVER['HTTPS'] ) && ( 'on' == $_SERVER['HTTPS'] || 1 == $_SERVER['HTTPS'] ) || isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
				$wpgmp_apilocation = 'https';
			} else {
				$wpgmp_apilocation = 'http';
			}

			$language = $wpgmp_settings['wpgmp_language'];

			if ( $language == '' ) {
				$language = 'en';
			}

			$language = apply_filters( 'wpgmp_map_lang', $language );

			if ( isset( $wpgmp_settings['wpgmp_api_key'] ) and $wpgmp_settings['wpgmp_api_key'] != '' ) {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?key=' . $wpgmp_settings['wpgmp_api_key'] . '&libraries=geometry,places,weather,panoramio,drawing&language=' . $language;
			} else {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?libraries=geometry,places,weather,panoramio,drawing&language=' . $language;
			}

			$scripts[] = array(
				'handle' => 'wpgmp-google-api',
				'src'    => $wpgmp_apilocation,
				'deps'   => array(),
			);


			if( isset( $wpgmp_settings['wpgmp_scripts_minify']) && $wpgmp_settings['wpgmp_scripts_minify'] == 'yes') {

				$scripts[] = array(
					'handle' => 'wpgmp-frontend',
					'src'    => WPGMP_JS . 'frontend.min.js',
					'deps'   => array( 'jquery-masonry', 'imagesloaded' ),
				);

			} else {
				
				$scripts[] = array(
					'handle' => 'wpgmp-jscrollpane',
					'src'    => WPGMP_JS . 'vendor/jscrollpane/jscrollpane.js',
					'deps'   => array(),
				);

				$scripts[] = array(
				'handle' => 'wpgmp-accordion',
				'src'    => WPGMP_JS . 'vendor/accordion/accordion.js',
				'deps'   => array(),
				);

				$scripts[] = array(
					'handle' => 'wpgmp-frontend',
					'src'    => WPGMP_JS . 'frontend.js',
					'deps'   => array( 'wpgmp-jscrollpane','jquery-masonry', 'imagesloaded','wpgmp-accordion', ),
				);
	
			}

			
			$where = $wpgmp_settings['wpgmp_scripts_place'];

			if ( $where == 'header' ) {
				$where = false;
			} else {
				$where = true;
			}

			if ( $wpgmp_settings['wpgmp_gdpr'] == true ) {
				$auto_fix = apply_filters( 'wpgmp_accept_cookies', false );
			}

			wp_enqueue_script('webfont', WPGMP_JS.'vendor/webfont/webfont.js', array(), WPGMP_VERSION, true );


			if ( $scripts ) {
				foreach ( $scripts as $script ) {
					if ( $auto_fix == 'true' ) {
						wp_enqueue_script( $script['handle'], $script['src'], $script['deps'], WPGMP_VERSION, $where );
					} else {
						wp_register_script( $script['handle'], $script['src'], $script['deps'], WPGMP_VERSION, $where );
					}
				}
			}

			$wpgmp_fjs_lang                     = array();
			$wpgmp_fjs_lang['ajax_url']         = admin_url( 'admin-ajax.php' );
			$wpgmp_fjs_lang['nonce']            = wp_create_nonce( 'fc-call-nonce' );
			if ( isset( $wpgmp_settings['wpgmp_days_to_remember'] ) ) {
				$wpgmp_fjs_lang['days_to_remember'] = $wpgmp_settings['wpgmp_days_to_remember'];
			} else {
				$wpgmp_fjs_lang['days_to_remember'] = '';
			}

			wp_localize_script( 'wpgmp-frontend', 'wpgmp_flocal', $wpgmp_fjs_lang );

			$wpgmp_local                              = array();
			$wpgmp_local['select_radius']             = esc_html__( 'Select Radius', 'wpgmp-google-map' );
			$wpgmp_local['search_placeholder']        = esc_html__( 'Enter address or latitude or longitude or title or city or state or country or postal code here...', 'wpgmp-google-map' );
			$wpgmp_local['select']                    = esc_html__( 'Select', 'wpgmp-google-map' );
			$wpgmp_local['select_all']                = esc_html__( 'Select All', 'wpgmp-google-map' );
			$wpgmp_local['select_category']           = esc_html__( 'Select Category', 'wpgmp-google-map' );
			$wpgmp_local['all_location']              = esc_html__( 'All', 'wpgmp-google-map' );
			$wpgmp_local['show_locations']            = esc_html__( 'Show Locations', 'wpgmp-google-map' );
			$wpgmp_local['sort_by']                   = esc_html__( 'Sort by', 'wpgmp-google-map' );
			$wpgmp_local['wpgmp_not_working']         = esc_html__( 'not working...', 'wpgmp-google-map' );
			$wpgmp_local['place_icon_url']            = WPGMP_ICONS;
			$wpgmp_local['wpgmp_location_no_results'] = esc_html__( 'No results found.', 'wpgmp-google-map' );
			$wpgmp_local['wpgmp_route_not_avilable']  = esc_html__( 'Route is not available for your requested route.', 'wpgmp-google-map' );
			$wpgmp_local['img_grid']                  = "<span class='span_grid'><a class='wpgmp_grid'><img src='" . WPGMP_IMAGES . "grid.png'></a></span>";
			$wpgmp_local['img_list']                  = "<span class='span_list'><a class='wpgmp_list'><img src='" . WPGMP_IMAGES . "list.png'></a></span>";
			$wpgmp_local['img_print']                 = "<span class='span_print'><a class='wpgmp_print' data-action='wpgmp-print'><img src='" . WPGMP_IMAGES . "print.png'></a></span>";
			$wpgmp_local['hide']                      = esc_html__( 'Hide', 'wpgmp-google-map' );
			$wpgmp_local['show']                      = esc_html__( 'Show', 'wpgmp-google-map' );
			$wpgmp_local['start_location']            = esc_html__( 'Start Location', 'wpgmp-google-map' );
			$wpgmp_local['start_point']               = esc_html__( 'Start Point', 'wpgmp-google-map' );
			$wpgmp_local['radius']                    = esc_html__( 'Radius', 'wpgmp-google-map' );
			$wpgmp_local['end_location']              = esc_html__( 'End Location', 'wpgmp-google-map' );
			$wpgmp_local['take_current_location']     = esc_html__( 'Take Current Location', 'wpgmp-google-map' );
			$wpgmp_local['center_location_message']   = esc_html__( 'Your Location', 'wpgmp-google-map' );
			$wpgmp_local['center_location_message']   = esc_html__( 'Your Location', 'wpgmp-google-map' );
			$wpgmp_local['driving']                   = esc_html__( 'Driving', 'wpgmp-google-map' );
			$wpgmp_local['bicycling']                 = esc_html__( 'Bicycling', 'wpgmp-google-map' );
			$wpgmp_local['walking']                   = esc_html__( 'Walking', 'wpgmp-google-map' );
			$wpgmp_local['transit']                   = esc_html__( 'Transit', 'wpgmp-google-map' );
			$wpgmp_local['metric']                    = esc_html__( 'Metric', 'wpgmp-google-map' );
			$wpgmp_local['imperial']                  = esc_html__( 'Imperial', 'wpgmp-google-map' );
			$wpgmp_local['find_direction']            = esc_html__( 'Find Direction', 'wpgmp-google-map' );
			$wpgmp_local['miles']                     = esc_html__( 'Miles', 'wpgmp-google-map' );
			$wpgmp_local['km']                        = esc_html__( 'KM', 'wpgmp-google-map' );
			$wpgmp_local['show_amenities']            = esc_html__( 'Show Amenities', 'wpgmp-google-map' );
			$wpgmp_local['find_location']             = esc_html__( 'Find Locations', 'wpgmp-google-map' );
			$wpgmp_local['locate_me']                 = esc_html__( 'Locate Me', 'wpgmp-google-map' );
			$wpgmp_local['prev']                      = esc_html__( 'Prev', 'wpgmp-google-map' );
			$wpgmp_local['next']                      = esc_html__( 'Next', 'wpgmp-google-map' );
			$wpgmp_local['ajax_url']                  = admin_url( 'admin-ajax.php' );
			$wpgmp_local['nonce']                     = wp_create_nonce( 'fc-call-nonce' );
			
			if ( isset( $wpgmp_settings['wpgmp_country_specific'] ) ) {
				$wpgmp_local['wpgmp_country_specific'] = ( $wpgmp_settings['wpgmp_country_specific'] == 'true' );
			} else {
				$wpgmp_local['wpgmp_country_specific'] = false;
			}

			if ( isset( $wpgmp_settings['wpgmp_countries'] ) ) {
				$wpgmp_local['wpgmp_countries'] = $wpgmp_settings['wpgmp_countries'];
			} else {
				$wpgmp_local['wpgmp_countries'] = false;
			}

			$wpgmp_local  = apply_filters( 'wpgmp_text_settings', $wpgmp_local );


			$scripts = array();

			if( isset( $wpgmp_settings['wpgmp_scripts_minify']) && $wpgmp_settings['wpgmp_scripts_minify'] == 'yes') {

				$scripts[] = array(
					'handle' => 'wpgmp-google-map-main',
					'src'    => WPGMP_JS . 'maps.min.js',
					'deps'   => array( 'wpgmp-google-api', 'jquery-masonry', 'imagesloaded' ),
				);	

			} else {

				$scripts[] = array(
				'handle' => 'wpgmp-markercluster',
				'src'    => WPGMP_JS . 'vendor/markerclustererplus/markerclustererplus.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-print',
				'src'    => WPGMP_JS . 'vendor/print/print.js',
				'deps'   => array(),
			);

			$scripts[] = array(
					'handle' => 'wpgmp-infobox',
					'src'    => WPGMP_JS . 'vendor/infobox/infobox.js',
					'deps'   => array(),
				);
			
			$scripts[] = array(
				'handle' => 'wpgmp-google-map-main',
				'src'    => WPGMP_JS . 'maps.js',
				'deps'   => array( 'wpgmp-google-api', 'jquery-masonry', 'imagesloaded', 'wpgmp-markercluster','wpgmp-print', 'wpgmp-infobox' ),
			);

			}

			

			if ( $scripts ) {
				foreach ( $scripts as $script ) {
					if ( $auto_fix == 'true' ) {
						wp_register_script( $script['handle'], $script['src'], $script['deps'], WPGMP_VERSION, $where );
					} else {
						wp_register_script( $script['handle'], $script['src'], $script['deps'], WPGMP_VERSION, $where );
					}
				}
			}

			wp_localize_script( 'wpgmp-google-map-main', 'wpgmp_local', $wpgmp_local );

			wp_enqueue_style( 'masonry' );

			if( isset( $wpgmp_settings['wpgmp_scripts_minify']) && $wpgmp_settings['wpgmp_scripts_minify'] == 'yes') {
							$frontend_styles = array(
								'wpgmp-frontend' => WPGMP_CSS . 'frontend.min.css',
							);
			} else {
							$frontend_styles = array(
							'wpgmp-frontend' => WPGMP_CSS . 'frontend.css',
						);	
			}	
			

			if ( $frontend_styles ) {
				foreach ( $frontend_styles as $frontend_style_key => $frontend_style_value ) {
					wp_register_style( $frontend_style_key, $frontend_style_value );
				}
			}

		}
		/**
		 * Display map at the frontend using put_wpgmp shortcode.
		 *
		 * @param  array  $atts   Map Options.
		 * @param  string $content Content.
		 */
		function wpgmp_show_location_in_map( $atts, $content = null ) {

			try {
				$factoryObject = new WPGMP_Controller();
				$viewObject    = $factoryObject->create_object( 'shortcode' );
				$output        = $viewObject->display( 'put-wpgmp', $atts );
				 return $output;

			} catch ( Exception $e ) {
				echo WPGMP_Template::show_message( array( 'error' => $e->getMessage() ) );

			}

		}
		/**
		 * Display map at the frontend using display_map shortcode.
		 *
		 * @param  array $atts    Map Options.
		 */
		function wpgmp_display_map( $atts ) {

			try {
				$factoryObject = new WPGMP_Controller();
				$viewObject    = $factoryObject->create_object( 'shortcode' );
				 $output       = $viewObject->display( 'display-map', $atts );
				 return $output;

			} catch ( Exception $e ) {
				echo WPGMP_Template::show_message( array( 'error' => $e->getMessage() ) );

			}

		}
		/**
		 * Ajax Call
		 */
		function wpgmp_ajax_call() {

			check_ajax_referer( 'fc-call-nonce', 'nonce' );
			$operation = sanitize_text_field( wp_unslash( $_POST['operation'] ) );
			$value     = wp_unslash( $_POST );
			if ( isset( $operation ) ) {
				$this->$operation( $value );
			}
			exit;
		}

		/**
		 * Process slug and display view in the backend.
		 */
		function wpgmp_processor() {

			$return = '';
			if ( isset( $_GET['page'] ) ) {
				$page = sanitize_text_field( wp_unslash( $_GET['page'] ) );
			} else {
				$page = 'wpgmp_view_overview';
			}

			$pageData      = explode( '_', $page );
			$obj_type      = $pageData[2];
			$obj_operation = $pageData[1];

			if ( count( $pageData ) < 3 ) {
				die( 'Cheating!' );
			}

			try {
				if ( count( $pageData ) > 3 ) {
					$obj_type = $pageData[2] . '_' . $pageData[3];
				}

				$factoryObject = new WPGMP_Controller();
				$viewObject    = $factoryObject->create_object( $obj_type );
				$viewObject->display( $obj_operation );

			} catch ( Exception $e ) {
				echo WPGMP_Template::show_message( array( 'error' => $e->getMessage() ) );

			}

		}
		/**
		 * Create backend navigation.
		 */
		function wpgmp_create_menu() {

			global $navigations;

			$pagehook1 = add_menu_page(
				esc_html__( 'WP Google Map Pro', 'wpgmp-google-map' ),
				esc_html__( 'WP Google Map Pro', 'wpgmp-google-map' ),
				'wpgmp_admin_overview',
				WPGMP_SLUG,
				array( $this, 'wpgmp_processor' ),
				WPGMP_IMAGES . '/flippercode.png'
			);

			if ( current_user_can( 'manage_options' ) ) {
								$role = get_role( 'administrator' );
								$role->add_cap( 'wpgmp_admin_overview' );
			}

			$this->wpgmp_load_modules_menu();

			add_action( 'load-' . $pagehook1, array( $this, 'wpgmp_backend_scripts' ) );

		}
		/**
		 * Read models and create backend navigation.
		 */
		function wpgmp_load_modules_menu() {

			$modules   = $this->modules;
			$pagehooks = array();
			if ( is_array( $modules ) ) {
				foreach ( $modules as $module ) {

						$object = new $module();

					if ( method_exists( $object, 'navigation' ) ) {

						if ( ! is_array( $object->navigation() ) ) {
							continue;
						}

						foreach ( $object->navigation() as $nav => $title ) {

							if ( current_user_can( 'manage_options' ) && is_admin() ) {
								$role = get_role( 'administrator' );
								$role->add_cap( $nav );

							}

							$pagehooks[] = add_submenu_page(
								WPGMP_SLUG,
								$title,
								$title,
								$nav,
								$nav,
								array( $this, 'wpgmp_processor' )
							);

						}
					}
				}
			}

			if ( is_array( $pagehooks ) ) {

				foreach ( $pagehooks as $key => $pagehook ) {
					add_action( 'load-' . $pagehooks[ $key ], array( $this, 'wpgmp_backend_scripts' ) );
				}
			}

		}
		/**
		 * Eneque scripts in the backend.
		 */
		function wpgmp_backend_scripts() {

			global $pagehook3, $pagehook6, $pagehook9, $pagehook11;

			$wpgmp_settings = get_option( 'wpgmp_settings', true );

			if ( isset( $_SERVER['HTTPS'] ) && ( 'on' == $_SERVER['HTTPS'] || 1 == $_SERVER['HTTPS'] ) || isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
					$wpgmp_apilocation = 'https';
			} else {
				$wpgmp_apilocation = 'http';
			}

			if ( $wpgmp_settings['wpgmp_api_key'] != '' ) {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?key=' . $wpgmp_settings['wpgmp_api_key'] . '&libraries=geometry,places,weather,panoramio,drawing&language=en';
			} else {
				$wpgmp_apilocation .= '://maps.google.com/maps/api/js?libraries=geometry,places,weather,panoramio,drawing&language=en';
			}

			wp_enqueue_style( 'thickbox' );
			wp_enqueue_style( 'wp-color-picker' );
			$wp_scripts = array( 'jQuery', 'thickbox', 'wp-color-picker', 'jquery-ui-datepicker', 'jquery-ui-sortable' );

			if ( $wp_scripts ) {
				foreach ( $wp_scripts as $wp_script ) {
					wp_enqueue_script( $wp_script );
				}
			}

			$scripts = array();

			$scripts[] = array(
				'handle' => 'flippercode-datatable',
				'src'    => WPGMP_JS . 'vendor/datatables/datatables.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'flippercode-webfont',
				'src'    => WPGMP_JS . 'vendor/webfont/webfont.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'flippercode-select2',
				'src'    => WPGMP_JS . 'vendor/select2/select2.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'flippercode-slick',
				'src'    => WPGMP_JS . 'vendor/slick/slick.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-backend-google-maps',
				'src'    => WPGMP_JS . 'backend.js',
				'deps'   => array("flippercode-datatable","flippercode-webfont"),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-backend-google-api',
				'src'    => $wpgmp_apilocation,
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-markercluster',
				'src'    => WPGMP_JS . 'vendor/markerclustererplus/markerclustererplus.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-infobox',
				'src'    => WPGMP_JS . 'vendor/infobox/infobox.js',
				'deps'   => array(),
			);

			$scripts[] = array(
				'handle' => 'wpgmp-accordion',
				'src'    => WPGMP_JS . 'vendor/accordion/accordion.js',
				'deps'   => array(),
				);

			$scripts[] = array(
				'handle' => 'wpgmp-map',
				'src'    => WPGMP_JS . 'maps.js',
				'deps'   => array("wpgmp-markercluster","wpgmp-infobox","wpgmp-accordion"),
			);

			$scripts[] = array(
				'handle' => 'flippercode-ui',
				'src'    => WPGMP_JS . 'flippercode-ui.js',
				'deps'   => array(),
			);


			if ( $scripts ) {
				foreach ( $scripts as $script ) {
					wp_enqueue_script( $script['handle'], $script['src'], $script['deps'], '2.3.4' );
				}
			}

			$wpgmp_js_lang                    = array();
			$wpgmp_js_lang['ajax_url']        = admin_url( 'admin-ajax.php' );
			$wpgmp_js_lang['nonce']           = wp_create_nonce( 'fc-call-nonce' );
			$wpgmp_js_lang['confirm']         = esc_html__( 'Are you sure to delete item?', 'wpgmp-google-map' );
			$wpgmp_js_lang['text_editable']   = array( '.fc-text', '.fc-post-link', '.place_title', '.fc-item-content', '.wpgmp_locations_content' );
			$wpgmp_js_lang['bg_editable']     = array( '.fc-bg', '.fc-item-box', '.fc-pagination', '.wpgmp_locations' );
			$wpgmp_js_lang['margin_editable'] = array( '.fc-margin', '.fc-item-title', '.wpgmp_locations_head', '.fc-item-content', '.fc-item-meta' );
			$wpgmp_js_lang['full_editable']   = array( '.fc-css', '.fc-item-title', '.wpgmp_locations_head', '.fc-readmore-link', '.fc-item-meta', 'a.page-numbers', '.current', '.wpgmp_location_meta' );
			$wpgmp_js_lang['image_path']      = WPGMP_IMAGES;

			$wpgmp_js_lang['geocode_stats']   = esc_html__( 'locations geocoded', 'wpgmp-google-map' );
			$wpgmp_js_lang['geocode_success'] = esc_html__( 'Click below to save geocoded locations', 'wpgmp-google-map' );

			wp_localize_script( 'flippercode-ui', 'settings_obj', $wpgmp_js_lang );

			$wpgmp_local               = array();
			$wpgmp_local['language']   = 'en';
			$wpgmp_local['urlforajax'] = admin_url( 'admin-ajax.php' );
			$wpgmp_local['hide']       = esc_html__( 'Hide', 'wpgmp-google-map' );
			$wpgmp_local['nonce']      = wp_create_nonce( 'fc_communication' );

			if ( isset( $wpgmp_settings['wpgmp_country_specific'] ) ) {
				$wpgmp_local['wpgmp_country_specific'] = ( $wpgmp_settings['wpgmp_country_specific'] == 'true' );
			} else {
				$wpgmp_local['wpgmp_country_specific'] = false;
			}

			if ( isset( $wpgmp_settings['wpgmp_countries'] ) ) {
				$wpgmp_local['wpgmp_countries'] = $wpgmp_settings['wpgmp_countries'];
			} else {
				$wpgmp_local['wpgmp_countries'] = false;
			}

			wp_localize_script( 'wpgmp-map', 'wpgmp_local', $wpgmp_local );
			wp_localize_script( 'flippercode-ui', 'wpgmp_local', $wpgmp_local );

			$wpgmp_js_lang            = array();
			$wpgmp_js_lang['confirm'] = esc_html__( 'Are you sure to delete item?', 'wpgmp-google-map' );
			wp_localize_script( 'wpgmp-backend-google-maps', 'wpgmp_js_lang', $wpgmp_js_lang );
			$admin_styles = array(
				'font_awesome_minimised'   => WPGMP_CSS . 'font-awesome.min.css',
				'wpgmp-map-bootstrap'      => WPGMP_CSS . 'flippercode-ui.css',
				'wpgmp-backend-google-map' => WPGMP_CSS . 'backend.css',
			);

			if ( $admin_styles ) {
				foreach ( $admin_styles as $admin_style_key => $admin_style_value ) {
					wp_enqueue_style( $admin_style_key, $admin_style_value );
				}
			}
		}
		/**
		 * Metabox stylesheet.
		 */
		function wpgmp_backend_styles() {

			wp_enqueue_style( 'wpgmp-backend-metabox', WPGMP_CSS . 'wpgmp-metabox-css.css' );
		}
		/**
		 * Load plugin language file.
		 */
		function wpgmp_load_plugin_languages() {

			$this->modules = apply_filters( 'wpgmp_extensions', $this->modules );
			load_plugin_textdomain( 'wpgmp-google-map', false, WPGMP_FOLDER . '/lang/' );
		}
		/**
		 * Call hook on plugin activation for both multi-site and single-site.
		 */
		function wpgmp_plugin_activation( $network_wide ) {

			if ( is_multisite() && $network_wide ) {
				global $wpdb;
				$currentblog = $wpdb->blogid;
				$activated   = array();
				$sql         = "SELECT blog_id FROM {$wpdb->blogs}";
				$blog_ids    = $wpdb->get_col( $wpdb->prepare( $sql, null ) );

				foreach ( $blog_ids as $blog_id ) {
					switch_to_blog( $blog_id );
					$this->wpgmp_activation();
					$activated[] = $blog_id;
				}

				switch_to_blog( $currentblog );
				update_site_option( 'op_activated', $activated );

			} else {
				$this->wpgmp_activation();
			}
		}
		/**
		 * Call hook on plugin deactivation for both multi-site and single-site.
		 */
		function wpgmp_plugin_deactivation() {

			if ( is_multisite() && $network_wide ) {
				global $wpdb;
				$currentblog = $wpdb->blogid;
				$activated   = array();
				$sql         = "SELECT blog_id FROM {$wpdb->blogs}";
				$blog_ids    = $wpdb->get_col( $wpdb->prepare( $sql, null ) );

				foreach ( $blog_ids as $blog_id ) {
					switch_to_blog( $blog_id );
					$this->wpgmp_deactivation();
					$activated[] = $blog_id;
				}

				switch_to_blog( $currentblog );
				update_site_option( 'op_activated', $activated );

			} else {
				$this->wpgmp_deactivation();
			}
		}

		/**
		 * Perform tasks on new blog create and table install.
		 */

		function wpgmp_on_blog_new_generate( $blog_id, $user_id, $domain, $path, $site_id, $meta ) {

			if ( is_plugin_active_for_network( plugin_basename( __FILE__ ) ) ) {
				switch_to_blog( $blog_id );
				$this->wpgmp_activation();
				restore_current_blog();
			}

		}

		/**
		 * Perform tasks on when blog deleted and remove plugin tables.
		 */

		function wpgmp_on_blog_delete( $tables ) {
			global $wpdb;
			$tables[] = str_replace( $wpdb->base_prefix, $wpdb->prefix, TBL_LOCATION );
			$tables[] = str_replace( $wpdb->base_prefix, $wpdb->prefix, TBL_GROUPMAP );
			$tables[] = str_replace( $wpdb->base_prefix, $wpdb->prefix, TBL_MAP );
			$tables[] = str_replace( $wpdb->base_prefix, $wpdb->prefix, TBL_ROUTES );
			return $tables;
		}
		/**
		 * Create choose icon tab in media manager.
		 *
		 * @param  array $tabs Current Tabs.
		 * @return array       New Tabs.
		 */
		function wpgmp_google_map_tabs_filter( $tabs ) {

			$newtab = array( 'ell_insert_gmap_tab' => esc_html__( 'Choose Icons', 'wpgmp-google-map' ) );
			return array_merge( $tabs, $newtab );
		}
		/**
		 * Intialize wp_iframe for icons tab
		 *
		 * @return [type] [description]
		 */
		function wpgmp_google_map_media_upload_tab() {

			return wp_iframe( array( $this, 'media_wpgmp_google_map_icon' ), array() );
		}
		/**
		 * Read images/icons folder.
		 */
		function media_wpgmp_google_map_icon() {

			wp_enqueue_style( 'media' );
			media_upload_header();
			$form_action_url = site_url( "wp-admin/media-upload.php?type={$GLOBALS['type']}&tab=ell_insert_gmap_tab", 'admin' );
			?>

		<style type="text/css">
		#select_icons .read_icons {
		width: 32px;
		height: 32px;ß
		}
		#select_icons .active img {
		border: 3px solid #000;
		width: 26px;
		}
		</style>

		<script type="text/javascript">

		jQuery(document).ready(function($) {

		$(".read_icons").click(function () {

		$(".read_icons").removeClass('active');
		$(this).addClass('active');
		});

		$('input[name="wpgmp_search_icon"]').keyup(function() {
		if($(this).val() == '')
		$('.read_icons').show();
		else {
		$('.read_icons').hide();
		$('img[title^="' + $(this).val() + '"]').parent().show();
		}

		});

		});

		function wpgmp_add_icon_to_images(target) {

		if(jQuery('.read_icons').hasClass('active'))
		{
		imgsrc = jQuery('.active').find('img').attr('src');
		var win = window.dialogArguments || opener || parent || top;
		win.send_icon_to_map(imgsrc,target);
		}
		else
		{
		alert('<?php esc_html_e( 'Choose marker icon', 'wpgmp-google-map' ); ?>');
		}
		}
		</script>

		<form enctype="multipart/form-data" method="post" action="<?php echo esc_attr( $form_action_url ); ?>" class="media-upload-form" id="library-form">
	<h3 class="media-title" style="color: #5A5A5A; font-family: Georgia, 'Times New Roman', Times, serif; font-weight: normal; font-size: 1.6em; margin-left: 10px;"><?php esc_html_e( 'Choose icon', 'wpgmp-google-map' ); ?> 	<input name="wpgmp_search_icon" id="wpgmp_search_icon" type='text' value="" placeholder="<?php esc_html_e( 'Search icons', 'wpgmp-google-map' ); ?>" />
</h3>
	<div style="margin-bottom:20px; float:left; width:100%;">
	<ul style="float:left; width:100%;" id="select_icons">
			<?php
			$dir          = WPGMP_ICONS_DIR;
			$file_display = array( 'jpg', 'jpeg', 'png', 'gif' );

			if ( file_exists( $dir ) == false ) {
				echo 'Directory \'', $dir, '\' not found!';

			} else {
				$dir_contents = scandir( $dir );
				foreach ( $dir_contents as $file ) {
					$image_data = explode( '.', $file );
					$file_type  = strtolower( end( $image_data ) );
					if ( '.' !== $file && '..' !== $file && true == in_array( $file_type, $file_display ) ) {
						?>
			<li class="read_icons" style="float:left;">
			<img alt="<?php echo esc_attr( $image_data[0] ); ?>" title="<?php echo esc_attr( $image_data[0] ); ?>" src="<?php echo esc_url( WPGMP_ICONS . $file ); ?>" style="cursor:pointer;" />
		</li>
						<?php
					}
				}
			}

			if ( isset( $_GET['target'] ) ) {
				$target = esc_js( $_GET['target'] );
			} else {
				$target = '';
			}

			?>
		</ul>
		<button type="button" class="button" style="margin-left:10px;" value="1" onclick="wpgmp_add_icon_to_images('<?php echo esc_attr( $target ); ?>');" name="send[<?php echo esc_attr( $picid ); ?>]"><?php esc_html_e( 'Insert into Post', 'wpgmp-google-map' ); ?></button>
	</div>
	</form>
			<?php
		}
		/**
		 * Perform tasks on plugin deactivation.
		 */
		function wpgmp_deactivation() {

		}

		/**
		 * Perform tasks on plugin deactivation.
		 */
		function wpgmp_activation() {

			global $wpdb;

			// migrate options data from previous version.
			if ( ! get_option( 'wpgmp_settings' ) and get_option( 'wpgmp_language' ) ) {
				$wpgmp_settings['wpgmp_language']      = get_option( 'wpgmp_language', 'en' );
				$wpgmp_settings['wpgmp_api_key']       = get_option( 'wpgmp_api_key', '' );
				$wpgmp_settings['wpgmp_scripts_place'] = get_option( 'wpgmp_scripts_place', true );
				$wpgmp_settings['wpgmp_allow_meta']    = get_option( 'wpgmp_allow_meta', true );
				$wpgmp_settings['wpgmp_scripts_minify']    = get_option( 'wpgmp_scripts_minify', true );
				update_option( 'wpgmp_settings', $wpgmp_settings );
			}


			require_once ABSPATH . 'wp-admin/includes/upgrade.php';

			$modules   = $this->modules;
			$pagehooks = array();
			if ( is_array( $modules ) ) {
				foreach ( $modules as $module ) {
					$object = new $module();
					if ( method_exists( $object, 'install' ) ) {
								$tables[] = $object->install();
					}
				}
			}

			if ( is_array( $tables ) ) {
				foreach ( $tables as $i => $sql ) {
					dbDelta( $sql );
				}
			}

		}
		/**
		 * Define all constants.
		 */
		private function wpgmp_define_constants() {

			global $wpdb;

			if ( ! defined( 'ALLOW_UNFILTERED_UPLOADS' ) && is_admin() ) {
				define( 'ALLOW_UNFILTERED_UPLOADS', true );
			}

			if ( ! defined( 'WPGMP_SLUG' ) ) {
				define( 'WPGMP_SLUG', 'wpgmp_view_overview' );
			}

			if ( ! defined( 'WPGMP_VERSION' ) ) {
				define( 'WPGMP_VERSION', '5.1.9' );
			}

			if ( ! defined( 'WPGMP_FOLDER' ) ) {
				define( 'WPGMP_FOLDER', basename( dirname( __FILE__ ) ) );
			}

			if ( ! defined( 'WPGMP_DIR' ) ) {
				define( 'WPGMP_DIR', plugin_dir_path( __FILE__ ) );
			}

			if ( ! defined( 'WPGMP_ICONS_DIR' ) ) {
				define( 'WPGMP_ICONS_DIR', WPGMP_DIR . 'assets/images/icons/' );
			}

			if ( ! defined( 'WPGMP_CORE_CLASSES' ) ) {
				define( 'WPGMP_CORE_CLASSES', WPGMP_DIR . 'core/' );
			}

			if ( ! defined( 'WPGMP_PLUGIN_CLASSES' ) ) {
				define( 'WPGMP_PLUGIN_CLASSES', WPGMP_DIR . 'classes/' );
			}

			if ( ! defined( 'WPGMP_TEMPLATES' ) ) {
				define( 'WPGMP_TEMPLATES', WPGMP_DIR . 'templates/' );
			}

			if ( ! defined( 'WPGMP_MODEL' ) ) {
				define( 'WPGMP_MODEL', WPGMP_DIR . 'modules/' );
			}

			if ( ! defined( 'WPGMP_CONTROLLER' ) ) {
				define( 'WPGMP_CONTROLLER', WPGMP_CORE_CLASSES );
			}

			if ( ! defined( 'WPGMP_CORE_CONTROLLER_CLASS' ) ) {
				define( 'WPGMP_CORE_CONTROLLER_CLASS', WPGMP_CORE_CLASSES . 'class.controller.php' );
			}

			if ( ! defined( 'WPGMP_MODEL' ) ) {
				define( 'WPGMP_MODEL', WPGMP_DIR . 'modules/' );
			}

			if ( ! defined( 'WPGMP_URL' ) ) {
				define( 'WPGMP_URL', get_template_directory_uri() . '/vendor/' . WPGMP_FOLDER . '/' );
			}

			if ( ! defined( 'WPGMP_TEMPLATES_URL' ) ) {
				define( 'WPGMP_TEMPLATES_URL', WPGMP_URL . 'templates/' );
			}

			if ( ! defined( 'FC_CORE_URL' ) ) {
				define( 'FC_CORE_URL', get_template_directory_uri() . '/vendor/' . WPGMP_FOLDER . '/core/' );
			}

			if ( ! defined( 'WPGMP_INC_URL' ) ) {
				define( 'WPGMP_INC_URL', WPGMP_URL . 'includes/' );
			}

			if ( ! defined( 'WPGMP_CSS' ) ) {
				define( 'WPGMP_CSS', WPGMP_URL . 'assets/css/' );
			}

			if ( ! defined( 'WPGMP_JS' ) ) {
				define( 'WPGMP_JS', WPGMP_URL . 'assets/js/' );
			}

			if ( ! defined( 'WPGMP_IMAGES' ) ) {
				define( 'WPGMP_IMAGES', WPGMP_URL . 'assets/images/' );
			}

			if ( ! defined( 'WPGMP_FONTS' ) ) {
				define( 'WPGMP_FONTS', WPGMP_URL . 'fonts/' );
			}

			if ( ! defined( 'WPGMP_ICONS' ) ) {
				define( 'WPGMP_ICONS', WPGMP_URL . 'assets/images/icons/' );
			}
			$upload_dir = wp_upload_dir();

			if ( ! defined( 'TBL_LOCATION' ) ) {
				define( 'TBL_LOCATION', $wpdb->prefix . 'map_locations' );
			}

			if ( ! defined( 'TBL_GROUPMAP' ) ) {
				define( 'TBL_GROUPMAP', $wpdb->prefix . 'group_map' );
			}

			if ( ! defined( 'TBL_MAP' ) ) {
				define( 'TBL_MAP', $wpdb->prefix . 'create_map' );
			}

			if ( ! defined( 'TBL_ROUTES' ) ) {
				define( 'TBL_ROUTES', $wpdb->prefix . 'map_routes' );
			}

		}
		/**
		 * Load all required core classes.
		 */
		private function wpgmp_load_files() {

			$coreInitialisationFile = plugin_dir_path( __FILE__ ) . 'core/class.initiate-core.php';
			if ( file_exists( $coreInitialisationFile ) ) {
				require_once $coreInitialisationFile;
			}

			// Load Plugin Files
			$plugin_files_to_include = array(
				'wpgmp-template.php',
				'wpgmp-controller.php',
				'wpgmp-model.php',
				'wpgmp-map-widget.php',
			);
			foreach ( $plugin_files_to_include as $file ) {

				if ( file_exists( WPGMP_PLUGIN_CLASSES . $file ) ) {
					require_once WPGMP_PLUGIN_CLASSES . $file;
				}
			}
			// Load all modules.
			$core_modules = array( 'overview', 'location', 'map', 'group_map', 'drawing', 'route', 'permissions', 'settings', 'tools' );
			if ( is_array( $core_modules ) ) {
				foreach ( $core_modules as $module ) {

					$file = WPGMP_MODEL . $module . '/model.' . $module . '.php';

					if ( file_exists( $file ) ) {
						include_once $file;
						$class_name = 'WPGMP_Model_' . ucwords( $module );
						array_push( $this->modules, $class_name );
					}
				}
			}

		}
	}
}

new WPGMP_Google_Maps_Pro();
