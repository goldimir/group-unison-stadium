<?php

define( 'YSPL_WIN_VERSION', '2.0.0' );


$attr_prefix = apply_filters( 'yspl_win_attribute_prefix', 'yspl_win' );
/**
 *
 * A string prefix for internal names and ids 
 */
define( 'YSPL_WIN_PREFIX', $attr_prefix );

/**
 * Plugin's file path
 */
define( 'YSPL_WIN_PATH', get_template_directory() . '/vendor/wp-widget-in-navigation/' );

/**
 * Plugin's url path
 */
define( 'YSPL_WIN_URL', get_template_directory_uri() . '/vendor/wp-widget-in-navigation/');

/**
 * Plugin's Slug
 */
define( 'YSPL_WIN_SLUG', 'widget_in_nav' );

/**
 * Include main plugin File
 */
include_once YSPL_WIN_PATH . 'init/main-yspl-win.php';

/**
 * Include menu plugin File
 */
include_once YSPL_WIN_PATH . 'init/menu-yspl-win.php';
/**
 * Object of main class
 */
$yspl_win_main = new YSPL_WIN_MAIN();
$yspl_win_main->yspl_init();
/**
 * Object of menu class
 */
$yspl_win_menu = new YSPL_WIN_MENU();
$yspl_win_menu->yspl_init();