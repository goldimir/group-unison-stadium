<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Menu_jQuery
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function menu_jquery_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'menu_jquery_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function menu_jquery_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'menu_jquery_pingback_header' );


/**
 * Broker Form
 */
class brokerForm {

	/**
	 * Class constructor
	 */
	public function __construct() {

		$this->define_hooks();

	}

	public function ajax_broker_form(){

		global $wpdb;
		$full_name = $_POST['name'];
		$email = $_POST['email'];
		$comments = $_POST['comment'];
		$firstUrl = $_POST['firstUrl'];
		$secondUrl = $_POST['secondUrl'];
		$lastUrl = $_POST['lastUrl'];
		$broker_id = (int)$_POST['id'];
		$broker_email = $wpdb->get_results( $wpdb->prepare("SELECT location_email FROM wp_map_locations WHERE location_id = %d", $broker_id ));

		// Send an email
		$to = $broker_email[0]->location_email;

		$subject = 'Website Change Reqest';

		$headers = "From: " . $email . "\r\n";
		$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message = '<html><body>';
		$message .= '<table rules="all" style="border: 1px solid #7d7d7d;" cellpadding="7">';
		$message .= "<tr style='background: #eee;'><td colspan='2'><h2 style='margin: 0;'>Customer</h2></td></tr>";
		$message .= "<tr><td><strong>Name:</strong> </td><td>" . strip_tags($full_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Comments:</strong> </td><td>" . $comments . "</td></tr>";
		$message .= "<tr style='background: #eee;'><td colspan='2'><h2 style='margin: 0;'>Visited links</h2></td></tr>";
		$message .= "<tr><td><strong>First Url:</strong> </td><td>" . $firstUrl . "</td></tr>";
		$message .= "<tr><td><strong>Second Url:</strong> </td><td>" . $secondUrl . "</td></tr>";
		$message .= "<tr><td><strong>Last Url:</strong> </td><td>" . $lastUrl . "</td></tr>";
		$message .= "</table>";
		$message .= "</body></html>";

		wp_mail($to, $subject, $message, $headers);

	}

	/**
	 * Display form
	 */
	public function display_form() {

		$full_name   = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
		$email       = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_STRING | FILTER_SANITIZE_EMAIL );
		$comments    = filter_input( INPUT_POST, 'comments', FILTER_SANITIZE_STRING );

		// Default empty array

		$output = '';

		$output .= '<form method="post" id="popup-broker-form">';
		$output .= '    <div class="form-group">';
		$output .= '        ' . $this->display_text( 'full_name', 'Name', $full_name );
		$output .= '    </div>';
		$output .= '    <div class="form-group">';
		$output .= '        ' . $this->display_email( 'email', 'Email', $email );
		$output .= '    </div>';
		$output .= '    <div class="form-group">';
		$output .= '        ' . $this->display_textarea( 'comments', 'Comments', $comments );
		$output .= '    </div>';
		$output .= '    <div class="form-group">';
		$output .= '        <button class="btn btn-primary" type="submit" name="submit"><span class="spinner-border" role="status" aria-hidden="true"></span></button>';
		$output .= '    </div>';
		$output .= '</form>';

		return $output;
	}

	/**
	 * Display text field
	 */
	private function display_text( $name, $label, $value = '' ) {

		$output = '';

		$output .= '<input type="text" name="' . esc_attr( $name ) . '" placeholder="' . esc_attr( $label ) . '" value="' . esc_attr( $value ) . '">';

		return $output;
	}

	/**
	 * Display email field
	 */
	private function display_email( $name, $label, $value = '' ) {

		$output = '';

		$output .= '<input type="email" name="' . esc_attr( $name ) . '" placeholder="' . esc_attr( $label ) . '" value="' . esc_attr( $value ) . '">';

		return $output;
	}

	/**
	 * Display textarea field
	 */
	private function display_textarea( $name, $label, $value = '' ) {

		$output = '';

		$output .= '<textarea name="' . esc_attr( $name ) . '" placeholder="' . esc_attr( $label ) . '">' . esc_html( $value ) . '</textarea>';

		return $output;
	}

	/**
	 * Display radios field
	 */
	private function display_radios( $name, $label, $options, $value = null ) {

		$output = '';

		$output .= '<label>' . esc_html__( $label, 'wpse_299521' ) . '</label>';

		foreach ( $options as $option_value => $option_label ):
			$output .= $this->display_radio( $name, $option_label, $option_value, $value );
		endforeach;

		return $output;
	}

	/**
	 * Display single checkbox field
	 */
	private function display_radio( $name, $label, $option_value, $value = null ) {

		$output = '';

		$checked = ( $option_value === $value ) ? ' checked' : '';

		$output .= '<label>';
		$output .= '    <input type="radio" name="' . esc_attr( $name ) . '" value="' . esc_attr( $option_value ) . '"' . esc_attr( $checked ) . '>';
		$output .= '    ' . esc_html__( $label, 'wpse_299521' );
		$output .= '</label>';

		return $output;
	}

	/**
	 * Display checkboxes field
	 */
	private function display_checkboxes( $name, $label, $options, $values = array() ) {

		$output = '';

		$name .= '[]';

		$output .= '<label>' . esc_html__( $label, 'wpse_299521' ) . '</label>';

		foreach ( $options as $option_value => $option_label ):
			$output .= $this->display_checkbox( $name, $option_label, $option_value, $values );
		endforeach;

		return $output;
	}

	/**
	 * Display single checkbox field
	 */
	private function display_checkbox( $name, $label, $available_value, $values = array() ) {

		$output = '';

		$checked = ( in_array($available_value, $values) ) ? ' checked' : '';

		$output .= '<label>';
		$output .= '    <input type="checkbox" name="' . esc_attr( $name ) . '" value="' . esc_attr( $available_value ) . '"' . esc_attr( $checked ) . '>';
		$output .= '    ' . esc_html__( $label, 'wpse_299521' );
		$output .= '</label>';

		return $output;
	}

	/**
	 * Get available colors
	 */
	private function get_available_colors() {

		return array(
			'red' => 'Red',
			'blue' => 'Blue',
			'green' => 'Green',
		);
	}

	/**
	 * Get available accessories
	 */
	private function get_available_accessories() {

		return array(
			'case' => 'Case',
			'tempered_glass' => 'Tempered glass',
			'headphones' => 'Headphones',
		);
	}

	/**
	 * Define hooks related to plugin
	 */
	private function define_hooks() {

		/**
		 * Add action to send email
		 */
		add_action( 'wp_ajax_nopriv_ajax_broker_form', array( $this, 'ajax_broker_form' ) );
		add_action( 'wp_ajax_ajax_broker_form', array( $this, 'ajax_broker_form' ) );

		/**
		 * Add shortcode to display form
		 */
		add_shortcode( 'broker-contact', array( $this, 'display_form' ) );
	}
}

new brokerForm();