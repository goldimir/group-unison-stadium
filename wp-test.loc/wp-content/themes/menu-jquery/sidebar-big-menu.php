<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Menu_jQuery
 */

if ( ! is_active_sidebar( 'big-menu' ) ) {
	return;
}
?>

<ul id="big-menu" class="widget-area">
	<?php dynamic_sidebar( 'big-menu' ); ?>
</ul><!-- #secondary -->
