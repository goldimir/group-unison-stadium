<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-test-loc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y!iHQ!OKYG&&^nGNa~ZXd3qsY8`]$%<d2DpLbFefI{/t[uZWU|:hs*M]2*j#!l9c');
define('SECURE_AUTH_KEY',  'tdbdIV>^ 4O3RHUdxOg|[/t~iAUC<?z~f~0@q:8+QYo/QxhfQ.9E|wLTb g!E#T#');
define('LOGGED_IN_KEY',    '=62s$olNIIFfg@FdosKn#z*iA#&gUhR#S$-b=]F1h%R#}1etD>jY<<DT4ts<jR9)');
define('NONCE_KEY',        '(5 V0$DCmuA%d&U+IV*0_:$;kDFqKM]dvlv)u5-xQ 0faE1RH*drXuLA|D)Jy]Rp');
define('AUTH_SALT',        '1Y1-SP:Q8|k.,KlP=f2 ]w_M1R:Fx+X!KXL!JgOQLv.(UgBjeV6ZL:G:Hl#R1Q1h');
define('SECURE_AUTH_SALT', 'H/Ks{mVa-=`e@{0Na6~?!7F!*/8r,_.SDLjVkKU511yd(XZp_q6cetW=]u9Co/X[');
define('LOGGED_IN_SALT',   '#E<)DB{_y|N](8z(/e/tc4}yp1QJUF|Skz8XF^%m7,+(KyN<X}Iqz`8P5D!A%}-X');
define('NONCE_SALT',       '|j:H3pwwD}>ODx98nvmuMXD%C?I$](M{0A;bEj?ZQR2tAGt%al0>Kz=[IikuSa@i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
