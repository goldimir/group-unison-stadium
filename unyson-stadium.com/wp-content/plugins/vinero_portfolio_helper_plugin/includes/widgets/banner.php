<?php


class vlthemes_widget_banner extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname' => 'vlt-widget-banner',
			'description' => esc_html__( 'Display Banner.', 'vlthemes' )
		);
		parent::__construct( 'vlthemes_widget_banner', esc_html__( 'VLThemes: Banner', 'vlthemes' ), $widget_details );

	}

  public function widget( $args, $instance ) {

		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = !empty( $instance['title'] ) ? $instance['title'] : '';

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];

		$widget_id = $args['widget_id'];

		$banner_image = get_field( 'banner_image', 'widget_' . $widget_id );
		$banner_link = get_field( 'banner_link', 'widget_' . $widget_id );
		$banner_target = get_field( 'banner_target', 'widget_' . $widget_id );


		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		?>

		<a href="<?php echo esc_url( $banner_link ); ?>" target="<?php echo esc_html( $banner_target );?>">
			<?php
				if ( $banner_image ) {
					echo wp_get_attachment_image( $banner_image, 'vinero-full' );
				}
			?>
		</a>


		<?php

		echo $args['after_widget'];

	}

	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';

	?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'vlthemes' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

	<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}
}

