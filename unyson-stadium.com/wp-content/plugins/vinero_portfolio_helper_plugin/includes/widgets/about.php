<?php


class vlthemes_widget_about extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname' => 'vlt-widget-about',
			'description' => esc_html__( 'Display Image, Text and Link.', 'vlthemes' )
		);
		parent::__construct( 'vlthemes_widget_about', esc_html__( 'VLThemes: About', 'vlthemes' ), $widget_details );

	}

  public function widget( $args, $instance ) {

		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = !empty( $instance['title'] ) ? $instance['title'] : '';

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];

		$widget_id = $args['widget_id'];

		$about_image = get_field( 'about_image', 'widget_' . $widget_id );
		$about_text = get_field( 'about_text', 'widget_' . $widget_id );
		$about_title = get_field( 'about_title', 'widget_' . $widget_id );
		$about_image_signature = get_field( 'about_image_signature', 'widget_' . $widget_id );

		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		if ( $about_image ) {
			echo wp_get_attachment_image( $about_image, 'vinero-full' );
		}

		if ( $about_title ) {
			echo '<h5 class="vlt-widget-about__title">' . $about_title . '</h5>';
		}

		if ( $about_text ) { ?>

			<p class="vlt-widget-about__content">
				<?php echo $about_text; ?>
			</p>

		<?php } ?>

		<?php if ( $about_image_signature ) { ?>

		<div class="vlt-widget-about__signature">
			<?php echo wp_get_attachment_image( $about_image_signature, 'vinero-full' ); ?>
		</div>

		<?php }

		echo $args['after_widget'];

	}

	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';

	?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'vlthemes' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

	<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}
}

