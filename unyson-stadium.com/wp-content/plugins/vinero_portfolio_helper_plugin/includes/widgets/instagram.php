<?php


class vlthemes_widget_instagram extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname' => 'vlt-widget-instagram',
			'description' => esc_html__( 'Display Instagram Feed.', 'vlthemes' )
		);
		parent::__construct( 'vlthemes_widget_instagram', esc_html__( 'VLThemes: Instagram', 'vlthemes' ), $widget_details );

	}

  	public function widget( $args, $instance ) {

		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = !empty( $instance['title'] ) ? $instance['title'] : '';

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];

		$widget_id = $args['widget_id'];

		$instagram_username = get_field( 'instagram_username', 'widget_' . $widget_id );
		$instagram_number = get_field( 'instagram_number', 'widget_' . $widget_id );
		$instagram_photo_size = get_field( 'instagram_photo_size', 'widget_' . $widget_id );
		$instagram_link_to = get_field( 'instagram_link_to', 'widget_' . $widget_id );


		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		if ( '' !== $instagram_username ) {


			$media_array = $this->scrape_instagram( $instagram_username );

			if ( is_wp_error( $media_array ) ) {

				echo wp_kses_post( $media_array->get_error_message() );

			} else {

				$media_array = array_slice( $media_array, 0, $instagram_number );

			?>
				<ul>
				<?php
					foreach( $media_array as $item ) {
						echo '<li><a href="'.esc_url( $item['link'] ) . '" target="_blank"><img src="' . esc_url( $item[$instagram_photo_size] ).'" alt="' . esc_attr( $item['description'] ) . '" title="' . esc_attr( $item['description'] ).'" class="vlt-widget-instagram__image"></a></li>';
					}
				?>
				</ul>
			<?php
			}
		}

		if ( $instagram_link_to ) {
			echo '<a href="'.$instagram_link_to['url'].'" target="'.$instagram_link_to['target'].'" class="vlt-btn vlt-btn--primary block">'.$instagram_link_to['title'].'</a>';
		}

		echo $args['after_widget'];

	}

	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';

	?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'vlthemes' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

	<?php

	}


	public function scrape_instagram( $username ) {

		$username = trim( strtolower( $username ) );


			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );
					break;

				default:
					$url = 'https://instagram.com/' . str_replace( '@', '', $username );
					break;
			}

			if ( false === ( $instagram = get_transient( 'instagram-vlt-' . sanitize_title_with_dashes( $username ) ) ) ) {


			$remote = wp_remote_get( $url );

			if ( is_wp_error( $remote ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'vlthemes' ) );
			}

			if ( 200 !== wp_remote_retrieve_response_code( $remote ) ) {
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'vlthemes' ) );
			}

			$shards = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], true );


			if ( ! $insta_array ) {
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'vlthemes' ) );
			}
			// var_dump($insta_array);

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
			} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'] ) ) {
				$images = $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
			} else {
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
			}
			if ( ! is_array( $images ) ) {
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'vlthemes' ) );
			}

			$instagram = array();

			foreach ( $images as $image ) {
				if ( true === $image['node']['is_video'] ) {
					$type = 'video';
				} else {
					$type = 'image';
				}
				$caption = __( 'Instagram Image', 'wp-instagram-widget' );
				if ( ! empty( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ) {
					$caption = wp_kses( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'], array() );
				}
				$instagram[] = array(
					'description' => $caption,
					'link'        => trailingslashit( '//instagram.com/p/' . $image['node']['shortcode'] ),
					'time'        => $image['node']['taken_at_timestamp'],
					'comments'    => $image['node']['edge_media_to_comment']['count'],
					'likes'       => $image['node']['edge_liked_by']['count'],
					'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ),
					'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ),
					'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][4]['src'] ),
					'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
					'type'        => $type,
				);
			} // End foreach().

			// do not set an empty transient - should help catch private or empty accounts.
			if ( ! empty( $instagram ) ) {
				$instagram = base64_encode( serialize( $instagram ) );
				set_transient( 'instagram-vlt-' . sanitize_title_with_dashes( $username ), $instagram, HOUR_IN_SECONDS * 2 );
			}
		}

		if ( ! empty( $instagram ) ) {

			return unserialize( base64_decode( $instagram ) );

		} else {

			return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'vlthemes' ) );

		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}
}
