<?php


class vlthemes_widget_popular_posts extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname' => 'vlt-widget-postlist',
			'description' => esc_html__( 'Display Popular Posts.', 'vlthemes' )
		);
		parent::__construct( 'vlthemes_widget_popular_posts', esc_html__( 'VLThemes: Popular Posts', 'vlthemes' ), $widget_details );
	}

	public function widget( $args, $instance ) {

		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = !empty( $instance['title'] ) ? $instance['title'] : '';

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];

		$widget_id = $args['widget_id'];


		$popular_posts_number_of_posts = get_field( 'popular_posts_number_of_posts', 'widget_' . $widget_id );

		$post_args = array(
			'post_type' => 'post',
			'posts_per_page' => $popular_posts_number_of_posts,
			'meta_key' => 'vinero_post_views_count',
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
			'ignore_sticky_posts' => true
		);
		$new_query = new WP_Query( $post_args );


		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		?>


		<ul>
			<?php if ( $new_query->have_posts() ): while ( $new_query->have_posts() ): $new_query->the_post(); ?>
				<li>
					<article <?php post_class( 'vlt-widget-post' ); ?> id="post-<?php the_ID(); ?>">
						<div class="vlt-widget-post__thumbnail">
							<?php the_post_thumbnail( 'vinero-thumbnail' ); ?>
						</div>
						<div class="vlt-widget-post__content">
							<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
							<span><time datetime="<?php the_time( 'c' ); ?>'"><?php echo get_the_date(); ?></time>
							</span>
						</div>
					</article>
				</li>
			<?php endwhile; endif; wp_reset_postdata(); ?>
		</ul>


		<?php echo $args['after_widget'];

	}

	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';

		?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'vlthemes' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}
}











