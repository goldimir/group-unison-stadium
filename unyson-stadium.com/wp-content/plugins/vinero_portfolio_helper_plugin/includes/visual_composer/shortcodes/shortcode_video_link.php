<?php

# Video link

if ( !class_exists( 'VLThemesVcVideoLinkShortcode' ) ) {
	class VLThemesVcVideoLinkShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_video_link';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'url' => '',
				'title' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-video-link';

			$output .= '<a class="'.$css_class.'" href="'.$url.'" data-fancybox="" data-caption="'.$title.'"><i class="icofont icofont-ui-play"></i></a>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a title for this shortcode.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'url',
					'heading' => esc_html__( 'Link to a Video (YouTube / Vimeo)', 'vlthemes' ),
					'description' => 'For example https://www.youtube.com/watch?v=iApVVKsF94E',
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' )
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' )
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Video Link', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcVideoLinkShortcode;

}
