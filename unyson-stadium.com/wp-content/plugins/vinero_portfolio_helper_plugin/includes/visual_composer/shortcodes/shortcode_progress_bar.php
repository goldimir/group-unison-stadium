<?php

# Progress bar

if ( !class_exists( 'VLThemesVcProgressBarShortcode' ) ) {
	class VLThemesVcProgressBarShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_progress_bar';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'title' => 'Title',
				'width' => '65',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-progress-bar';

			$output .= '<div class="'.$css_class.'">';
			$output .= '<h5 class="vlt-progress-bar__title">'.$title.'</h5>';
			$output .= '<div class="vlt-progress-bar__bar"><span><span class="vlt-progress-bar__percent">'.$width.'</span></span></div>';
			$output .= '</div>';

			wp_enqueue_script( 'countTo' );

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a title for the progress bar.', 'vlthemes' ),
					'value' => 'Title',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'width',
					'heading' => esc_html__( 'Width', 'vlthemes'),
					'description' => esc_html__( 'The final value.', 'vlthemes' ),
					'value' => '65',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Progress Bar', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcProgressBarShortcode;
}
