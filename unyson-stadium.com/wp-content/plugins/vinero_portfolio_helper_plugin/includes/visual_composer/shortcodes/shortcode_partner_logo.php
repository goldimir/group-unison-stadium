<?php

# Partner

if ( !class_exists( 'VLThemesVcPartnerLogoShortcode' ) ) {
	class VLThemesVcPartnerLogoShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_partner_logo';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'name' => '',
				'image' => '',
				'link' => '',
				'image_height' => 85,
				'item_height' => 135,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			if ( VLThemesVcShortcodeHelper::is_has_link( $link ) ) {
				$link_attributes = VLThemesVcShortcodeHelper::get_link_attributes( $link, $link_class );
				$link_start = '<a ' . implode( ' ', $link_attributes ) . '>';
				$link_end = '</a>';
			}

			$css_class .= ' vlt-partner-logo';

			if ( $name ) {
				$css_class .= ' tooltip';
			}

			$output .= '<div class="'.$css_class.'" title="'.$name.'" style="height:' . $item_height . 'px;">';
			$output .= $link_start . $link_end;
			$output .= '<div class="vlt-partner-logo__image" style="height:'.$image_height.'px;">';

			$output .= wp_get_attachment_image( $image, 'vinero-full' );

			$output .= '</div>';

			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map =  array(
				array(
					'type' => 'textfield',
					'param_name' => 'name',
					'heading' => esc_html__( 'Name', 'vlthemes' ),
					'description' => esc_html__( 'Enter a name for the partner.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'attach_image',
					'param_name' => 'image',
					'heading' => esc_html__( 'Logo', 'vlthemes' ),
					'description' => esc_html__( 'Select an image logo for the partner.', 'vlthemes' ),
					'value' => '',
					'holder' => 'img',
					'class' => 'vc-preview-image',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vc_link',
					'param_name' => 'link',
					'heading' => esc_html__( 'Link', 'vlthemes' ),
					'description' => esc_html__( 'Enter a link for the partner.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'item_height',
					'heading' => esc_html__( 'Item Height (in px)', 'vlthemes' ),
					'description' => esc_html__( 'Enter a height of the item (in px).', 'vlthemes' ),
					'value' => 135,
					'min' => 0,
					'max' => 500,
					'step' => 5,
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'image_height',
					'heading' => esc_html__( 'Image Height (in px)', 'vlthemes' ),
					'description' => esc_html__( 'Enter a height of the image (in px).', 'vlthemes' ),
					'value' => 85,
					'min' => 0,
					'max' => 500,
					'step' => 5,
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' )
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' )
				)
			);


			vc_map( array(
				'name' => esc_html__( 'Partner Logo', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcPartnerLogoShortcode;
}
