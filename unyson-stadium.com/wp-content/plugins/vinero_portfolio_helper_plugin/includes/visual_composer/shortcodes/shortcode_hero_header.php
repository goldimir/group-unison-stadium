<?php

# Hero Header

if ( !class_exists( 'VLThemesVcHeroHeaderShortcode' ) ) {
	class VLThemesVcHeroHeaderShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_hero_header';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'image' => '',
				'title' => 'Example text',
				'subtitle' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$subtitle = vc_value_from_safe( $subtitle );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-hero-title';

			if ( $image ) {
				$css_class .= ' jarallax';
			}

			$output .= '<div class="' . $css_class . '">';

			if ( $image ) {
				$output .= '<img src="'.wp_get_attachment_image_url( $image, 'vinero-full' ).'" alt="" class="jarallax-img">';
			}

			$output .= '<div class="vlt-hero-title__content">';

			$output .= '<h1>' . $title . '</h1>';
			if ( $subtitle ) {
				$output .= '<p>' . $subtitle . '</p>';
			}

			$output .= '</div>';
			$output .= '</div>';

			return $output;

		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'attach_image',
					'param_name' => 'image',
					'heading' => esc_html__( 'Image', 'vlthemes' ),
					'description' => esc_html__( 'Select an image for this shortcode.', 'vlthemes' ),
					'value' => '',
					'holder' => 'img',
					'class' => 'vc-preview-image',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a title for this shortcode.', 'vlthemes' ),
					'value' => 'Example text',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'subtitle',
					'heading' => esc_html__( 'Subtitle', 'vlthemes' ),
					'description' => esc_html__( 'Enter a text for the subtitle.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Hero Header', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcHeroHeaderShortcode;
}
