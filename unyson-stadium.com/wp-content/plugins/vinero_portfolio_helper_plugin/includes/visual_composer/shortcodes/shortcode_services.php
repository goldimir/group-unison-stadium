<?php

# Services

if ( !class_exists( 'VLThemesVcServicesShortcode' ) ) {
	class VLThemesVcServicesShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_services';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'title' => 'Title',
				'text' => 'Example text',
				'el_class' => '',
				'css' => '',
				// Icons
				'position' => 'top-center',
				'icon_color' => '#333333',
				'icon_type' => 'none',
				'icon_fontawesome' => '',
				'icon_openiconic' => '',
				'icon_typicons' => '',
				'icon_entypo' => '',
				'icon_linecons' => '',
				'icon_elusive' => '',
				'icon_etline' => '',
				'icon_iconmoon' => '',
				'icon_linearicons' => '',
				'icon_iconsmind' => '',
				'icon_icofont' => '',
			), $atts ) );

			switch ( $icon_type ) {
				case 'fontawesome':
					$icon = $atts['icon_fontawesome'];
					break;
				case 'openiconic':
					$icon = $atts['icon_openiconic'];
					break;
				case 'typicons':
					$icon = $atts['icon_typicons'];
					break;
				case 'entypo':
					$icon = $atts['icon_entypo'];
					break;
				case 'linecons':
					$icon = $atts['icon_linecons'];
					break;
				case 'elusive':
					$icon = $atts['icon_elusive'];
					break;
				case 'etline':
					$icon = $atts['icon_etline'];
					break;
				case 'iconmoon':
					$icon = $atts['icon_iconmoon'];
					break;
				case 'linearicons':
					$icon = $atts['icon_linearicons'];
					break;
				case 'iconsmind':
					$icon = $atts['icon_iconsmind'];
					break;
				case 'icofont':
					$icon = $atts['icon_icofont'];
					break;
			}

			vc_icon_element_fonts_enqueue( $icon_type );
			$icon = ( $icon_type != 'none' ) ? '<i class="'.$icon.'"></i>' : '';

			$content = $text;
			$content = vc_value_from_safe( $content );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );


			$css_class .= ' vlt-services';
			$css_class .= ' vlt-services--' . $position;

			$output .= '<div class="'.$css_class.'">';

			if ( $icon_type != 'none' ) {
				$output .= '<div class="vlt-services__icon" style="color:'.$icon_color.';">';
				$output .= $icon;
				$output .= '</div>';
			}

			$output .= '<div class="vlt-services__content">';
			if ( $title ) {
				$output .= '<h5 class="vlt-services__title">'.$title.'</h5>';
			}

			if ( $content ) {
				$output .= '<p class="vlt-services__text" >'.$content.'</p>';
			}
			$output .= '</div>';
			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {
			$vc_map = array(
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a text for the title.', 'vlthemes' ),
					'admin_label' => true,
					'value' => 'Title',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'text',
					'heading' => esc_html__( 'Content', 'vlthemes' ),
					'description' => esc_html__( 'Enter a content for this shortcode (HTML tags available).', 'vlthemes' ),
					'value' => 'Example text',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'position',
					'heading' => esc_html__( 'Icon Position', 'vlthemes' ),
					'description' => esc_html__( 'Select a position of the icon from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Top Center', 'vlthemes' ) => 'top-center',
						esc_html__( 'Top Left', 'vlthemes' ) => 'top-left',
						esc_html__( 'Top Right', 'vlthemes' ) => 'top-right',
						esc_html__( 'Left', 'vlthemes' ) => 'left',
						esc_html__( 'Right', 'vlthemes' ) => 'right'
					),
					'group' => esc_html__( 'Icon', 'vlthemes' ),
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'icon_color',
					'heading' => esc_html__( 'Icon Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the icon.', 'vlthemes' ),
					'value' => '#333333',
					'group' => esc_html__( 'Icon', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			$vc_map = array_merge( $vc_map, VLThemesVcIconsList::get_icons_map() );

			vc_map( array(
				'name' => esc_html__( 'Services', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcServicesShortcode;
}
