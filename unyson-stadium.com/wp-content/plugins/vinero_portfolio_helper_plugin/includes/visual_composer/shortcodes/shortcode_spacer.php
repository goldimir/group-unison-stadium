<?php

# Spacer

if ( !class_exists( 'VLThemesVcSpacerShortcode' ) ) {
	class VLThemesVcSpacerShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_spacer';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'height' => '',
				'height_on_tabs' => '',
				'height_on_tabs_portrait' => '',
				'height_on_mob' => '',
				'height_on_mob_landscape' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-spacer';

			if ( $height_on_mob == '' && $height_on_tabs == '' ) {
				$height_on_mob = $height_on_tabs = $height;
			}

			$output .= '<div class="'.$css_class.'" data-id="'.$identifier_class.'" data-height="'.esc_attr($height).'" data-height-mobile="'.esc_attr($height_on_mob).'" data-height-tab="'.esc_attr($height_on_tabs).'" data-height-tab-portrait="'.esc_attr($height_on_tabs_portrait).'" data-height-mobile-landscape="'.esc_attr($height_on_mob_landscape).'"></div>';

			return $output;

		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'vl_number',
					'param_name' => 'height',
					'heading' => '<i class="dashicons dashicons-desktop"></i> ' . esc_html__( 'Desktop', 'vlthemes' ),
					'value' => 10,
					'min' => 0,
					'max' => 9999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'height_on_tabs',
					'heading' => '<i class="dashicons dashicons-tablet" style="transform: rotate(90deg);"></i> ' . esc_html__( 'Tabs', 'vlthemes' ),
					'value' => '',
					'min' => 0,
					'max' => 9999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'height_on_tabs_portrait',
					'heading' => '<i class="dashicons dashicons-tablet"></i> ' . esc_html__( 'Tabs', 'vlthemes' ),
					'value' => '',
					'min' => 0,
					'max' => 9999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'height_on_mob_landscape',
					'heading' => '<i class="dashicons dashicons-smartphone" style="transform: rotate(90deg);"></i> ' . esc_html__( 'Mobile', 'vlthemes' ),
					'value' => '',
					'min' => 0,
					'max' => 9999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'height_on_mob',
					'heading' => '<i class="dashicons dashicons-smartphone"></i> ' . esc_html__( 'Mobile', 'vlthemes' ),
					'value' => '',
					'min' => 0,
					'max' => 9999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Spacer / Gap', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcSpacerShortcode;
}
