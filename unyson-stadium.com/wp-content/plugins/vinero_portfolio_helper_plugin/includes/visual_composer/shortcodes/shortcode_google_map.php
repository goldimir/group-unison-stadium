<?php

# Google Map

if ( !class_exists( 'VLThemesVcGoogleMapShortcode' ) ) {
	class VLThemesVcGoogleMapShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_google_map';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'api_key' => '',
				'lat' => '40.716361',
				'lng' => '-74.001836',
				'zoom' => '13',
				'marker' => '',
				'height' => 420,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-google-map';
			$marker = wp_get_attachment_image_url( $marker, 'vinero-full' );

			ob_start();

			?>
				<div class="<?php echo $css_class; ?>">
					<div id="<?php echo $identifier_class; ?>" style="height:<?php echo $height; ?>px;"></div>
					<div class="vlt-google-map__controls">
						<div id="zoom-in"><i class="icofont icofont-plus"></i></div><div id="zoom-out"><i class="icofont icofont-minus"></i></div>
					</div>
				</div>

				<?php if ( $api_key ) : ?>
					<script src="https://maps.google.com/maps/api/js?key=<?php echo $api_key; ?>"></script>
				<?php endif; ?>

				<script>
					function initGMap() {
						var LatLng = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};
						var mapCanvas = document.getElementById('<?php echo $identifier_class; ?>');
						var mapOptions = {
							center : LatLng,
							scrollwheel : false,
							zoom : <?php echo $zoom; ?>,
							backgroundColor: 'none',
							mapTypeId : 'vinero',
							panControl: false,
							zoomControl: false,
							mapTypeControl: false,
							streetViewControl: false,
						};
						var map = new google.maps.Map(mapCanvas, mapOptions);
						var marker = new google.maps.Marker({
							position: LatLng,
							map: map,
							<?php if ( $marker ) : ?>
							icon: '<?php echo $marker; ?>'
							<?php endif; ?>
						});
						//style from https://snazzymaps.com/style/151/ultra-light-with-labels
						var styledMapType = new google.maps.StyledMapType([{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}], { name: 'nKStyle' });
						map.mapTypes.set('vinero', styledMapType);

						function CustomZoomControl(controlDiv, map) {
							var controlUIzoomIn = document.getElementById('zoom-in'),
								controlUIzoomOut = document.getElementById('zoom-out');

							google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
								map.setZoom(map.getZoom() + 1)
							});
							google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
								map.setZoom(map.getZoom() - 1)
							});
						}
						var zoomControlDiv = document.createElement('div');
						var zoomControl = new CustomZoomControl(zoomControlDiv, map);
						map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);

					}
					if(typeof google !== 'undefined') {
						google.maps.event.addDomListener(window, 'load', initGMap);
					}


				</script>

			<?php

			return ob_get_clean();

		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'textfield',
					'param_name' => 'api_key',
					'heading' => esc_html__( 'API Key', 'vlthemes' ),
					'description' => esc_html__( 'Enter API key for the Google Map.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'lat',
					'heading' => esc_html__( 'Latitude', 'vlthemes' ),
					'description' => 'https://www.latlong.net',
					'value' => '40.71636',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'lng',
					'heading' => esc_html__( 'Longitude', 'vlthemes' ),
					'description' => 'https://www.latlong.net',
					'value' => '-74.001836',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'height',
					'heading' => esc_html__( 'Map Height (in px)', 'vlthemes' ),
					'value' => 420,
					'min' => 0,
					'max' => 9999,
					'step' => 5,
					'admin_label' => true,
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'zoom',
					'heading' => esc_html__( 'Map Zoom', 'vlthemes' ),
					'value' => 13,
					'min' => 1,
					'max' => 20,
					'step' => 1,
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'attach_image',
					'param_name' => 'marker',
					'heading' => esc_html__( 'Marker', 'vlthemes' ),
					'description' => esc_html__( 'Select an image for the map marker.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Google Map', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcGoogleMapShortcode;

}
