<?php

# Alert message

if ( !class_exists( 'VLThemesVcAlertMessageShortcode' ) ) {
	class VLThemesVcAlertMessageShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_alert_message';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'text' => 'Example text',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$content = $text;
			$content = vc_value_from_safe( $content );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-alert';


			$output .= '<p class="'.$css_class.'">';
			$output .= $content;
			$output .= '</p>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'textarea_safe',
					'param_name' => 'text',
					'heading' => esc_html__( 'Text', 'vlthemes' ),
					'description' => esc_html__( 'Enter a text for the alert message.', 'vlthemes' ),
					'value' => 'Example text',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Alert Message', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcAlertMessageShortcode;
}
