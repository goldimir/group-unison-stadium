<?php

# ADS banner

if ( !class_exists( 'VLThemesVcAdsBannerShortcode' ) ) {
	class VLThemesVcAdsBannerShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_ads_banner';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'image' => '',
				'link' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-ads-banner';

			if ( VLThemesVcShortcodeHelper::is_has_link( $link ) ) {
				$link_attributes = VLThemesVcShortcodeHelper::get_link_attributes( $link, $css_class );
				$link_start = '<a ' . implode( ' ', $link_attributes ) . '>';
				$link_end = '</a>';
			}

			$output .= $link_start;
			$output .= wp_get_attachment_image( $image, 'vinero-full' );
			$output .= $link_end;

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'attach_image',
					'param_name' => 'image',
					'heading' => esc_html__( 'Banner Image', 'vlthemes' ),
					'description' => esc_html__( 'Select ADS image for the banner.', 'vlthemes' ),
					'value' => '',
					'holder' => 'img',
					'class' => 'vc-preview-image',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vc_link',
					'param_name' => 'link',
					'heading' => esc_html__( 'Link', 'vlthemes' ),
					'description' => esc_html__( 'Enter a link for the banner.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'ADS Banner', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcAdsBannerShortcode;
}
