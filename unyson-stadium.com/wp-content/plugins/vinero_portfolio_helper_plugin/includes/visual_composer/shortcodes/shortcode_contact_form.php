<?php

# CF7

if ( !class_exists( 'VLThemesVcCF7Shortcode' ) ) {
	class VLThemesVcCF7Shortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_cf7';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'id' => '',
				'title' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$output .= '<div class="'.$css_class.'">';

			if ( class_exists( 'WPCF7_ContactForm' ) ){
				$output .= do_shortcode( html_entity_decode( '[contact-form-7 id="'.$id.'" title="'.$title.'"]' ) );
			} else {
				$output .= '<p>'.esc_html__( 'You did not install Contact Form 7 (By Takayuki Miyoshi) plugin.', 'vlthemes' ).'</p>';
			}

			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'textfield',
					'param_name' => 'id',
					'heading' => esc_html__( 'ID', 'vlthemes' ),
					'description' => esc_html__( 'Enter the contact form ID.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter the title of the contact form.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Contact Form 7', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcCF7Shortcode;
}
