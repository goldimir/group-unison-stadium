<?php

# Pricing table

if ( !class_exists( 'VLThemesVcPricingTableShortcode' ) ) {
	class VLThemesVcPricingTableShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_pricing_table';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'name' => 'Starter',
				'amount' => '29',
				'currecny' => '$',
				'period' => '/ monthly',
				'text' => '',
				'featured' => false,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$content = $text;
			$content = vc_value_from_safe( $content );

			$css_class .= ' vlt-pricing-table text-center';

			if ( $featured ) {
				$css_class .= ' vlt-pricing-table--featured';
			}

			$output .= '<div class="'.$css_class.'">';

			if ( $featured ) {
				$output .= '<i class="vlt-pricing-table__featured-flag icofont icofont-flash"></i>';
			}

			$output .= '<div class="vlt-pricing-table__header">';

				$output .= '<h5 class="vlt-pricing-table__name"">'.$name.'</h5>';

				$output .= '<div class="vlt-pricing-table__price">';
					$output .= '<span class="vlt-pricing-table__currecny">'.$currecny.'</span>';
					$output .= '<span class="vlt-pricing-table__amount">'.$amount.'</span>';
					$output .= '<span class="vlt-pricing-table__period">'.$period.'</span>';
				$output .= '</div>';

			$output .= '</div>';

			$output .= '<div class="vlt-pricing-table__text">';
			$output .= $content;
			$output .= '</div>';

			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {


			$vc_map = array(
				array(
					'type' => 'checkbox',
					'param_name' => 'featured',
					'heading' => esc_html__( 'Featured', 'vlthemes' ),
					'description' => esc_html__( 'Check it if this is the recommended pricing table.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => false,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'name',
					'heading' => esc_html__( 'Name', 'vlthemes' ),
					'description' => esc_html__( 'Enter a table name.', 'vlthemes' ),
					'value' => 'Starter',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'amount',
					'heading' => esc_html__( 'Amount', 'vlthemes' ),
					'description' => esc_html__( 'Enter an amount.', 'vlthemes' ),
					'value' => '29',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'currecny',
					'heading' => esc_html__( 'Currency', 'vlthemes' ),
					'description' => esc_html__( 'Enter a currency symbol.', 'vlthemes' ),
					'value' => '$',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'period',
					'heading' => esc_html__( 'Period', 'vlthemes' ),
					'description' => esc_html__( 'Enter a period.', 'vlthemes' ),
					'value' => '/ monthly',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'text',
					'heading' => esc_html__( 'Content', 'vlthemes' ),
					'description' => esc_html__( 'Enter a content for this shortcode (HTML tags available).', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Pricing Table', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcPricingTableShortcode;
}
