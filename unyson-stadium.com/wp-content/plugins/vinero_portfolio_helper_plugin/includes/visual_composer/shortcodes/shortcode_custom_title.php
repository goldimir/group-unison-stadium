<?php

# Custom Title

if ( !class_exists( 'VLThemesVltCustomTitleShortcode' ) ) {
	class VLThemesVltCustomTitleShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_custom_title';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'heading' => 'h2',
				'title' => 'Example text',
				'subtitle' => '',
				'align' => 'center',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$title = vc_value_from_safe( $title );
			$subtitle = vc_value_from_safe( $subtitle );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-custom-title';
			$css_class .= ' text-' . $align;

			$output .= '<div class="' . $css_class . '">';

			if ( $title ) {
				$output .= '<'.$heading.'>' . $title . '</'.$heading.'>';
			}

			if ( $subtitle  ) {
				$output .= '<p>' . $subtitle . '</p>';
			}

			$output .= '</div>';

			return $output;

		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'dropdown',
					'param_name' => 'heading',
					'heading' => esc_html__( 'Heading', 'vlthemes' ),
					'description' => esc_html__( 'Select heading from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Heading 2', 'vlthemes' ) => 'h2',
						esc_html__( 'Heading 1', 'vlthemes' ) => 'h1',
						esc_html__( 'Heading 3', 'vlthemes' ) => 'h3',
						esc_html__( 'Heading 4', 'vlthemes' ) => 'h4',
						esc_html__( 'Heading 5', 'vlthemes' ) => 'h5',
						esc_html__( 'Heading 6', 'vlthemes' ) => 'h6',
					),
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a title for this shortcode.', 'vlthemes' ),
					'value' => 'Example text',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'subtitle',
					'heading' => esc_html__( 'Subtitle', 'vlthemes' ),
					'description' => esc_html__( 'Enter a text for the subtitle.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'align',
					'heading' => esc_html__( 'Title Align', 'vlthemes' ),
					'description' => esc_html__( 'Select the title alignment from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Center', 'vlthemes' ) => 'center',
						esc_html__( 'Left', 'vlthemes' ) => 'left',
						esc_html__( 'Right', 'vlthemes' ) => 'right',
					),
					'admin_label' => true,
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Custom Title', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVltCustomTitleShortcode;
}
