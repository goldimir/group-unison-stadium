<?php

# Collage

if ( !class_exists( 'VLThemesVcCollageShortcode' ) ) {
	class VLThemesVcCollageShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_image_collage';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'height' => 340,
				'justify' => 'justify',
				'gutter' => 30,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-justified-gallery';

			$output .= '<div class="'.$css_class.'" data-gutter="'.$gutter.'" data-height="'.$height.'" data-lastrow="'.$justify.'">';
			$output .= do_shortcode( $content );
			$output .= '</div>';

			wp_enqueue_script( 'justifiedGallery' );

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'vl_number',
					'param_name' => 'height',
					'heading' => esc_html__( 'Image Height', 'vlthemes' ),
					'description' => esc_html__( 'Height of one row of images (in px).', 'vlthemes' ),
					'value' => 340,
					'min' => 1,
					'max' => 999999,
					'step' => 1,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'justify',
					'heading' => esc_html__( 'Justify', 'vlthemes' ),
					'description' => esc_html__( 'Justify the last image or save the original size.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Justify', 'vlthemes' ) => 'justify',
						esc_html__( 'No justify', 'vlthemes' ) => 'nojustify',
					),
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'gutter',
					'heading' => esc_html__( 'Gutter', 'vlthemes' ),
					'description' => esc_html__( 'Enter a spacing between an images.', 'vlthemes' ),
					'value' => 30,
					'min' => 1,
					'max' => 200,
					'step' => 1,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Image Collage', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
				'as_parent' => array( 'only' => 'vlt_single_image' ),
				'content_element' => true,
				'js_view' => 'VcColumnView',
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcCollageShortcode;

	/**
	 * The "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
	 */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Vlt_Image_Collage extends WPBakeryShortCodesContainer {}
	}
}
