<?php

# Testimonials child

if ( !class_exists( 'VLThemesVcTestimonialsChildShortcode' ) ) {
	class VLThemesVcTestimonialsChildShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_testimonials_child';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'name' => 'Member Name',
				'function' => 'Member Function',
				'text' => 'Example text',
				'name_color' => '#404040',
				'function_color' => '#606060',
				'text_color' => '#606060',
				'icon_color' => '#bdbdbd',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$content = $text;
			$content = vc_value_from_safe( $content );

			$css_class .= ' vlt-testimonials-child text-center';

			$output .= '<div class="'.$css_class.'">';

			$output .= '<div class="vlt-testimonials-child__content">';
			$output .= '<div class="vlt-testimonials-child__icon"><i class="fa fa-quote-left" style="color: '. $icon_color .'"></i></div>';
			$output .= '<p class="vlt-testimonials-child__text" style="color:'.$text_color.';">';
			$output .= $content;
			$output .= '</p>';

			$output .= '<div class="vlt-testimonials-child__meta">';

			if ( $name ) {
				$output .= '<h5 class="vlt-testimonials-child__name" style="color:'.$name_color.';">'.$name.'</h5>';
			}

			if ( $function ) {
				$output .= '<p class="vlt-testimonials-child__function" style="color:'.$function_color.';">'.$function.'</p>';
			}

			$output .= '</div>';

			$output .= '</div>';


			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map =  array(
				array(
					'type' => 'textfield',
					'param_name' => 'name',
					'heading' => esc_html__( 'Member Name', 'vlthemes' ),
					'description' => esc_html__( 'Enter a member name.', 'vlthemes' ),
					'value' => 'Member Name',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'function',
					'heading' => esc_html__( 'Member Function', 'vlthemes' ),
					'description' => esc_html__( 'Enter a member function.', 'vlthemes' ),
					'value' => 'Member Function',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textarea_safe',
					'param_name' => 'text',
					'heading' => esc_html__( 'Content', 'vlthemes' ),
					'description' => esc_html__( 'Enter a content for this shortcode (HTML tags available).', 'vlthemes' ),
					'value' => 'Example text',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'icon_color',
					'heading' => esc_html__( 'Icon Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the icon.', 'vlthemes' ),
					'value' => '#404040',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'text_color',
					'heading' => esc_html__( 'Text Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the content.', 'vlthemes' ),
					'value' => '#606060',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'name_color',
					'heading' => esc_html__( 'Name Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the name.', 'vlthemes' ),
					'value' => '#606060',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'function_color',
					'heading' => esc_html__( 'Function Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the function.', 'vlthemes' ),
					'value' => '#bdbdbd',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' )
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' )
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Testimonials Child', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcTestimonialsChildShortcode;
}
