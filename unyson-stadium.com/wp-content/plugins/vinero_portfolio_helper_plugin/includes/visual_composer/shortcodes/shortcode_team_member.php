<?php

# Team member

if ( !class_exists( 'VLThemesVcTeamMemberShortcode' ) ) {
	class VLThemesVcTeamMemberShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_team_member';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'image' => '',
				'name' => 'Member Name',
				'function' => 'Member Function',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );


			$css_class .= ' vlt-team-member';

			$output .= '<div class="'.$css_class.'">';

			$output .= '<div class="vlt-team-member__avatar">';
			if ( $content ) {
				$output .= '<div class="vlt-team-member__links">';
				$output .= do_shortcode( $content );
				$output .= '</div>';
			}
			$output .= wp_get_attachment_image( $image, 'vinero-full' );
			$output .= '</div>';

			$output .= '<div class="vlt-team-member__info">';
			if ( $name ) {
				$output .= '<h5 class="vlt-team-member__name" >'.$name.'</h5>';
			}
			if ( $function ) {
				$output .= '<p class="vlt-team-member__function">'.$function.'</p>';
			}
			$output .= '</div>';

			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'attach_image',
					'param_name' => 'image',
					'heading' => esc_html__( 'Image', 'vlthemes' ),
					'description' => esc_html__( 'Select an image for this shortcode.', 'vlthemes' ),
					'value' => '',
					'holder' => 'img',
					'class' => 'vc-preview-image',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'name',
					'heading' => esc_html__( 'Member Name', 'vlthemes' ),
					'description' => esc_html__( 'Enter a member name.', 'vlthemes' ),
					'value' => 'Member Name',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'function',
					'heading' => esc_html__( 'Member Function', 'vlthemes' ),
					'description' => esc_html__( 'Enter a member function.', 'vlthemes' ),
					'value' => 'Member Function',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' )
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' )
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Team Member', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
				'as_parent' => array( 'only' => 'vlt_icon_link' ),
				'content_element' => true,
				'js_view' => 'VcColumnView',
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcTeamMemberShortcode;

	/**
	 * The "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
	 */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Vlt_Team_Member extends WPBakeryShortCodesContainer {}
	}
}
