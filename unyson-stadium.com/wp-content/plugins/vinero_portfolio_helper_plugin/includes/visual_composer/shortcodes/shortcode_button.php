<?php

# Button

if ( !class_exists( 'VLThemesVcButtonShortcode' ) ) {
	class VLThemesVcButtonShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_button';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'text' => 'Button Text',
				'style' => 'vlt-btn--primary',
				'size' => 'vlt-btn--md',
				'link' => '',
				'block' => false,
				'el_class' => '',
				'css' => '',
				// Icons
				'icon_type' => 'none',
				'icon_fontawesome' => '',
				'icon_openiconic' => '',
				'icon_typicons' => '',
				'icon_entypo' => '',
				'icon_linecons' => '',
				'icon_elusive' => '',
				'icon_etline' => '',
				'icon_iconmoon' => '',
				'icon_linearicons' => '',
				'icon_iconsmind' => '',
				'icon_icofont' => '',
			), $atts ) );

			switch ( $icon_type ) {
				case 'fontawesome':
					$icon = $atts['icon_fontawesome'];
					break;
				case 'openiconic':
					$icon = $atts['icon_openiconic'];
					break;
				case 'typicons':
					$icon = $atts['icon_typicons'];
					break;
				case 'entypo':
					$icon = $atts['icon_entypo'];
					break;
				case 'linecons':
					$icon = $atts['icon_linecons'];
					break;
				case 'elusive':
					$icon = $atts['icon_elusive'];
					break;
				case 'etline':
					$icon = $atts['icon_etline'];
					break;
				case 'iconmoon':
					$icon = $atts['icon_iconmoon'];
					break;
				case 'linearicons':
					$icon = $atts['icon_linearicons'];
					break;
				case 'iconsmind':
					$icon = $atts['icon_iconsmind'];
					break;
				case 'icofont':
					$icon = $atts['icon_icofont'];
					break;
			}
			vc_icon_element_fonts_enqueue( $icon_type );
			$icon = ( $icon_type != 'none' ) ? '<i class="'.$icon.'"></i>' : '';

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$link_class .= $css_class;
			$link_class .= ' vlt-btn ' . $style;
			$link_class .= ' ' . $size;

			if ( $block ) {
				$link_class .= ' block';
			}

			if ( VLThemesVcShortcodeHelper::is_has_link( $link ) ) {
				$link_attributes = VLThemesVcShortcodeHelper::get_link_attributes( $link, $link_class );
				$link_start = '<a ' . implode( ' ', $link_attributes ) . '>';
				$link_end = '</a>';
			}

			$output .= $link_start;
			$output .= '<span>' . $text . '</span>';
			$output .= $icon;
			$output .= $link_end;

			return $output;
		}

		public function vc_map_shortcode() {


			$vc_map = array(
				array(
					'type' => 'dropdown',
					'param_name' => 'style',
					'heading' => esc_html__( 'Style Button', 'vlthemes' ),
					'description' => esc_html__( 'Select the style for the button from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Primary', 'vlthemes' ) => 'vlt-btn--primary',
						esc_html__( 'Secondary', 'vlthemes' ) => 'vlt-btn--secondary',
					),
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'size',
					'heading' => esc_html__( 'Button Size', 'vlthemes' ),
					'description' => esc_html__( 'Select the size for the button from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Medium', 'vlthemes' ) => 'vlt-btn--md',
						esc_html__( 'Large', 'vlthemes' ) => 'vlt-btn--lg',
						esc_html__( 'Small', 'vlthemes' ) => 'vlt-btn--sm',
					),
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'text',
					'heading' => esc_html__( 'Text', 'vlthemes' ),
					'description' => esc_html__( 'Type the text for the button.', 'vlthemes' ),
					'value' => 'Button Text',
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'vc_link',
					'param_name' => 'link',
					'heading' => esc_html__( 'Link', 'vlthemes' ),
					'description' => esc_html__( 'Enter a link for the button.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'block',
					'heading' => esc_html__( 'Fullwidth Button', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to display the full width of the parent block.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => false,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			$vc_map = array_merge( $vc_map, VLThemesVcIconsList::get_icons_map() );

			vc_map( array(
				'name' => esc_html__( 'Button', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcButtonShortcode;
}
