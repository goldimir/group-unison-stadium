<?php

# Single image

if ( !class_exists( 'VLThemesVcSingleImageShortcode' ) ) {
	class VLThemesVcSingleImageShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_single_image';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'image' => '',
				'size' => 'vinero-full',
				'buttons' => true,
				'gallery_id' => '',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-single-image';


			$caption = get_the_title( $image );

			$output .= '<div class="'.$css_class.'">';
			$output .= '<a data-fancybox="'.$gallery_id.'" href="'.wp_get_attachment_image_url( $image, 'vinero-full' ).'" data-caption="'.$caption.'"></a>';
			if ( $caption ) {
				$output .= '<span>'.$caption.'</span>';
			}
			$output .= wp_get_attachment_image( $image, $size );
			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'attach_image',
					'param_name' => 'image',
					'heading' => esc_html__( 'Image', 'vlthemes' ),
					'description' => esc_html__( 'Select an image for this shortcode.', 'vlthemes' ),
					'value' => '',
					'holder' => 'img',
					'class' => 'vc-preview-image',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'size',
					'heading' => esc_html__( 'Image Size', 'vlthemes' ),
					'description' => esc_html__( 'Select image size from the list below.', 'vlthemes' ),
					'admin_label' => true,
					'value' => VLThemesVcShortcodeHelper::get_image_sizes(),
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'buttons',
					'heading' => esc_html__( 'Buttons', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to show buttons.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => true,
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'gallery_id',
					'heading' => esc_html__( 'Gallery ID', 'vlthemes' ),
					'description' => esc_html__( 'Enter an ID for the gallery.', 'vlthemes' ),
					'value' => '',
					'admin_label' => true,
					'dependency' => array(
						'element' => 'buttons',
						'value' => '1'
					),
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' )
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' )
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Single Image', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcSingleImageShortcode;
}
