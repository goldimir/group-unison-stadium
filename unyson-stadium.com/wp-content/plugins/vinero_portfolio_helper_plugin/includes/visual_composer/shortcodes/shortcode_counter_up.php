<?php

# Counter up

if ( !class_exists( 'VLThemesVcCounterUpShortcode' ) ) {
	class VLThemesVcCounterUpShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_counter_up';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'title' => 'Title',
				'value' => 99,
				'title_color' => '#333333',
				'value_color' => '#333333',
				'preffix' => '',
				'suffix' => '',
				'el_class' => '',
				'css' => '',
				// Icons
				'icon_type' => 'none',
				'icon_fontawesome' => '',
				'icon_openiconic' => '',
				'icon_typicons' => '',
				'icon_entypo' => '',
				'icon_linecons' => '',
				'icon_elusive' => '',
				'icon_etline' => '',
				'icon_iconmoon' => '',
				'icon_linearicons' => '',
				'icon_iconsmind' => '',
				'icon_icofont' => '',
			), $atts ) );

			switch ( $icon_type ) {
				case 'fontawesome':
					$icon = $atts['icon_fontawesome'];
					break;
				case 'openiconic':
					$icon = $atts['icon_openiconic'];
					break;
				case 'typicons':
					$icon = $atts['icon_typicons'];
					break;
				case 'entypo':
					$icon = $atts['icon_entypo'];
					break;
				case 'linecons':
					$icon = $atts['icon_linecons'];
					break;
				case 'elusive':
					$icon = $atts['icon_elusive'];
					break;
				case 'etline':
					$icon = $atts['icon_etline'];
					break;
				case 'iconmoon':
					$icon = $atts['icon_iconmoon'];
					break;
				case 'linearicons':
					$icon = $atts['icon_linearicons'];
					break;
				case 'iconsmind':
					$icon = $atts['icon_iconsmind'];
					break;
				case 'icofont':
					$icon = $atts['icon_icofont'];
					break;
			}

			vc_icon_element_fonts_enqueue( $icon_type );
			$icon = ( $icon_type != 'none' ) ? '<i class="'.$icon.'"></i>' : '';

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );


			$css_class .= ' vlt-counter-up';

			$output .= '<div class="'.$css_class.'">';

			$output .= '<div class="vlt-counter-up__header" style="color:'.$value_color.';">';
			if ( $icon_type != 'none' ) {
				$output .= '<span class="vlt-counter-up__icon">';
				$output .= $icon;
				$output .= '</span>';
			}
			$output .= '<span class="vlt-counter-up__number">'.$preffix.'<span data-value="'.$value.'">0</span>'.$suffix.'</span>';
			$output .= '</div>';

			if ( $title ) {
				$output .= '<h5 class="vlt-counter-up__title" style="color:'.$title_color.';">'.$title.'</h5>';
			}

			$output .= '</div>';

			wp_enqueue_script( 'countTo' );

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'vl_number',
					'param_name' => 'value',
					'heading' => esc_html__( 'Final Value', 'vlthemes' ),
					'description' => esc_html__( 'Enter a final value for the counter.', 'vlthemes' ),
					'value' => 99,
					'min' => 1,
					'max' => 9999999,
					'step' => 1,
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'preffix',
					'heading' => esc_html__( 'Prefix', 'vlthemes' ),
					'description' => esc_html__( 'This text will be prepended to the value.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'suffix',
					'heading' => esc_html__( 'Suffix', 'vlthemes' ),
					'description' => esc_html__( 'Tis text will be appended to the value.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__( 'Title', 'vlthemes' ),
					'description' => esc_html__( 'Enter a text for the title.', 'vlthemes' ),
					'admin_label' => true,
					'value' => 'Title',
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'title_color',
					'heading' => esc_html__( 'Title Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the title.', 'vlthemes' ),
					'value' => '#333333',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'value_color',
					'heading' => esc_html__( 'Value Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the value.', 'vlthemes' ),
					'value' => '#333333',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			$vc_map = array_merge( $vc_map, VLThemesVcIconsList::get_icons_map() );

			vc_map( array(
				'name' => esc_html__( 'Counter Up', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcCounterUpShortcode;
}
