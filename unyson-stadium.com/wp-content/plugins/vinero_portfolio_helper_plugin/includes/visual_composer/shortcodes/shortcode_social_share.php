<?php

# Social share

if ( !class_exists( 'VLThemesVcSocialShareShortcode' ) ) {
	class VLThemesVcSocialShareShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_social_share';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'socials' => '',
				'align' => 'center',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-social-share';
			$css_class .= ' text-' . $align;

			$socials_list = '';

			if( $socials ){
				$socials_list = trim( $socials, ' ' );
				$socials_list = explode( ',', $socials_list );
			}

			$output .= '<div class="'.$css_class.'">';

			global $post;

			$url = urlencode( get_permalink( $post->ID ) );
			$title = urlencode( get_the_title( $post->ID ) );
			$media = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID, 'vinero-full' ) );

			foreach ( $socials_list as $key => $value ) {
				if ( $value == 'twitter' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="https://twitter.com/home?status='.$title.'+'.$url.'"><i class="fa fa-twitter"></i></a>';
				} elseif( $value == 'facebook' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="https://www.facebook.com/share.php?u='.$url.'&title='.$title.'"><i class="fa fa-facebook"></i></a>';
				} elseif( $value == 'vk' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="https://vk.com/share.php?url='.$url.'&title='.$title.'"><i class="fa fa-vk"></i></a>';
				} elseif( $value == 'googleplus' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="https://plus.google.com/share?url='.$url.'"><i class="fa fa-google-plus"></i></a>';
				} elseif( $value == 'linkedin' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.$url.'"><i class="fa fa-linkedin"></i></a>';
				} elseif( $value == 'pinterest' ) {
					$output .= '<a class="vlt-single-icon vlt-social-share__item" target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media='.$media[0].'&url='.$url.'&is_video=false&description='.$title.'"><i class="fa fa-pinterest"></i></a>';
				}
			}

			$output .= '</div>';

			return $output;
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'vl_select',
					'param_name' => 'socials',
					'heading' => esc_html__( 'Socials', 'vlthemes' ),
					'description' => esc_html__( 'Select social icons for this shortcode from the list below.', 'vlthemes' ),
					'multiple' => 'multiple',
					'value' => array(
						'twitter' => esc_html__( 'Twitter', 'vlthemes' ),
						'facebook' => esc_html__( 'Facebook', 'vlthemes' ),
						'vk' => esc_html__( 'VK', 'vlthemes' ),
						'googleplus' => esc_html__( 'GooglePlus', 'vlthemes' ),
						'linkedin' => esc_html__( 'LinkedIn', 'vlthemes' ),
						'pinterest' => esc_html__( 'Pinterest', 'vlthemes' ),
					),
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'align',
					'heading' => esc_html__( 'Socials Align', 'vlthemes' ),
					'description' => esc_html__( 'Select the position of the socials from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Center', 'vlthemes' ) => 'center',
						esc_html__( 'Left', 'vlthemes' ) => 'left',
						esc_html__( 'Right', 'vlthemes' ) => 'right'
					),
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Social Share', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcSocialShareShortcode;
}
