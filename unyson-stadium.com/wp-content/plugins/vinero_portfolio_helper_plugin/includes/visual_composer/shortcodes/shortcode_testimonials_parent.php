<?php

# Testimonials parent

if ( !class_exists( 'VLThemesVcTestimonialsParentShortcode' ) ) {
	class VLThemesVcTestimonialsParentShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_testimonials_parent';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'dots' => false,
				'dots_color' => '#5b5b5b',
				'loop' => false,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$css_class .= ' vlt-testimonials-parent';
			$loop = $loop ? 'true' : 'false';
			$dots = $dots ? 'true' : 'false';

			ob_start();

			?>

			<div class="<?php echo $css_class; ?>">
				<div class="slick-slider">
					<?php echo do_shortcode( $content ); ?>
				</div>
			</div>

			<style type="text/css">
				:root {
					--dots-color: <?php echo $dots_color; ?>;
				}
			</style>

			<script type="text/javascript">
				jQuery.noConflict()(function($){
					$(".<?php echo $identifier_class; ?> .slick-slider").imagesLoaded( function(){
						$(".<?php echo $identifier_class; ?> .slick-slider").slick({
							infinite: <?php echo $loop; ?>,
							dots: <?php echo $dots; ?>,
							slidesToShow: 1,
							arrows: false,
							speed: 500,
							dotsClass: 'vlt-slick-dots',
							customPaging : function(slider, i) {
	 							return '<span></span>';
   							},
						});
					});
				});
			</script>

			<?php

			wp_enqueue_script('slickSlider');

			return ob_get_clean();
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'checkbox',
					'param_name' => 'dots',
					'heading' => esc_html__( 'Dots', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to show dot indicators.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => false,
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'loop',
					'heading' => esc_html__( 'Loop', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to activate infinite sliding.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => false,
					'group' => esc_html__( 'General', 'vlthemes' )
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'dots_color',
					'heading' => esc_html__( 'Dots Color', 'vlthemes' ),
					'description' => esc_html__( 'Pick a color for the dots.', 'vlthemes' ),
					'value' => '#5b5b5b',
					'group' => esc_html__( 'Style', 'vlthemes' )
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Testimonials Parent', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
				'as_parent' => array( 'only' => 'vlt_testimonials_child' ),
				'is_container' => true,
				'content_element' => true,
				'js_view' => 'VcColumnView',
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcTestimonialsParentShortcode;

	/**
	 * The "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
	 */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Vlt_Testimonials_Parent extends WPBakeryShortCodesContainer {}
	}
}
