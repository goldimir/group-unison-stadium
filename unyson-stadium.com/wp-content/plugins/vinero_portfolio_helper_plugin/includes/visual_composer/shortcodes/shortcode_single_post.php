<?php

# Single post

if ( !class_exists( 'VLThemesVcSinglePostShortcode' ) ) {
	class VLThemesVcSinglePostShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_single_post';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'post' => '',
				'style' => 'default',
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$args = array(
				'post_type' => 'post',
				'p' => $post,
			);
			$new_query = new WP_Query( $args );

			ob_start();

		?>

		<div class="<?php echo $css_class; ?>">
			<?php
				if ( $new_query->have_posts() ):
					while ( $new_query->have_posts() ) : $new_query->the_post();
						get_template_part( 'template-parts/post/post-style', $style );
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>

		<?php

			return ob_get_clean();
		}

		public function vc_map_shortcode() {


			$vc_map = array(
				array(
					'type' => 'vl_select',
					'param_name' => 'post',
					'heading' => esc_html__( 'Select Post', 'vlthemes' ),
					'description' => esc_html__( 'Select a post from the list below..', 'vlthemes' ),
					'value' => VLThemesVcShortcodeHelper::get_posts(
						array(
							'post_type' => 'post',
							'posts_per_page' => -1,
						)
					),
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'style',
					'heading' => esc_html__( 'Post Style', 'vlthemes' ),
					'description' => esc_html__( 'Select a style from the list below.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Default', 'vlthemes' ) => 'default',
						esc_html__( 'Masonry', 'vlthemes' ) => 'masonry',
					),
					'admin_label' => true,
					'group' => esc_html__( 'General', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Single Post', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcSinglePostShortcode;
}
