<?php

# Blog layouts

if ( !class_exists( 'VLThemesVcBlogLayoutsShortcode' ) ) {
	class VLThemesVcBlogLayoutsShortcode extends VLThemesVcShortcode {

		function shortcode_name() {
			return 'vlt_blog_layouts';
		}

		public function __construct() {
			parent::__construct();
		}

		public function register_shortcode( $atts, $content = null ) {
			$identifier_class = uniqid( 'vlthemes_custom_' );
			$output = $el_class = $css = $custom_css = $link_class = $link_start = $link_end = '';

			extract( shortcode_atts( array(
				'layout' => 1,
				'category' => '',
				'ignore' => '',
				'maxposts' => 6,
				'effect' => true,
				'pagination' => 'numeric',
				'insert_number' => '',
				'shortcode_fullwidth' => true,
				'el_class' => '',
				'css' => '',
			), $atts ) );

			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $identifier_class . ' ' . $el_class . ' ' . vc_shortcode_custom_css_class( $css, ' ' ), $this->shortcode_name() );

			$categories = $ignored = '';

			if ( $category ){
				$categories = trim( $category, ' ' );
				$categories = explode( ',', $categories );
			}

			if ( $ignore ){
				$ignored = trim( $ignore, ' ' );
				$ignored = explode( ',', $ignored );
			}

			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : (get_query_var( 'page' ) ? get_query_var( 'page' ) : 1);

			$args = array(
				'post_type' => 'post',
				'cat' => $categories,
				'posts_per_page' => $pagination == 'none' ? -1 : $maxposts,
				'paged' => $paged,
				'post__not_in' => $ignored
			);
			$new_query = new WP_Query( $args );

			$css_class .= ' vlt-posts-layout'.$layout.'-container';
			$css_class .= ' vlt-pagination-'.$pagination.'-container';
			$css_class .= ' clearfix';

			ob_start();

		?>

			<div class="<?php echo $css_class; ?>">

				<!-- Default -->
				<?php if ( $layout == 1 ) : ?>
					<div class="masonry clearfix" data-masonry-col="1" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>
						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">';
								get_template_part( 'template-parts/post/post-style', 'default' );
								echo '</div>';

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Masonry 2 Col -->
				<?php if ( $layout == 2 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">';
								get_template_part( 'template-parts/post/post-style', 'masonry' );
								echo '</div>';

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Masonry 3 Col -->
				<?php if ( $layout == 3 ) : ?>
					<div class="masonry clearfix" data-masonry-col="3" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								echo '<div class="grid-item">';
								get_template_part( 'template-parts/post/post-style', 'masonry' );
								echo '</div>';

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- First Default / Other Masonry 2 Col -->
				<?php if ( $layout == 4 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- First Default / Other Masonry 3 Col -->
				<?php if ( $layout == 5 ) : ?>
					<div class="masonry clearfix" data-masonry-col="3" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<!-- Default / Every second and third post Masonry -->
				<?php if ( $layout == 6 ) : ?>
					<div class="masonry clearfix" data-masonry-col="2" data-appearance-effect="<?php echo esc_attr( $effect ); ?>">
						<div class="gutter-sizer"></div>
						<div class="grid-sizer"></div>

						<?php
							while ( $new_query->have_posts() ) : $new_query->the_post();
								global $post;
								$current = $new_query->current_post + 1;

								if ( $current % 3 == 1 ) {
									echo '<div class="grid-item w100">';
									get_template_part( 'template-parts/post/post-style', 'default' );
									echo '</div>';
								} else {
									echo '<div class="grid-item">';
									get_template_part( 'template-parts/post/post-style', 'masonry' );
									echo '</div>';
								}

								if ( $current == $insert_number ) {
									if ( $shortcode_fullwidth ) {
										echo '<div class="grid-item grid-item--inserted w100">';
									} else {
										echo '<div class="grid-item grid-item--inserted">';
									}
									echo do_shortcode( $content );
									echo '</div>';
								}
							endwhile;
						?>
					</div>
					<!-- /.masonry -->
				<?php endif; ?>

				<?php

					echo vinero_get_pagination( $new_query, $pagination );
					wp_reset_postdata();

				?>

			</div>

		<?php

			return ob_get_clean();
		}

		public function vc_map_shortcode() {

			$vc_map = array(
				array(
					'type' => 'dropdown',
					'param_name' => 'layout',
					'heading' => esc_html__( 'Layout', 'vlthemes' ),
					'description' => esc_html__( 'Select a blog layout from the list below.', 'vlthemes' ),
					'admin_label' => true,
					'value' => array(
						esc_html__( 'Default', 'vlthemes' ) => 1,

						esc_html__( 'Masonry 2 Columns', 'vlthemes' ) => 2,
						esc_html__( 'Masonry 3 Columns', 'vlthemes' ) => 3,

						esc_html__( 'First Post Default / Other Masonry 2 Columns', 'vlthemes' ) => 4,
						esc_html__( 'First Post Default / Other Masonry 3 Columns', 'vlthemes' ) => 5,

						esc_html__( 'Default / Each second and third post Masonry', 'vlthemes' ) => 6
					),
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'pagination',
					'heading' => esc_html__( 'Type navigation', 'vlthemes' ),
					'description' => esc_html__( 'Select a type of navigation or disable it.', 'vlthemes' ),
					'admin_label' => true,
					'value' => array(
						esc_html__( 'Numeric', 'vlthemes' ) => 'numeric',
						esc_html__( 'Paged', 'vlthemes' ) => 'paged',
						esc_html__( 'Load More', 'vlthemes' ) => 'load-more',
						esc_html__( 'None', 'vlthemes' ) => 'none',
					),
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'effect',
					'heading' => esc_html__( 'Appearance Effect', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to activate appearance effect.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => true,
					'group' => esc_html__( 'Style', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'maxposts',
					'heading' => esc_html__( 'Maximum Posts', 'vlthemes' ),
					'description' => esc_html__( 'Select a maximum number of posts (If you select -1 then all posts will be shown).', 'vlthemes' ),
					'admin_label' => true,
					'value' => 6,
					'min' => -1,
					'max' => 999999,
					'step' => 1,
					'group' => esc_html__( 'Loop', 'vlthemes' ),
				),
				array(
					'type' => 'vl_select',
					'param_name' => 'category',
					'heading' => esc_html__( 'Show posts from categories', 'vlthemes' ),
					'description' => esc_html__( 'Enter a categories which you want to display (If this field is empty, all posts will be displayed).', 'vlthemes' ),
					'admin_label' => true,
					'multiple' => 'multiple',
					'value' => VLThemesVcShortcodeHelper::get_terms( 'category' ),
					'group' => esc_html__( 'Loop', 'vlthemes' ),
				),
				array(
					'type' => 'vl_select',
					'param_name' => 'ignore',
					'heading' => esc_html__( 'Ignore Posts', 'vlthemes' ),
					'description' => esc_html__( 'Enter a posts which you want to hide from loop (If this field is empty, all posts will be displayed).', 'vlthemes' ),
					'admin_label' => true,
					'multiple' => 'multiple',
					'value' => VLThemesVcShortcodeHelper::get_posts(
						array(
							'post_type' => 'post',
							'posts_per_page' => -1,
						)
					),
					'group' => esc_html__( 'Loop', 'vlthemes' ),
				),
				array(
					'type' => 'vl_number',
					'param_name' => 'insert_number',
					'heading' => esc_html__( 'Insert Number', 'vlthemes' ),
					'description' => esc_html__( 'Insert a shortcode after the N-th post.', 'vlthemes' ),
					'value' => '',
					'min' => 1,
					'max' => 999999,
					'step' => 1,
					'group' => esc_html__( 'Shortcode', 'vlthemes' ),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'shortcode_fullwidth',
					'heading' => esc_html__( 'Shortcode Fullwidth', 'vlthemes' ),
					'description' => esc_html__( 'Check it if you want to display a shortcode for the full width of the container.', 'vlthemes' ),
					'value' => array(
						esc_html__( 'Enable', 'vlthemes' ) => true
					),
					'std' => true,
					'group' => esc_html__( 'Shortcode', 'vlthemes' ),
				),
				array(
					'type' => 'textfield',
					'param_name' => 'el_class',
					'heading' => esc_html__( 'Extra class', 'vlthemes' ),
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vlthemes' ),
					'value' => '',
					'group' => esc_html__( 'Extras', 'vlthemes' ),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__( 'CSS', 'vlthemes' ),
					'group' => esc_html__( 'Design', 'vlthemes' ),
				)
			);

			vc_map( array(
				'name' => esc_html__( 'Blog Layouts', 'vlthemes' ),
				'base' => $this->shortcode_name(),
				'icon' => vltheme_helper_plugin()->plugin_url . 'assets/img/vc_shortcode.png',
				'category' => esc_html__( 'VLThemes', 'vlthemes' ),
				'params' => $vc_map,
				// 'is_container' => true,
				// 'content_element' => true,
				// 'js_view' => 'VcColumnView',
			) );

		}
	}
	/**
	 * Init class
	 */
	new VLThemesVcBlogLayoutsShortcode;

	/**
	 * The "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
	 */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_Vlt_Blog_Layouts extends WPBakeryShortCodesContainer {}
	}

}
