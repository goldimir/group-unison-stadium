<?php

# Share Buttons
function vlthemes_get_post_share_buttons( $postID ) {
	$url = urlencode( get_permalink( $postID ) );
	$title = urlencode( get_the_title( $postID ) );
	$media = wp_get_attachment_image_src(get_post_thumbnail_id( $postID, 'vinero-full' ) );
	$output = '';
	$output .= '<a class="vlt-single-icon twitter" target="_blank" href="https://twitter.com/home?status='.$title.'+'.$url.'"><i class="fa fa-twitter"></i></a>';
	$output .= '<a class="vlt-single-icon facebook" target="_blank" href="https://www.facebook.com/share.php?u='.$url.'&title='.$title.'"><i class="fa fa-facebook"></i></a>';
	$output .= '<a class="vlt-single-icon pinterest" target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media='.$media[0].'&url='.$url.'&is_video=false&description='.$title.'"><i class="fa fa-pinterest"></i></a>';
	$output .= '<a class="vlt-single-icon google-plus" target="_blank" href="https://plus.google.com/share?url='.$url .'"><i class="fa fa-google-plus"></i></a>';
	$output .= '<a class="vlt-single-icon linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?url='. $url .'&title=' . $title . '"><i class="fa fa-linkedin"></i></a>';

	return apply_filters( 'vlthemes/get_post_share_buttons', $output );
}