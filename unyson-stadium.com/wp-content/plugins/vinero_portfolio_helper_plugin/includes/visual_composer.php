<?php


if ( ! class_exists( 'VLThemesVcShortcode' ) ) {
	abstract class VLThemesVcShortcode {
		
		public function __construct() {
			add_shortcode( $this->shortcode_name() , array( $this, 'register_shortcode' ) );
			add_action( 'vc_before_init', array( $this, 'vc_map_shortcode' ) );
		}

		abstract function shortcode_name();

		public abstract function register_shortcode( $atts, $content = null );

		public abstract function vc_map_shortcode();

	}
}


if ( ! class_exists( 'VLThemesVcShortcodeParams' ) ) {
	class VLThemesVcShortcodeParams {

		public function __construct() {
			vc_add_shortcode_param( 'vl_select', array( $this, 'vl_select' ) );
			vc_add_shortcode_param( 'vl_number', array( $this, 'vl_number' ) );
		}

		public function vl_select( $args, $value ) {
			$selected = is_array( $value ) ? $value : explode( ',', $value );
			$args = wp_parse_args( $args, array(
				'param_name' => '',
				'heading' => '',
				'class' => 'wpb_vc_param_value wpb-input wpb-select dropdown',
				'multiple' => '',
				'size' => '',
				'disabled' => '',
				'selected' => $selected,
				'none' => '',
				'value' => array(),
				'style' => '',
				'empty' => '',
				'format' => 'keyval',
				'noselect' => ''
			) );
			$options = array();
			if( ! is_array($args['value'] ) ){
				$args['value'] = array();
			} 
			if ( $args['param_name'] ) {
				$name = ' name="' . $args['param_name'] . '"';
			}
			if ( $args['param_name'] ){
				$args['param_name'] = ' id="' . $args['param_name'] . '"';  
			}
			if ( $args['class'] ){
				$args['class'] = ' class="' . $args['class'] . '"';
			}
			if ( $args['style'] ){
				$args['style'] = ' style="' . esc_attr( $args['style'] ) . '"';
			}
			if( $args['multiple'] ) {
				$args['multiple'] = ' multiple="multiple"';
			}
			if( $args['disabled'] ) {
				$args['disabled'] = ' disabled="disabled"';
			}
			if ( $args['size'] ) {
				$args['size'] = ' size="' . $args['size'] . '"';
			}
			if ( $args['none'] && $args['format'] === 'keyval' ) {
				$args['options'][0] = $args['none'];
			}
			if ( $args['none'] && $args['format'] === 'idtext' ) {
				array_unshift( $args['options'], array( 'id' => '0', 'text' => $args['none'] ) );
			}

			if ( $args['format'] === 'keyval' ) foreach ( $args['value'] as $id => $text ) {
				$options[] = '<option value="' . (string) $id . '">' . (string) $text . '</option>';
			}elseif ( $args['format'] === 'idtext' ) foreach ( $args['options'] as $option ) {
				if ( isset( $option['id'] ) && isset( $option['text'] ) ){
					$options[] = '<option value="' . (string) $option['id'] . '">' . (string) $option['text'] . '</option>';
				}
			}
			$options = implode('', $options);
			if ( is_array( $args['selected'] ) ) {
				foreach ( $args['selected'] as $key => $value ) {
					$options = str_replace( 'value="' . $value . '"', 'value="' . $value . '" selected="selected"', $options );
				}
			} else {
				$options = str_replace('value="' . $args['selected'] . '"', 'value="' . $args['selected'] . '" selected="selected"', $options);
			}
			$output = ( $args['noselect'] ) ? $options : '<select' .$name. $args['param_name'] . $args['class'] . $args['multiple'] . $args['size'] . $args['disabled'] . $args['style'] . '>' . $options . '</select>';
			return $output;
		}


		public function vl_number( $args, $value ) {
			return '<input name="' . esc_attr( $args['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' . esc_attr( $args['param_name'] ) . ' ' . esc_attr( $args['type'] ) . '_field" type="number" min="'.intval( $args['min'] ).'" max="'.intval( $args['max'] ).'" step="'.intval( $args['step'] ).'" value="' . esc_attr( $value ) . '" />';
		}
	}
	new VLThemesVcShortcodeParams;
}



if ( ! class_exists( 'VLThemesVcShortcodeHelper' ) ) {
	class VLThemesVcShortcodeHelper {

		public function __construct() {
		}

		public static function get_image_sizes() {
			$sizes = get_intermediate_image_sizes();
			array_unshift( $sizes, 'full' );
			return $sizes;
		}

		public static function is_has_link( $link = '' ) {
			$has_link = false;
			if ( !empty( $link ) ){
				$link = vc_build_link( $link );
				if ( strlen( $link['url'] ) > 0 ) {
					$has_link = true;
				}
			}
			return $has_link;
		}

		public static function get_link_attributes( $link = '', $class = '', $style = ''  ) {
			$attributes = array();
			$a_href = $a_title = $a_target = $a_rel = '';
			$use_link = false;
			if ( !empty( $link ) ){
				$link = vc_build_link( $link );
				if ( strlen( $link['url'] ) > 0 ) {
					$use_link = true;
					$a_href = $link['url'];
					$a_title = $link['title'];
					$a_target = $link['target'];
					$a_rel = $link['rel'];
				}
			}
			if ( $use_link ) {
				$attributes[] = 'href="' . esc_url( $a_href ) . '"';
				if ( ! empty( $a_title ) ) {
					$attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
				}
				if ( ! empty( $a_target ) ) {
					$attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
				}
				if ( ! empty( $a_rel ) ) {
					$attributes[] = 'rel="' . esc_attr( trim( $a_rel ) ) . '"';
				}
			} else {
				$attributes[] = 'href="#"';
			}
			if ( ! empty( $class ) ) {
				$attributes[] = 'class="' . esc_attr( $class ) . '"';
			}
			if ( ! empty( $style ) ) {
				$attributes[] = 'style="' . esc_attr( $style ) . '"';
			}
			return $attributes;
		}

		public static function get_sidebar() {
			global $wp_registered_sidebars;
			$dropdown = array();
			foreach( $wp_registered_sidebars as $sidebar ) :
				$dropdown[] = $sidebar['id'];
			endforeach;
			return $dropdown;
		}

		public static function get_posts( $args = array() ) {
			global $wpdb, $post;
			$dropdown = array();
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				while( $the_query->have_posts() ){
					$the_query->the_post(); 
					$dropdown[get_the_ID()] = get_the_title();
				}
			}
			wp_reset_postdata();
			return $dropdown;
		}

		public static function get_terms( $tax = 'category', $key = 'id' ) {
			$terms = array();
			if ( ! taxonomy_exists( $tax ) ) {
				return false;
			}
			if ( $key === 'id' ) {
				foreach( (array) get_terms( $tax, array( 'hide_empty' => true ) ) as $term ){
					$terms[$term->term_id] = $term->name;
				}
			} elseif( $key === 'slug' ) {
				foreach( (array) get_terms( $tax, array( 'hide_empty' => true ) ) as $term ){
					$terms[$term->slug] = $term->name;
				}
			}
			return $terms;
		}
	}
	new VLThemesVcShortcodeHelper;
}