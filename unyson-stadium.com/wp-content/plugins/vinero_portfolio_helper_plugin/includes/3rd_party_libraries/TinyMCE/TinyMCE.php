<?php

/**
 * Add Buttons to TinyMCE
 */

function vlthemes_add_buttons( $plugin_array ) {
	$plugin_array['vlthemes'] = vltheme_helper_plugin()->plugin_url . 'includes/3rd_party_libraries/TinyMCE/TinyMCE.js';
	return $plugin_array;
}

function vlthemes_register_buttons( $buttons ) {
	array_push( $buttons, 'dropcap', 'highlight' );
	return $buttons;
}

add_filter( 'mce_external_plugins', 'vlthemes_add_buttons' );
add_filter( 'mce_buttons', 'vlthemes_register_buttons' );
