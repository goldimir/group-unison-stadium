(function() {
    tinymce.create('tinymce.plugins.VLThemes', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {

            ed.addButton('lead', {
                type: 'menubutton',
                text : 'Lead',
                classes: 'listbox',
                menu: [{
                    text: 'Lead Left',
                    onclick: function() {
                        var return_text = ed.selection.getContent();
                        return_text = '<p class="vlt-lead vlt-lead--left">' + return_text + '</p>';
                        ed.execCommand('mceInsertContent', 0, return_text);
                    }
                }, {
                    text: 'Lead Right',
                    onclick: function() {
                        var return_text = ed.selection.getContent();
                        return_text = '<p class="vlt-lead vlt-lead--right">' + return_text + '</p>';
                        ed.execCommand('mceInsertContent', 0, return_text);
                    }
                }]
            });

        	ed.addButton('dropcap', {
                type: 'menubutton',
                text : 'DropCap',
                classes: 'listbox',
                menu: [{
                    text: 'Default',
                    onclick: function() {
                        var return_text = ed.selection.getContent();
                        return_text = '<p class="vlt-dropcap">' + return_text + '</p>';
                        ed.execCommand('mceInsertContent', 0, return_text);
                    }
                }, {
                    text: 'Bordered',
                    onclick: function() {
                        var return_text = ed.selection.getContent();
                        return_text = '<p class="vlt-dropcap vlt-dropcap--border">' + return_text + '</p>';
                        ed.execCommand('mceInsertContent', 0, return_text);
                    }
                }, {
                    text: 'Rounded',
                    onclick: function() {
                        var return_text = ed.selection.getContent();
                        return_text = '<p class="vlt-dropcap vlt-dropcap--rounded">' + return_text + '</p>';
                        ed.execCommand('mceInsertContent', 0, return_text);
                    } 
                }],
            });
 
        },


        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },
 
        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                longname : 'VLThemes Buttons',
                author : 'VLThemes',
                authorurl : 'http://vlthemes.com',
                infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
                version : '1.0'
            };
        }
    });
 
    // Register plugin
    tinymce.PluginManager.add( 'vlthemes', tinymce.plugins.VLThemes );
})();