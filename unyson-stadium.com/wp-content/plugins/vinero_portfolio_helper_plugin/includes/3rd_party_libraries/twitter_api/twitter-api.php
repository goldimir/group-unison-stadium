<?php


include_once plugin_dir_path( __FILE__ ) . '/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;


function dreamy_get_tweets($consumer_key, $consumer_secret, $access_token, $access_token_secret, $options) {

    if(!empty($consumer_key) && !empty($consumer_key) && !empty($access_token) && !empty($access_token_secret)) {
        $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
        $tweets = $connection->get('statuses/user_timeline', $options);
        return $tweets;
    }

}

