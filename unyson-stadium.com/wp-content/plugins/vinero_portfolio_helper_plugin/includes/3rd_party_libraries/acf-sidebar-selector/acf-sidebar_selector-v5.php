<?php

if(class_exists('acf')):

class acf_field_sidebar_selector extends acf_field {

	function __construct() {

		$this->name = 'sidebar_selector';
		$this->label = esc_html__( 'Sidebar Selector', 'vlthemes' );
		$this->category = esc_html__( 'Choice', 'vlthemes' );
		$this->defaults = array(
			'allow_null' => '1',
			'default_value' => ''
		);


    	parent::__construct();

	}


	function render_field_settings( $field ) {

		acf_render_field_setting( $field, array(
			'label'			=> esc_html__('Allow Null?','vlthemes'),
			'type'			=> 'radio',
			'name'			=> 'allow_null',
			'layout'  =>  'horizontal',
			'choices' =>  array(
				'1' => esc_attr__('Yes', 'vlthemes'),
				'0' => esc_attr__('No', 'vlthemes'),
			)
		));

		acf_render_field_setting( $field, array(
			'label'			=> esc_html__('Default Value','vlthemes'),
			'type'			=> 'text',
			'name'			=> 'default_value',
		));


	}

	function render_field( $field ) {
		global $wp_registered_sidebars;
		?>
		<div>
			<select name='<?php echo $field['name'] ?>'>
				<?php if ( !empty( $field['allow_null'] ) ) : ?>
					<option value=''><?php esc_html_e( 'Select a Sidebar', 'vlthemes' ); ?></option>
				<?php endif ?>
				<?php
					foreach( $wp_registered_sidebars as $sidebar ) :
					$selected = ( ( $field['value'] == $sidebar['id'] ) || ( empty( $field['value'] ) && $sidebar['id'] == $field['default_value'] ) ) ? 'selected="selected"' : '';
				?>
					<option <?php echo $selected ?> value='<?php echo $sidebar['id'] ?>'><?php echo $sidebar['name'] ?></option>
				<?php endforeach; ?>

			</select>
		</div>

		<?php
	}

}

// create field
new acf_field_sidebar_selector();

endif;

?>
