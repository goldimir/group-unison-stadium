<?php

/**
 * Plugin Name: Vinero Helper Plugin
 * Plugin URI: http://vlthemes.com
 * Description: Vinero Helper Plugin expands the functionality of the theme. Adds new icons, shortcodes and much more.
 * Version: 2.1
 * Author: VLThemes
 * Author URI: http://themeforest.net/user/vlthemes
 * License: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: vlthemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! function_exists( 'VLThemesHelperPlugin' ) ) {

	class VLThemesHelperPlugin {

		/**
		 * The single class instance.
		 * @var $_instance
		 */
		private static $_instance = null;

		/**
		 * Main Instance
		 * Ensures only one instance of this class exists in memory at any one time.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
				self::$_instance->init_hooks();
				self::$_instance->theme_init();
				self::$_instance->init_option();
				self::$_instance->init_includes();
			}
			return self::$_instance;
		}

		/**
		 * Path to the plugin directory
		 * @var $plugin_path
		 */
		public $plugin_path;

		/**
		 * URL to the plugin directory
		 * @var $plugin_url
		 */
		public $plugin_url;

		/**
		 * Plugin dashboard suffix for the link
		 * @var $plugin_dashboard_suffix
		 */
		public $plugin_dashboard_suffix;

		/**
		 * Plugin dashboard slug
		 * @var $plugin_dashboard_slug
		 */
		public $plugin_dashboard_slug;


		/**
		 * Plugin name
		 * @var $plugin_name
		 */
		public $plugin_name;

		/**
		 * Plugin version
		 * @var $plugin_version
		 */
		public $plugin_version;

		/**
		 * Plugin slug
		 * @var $plugin_slug
		 */
		public $plugin_slug;

		public function __construct() {
			# We do nothing here!
		}

		public function plugin_init() {
			# General variables of plugin
			$data                 = get_plugin_data( __FILE__ );
			$this->plugin_name    = $data['Name'];
			$this->plugin_version = $data['Version'];
			$this->plugin_slug    = 'vlthemes_helper_plugin';
		}

		public function init_option() {
			# Init plugin vars.
			$this->plugin_path             = plugin_dir_path( __FILE__ );
			$this->plugin_url              = plugin_dir_url( __FILE__ );
			$this->plugin_dashboard_suffix = '';
			$this->plugin_dashboard_slug   = 'theme-dashboard';

			# Load textdomain.
			load_plugin_textdomain( 'vlthemes', false, $this->plugin_path . 'languages/' );

			# Init methods.
			$this->add_image_sizes();

			# Init vc.
			if ( class_exists( 'Vc_Manager' ) ) {
				$this->init_vc();
			}

		}

		public function init_hooks() {
			add_action( 'admin_init', array( $this, 'plugin_init' ) );
			if ( is_admin() ) {
				add_action( 'admin_print_styles', array( $this, 'init_styles' ) );
			}
			add_action( 'widgets_init', array( $this, 'init_widgets' ) );
		}

		public function theme_init() {
			# General variables of theme
			$theme_info   = wp_get_theme();
			$theme_parent = $theme_info->parent();
			if ( !empty( $theme_parent ) ) {
				$theme_info = $theme_parent;
			}
			$this->theme_slug     = $theme_info->get_stylesheet();
			$this->theme_name     = $theme_info['Name'];
			$this->theme_version  = $theme_info['Version'];
			$this->theme_is_child = !empty( $theme_parent );

		}

		public function add_image_sizes() {

		}

		public function init_includes() {

			# Dashboard.
			require_once $this->plugin_path . 'dashboard/dashboard.php';

			# Helper Functions
			require_once $this->plugin_path . 'includes/helper_functions.php';

			# Third-Party Libraries.
			require_once $this->plugin_path . 'includes/3rd_party_libraries/TinyMCE/TinyMCE.php';
			require_once $this->plugin_path . 'includes/3rd_party_libraries/twitter_api/twitter-api.php';
			require_once $this->plugin_path . 'includes/3rd_party_libraries/acf-sidebar-selector/acf-sidebar_selector-v5.php';
			require_once $this->plugin_path . 'includes/3rd_party_libraries/featured-post/featured-post.php';

		}

		# Action Widgets Init
		public function init_widgets() {
			$widgets = array(
				'about' => 'vlthemes_widget_about',
				'subscribe' => 'vlthemes_widget_subscribe',
				'socials' => 'vlthemes_widget_socials',
				'recent-posts' => 'vlthemes_widget_recent_posts',
				'popular-posts' => 'vlthemes_widget_popular_posts',
				'trending-posts' => 'vlthemes_widget_trending_posts',
				'banner' => 'vlthemes_widget_banner',
				'instagram' => 'vlthemes_widget_instagram'
			);
			if ( class_exists( 'acf' ) ) {
				foreach ( $widgets as $key => $value ) {
					require_once $this->plugin_path . 'includes/widgets/' . sanitize_key( $key ) . '.php';
					register_widget( $value );
				}
			}
		}

		public function init_vc() {

			# Visual Composer.
			require_once $this->plugin_path . 'includes/visual_composer.php';
			require_once $this->plugin_path . 'includes/visual_composer_icons.php';

			# Visual Composer Shortcodes.
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_button.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_ads_banner.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_alert_message.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_social_share.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_work_meta.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_hero_header.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_single_image.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_contact_form.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_image_collage.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_partner_logo.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_progress_bar.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_video_link.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_spacer.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_services.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_counter_up.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_custom_title.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_team_member.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_pricing_table.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_google_map.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_icon_link.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_testimonials_parent.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_testimonials_child.php';

			// Blog
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_blog_layouts.php';
			require_once $this->plugin_path . 'includes/visual_composer/shortcodes/shortcode_single_post.php';

		}

		public function init_styles() {
			# Init JS libraries.
			wp_enqueue_script( 'tether', vltheme_helper_plugin()->plugin_url . 'assets/script/tether.min.js', array( 'jquery' ), $this->plugin_version, true );
			wp_enqueue_script( 'drop_js', vltheme_helper_plugin()->plugin_url . 'assets/script/drop.min.js', array( 'jquery' ), $this->plugin_version, true );
			wp_enqueue_script( 'dashboard_js', vltheme_helper_plugin()->plugin_url . 'assets/script/scripts.js', array( 'jquery' ), $this->plugin_version, true );

			# Init CSS stylesheets.
			wp_enqueue_style( 'drop_css', vltheme_helper_plugin()->plugin_url . 'assets/css/drop-theme-twipsy.min.css', array(), $this->plugin_version );
			wp_enqueue_style( 'vlthemes_font', vltheme_helper_plugin()->plugin_url . 'assets/fonts/vlthemes.css', array(), $this->plugin_version );
			wp_enqueue_style( 'dashboard_css', vltheme_helper_plugin()->plugin_url . 'assets/css/style.css', array(), $this->plugin_version );
			wp_enqueue_style( 'admin_css', vltheme_helper_plugin()->plugin_url . 'assets/css/admin.css', array(), $this->plugin_version );
		}

	}

	function vltheme_helper_plugin() {
		return VLThemesHelperPlugin::instance();
	}
	add_action( 'plugins_loaded', 'vltheme_helper_plugin' );

}