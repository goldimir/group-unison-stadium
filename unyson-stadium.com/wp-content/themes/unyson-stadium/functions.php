<?php
/**
 * unyson-stadium functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package unyson-stadium
 */

// add Carbon Fields Plugin
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    require_once( get_template_directory() . '/inc/carbon-fields/vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}

// Add Custom Options for Carbon Fields plugin
add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    require( get_template_directory() . '/inc/custom-fields-options/theme-options.php' );
    require( get_template_directory() . '/inc/custom-fields-options/metabox.php' );
}

/**
 * Implement the Custom Styles feature.
 */
require get_template_directory() . '/inc/custom-styles.php';

/**
 * Implement the Custom Scripts feature.
 */
require get_template_directory() . '/inc/custom-scripts.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Implement the Custom Widgets.
 */
require get_template_directory() . '/inc/custom-widgets.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Implement help functions for develop.
 */
require( get_template_directory() . '/inc/helpers.php' );

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Kirki Customizer additions.
 */
if ( class_exists( 'Kirki' ) ) {
    require get_template_directory() . '/inc/kirki-config.php';
}

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load TGM Plugins.
 */
require get_template_directory() . '/inc/unyson-stadium.php';

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );

 /**
 * Hide Title option.
 */
require get_parent_theme_file_path( '/inc/dojo-digital-hide-title.php' );


function custom_shortcode ( $shortcode , $function ){
	add_shortcode($shortcode , $function);
}
include(get_template_directory() .'/inc/custom-shortcodes/custom_button.php');
// include(get_template_directory() .'/inc/custom-shortcodes/vc_single_image.php');

$attributes = array(
    'type' => 'dropdown',
    'heading' => "Style",
    'param_name' => 'style',
    'value' => array( "one", "two", "three" ),
    'description' => __( "New style attribute", "my-text-domain" )
);
vc_add_param( 'vc_message', $attributes ); // Note: 'vc_message' was used as a base for "Message box" element

$attributes = array(
    'type' => 'checkbox',
    'heading' => "Add caption on image",
    'param_name' => 'style',
    'weight' => 1,
    'value' => array( "caption-on-image" ),
    'description' => __( "Add caption on image", "my-text-domain" )
);
vc_add_param( 'vc_single_image', $attributes ); // Note: 'vc_single_image' was used as a base for "vc_single_image box" element