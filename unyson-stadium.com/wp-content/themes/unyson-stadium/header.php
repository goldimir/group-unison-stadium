<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package unyson-stadium
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700|Open+Sans:400,400i,600,600i,800,800i" rel="stylesheet" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'unyson-stadium' ); ?></a>

    <header id="masthead" class="site-header">
        <div class="site-branding top-logo-menu" style="background: <?php echo get_theme_mod( 'color_setting_rgba', '#FFFFFF' ); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="logo">
                          <?php
                          if ( true == get_theme_mod( 'my_setting', true ) ) : 
                            if( function_exists( 'the_custom_logo' ) ) {
                                if( has_custom_logo() ) {
                                    the_custom_logo();
                                }
                            }
                           ?>
                          <?php else : ?>
                            <h2 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
                          <?php
                            $description = get_bloginfo( 'description', 'display' );
                          if ( $description || is_customize_preview() ) : ?>
                                <p class="site-description"><?php echo $description; ?></p>
                              <?php
                            endif;
                          endif;
                          ?>
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-10">
                      <!-- #site-navigation -->
                    <?php if ( has_nav_menu( 'top-menu' ) ) : ?>
                      <div class="navigation-top js">
                          <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">
                            <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
                              <?php
                              echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
                              echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
                              _e( 'Menu', 'twentyseventeen' );
                              ?>
                            </button>

                            <?php wp_nav_menu( array(
                              'theme_location' => 'top-menu',
                              'menu_id'        => 'top-menu',
                            ) ); ?>

                          </nav>
                      </div><!-- .navigation-top -->
                    <?php endif; ?>

                    </div>
                </div>
            </div>
        </div><!-- .site-branding -->
    </header><!-- #masthead -->

    <div id="content" class="site-content">
