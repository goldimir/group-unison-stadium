<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', __( 'Page Options', 'crb' ) )
    ->where( 'post_type', '=', 'page' ) // only show our new fields on pages
    ->add_fields( array(
        Field::make( 'complex', 'crb_slides', 'Slides' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'title', 'Title' ),
                Field::make( 'color', 'title_color', 'Title Color' ),
                Field::make( 'image', 'image', 'Image' ),
            ) ),
    ) );




Container::make( 'post_meta', 'Custom Data' )
    ->where( 'post_type', '=', 'page' )
    ->set_context( 'advanced' )
    ->set_priority( 'core' )
    ->add_fields( array(
        Field::make( 'map', 'crb_location' )
            ->set_position( 37.423156, -122.084917, 14 ),
        Field::make( 'sidebar', 'crb_custom_sidebar' ),
        Field::make( 'image', 'crb_photo' ),
    ));







