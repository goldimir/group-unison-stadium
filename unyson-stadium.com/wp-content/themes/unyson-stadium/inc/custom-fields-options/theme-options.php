<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

// Default options page
Container::make( 'theme_options', 'Basic Options' )
    ->add_tab( __('Header'), array(
        Field::make( 'text', 'crb_first_name', 'First Name' ),
        Field::make( 'text', 'crb_last_name', 'Last Name' ),
        Field::make( 'text', 'crb_position', 'Position' ),
    ) )
    ->add_tab( __('Footer'), array(
        Field::make( 'text', 'crb_email', 'Notification Email' ),
        Field::make( 'text', 'crb_phone', 'Phone Number' ),
    ) )


    ->add_tab( __('Footer2'), array(
        Field::make( 'complex', 'crb_complex', 'My Complex Field' )
            ->add_fields( array(
                Field::make( 'text', 'my_text_field', 'My Text Field' )
            ) )
            ->set_default_value( array(
                array(
                    'my_text_field' => 'Hello',
                ),
                array(
                    'my_text_field' => 'World!',
                ),
            ) ),
    ) );


Container::make('nav_menu_item', 'Menu Settings')
    ->add_fields(array(
        Field::make('color', 'crb_color'),
    ));



// Container::make( 'nav_menu_item', __( 'Menu Settings' ) )
//     ->add_fields( array(
//         Field::make( 'color', 'crb_color', __( 'Color' ) ),
//     ));

// // Default options page
// $basic_options_container = Container::make( 'theme_options', 'Basic Options' )
//     ->add_fields( array(
//         Field::make( 'header_scripts', 'crb_header_script' ),
//         Field::make( 'footer_scripts', 'crb_footer_script' ),
//     ) );

// // Add second options page under 'Basic Options'
// Container::make( 'theme_options', 'Social Links' )
//     ->set_page_parent( $basic_options_container ) // reference to a top level container
//     ->add_fields( array(
//         Field::make( 'text', 'crb_facebook_link' ),
//         Field::make( 'text', 'crb_twitter_link' ),
//     ) );

// // Add third options page under "Appearance"
// Container::make( 'theme_options', 'Customize Background' )
//     ->set_page_parent( 'themes.php' ) // identificator of the "Appearance" admin section
//     ->add_fields( array(
//         Field::make( 'color', 'crb_background_color' ),
//         Field::make( 'image', 'crb_background_image' ),
//     ) );