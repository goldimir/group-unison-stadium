<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Enqueue scripts.
 */
function unyson_stadium_scripts() {
    wp_enqueue_script( 'bootstrap-bundle-js', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery'), '4.1.1', true);
    wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

     // wp_enqueue_script( 'customizer-js', get_theme_file_uri( '/assets/js/customizer.js' ), array(), '1.0', true );

    $twentyseventeen_l10n = array(
        'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),
    );

    if ( has_nav_menu( 'top-menu' ) ) {
        wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
        $twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );
        $twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );
        $twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
    }

    wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'unyson_stadium_scripts' );