<?php
/**
 * unyson-stadium Theme Customizer
 *
 * @package unyson-stadium
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function unyson_stadium_customize_register( $wp_customize ) {
    $wp_customize->get_section( 'title_tagline' )->title = __( 'Logo & Icon' );
    $wp_customize->get_section( 'colors' )->title = __( 'Site Colors' );

    $wp_customize->get_section( 'title_tagline' )->priority = __( 0 );
    $wp_customize->get_section( 'colors' )->priority = __( 10 );
    
    // $wp_customize->remove_control( 'blogname' );
    // $wp_customize->remove_control( 'blogdescription' );

    $wp_customize->get_control( 'blogdescription' )->label = __( 'Site Slogan' );
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    if ( isset( $wp_customize->selective_refresh ) ) {
        $wp_customize->selective_refresh->add_partial( 'blogname', array(
            'selector'        => '.site-title a',
            'render_callback' => 'unyson_stadium_customize_partial_blogname',
        ) );
        $wp_customize->selective_refresh->add_partial( 'blogdescription', array(
            'selector'        => '.site-description',
            'render_callback' => 'unyson_stadium_customize_partial_blogdescription',
        ) );
    }

// NEW SECTIONS
    $wp_customize->add_section( 'new_section_header_textcolor' , array(
        'title'      => __( 'My Section Header Textcolor', 'unyson-stadium' ),
        'priority'   => 10,
    ) );

// SETTINGS
    $wp_customize->add_setting( 'head_background_color' , array(
        'default'   => '#fff',
        'transport' => 'postMessage',
    ) );
    $wp_customize->add_setting( 'main_background_color' , array(
        'default'   => '#fff',
        'transport' => 'postMessage',
    ) );
    $wp_customize->add_setting( 'footer_background_color' , array(
        'default'   => '#C5161D',
        'transport' => 'postMessage',
    ) );

// CONTROLS
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_back_color', array(
        'label'      => __( 'Head Background Color', 'unyson-stadium' ),
        'section'    => 'colors',
        'settings'   => 'head_background_color',
    ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_back_color', array(
        'label'      => __( 'Main Background Color', 'unyson-stadium' ),
        'section'    => 'colors',
        'settings'   => 'main_background_color',
    ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_back_color', array(
        'label'      => __( 'Footer Background Color', 'unyson-stadium' ),
        'section'    => 'colors',
        'settings'   => 'footer_background_color',
    ) ) );

}
add_action( 'customize_register', 'unyson_stadium_customize_register' );

// ADD STYLES
function mytheme_customize_css()
{
    ?>
        <!--Customizer CSS--> 
         <style type="text/css">
             .top-logo-menu { background-color: <?php echo get_theme_mod('head_background_color'); ?>; }
             body { background-color: <?php echo get_theme_mod('main_background_color'); ?>; }
             footer { background-color: <?php echo get_theme_mod('footer_background_color'); ?>; }
         </style>
         <!--/Customizer CSS--> 
    <?php
}
add_action( 'wp_head', 'mytheme_customize_css');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function unyson_stadium_customize_partial_blogname() {
    bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function unyson_stadium_customize_partial_blogdescription() {
    bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function unyson_stadium_customize_preview_js() {
    wp_enqueue_script( 'unyson-stadium-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'unyson_stadium_customize_preview_js' );
