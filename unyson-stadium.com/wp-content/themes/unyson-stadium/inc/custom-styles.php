<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Enqueue styles.
 */
function unyson_stadium_styles() {
    wp_enqueue_style( 'unyson-stadium-style', get_template_directory_uri() . '/assets/css/theme.css', array( 'bootstrap-css' ) );
    wp_enqueue_style( 'themify-page-builder-style', get_template_directory_uri() . '/assets/css/themify-custom.css' );
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'dashicons' );
}
add_action( 'wp_enqueue_scripts', 'unyson_stadium_styles' );

/**
 * Enqueue Customizer Preview styles.
 */
function unyson_stadium_customize_preview_styles() {
    wp_enqueue_style( 'unyson-stadium-customizer-preview-style', get_template_directory_uri() . '/assets/css/customizer-preview.css', array() );
}
add_action( 'customize_preview_init', 'unyson_stadium_customize_preview_styles' );