<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Render the functions for the selective refresh partial.
 */
function unyson_stadium_customize_partial_blogname() {
    bloginfo( 'name' );
}
function unyson_stadium_customize_partial_blogdescription() {
    bloginfo( 'description' );
}

/**
 * Remove options from admin panel and Customizer
 */
add_action( 'after_setup_theme','remove_twentyeleven_options', 100 );
function remove_twentyeleven_options() {    
    remove_theme_support( 'custom-background' );
    remove_theme_support( 'custom-header' );
}



/**
 * Edit some default customizer settings
 */
function unyson_stadium_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_control( 'blogdescription' )->label = __( 'Site Slogan' );
    $wp_customize->selective_refresh->add_partial( 'blogname', array(
        'selector' => '.site-title a',
        'render_callback' => 'unyson_stadium_customize_partial_blogname',
    ) );
    $wp_customize->selective_refresh->add_partial( 'blogdescription', array(
        'selector' => '.site-description',
        'render_callback' => 'unyson_stadium_customize_partial_blogdescription',
    ) );

}
add_action( 'customize_register', 'unyson_stadium_customize_register', 50 );

/*
Kirki Config Options
*/
Kirki::add_config( 'my_theme_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

Kirki::add_panel( 'general_panel_id', array(
    'title'       => esc_attr__( 'Unyson Theme', 'unyson' ),
    'priority'    => 0,
) );

/*
Logo\Title\Favicon Section
*/
Kirki::add_section( 'title_tagline', array(
    'title'          => esc_attr__( 'Logo, Title, Favicon', 'unyson' ),
    'description'    => esc_attr__( 'Customize Your Logo, Favicon & Title.', 'unyson' ),
    'panel'          => 'general_panel_id',
    'priority'       => 0,
) );
Kirki::add_field( 'toggle_logo_id', array(
    'type'        => 'toggle',
    'settings'    => 'my_setting',
    'label'       => esc_attr__( 'Use Image for Logo?', 'unyson' ),
    'description'    => esc_attr__( 'If unchecked, plain text will be used instead. (Title and Slogan will be used)', 'unyson' ),
    'section'     => 'title_tagline',
    'default'     => '1',
    'priority'    => 8,
) );

/*
Colors Section
*/
Kirki::add_section( 'color_setting', array(
    'title'          => esc_attr__( 'Theme Colors', 'unyson' ),
    'description'    => esc_attr__( 'Color Setting.', 'unyson' ),
    'panel'          => 'general_panel_id',
    'priority'       => 0,
) );
Kirki::add_field( 'my_theme_config_id', array(
    'type'        => 'color',
    'settings'    => 'color_setting_rgba',
    'label'       => __( 'Color Control (with alpha channel)', 'unyson' ),
    'description' => esc_attr__( 'This is a color control - with alpha channel.', 'unyson' ),
    'section'     => 'color_setting',
    'default'     => '#fff',
    'transport'   => 'postMessage',
    'choices'     => array(
        'alpha' => true,
    ),
) );

/*
Homepage Settings Section
*/
Kirki::add_section( 'static_front_page', array(
    'title'          => esc_attr__( 'Homepage Settings', 'unyson' ),
    'description'    => esc_attr__( 'You can choose what’s displayed on the homepage of your site. It can be posts in reverse chronological order (classic blog), or a fixed/static page. To set a static homepage, you first need to create two Pages. One will become the homepage, and the other will be where your posts are displayed.', 'unyson' ),
    'panel'          => 'general_panel_id',
    'priority'       => 0,
) );

/*
Menus Section
*/
Kirki::add_panel( 'nav_menus', array(
    'title'          => esc_attr__( 'Menus', 'unyson' ),
    'description'    => esc_attr__( '', 'unyson' ),
    'panel'          => 'general_panel_id',
    'priority'       => 0,
) );















Kirki::add_panel( 'panel_general', array(
    'title' => esc_html__( 'General Options', 'unyson' ),
    'priority' => 0,
    // 'icon' => 'dashicons-admin-generic',
) );

// Kirki::add_section( 'section_general', array(
//     'panel' => 'panel_general',
//     'title' => esc_html__( 'General Options', 'unyson' ),
//     'priority' => 10,
//     // 'icon' => 'dashicons-admin-generic',
// ) );


Kirki::add_section( 'general_selection', array(
    'panel' => 'nav_menus',
    'title' => esc_html__( 'Menu Options', 'unyson' ),
    'priority' => 10,
    'icon' => 'dashicons-editor-underline',
) );

# Selection

// Kirki::add_field( 'theme_config_id', [
//     'type'        => 'toggle',
//     'settings'    => 'my_setting',
//     'label'       => esc_html__( 'Add icon to menu items', 'unyson' ),
//     'section'     => 'general_selection',
//     'default'     => '0',
//     'priority'    => 9,
// ] );

Kirki::add_field( 'theme_config_id', [
    'type'        => 'switch',
    'settings'    => 'icco_setting',
    'label'       => esc_html__( 'Add icon to menu items', 'unyson' ),
    'section'     => 'general_selection',
    'default'     => '0',
    'priority'    => 9,
    'choices'     => [
        'on'  => esc_html__( 'Enable', 'unyson' ),
        'off' => esc_html__( 'Disable', 'unyson' ),
    ],
] );

Kirki::add_field( 'dashicons_config_id', [
    'type'     => 'dashicons',
    'settings' => 'icons_setting',
    'label'    => esc_html__( 'Add Icons to menu', 'unyson' ),
    'section'  => 'general_selection',
    'default'  => 'menu',
    'priority' => 10,
    'output' => array(
        array(
            'element' => '#site-navigation',
            'property' => 'max-width'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'icco_setting',
            'operator' => '==',
            'value' => 1
        )
    )
] );






// Kirki::add_field( 'vinero_customize', array(
//     'type' => 'switch',
//     'settings' => 'boxed_mode',
//     'section' => 'general_selection',
//     'label' => esc_html__( 'Boxed Mode', 'vinero' ),
//     'description' => esc_html__( 'Switch "Enable" if you want to activate "Boxed Mode".', 'vinero' ),
//     'priority' => 5,
//     'transport' => 'auto',
//     'choices' => array(
//         'on' => esc_html__( 'Enable', 'vinero' ),
//         'off' => esc_html__( 'Disable', 'vinero' )
//     ),
//     'default' => 0
// ) );

// Kirki::add_field( 'vinero_customize', array(
//     'type' => 'dimension',
//     'settings' => 'boxed_mode_width',
//     'section' => 'general_selection',
//     'label' => esc_html__( 'Max Width of Content', 'vinero' ),
//     'description' => esc_html__( 'Controls the overall content width. (in px)', 'vinero' ),
//     'priority' => 6,
//     'transport' => 'auto',
//     'default' => '1200px',
//     'output' => array(
//         array(
//             'element' => '.vlt-entry-site--boxed',
//             'property' => 'max-width'
//         )
//     ),
//     'active_callback' => array(
//         array(
//             'setting' => 'boxed_mode',
//             'operator' => '==',
//             'value' => 1
//         )
//     )
// ) );


































Kirki::add_section( 'general_scrollbar', array(
    'panel' => 'panel_general',
    'title' => esc_html__( 'Scrollbar', 'unyson' ),
    'priority' => 10,
    // 'icon' => 'dashicons-sort',
) );

# Scrollbar

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
    'type' => 'switch',
    'settings' => 'custom_scrollbar',
    'section' => 'general_scrollbar',
    'label' => esc_html__( 'Custom Scrollbar', 'vinero' ),
    'description' => esc_html__( 'Switch "YES" if you want to configure the scrollbar.', 'vinero' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'on' => esc_html__( 'Yes', 'vinero' ),
        'off' => esc_html__( 'No', 'vinero' )
    ),
    'default' => '0',
) );

Kirki::add_field( 'vinero_customize', array(
    'type' => 'color',
    'settings' => 'custom_scrollbar_bg',
    'section' => 'general_scrollbar',
    'label' => esc_html__( 'Bar Background', 'vinero' ),
    'description' => esc_html__( 'Select background color for scrollbar.', 'vinero' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => false
    ),
    'default' => '',
    'output' => array(
        array(
            'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar',
            'property' => 'background-color'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'custom_scrollbar',
            'operator' => '==',
            'value' => '1'
        )
    )
) );

Kirki::add_field( 'vinero_customize', array(
    'type' => 'color',
    'settings' => 'custom_scrollbar_color',
    'section' => 'general_scrollbar',
    'label' => esc_html__( 'Bar Color', 'vinero' ),
    'description' => esc_html__( 'Select color for scrollbar.', 'vinero' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => false
    ),
    'default' => '',
    'output' => array(
        array(
            'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar-thumb',
            'property' => 'background-color'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'custom_scrollbar',
            'operator' => '==',
            'value' => '1'
        )
    )
) );

Kirki::add_field( 'vinero_customize', array(
    'type' => 'slider',
    'settings' => 'custom_scrollbar_width',
    'section' => 'general_scrollbar',
    'label' => esc_html__( 'Bar Width', 'vinero' ),
    'description' => esc_html__( 'Select the thickness of the scrollbar.', 'vinero' ),
    'priority' => $priority++,
    'transport' => 'auto',
    'choices' => array(
        'min' => '0',
        'max' => '16',
        'step' => '2'
    ),
    'default' => '',
    'output' => array(
        array(
            'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar',
            'property' => 'width',
            'units' => 'px'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'custom_scrollbar',
            'operator' => '==',
            'value' => '1'
        )
    )
) );
















Kirki::add_section( 'general_login_logo', array(
    'panel' => 'panel_general',
    'title' => esc_html__( 'Login Page', 'unyson' ),
    'priority' => 10,
    // 'icon' => 'dashicons-shield',
) );

// Kirki::add_panel( 'general_panel_id', array(
//     'title'       => esc_attr__( 'Unyson Theme', 'unyson' ),
//     'priority'    => 0,
// ) );
// Kirki::add_section( 'icons_setting', array(
//     'title'          => esc_attr__( 'Menu Icons', 'unyson' ),
//     'description'    => esc_attr__( 'Color Setting.', 'unyson' ),
//     'panel'          => 'general_panel_id',
//     'priority'       => 0,
// ) );

// Kirki::add_field( 'my_theme_config_id', array(
//     'type'        => 'color',
//     'settings'    => 'color_setting_rgba',
//     'label'       => __( 'Color Control (with alpha channel)', 'unyson' ),
//     'description' => esc_attr__( 'This is a color control - with alpha channel.', 'unyson' ),
//     'section'     => 'color_setting',
//     'default'     => '#fff',
//     'transport'   => 'postMessage',
//     'choices'     => array(
//         'alpha' => true,
//     ),
// ) );






// Kirki::add_field( 'new_txt_menu', [
//     'type'     => 'text',
//     'settings' => 'my_setting',
//     'label'    => esc_html__( 'Text Menu', 'unyson' ),
//     'section'  => 'nav_menu_item',
//     'default'  => esc_html__( 'This is a default value', 'unyson' ),
//     'priority' => 10,
// ] );





/*
Widgets Section
*/
Kirki::add_panel( 'widgets', array(
    'title'          => esc_attr__( 'Widgets', 'unyson' ),
    'description'    => esc_attr__( '', 'unyson' ),
    'panel'          => 'general_panel_id',
    'priority'       => 0,
) );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function unyson_stadium_customize_preview_js() {
    wp_enqueue_script( 'unyson-stadium-kirki-customizer', get_template_directory_uri() . '/assets/js/kirki-customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'unyson_stadium_customize_preview_js' );














function nssra_customizer_options( $wp_customize ) {
    // add a custom section
    $wp_customize->add_section( 'nav_menus', array(
        'title' => __( 'Custom Section Title', 'nssra' ),
        'panel' => 'nav_menus'
    ) );

    // add "menu primary flex" checkbox setting
    $wp_customize->add_setting( 'menu_primary_flex', array(
        'capability'        => 'edit_theme_options',
        'default'           => '1',
        'sanitize_callback' => 'nssra_sanitize_checkbox',
    ) );

    // add 'menu primary flex' checkbox control
    $wp_customize->add_control( 'menu_primary_flex', array(
        'label'    => __( 'Stretch the primary menu to fill the available space.', 'nssra' ),
        'section'  => 'nav_menus',
        'settings' => 'menu_primary_flex',
        'std'      => '1',
        'type'     => 'checkbox',
        'priority' => 1,
    ));
}
add_action( 'customize_register', 'nssra_customizer_options' );

// sanitize checkbox fields
function nssra_sanitize_checkbox( $input, $setting ) {
    return sanitize_key( $input ) === '1' ? 1 : 0;
}