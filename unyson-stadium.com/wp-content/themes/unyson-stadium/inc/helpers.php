<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Debug function print_r
if ( ! function_exists( 'get_pr' ) ) {
    function get_pr( $var, $die = true ){
        echo '<pre>';
        print_r( $var );
        echo '</pre>';
        if ( $die ) {
            die();
        }
    }
}

// Debug function var_dump
if ( ! function_exists( 'get_vd' ) ) {
    function get_vd( $var, $die = true ){
        echo '<pre>';
        var_dump( $var );
        echo '</pre>';
        if ( $die ) {
            die();
        }
    }
}