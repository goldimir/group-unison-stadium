<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title'         => __('HTML Box', '{domain}'),
        'description'   => __('Add a Custom HTML', '{domain}'),
        'tab'           => __('Content Elements', '{domain}'),
        'popup_size'    => 'small', // can be large, medium or small
    )
);