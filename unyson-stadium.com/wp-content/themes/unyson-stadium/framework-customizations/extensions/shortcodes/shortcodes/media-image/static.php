<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

wp_enqueue_style( 
	'media-image-css',
	 get_template_directory_uri() . '/framework-customizations/extensions/shortcodes/shortcodes/media-image/static/css/media-image-style.css'
);