<?php if (!defined('FW')) die('Forbidden');

$html_box = ( isset( $atts['htmlbox'] ) && $atts['htmlbox'] ) ? $atts['htmlbox'] : '';
$add_class = ( isset( $atts['addclass'] ) && $atts['addclass'] ) ? $atts['addclass'] : '';

?>

<div class="custom-html-box <?php echo $add_class;?>"><?php echo $html_box;?></div>