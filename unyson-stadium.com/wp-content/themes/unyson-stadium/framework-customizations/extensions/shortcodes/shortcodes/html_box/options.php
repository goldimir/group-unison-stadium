<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'htmlbox' => array(
	    'type'  => 'textarea',
	    'value' => 'default hidden value',
	    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
	    'label' => __('Write your beautiful code', '{domain}'),
	    'desc'  => __('', '{domain}'),
	    'help'  => __('Help tip', '{domain}'),
	    'html'  => 'My <b>custom</b> <em>HTML</em>',
	),
	'addclass' => array(
	    'type'  => 'text',
	    'label' => __('Add Class', '{domain}'),
	),
);
