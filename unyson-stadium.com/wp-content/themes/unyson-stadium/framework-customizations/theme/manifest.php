<?php if (!defined('FW')) die('Forbidden');

$manifest = array(); 

$manifest['supported_extensions'] = array(
	'page-builder' => array(),
	'slider' => array(),
);

$manifest['name'] = __('Unyson', 'fw');

$manifest['version'] = '2.7.12';