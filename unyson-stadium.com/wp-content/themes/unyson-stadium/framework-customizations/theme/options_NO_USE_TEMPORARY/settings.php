<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'box_id' => array(
        'type' => 'box',
        'options' => array(
            'option_id'  => array( 'type' => 'text' ),
        ),
        'title' => __('Box Title', '{domain}'),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),

        /**
         * When used in Post Options on the first array level
         * the ``box`` container accepts additional parameters
         */
        //'context' => 'normal|advanced|side',
        //'priority' => 'default|high|core|low',
    ),

    'tab_id' => array(
        'type' => 'tab',
        'options' => array(
            'option_id'  => array( 'type' => 'text' ),
        ),
        'title' => __('Tab Title', '{domain}'),
            'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),
    'tab_id_2' => array(
        'type' => 'tab',
        'options' => array(
            'option_id_2'  => array( 'type' => 'text' ),
        ),
        'title' => __('Tab Title #2', '{domain}'),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),

    'group_id' => array(
        'type' => 'group',
        'options' => array(
            'option_id'  => array( 'type' => 'text' ),
        ),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),

    'popup_id' => array(
        'type' => 'popup',
        'options' => array(
            'option_id'  => array( 'type' => 'text' ),
        ),
        'title' => __('Button and Popup Title', '{domain}'),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
        'modal-size' => 'small', // small, medium, large
        'desc' => __('Button Description', '{domain}'),
    ),
);


