/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

    wp.customize( 'color_setting_rgba', function( value ) {

        // When the value changes.
        value.bind( function( newval ) {

            // Add CSS to elements.
            // jQuery( '.site-branding' ).css( 'color', newval );
            jQuery( '.site-branding' ).css( 'background-color', newval );
        } );

    } );
    // Site title and description.
    wp.customize( 'blogname', function( value ) {
        value.bind( function( to ) {
            $( '.site-title a' ).text( to );
        } );
    } );
    wp.customize( 'blogdescription', function( value ) {
        value.bind( function( to ) {
            $( '.site-description' ).text( to );
        } );
    } );


wp.customize( 'color_setting_hex', function( value ) {

    // When the value changes.
    value.bind( function( value ) {

        // Add CSS to elements.
        jQuery( '.main-navigation > div > ul > li > a' ).css( 'color', value );
    } );
} );

    // // Header text color.
    // wp.customize( 'header_textcolor', function( value ) {
    //     value.bind( function( to ) {
    //         if ( 'blank' === to ) {
    //             $( '.site-title, .site-description' ).css( {
    //                 'clip': 'rect(1px, 1px, 1px, 1px)',
    //                 'position': 'absolute'
    //             } );
    //         } else {
    //             $( '.site-title, .site-description' ).css( {
    //                 'clip': 'auto',
    //                 'position': 'relative'
    //             } );
    //             $( '.site-title a, .site-description' ).css( {
    //                 'color': to
    //             } );
    //         }
    //     } );
    // } );

    // //Update site background color...
    // wp.customize( 'head_background_color', function( value ) {
    //     value.bind( function( to ) {
    //         $('.top-logo-menu').css('background-color', to );
    //     } );
    // } );
    // wp.customize( 'main_background_color', function( value ) {
    //     value.bind( function( to ) {
    //         $('body').css('background-color', to );
    //     } );
    // } );
    // wp.customize( 'footer_background_color', function( value ) {
    //     value.bind( function( to ) {
    //         $('footer').css('background-color', to );
    //     } );
    // } );
} )( jQuery );
