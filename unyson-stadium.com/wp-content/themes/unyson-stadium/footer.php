<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package unyson-stadium
 */

?>

    </div><!-- #content -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <?php
                      wp_nav_menu( array(
                          'theme_location' => 'footer-menu',
                          'menu_id'        => 'sec-menu',
                          'menu_class'     => 'footer-nav',
                          'container'      => 'nav',
                      ) );
                    ?>
                    <div class="footer-address">
                        <button>Contact Us</button>
                        <p>LiVE! Management Ltd. Wiesenstrasse 17. 8008 Zurich/Switzerland</p>
                        <ul class="contact-info">
                            <li><a href="mailto:info@live-stadium.com">info@live-stadium.com</a></li>
                            <li><a href="tel:+41432996900">+41 43 299 69 00</a></li>
                        </ul>
                    </div>
                    <div class="row footer-bottom-menu">
                        <div class="col-sm-4">
                            <p>&copy; 2016 Live! All rights reserved</p>
                        </div>
                        <div class="col-sm-8">
                            <?php
                              wp_nav_menu( array(
                                  'theme_location' => 'customer-menu',
                                  'menu_id'        => 'footer-customer-nav',
                                  'menu_class'     => 'footer-bottom-nav',
                                  'container'      => 'nav',
                              ) );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- #footer -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
