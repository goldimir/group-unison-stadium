<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

# Header General

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
	'type' => 'select',
	'settings' => 'header_layout',
	'section' => 'section_header_general',
	'label' => esc_html__( 'Header layout', 'vinero' ),
	'description' => esc_html__( 'Select header layout from the list below.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'default' => esc_html__( 'Default', 'vinero' ),
		'fullscreen' => esc_html__( 'Fullscreen', 'vinero' ),
		'aside' => esc_html__( 'Aside', 'vinero' ),
	),
	'default' => 'fullscreen',
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'header_sticky_mode',
	'section' => 'section_header_general',
	'label' => esc_html__( 'Sticky Mode', 'vinero' ),
	'description' => esc_html__( 'Switch "Enable" if you want to activate "Sticky Mode".', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Enable', 'vinero' ),
		'off' => esc_html__( 'Disable', 'vinero' )
	),
	'default' => 0,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'custom',
	'settings' => 'shg_' . $priority++,
	'section' => 'section_header_general',
	'default' => '<div class="kirki-separator">'.esc_html__( 'Logo', 'vinero' ).'</div>',
	'priority' => $priority++,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'image',
	'settings' => 'header_default_logo',
	'section' => 'section_header_general',
	'label' => esc_html__( 'Logo', 'vinero' ),
	'description' => esc_html__( 'Choose a logo image to display for header.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '',
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'dimension',
	'settings' => 'header_logo_height',
	'section' => 'section_header_general',
	'label' => esc_html__( 'Logo height', 'vinero' ),
	'description' => esc_html__( 'Enter the height of the logo.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '',
	'output' => array(
		array(
			'element' => '.vlt-header .vlt-header__logo img, .vlt-footer .vlt-footer__logo img',
			'property' => 'height'
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'custom',
	'settings' => 'shg_' . $priority++,
	'section' => 'section_header_general',
	'default' => '<div class="kirki-separator">'.esc_html__( 'Additional', 'vinero' ).'</div>',
	'priority' => $priority++,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'header_show_social',
	'section' => 'section_header_general',
	'label' => esc_html__( 'Show Socials', 'vinero' ),
	'description' => esc_html__( 'Switch "Show" if you want to display "Socials Lists".', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on'  => esc_html__( 'Show', 'vinero' ),
		'off' => esc_html__( 'Hide', 'vinero' )
	),
	'default' => 1
) );