<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

# General

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
	'type' => 'custom',
	'settings' => 'heading' . $priority++,
	'section' => 'core_general',
	'default' => '<div class="kirki-separator">'.esc_html__( 'Site Layout', 'vinero' ).'</div>',
	'priority' => $priority++,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'dimension',
	'settings' => 'container_width',
	'section' => 'core_general',
	'label' => esc_html__( 'Max Width of Container', 'vinero' ),
	'description' => esc_html__( 'Controls the overall container width. (in px)', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '1140px',
	'output' => array(
		array(
			'element' => '.container',
			'property' => 'max-width'
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'background',
	'settings' => 'body_background',
	'section' => 'core_general',
	'label' => esc_html__( 'Body Background', 'vinero' ),
	'description' => esc_html__( 'Select background for body.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => array(
		'background-color' => '#ffffff',
		'background-image' => '',
		'background-repeat' => 'repeat',
		'background-position' => 'center center',
		'background-size' => 'cover',
		'background-attachment' => 'scroll',
	),
	'output' => array(
		array(
			'element' => 'body',
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'custom',
	'settings' => 'sg_' . $priority++,
	'section' => 'core_general',
	'default' => '<div class="kirki-separator">'.esc_html__( 'Preloader', 'vinero' ).'</div>',
	'priority' => $priority++,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'show_preloader',
	'section' => 'core_general',
	'label' => esc_html__( 'Show Preloader', 'vinero' ),
	'description' => esc_html__( 'Switch "Show" if you want to show preloader.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Show', 'vinero' ),
		'off' => esc_html__( 'Hide', 'vinero' )
	),
	'default' => 1
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'custom',
	'settings' => 'sg_' . $priority++,
	'section' => 'core_general',
	'default' => '<div class="kirki-separator">'.esc_html__( 'Additional', 'vinero' ).'</div>',
	'priority' => $priority++,
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'boxed_mode',
	'section' => 'core_general',
	'label' => esc_html__( 'Boxed Mode', 'vinero' ),
	'description' => esc_html__( 'Switch "Enable" if you want to activate "Boxed Mode".', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Enable', 'vinero' ),
		'off' => esc_html__( 'Disable', 'vinero' )
	),
	'default' => 0
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'dimension',
	'settings' => 'boxed_mode_width',
	'section' => 'core_general',
	'label' => esc_html__( 'Max Width of Content', 'vinero' ),
	'description' => esc_html__( 'Controls the overall content width. (in px)', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '1200px',
	'output' => array(
		array(
			'element' => '.vlt-entry-site--boxed',
			'property' => 'max-width'
		)
	),
	'active_callback' => array(
		array(
			'setting' => 'boxed_mode',
			'operator' => '==',
			'value' => 1
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'show_back_to_top',
	'section' => 'core_general',
	'label' => esc_html__( 'Back to Top Button', 'vinero' ),
	'description' => esc_html__( 'Switch "Show" if you want to show "Back to Top" button.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Show', 'vinero' ),
		'off' => esc_html__( 'Hide', 'vinero' )
	),
	'default' => 1
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'grayscale_filter',
	'section' => 'core_general',
	'label' => esc_html__( 'Grayscale Images', 'vinero' ),
	'description' => esc_html__( 'Switch "Enable" if you want to activate grayscale filter.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Enable', 'vinero' ),
		'off' => esc_html__( 'Disable', 'vinero' )
	),
	'default' => 0
) );

# Selection

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
	'type' => 'color',
	'settings' => 'selection_color',
	'section' => 'core_selection',
	'label' => esc_html__( 'Selection Text Color', 'vinero' ),
	'description' => esc_html__( 'Select the text color, matches the portion of an element that is selected by a user.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'alpha' => false
	),
	'default' => '#ffffff',
	'output' => array(
		array(
			'element' => '::selection',
			'property' => 'color',
			'suffix' => '!important'
		),
		array(
			'element' => '::-moz-selection',
			'property' => 'color',
			'suffix' => '!important'
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'color',
	'settings' => 'selection_background_color',
	'section' => 'core_selection',
	'label' => esc_html__( 'Selection Background Color', 'vinero' ),
	'description' => esc_html__( 'Select the text background color, matches the portion of an element that is selected by a user.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'alpha' => true
	),
	'default' => '#333333',
	'output' => array(
		array(
			'element' => '::selection',
			'property' => 'background-color',
			'suffix' => '!important'
		),
		array(
			'element' => '::-moz-selection',
			'property' => 'background-color',
			'suffix' => '!important'
		)
	)
) );

# Scrollbar

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
	'type' => 'switch',
	'settings' => 'custom_scrollbar',
	'section' => 'core_scrollbar',
	'label' => esc_html__( 'Custom Scrollbar', 'vinero' ),
	'description' => esc_html__( 'Switch "YES" if you want to configure the scrollbar.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'on' => esc_html__( 'Yes', 'vinero' ),
		'off' => esc_html__( 'No', 'vinero' )
	),
	'default' => '0',
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'color',
	'settings' => 'custom_scrollbar_bg',
	'section' => 'core_scrollbar',
	'label' => esc_html__( 'Bar Background', 'vinero' ),
	'description' => esc_html__( 'Select background color for scrollbar.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'alpha' => false
	),
	'default' => '',
	'output' => array(
		array(
			'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar',
			'property' => 'background-color'
		)
	),
	'active_callback' => array(
		array(
			'setting' => 'custom_scrollbar',
			'operator' => '==',
			'value' => '1'
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'color',
	'settings' => 'custom_scrollbar_color',
	'section' => 'core_scrollbar',
	'label' => esc_html__( 'Bar Color', 'vinero' ),
	'description' => esc_html__( 'Select color for scrollbar.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'alpha' => false
	),
	'default' => '',
	'output' => array(
		array(
			'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar-thumb',
			'property' => 'background-color'
		)
	),
	'active_callback' => array(
		array(
			'setting' => 'custom_scrollbar',
			'operator' => '==',
			'value' => '1'
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'slider',
	'settings' => 'custom_scrollbar_width',
	'section' => 'core_scrollbar',
	'label' => esc_html__( 'Bar Width', 'vinero' ),
	'description' => esc_html__( 'Select the thickness of the scrollbar.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'choices' => array(
		'min' => '0',
		'max' => '16',
		'step' => '2'
	),
	'default' => '',
	'output' => array(
		array(
			'element' => '.vlt-custom-scrollbar ::-webkit-scrollbar',
			'property' => 'width',
			'units' => 'px'
		)
	),
	'active_callback' => array(
		array(
			'setting' => 'custom_scrollbar',
			'operator' => '==',
			'value' => '1'
		)
	)
) );

# Login Logo

$priority = 0;

Kirki::add_field( 'vinero_customize', array(
	'type' => 'image',
	'settings' => 'login_logo_image',
	'section' => 'core_login_logobar',
	'label' => esc_html__( 'Authorization Logo', 'vinero' ),
	'description' => esc_html__( 'If you want to change the logo of WordPress to your logo, you can use this options.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '',
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'dimension',
	'settings' => 'login_logo_image_height',
	'section' => 'core_login_logobar',
	'label' => esc_html__( 'Logo Height', 'vinero' ),
	'description' => esc_html__( 'Enter logo height.', 'vinero' ),
	'priority' => $priority++,
	'transport' => 'auto',
	'default' => '',
	'active_callback' => array(
		array(
			'setting' => 'login_logo_image',
			'operator' => '!=',
			'value' => ''
		)
	)
) );

Kirki::add_field( 'vinero_customize', array(
	'type' => 'dimension',
	'settings' => 'login_logo_image_width',
	'section' => 'core_login_logobar',
	'label' => esc_html__( 'Logo Width', 'vinero' ),
	'description' => esc_html__( 'Enter logo width.', 'vinero' ),
	'transport' => 'auto',
	'priority' => $priority++,
	'default' => '',
	'active_callback' => array(
		array(
			'setting' => 'login_logo_image',
			'operator' => '!=',
			'value' => ''
		)
	)
) );