<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

get_header(); the_post(); ?>

<?php

	$post_sidebar_position = 'right-sidebar';

	if ( class_exists( 'acf' ) ) {
		$post_sidebar_position = get_field( 'post_sidebar_position', get_queried_object_id() ) ? get_field( 'post_sidebar_position', get_queried_object_id() ) : $post_sidebar_position;
	}

	get_template_part( 'template-parts/page-title/page-title', 'blog' );

?>

<main class="vlt-main vlt-main--padding">

	<?php get_template_part( 'template-parts/single-post/post', $post_sidebar_position ); ?>

</main>
<!-- /.vlt-main -->

<?php get_footer(); ?>