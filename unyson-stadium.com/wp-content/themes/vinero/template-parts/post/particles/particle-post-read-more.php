<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

?>

<a class="vlt-btn vlt-btn--primary" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Read More', 'vinero' ); ?></a>