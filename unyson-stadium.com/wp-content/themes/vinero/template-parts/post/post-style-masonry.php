<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

$size = 'vinero-masonry';

?>

<article <?php post_class( 'vlt-post vlt-post--style-masonry' ); ?> id="post-<?php the_ID(); ?>">


	<?php if ( has_post_thumbnail() ) : ?>

		<div class="vlt-post-thumbnail clearfix">
			<?php echo vinero_get_post_thumbnail( $size ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'thumbnail-link' ); ?>
		</div>
		<!-- /.vlt-post-thumbnail -->

	<?php endif; ?>

	<div class="vlt-post-content">

		<header class="vlt-post-header">

			<?php get_template_part( 'template-parts/post/particles/particle', 'post-meta-small' ); ?>
			<?php get_template_part( 'template-parts/post/particles/particle', 'post-title' ); ?>

		</header>
		<!-- /.vlt-post-header -->

		<div class="vlt-post-excerpt">
			<?php echo vinero_limit_text( get_the_content(), 23 ); ?>
		</div>
		<!-- /.vlt-post-excerpt -->

	</div>
	<!-- /.vlt-post-content -->

</article>
<!-- /.vlt-post -->