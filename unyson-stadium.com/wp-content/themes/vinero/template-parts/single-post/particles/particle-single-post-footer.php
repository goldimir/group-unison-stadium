<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

?>

<div class="d-flex align-items-center justify-content-md-between flex-column flex-md-row">

	<?php if ( function_exists( 'vlthemes_get_post_share_buttons' ) ) : ?>
		<div class="vlt-post-share">
			<?php echo vlthemes_get_post_share_buttons( get_the_ID() ); ?>
		</div>
		<!-- /.vlt-post-share -->
	<?php endif; ?>

	<?php if ( get_the_tags() ) : ?>
		<div class="vlt-post-tags">
			<?php the_tags( '', '', '' ); ?>
		</div>
		<!-- /.vlt-post-tags -->
	<?php endif; ?>

</div>