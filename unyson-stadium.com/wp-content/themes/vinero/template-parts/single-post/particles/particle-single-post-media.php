<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

$postID = get_the_ID();
$size = 'vinero-standard';

?>

<?php if ( has_post_thumbnail() ) : ?>

<div class="vlt-post-media">

	<div class="vlt-single-image">

		<?php if ( get_the_title( get_post_thumbnail_id( $postID ) ) ) : ?>
			<span><?php echo get_the_title( get_post_thumbnail_id( $postID ) ); ?></span>
		<?php endif; ?>

		<?php echo wp_get_attachment_image( get_post_thumbnail_id( $postID ), $size ); ?>

	</div>
	<!-- /.vlt-single-image -->

</div>
<!-- /.vlt-post-media -->

<?php endif; ?>