<?php

/**
 * @author: VLThemes
 * @version: 1.0
 */

$page_title = get_theme_mod( 'shop_title', esc_html__( 'Online Store', 'vinero' ) );
$page_subtitle = get_theme_mod( 'shop_subtitle', esc_html__( 'There are some things money can\'t buy.', 'vinero' ) );

$page_bg_img = get_theme_mod( 'shop_bg' );

$class = $page_bg_img ? 'vlt-hero-title jarallax' : 'vlt-hero-title';

?>

<div class="<?php echo vinero_sanitize_class( $class ); ?>">

	<?php if ( $page_bg_img ) : ?>
		<img src="<?php echo esc_url( $page_bg_img ); ?>" alt="" class="jarallax-img">
	<?php endif; ?>

	<div class="vlt-hero-title__content">
		<h1><?php echo esc_html( $page_title ); ?></h1>
		<p><?php echo esc_html( $page_subtitle ); ?></p>
	</div>

</div>
<!--/.vlt-hero-title-->